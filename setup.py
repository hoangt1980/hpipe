from setuptools import setup, find_packages

setup(
    name="HPIPE",
    url="https://gitlab.com/mathewkhall/hpipe",
    author="Mathew Hall",
    author_email="mathew.k.hall@gmail.com",
    packages=find_packages(),
    install_requires=[
        "tensorflow",
        "numpy",
        "matplotlib",
        "pillow",
        "bashplotlib",
        "sphinx",
        "seaborn",
        "imageio"
    ],
    version="0.1"
)