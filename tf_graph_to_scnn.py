import tensorflow as tf
from tensorflow.python.framework import tensor_util
from tensorflow.core.framework import graph_pb2
from tensorflow.python.platform import gfile

import numpy as np
import imageio
from matplotlib.image import imread
import matplotlib.pyplot as plt
import imagenet_preprocessing

import sys
import getopt
import os
import re
import math
import copy

import seaborn as sns

INTEGER_BITS = 4
FRACTIONAL_BITS = 11


def get_quant_op(tensor, integer_bits=INTEGER_BITS, fractional_bits=FRACTIONAL_BITS):
    max_value = 2**integer_bits
    smallest_fraction = 2**-fractional_bits
    min_value = -max_value - smallest_fraction
    return tf.quantize(tensor, min_value, max_value, tf.qint16)

# 164 Const, 53 Conv2D, 49 Relu, 33 BiasAdd, 17 FusedBatchNorm, 16 Add, 7
# Pad, 1 Identity, 1 MatMul, 1 MaxPool, 1 Mean, 1 Placeholder, 1 Reshape


def get_legal_combination_of_subgraphs(subgraph_match_indices, level=0, current_matches=None):
    if current_matches is None:
        current_matches = []
    if len(subgraph_match_indices) == level:
        return (True, [])
    current_matches.append(-1)
    for match in subgraph_match_indices[level]:
        if match in current_matches:
            continue
        current_matches[-1] = match
        found_match, matches = get_legal_combination_of_subgraphs(
            subgraph_match_indices, level + 1, current_matches)
        if found_match:
            total_matches = [match] + matches
            return (True, total_matches)

    return (False, [])


def remove_items_from_list_if_they_exist(items, list):
    for item in items:
        try:
            del list[list.index(item)]
        except ValueError:
            pass
    return list


def try_match_subgraph(tf_graph_db, head, subgraph_spec):
    '''
    Attempts to match the type of 'head' to the subgraph spec
    If it matches, the fuction iterates over all of the nodes in the
    graph that cosume the output of 'head' and recursively calls itself
    to try to match the remainder of the subgraph.

    After the recursive calls it knows which consumer trees match the
    subgraph and it searches to find a legal combination of matches for
    all subtrees from the input spec.  It will find the first legal match,
    but it is possible that there are other matches.

    Returns: a tuple of whether or not the subgraph matches a tree from the
     head node, followed by the matching nodes in the matching subgraph, and
     finally any outputs that the subgraph exports.

     For example the following subgraph_spec:

            [
                    "Conv2D", 
                            [
                                    ["Add*",
                                            [
                                                    ["BiasAdd",
                                                            [
                                                                    ["Relu"]
                                                            ]
                                                    ]
                                            ]
                                    ]
                            ],
                    "Conv2D"
            ] 

    Would match a Conv2D layer that outputs to an Add layer that also accepts
     another input from another Conv2D layer and outputs to a BiasAdd and a Relu

     Note that the * after Add in the spec indicates that the output of Add should
     be exported in the output_nodes list in the return tuple
    '''

    # At each level of recursion we look first to see if the input 'head' node
    # type matches the first element of the subgraph spec
    target = subgraph_spec[0]

    # In the input spec a * indicates that the output of a node
    # should be preserved.  By default we only export an output
    # if it is not consumed by another node in the subgraph, but
    # if the node spec contains a * then we will export it.
    add_node_as_output = False

    # Get a variable referencing a negative match return value since it is
    # used for early exits in a couple of locations
    non_match_return = (False, None, None)

    # if there is only one element in the input list we have reached a leaf node,
    # so we initialize just_find_head to True
    just_find_head = True
    # If there is more than one element, we disable that and get a reference to the
    # remaining subtree
    if len(subgraph_spec) > 1:
        remaining_outputs = subgraph_spec[1]
        just_find_head = False

    # check to see if this node should be exporting its output
    if target[-1] == "*":
        # remove the * from the end of the string for our comparisions
        target = target[:-1]
        # note that we should export this output
        add_node_as_output = True
        # we need to make sure that we don't match ops that export an output to
        # subgraphs that don't. Basically we don't allow the spec to export outputs
        # unless the output is actually used somewhere else in the full graph.
        # Obviously, if it's a leaf (just_find_head is true), then its output is assumed
        # to be used elsewhere in the full graph
        if not just_find_head:
            if len(tf_graph_db.get_consumers(head)) < 2:
                return non_match_return

    # If the type of the head matches the target, continue the search
    #
    if head.type == target:

        # If we were looking for a leaf node, we found a match, so just
        # return it.
        #
        if just_find_head:
            return (True, [head], [head])

        # Now we need to iterate over all of the remaining outputs
        # in the subgraph spec and try to match them to all of the
        # consumers of the head node
        #
        consumers = tf_graph_db.get_consumers(head)

        # We store the results of our match in a matrix of subgraph_match_results
        # (the first dimension is the nodes in the spec, the second is nodes in the
        # graph).  We store the indices of all of the matches in a list of lists.
        # (just makes things a little nicer to iterate over)
        #
        subgraph_match_results = []
        subgraph_match_indices = []
        for i, subgraph in enumerate(remaining_outputs):
            subgraph_match_results.append([])
            subgraph_match_indices.append([])
            for j, consumer in enumerate(consumers):
                result = try_match_subgraph(tf_graph_db, consumer, subgraph)
                subgraph_match_results[i].append(result)
                if result[0]:
                    subgraph_match_indices[i].append(j)

        # Now we've found all possible matches, but we might, for example,
        # only have one conv layer that matched two conv layers in the spec,
        # so we need to try assigning the nodes to the nodes in the spec
        # and find a legal combination of them.
        #
        # For example, if we had an Add node that was fed by an Add and a Conv
        # in the actual graph, but the spec called for an Add fed by two Convs,
        # we would have matched the one Conv to both Conv inputs, but that is
        # illegal, so this function will see if there is a legal combination
        # and return the first one it finds if there is one
        #
        found_match, matches = get_legal_combination_of_subgraphs(
            subgraph_match_indices)
        if not found_match:
            return non_match_return

        # Now that we have a legal combination of subgraphs we need to combine
        # the head node with the matched subtree results from the earlier recursive
        # call
        #
        output_nodes = []
        matched_nodes = [head]
        for i, j in enumerate(matches):
            matched_nodes += subgraph_match_results[i][j][1]
            output_nodes += subgraph_match_results[i][j][2]

        # If we are supposed to export this output we add it to the output list
        #
        if add_node_as_output:
            output_nodes += [head]

        # If the consumer of this head has 2 inputs we try to match its producers with
        # the subgraph_spec
        #
        if len(subgraph_spec) == 3:
            producers = tf_graph_db.get_producers(
                subgraph_match_results[i][j][1][0])
            del producers[producers.index(head)]
            try:
                index = [o.type for o in producers].index(subgraph_spec[2])
                matched_nodes = [producers[index]] + matched_nodes
            except ValueError:
                return non_match_return
        return (True, matched_nodes, output_nodes)

    # If we got here, the head didn't match
    return non_match_return


def flatten_list(_list):
    flat_list = []
    remaining_items = _list
    while len(remaining_items) > 0:
        item = remaining_items.pop()
        if isinstance(item, list):
            remaining_items.extend(item)
        else:
            flat_list.append(item)
    return flat_list


def get_dimension_values_from_list(dimension_list):
    return [dimension_list[i].value for i in range(len(dimension_list))]


def get_conv_padding_from_parts(stride, input_size, output_size, kernel_size):
    padded_input_size = (output_size - 1) * stride + kernel_size
    padding = padded_input_size - input_size
    padding_1 = padding // 2
    padding_0 = padding - padding_1
    return [padding_0, padding_1]


def get_conv_padding(conv_op):
    strides = conv_op.get_attr("strides")
    if strides[0] != 1 or strides[3] != 1:
        print("Haven't implemented non-1 strides for IC or OC conv")
        sys.exit(0)
    weights = conv_op.inputs[1]
    input_tensor = conv_op.inputs[0]
    weight_shape = get_dimension_values_from_list(weights.shape)
    input_shape = get_dimension_values_from_list(input_tensor.shape)
    output_shape = get_dimension_values_from_list(conv_op.outputs[0].shape)
    paddings = []
    paddings.append([0, 0])
    paddings.append(get_conv_padding_from_parts(
        strides[1], input_shape[1], output_shape[1], weight_shape[0]))
    paddings.append(get_conv_padding_from_parts(
        strides[2], input_shape[2], output_shape[2], weight_shape[1]))
    paddings.append([0, 0])
    return paddings


class SCNNOp():

    @staticmethod
    def op_pattern():
        return []

    def __init__(self, input_names_for_head_nodes, nodes, output_nodes, db, accelerator):
        self.nodes = nodes
        self.output_nodes = output_nodes
        self.input_names_for_head_nodes = input_names_for_head_nodes
        self.db = db
        self.accelerator = accelerator

    def print_node_attributes(self):
        for node in self.nodes:
            print("  " + node.type)
            for attr in node.node_def.attr:
                print("    " + attr + ": " + str(node.get_attr(attr)))

    def get_inputs(self):
        return self.input_names_for_head_nodes

    def add_fake_output(self, node):
        # Note that this is "fake" by virtue of it not being
        # added to the self.nodes list
        # If it was added to that list then it would get re-
        # computed
        #
        self.output_nodes.append(node)

    def get_outputs(self):
        return self.output_nodes

    def _get_ops_of_type(self, _type):
        op_list = []
        for node in self.nodes:
            if node.type == _type:
                op_list.append(node)
        return op_list

    def _op_dict(self):
        flat_list = flatten_list(self.op_pattern())
        def remove_decorators(l):
            new_l = []
            for i in l:
                new_l.append(i.translate(dict.fromkeys(map(ord, "*"))))
            return new_l
        flat_list = remove_decorators(flat_list)
        unique_op_names = {name: 1 for name in flat_list}
        return {name: self._get_ops_of_type(name) for name in unique_op_names.keys()}

    def consumer_ops_for_tensor(self, tensor):
        op_list = []
        tensor_list = [tensor]
        while len(tensor_list) > 0:
            tensor = tensor_list.pop()
            for node in self.nodes:
                if tensor in node.inputs:
                    tensor_list.extend(node.outputs)
                    op_list.append(node)
        return op_list

    def get_consumer_strides(self, tensor):
        consumers = self.db.get_consumers(tensor)
        scnn_ops = [self.db.get_scnn_op_for_tf_op(op) for op in consumers]
        strides = []
        for scnn_op, consumer in zip(scnn_ops, consumers):
            next_consumer = consumer
            found_conv = False
            while self.db.get_scnn_op_for_tf_op(next_consumer) == scnn_op:
                if next_consumer.type == "Conv2D":
                    found_conv = True
                    stride_to_add = next_consumer.get_attr("strides")
                    stride_matches_existing = False
                    for stride in strides:
                        all_components_match = True
                        for v1, v2 in zip(stride, stride_to_add):
                            if v1 != v2:
                                all_components_match = False
                        if all_components_match:
                            stride_matches_existing = True
                    if not stride_matches_existing:
                        strides.append(stride_to_add)
                    break
                next_consumer = self.db.get_consumers(next_consumer)[0]
            assert(found_conv)
        return strides

    def get_padding_information_for_conv_op(self, sess, conv_op):
        #conv_op = self._get_ops_of_type("Conv2D")[0]
        conv_paddings = get_conv_padding(conv_op)
        producers = self.db.get_producers(conv_op)
        if producers[0].type == "Pad":
            pad_op = producers[0]
            paddings_tensor = pad_op.inputs[1]
            paddings = sess.run(paddings_tensor)
            #paddings = [paddings[i] for i in [1,2]]
        else:
            paddings = [[0, 0] for p in conv_paddings]

        total_paddings = np.array(
            [[p1[0] + p2[0], p1[1] + p2[1]] for p1, p2 in zip(paddings, conv_paddings)])
        return total_paddings

    def write_layer_name(self, directory):
        filename = directory + "layer_name.txt"
        f = open(filename, "w")
        f.write(type(self).__name__)
        f.write("\n")
        f.close()

    def _get_producer_convs_in_layer(self, op):
        producers = self.db.get_producers(op)
        matching_producers = []
        for producer in producers:
            if producer not in self.nodes:
                matching_producers.append(0)
                continue
            if producer.type in ["Conv2D", "Identity"]:
                matching_producers.append(producer)
                continue
            matching_producers.append(
                self._get_producer_convs_in_layer(producer)[0])
        return matching_producers

    def _get_add_input_index(self, add_op):
        input_nodes = self.input_names_for_head_nodes.keys()
        current_op = add_op
        if current_op.name not in input_nodes:
            current_op = self.db.get_producers(current_op)[1]
            return self._get_conv_input_index(current_op)
        # find inputs that aren't in subgraph
        node_names = [n.name for n in self.nodes]
        input_names = [name for name in 
                        self.input_names_for_head_nodes[current_op.name]
                            if name not in node_names]
        return self.accelerator.get_activation_index_for_name(input_names[0])

    def _get_conv_input_index(self, conv_op):
        input_nodes = self.input_names_for_head_nodes.keys()
        current_op = conv_op
        while current_op.name not in input_nodes:
            current_op = self.db.get_producers(current_op)[0]
        return self.accelerator.get_activation_index_for_name(self.input_names_for_head_nodes[current_op.name][0])

    def _write_parameter_tensors_to_directory(self, parameter_tensors, directory):
        for name, op_param in parameter_tensors.items():
            filename = directory + name + ".npy"
            #plt.hist(np.array(op_param).flatten(), bins='auto')
            # plt.show()
            np.save(filename, op_param, False)

    def _write_list_to_file(self, parameter_names, filename):
        f = open(filename, "w")
        for parameter_name in parameter_names:
            f.write(parameter_name)
            f.write("\n")
        f.close()

    def get_output_properties(self, node):
        properties = {}

        def merge_output_properties(op):
            for key, value in op.items():
                properties[key] = value
        if node.type in ["MaxPool", "Relu", "BiasAdd"]:
            properties[node.type] = True
        inputs = self.db.get_producers(node)
        for _input in inputs:
            if _input not in self.nodes:
                continue
            prior_properties = self.get_output_properties(_input)
            merge_output_properties(prior_properties)
        return properties

    def write_op_description_to_directory(self, directory, sess, layer_num):
        mkdir_if_it_doesnt_exist(directory)
        self.write_layer_name(directory)
        op_dict = self._op_dict()
        op_dir_list = []

        output_indices_f = open(directory + "output_indices.txt", "w")
        output_relus_f = open(directory + "output_relus.txt", "w")
        output_biases_f = open(directory + "output_biases.txt", "w")
        output_pools_f = open(directory + "output_pools.txt", "w")
        for output in self.output_nodes:
            index = self.accelerator.get_activation_index_for_name(output.name)
            output_indices_f.write(str(index) + "\n")
            output_properties = self.get_output_properties(output)
            for _property, f in zip(["MaxPool", "Relu", "BiasAdd"], [output_pools_f, output_relus_f, output_biases_f]):
                if _property in output_properties.keys():
                    f.write("1\n")
                else:
                    f.write("0\n")

        output_indices_f.close()
        output_relus_f.close()
        output_biases_f.close()
        output_pools_f.close()

        def create_op_dir(op_name, suffix, op):
            nonlocal op_dir_list
            op_dir_name = op_name + "_" + str(suffix)
            op_dir_list.append(op_dir_name)
            op_dir = directory + op_dir_name + "/"
            mkdir_if_it_doesnt_exist(op_dir)
            f = open(op_dir + "type.txt", "w")
            f.write(op.type + "\n")
            f.close()
            return op_dir

        for op_name, op_list in op_dict.items():
            if op_name in ["Mean", "MatMul"]:
                print("I haven't implemented this yet")
                return -1
                self._write_list_to_file(
                    op_dir_list, directory + "layer_names.txt")

            if op_name not in ["Conv2D", "BiasAdd", "MaxPool", "Add"]:
                continue
            if op_name == "Add":
                # Only supports Adds with at most one non-conv input
                op = op_list[0]
                conv_producers = self._get_producer_convs_in_layer(op)
                num_zeros_in_conv_producers = sum([1 for el in conv_producers if el == 0])
                if num_zeros_in_conv_producers == 0:
                    continue
                if num_zeros_in_conv_producers == 1:
                    op_dir = create_op_dir(op_name, 0, op)
                    input_index = self._get_add_input_index(op)
                    f = open(op_dir + "input_index.txt", "w")
                    f.write(str(input_index) + "\n")
                    f.close()
                    # Add has only input_index which is implied
                    self._write_list_to_file(
                        [], op_dir + "layer_components.txt")
                    continue

                print("I haven't implemented this yet")
                return -1
                self._write_list_to_file(
                    op_dir_list, directory + "layer_names.txt")
                return

            for i, op in enumerate(op_list):
                parameter_tensors = {}
                layer_component_names = []
                op_dir = create_op_dir(op_name, i, op)

                if op.type == "Conv2D":
                    input_index = self._get_conv_input_index(op)
                    f = open(op_dir + "input_index.txt", "w")
                    f.write(str(input_index) + "\n")
                    f.close()
                    parameter_tensors["Strides"] = op.get_attr("strides")
                    parameter_tensors[
                        "Pad"] = self.get_padding_information_for_conv_op(sess, op)

                if op.type == "MaxPool":
                    parameter_tensors["KernelSize"] = op.get_attr("ksize")
                    parameter_tensors["Strides"] = op.get_attr("strides")

                if op.type in ["Conv2D", "BiasAdd"]:
                    param_tensor_op = self.db.get_second_input_tensor(op)
                    if op.type == "Conv2D" and layer_num == 0:
                        param_tensor_op = param_tensor_op * 16
                    quant_param_tensor_op = get_quant_op(param_tensor_op)
                    param_tensor = sess.run(quant_param_tensor_op).output
                    parameter_tensors["ParameterTensor"] = param_tensor

                layer_component_names.extend(parameter_tensors.keys())
                self._write_list_to_file(
                    layer_component_names, op_dir + "layer_components.txt")
                self._write_parameter_tensors_to_directory(
                    parameter_tensors, op_dir)
        self._write_list_to_file(op_dir_list, directory + "layer_names.txt")
        return 0


class PadConvBiasRelu(SCNNOp):

    @staticmethod
    def op_pattern():
        return [
            "Pad",
            [
                ["Conv2D",
                 [
                     ["BiasAdd",
                      [
                          ["Relu"]
                      ]
                      ]
                 ]
                 ]
            ]
        ]


class PadConvBiasMaxPoolRelu(SCNNOp):

    @staticmethod
    def op_pattern():
        return [
            "Pad",
            [
                ["Conv2D",
                 [
                     ["BiasAdd",
                      [
                          ["MaxPool",
                           [
                               ["Relu"]
                           ]
                           ]
                      ]
                      ]
                 ]
                 ]
            ]
        ]


class DoubleConvAddNoReluBiasAddRelu(SCNNOp):

    @staticmethod
    def op_pattern():
        return [
            "Conv2D",
            [
                ["Add*",
                 [
                     ["BiasAdd",
                      [
                          ["Relu"]
                      ]
                      ]
                 ]
                 ]
            ],
            "Conv2D"
        ]


class ConvIdentityAddNoReluBiasAddRelu(SCNNOp):

    @staticmethod
    def op_pattern():
        return [
            "Conv2D",
            [
                ["Add*",
                 [
                     ["BiasAdd",
                      [
                          ["Relu"]
                      ]
                      ]
                 ]
                 ]
            ],
        ]


class ConvIdentityAddBiasAddRelu(SCNNOp):

    @staticmethod
    def op_pattern():
        return [
            "Conv2D",
            [
                ["Add",
                 [
                     ["BiasAdd",
                      [
                          ["Relu"]
                      ]
                      ]
                 ]
                 ]
            ]
        ]


class ConvIdentityAddBiasAddReluMean(SCNNOp):

    @staticmethod
    def op_pattern():
        return [
            "Conv2D",
            [
                ["Add",
                 [
                     ["BiasAdd",
                      [
                          ["Relu",
                           [
                               ["Mean"]
                           ]
                           ]
                      ]
                      ]
                 ]
                 ]
            ]
        ]


class PadDoubleConvAddNoReluBiasAddRelu(SCNNOp):

    @staticmethod
    def op_pattern():
        return [
            "Pad",
            [
                ["Conv2D",
                 [
                     ["Add*",
                      [
                          ["BiasAdd",
                           [
                               ["Relu"]
                           ]
                           ]
                      ]
                      ]
                 ],
                 "Conv2D"
                 ]
            ]
        ]


class ConvBiasRelu(SCNNOp):

    @staticmethod
    def op_pattern():
        return [
            "Conv2D",
            [
                ["BiasAdd",
                 [
                     ["Relu"]
                 ]
                 ]
            ]
        ]


class ReshapeMatMulBiasAdd(SCNNOp):

    @staticmethod
    def op_pattern():
        return [
            "Reshape",
            [
                ["MatMul",
                 [
                     ["BiasAdd"]
                 ]
                 ]
            ]
        ]

class FakeOp():
    def __init__(self, node):
        self.name = node.name
        self.type = node.type
        self.inputs = node.inputs
        self.outputs = node.outputs

class SCNN():

    def __init__(self, graphdb, input_node_name, output_names):
        self.ops = [
            ConvIdentityAddBiasAddReluMean,
            PadConvBiasMaxPoolRelu,
            PadDoubleConvAddNoReluBiasAddRelu,
            DoubleConvAddNoReluBiasAddRelu,
            ConvBiasRelu,
            ConvIdentityAddNoReluBiasAddRelu,
            ConvIdentityAddBiasAddRelu,
            PadConvBiasRelu,
            ReshapeMatMulBiasAdd
        ]
        self.op_list = []
        self.graphdb = graphdb
        self.computed_ops = {}
        self.output_indices = {"import/Placeholder": 0}
        self.output_index = 0
        self._map_graph(graphdb, input_node_name, output_names)
        self._dump_scnn_op_list_summary()
        self._add_graph_nodes_for_output_duplication()
        self._compute_infinite_output_indices()
        self.max_output_buffers = 0
        self._compute_correct_output_indices()

    def _add_graph_nodes_for_output_duplication(self):
        def get_first_add_or_conv_or_matmul_op(node):
            if node.type in ["Conv2D", "Add", "MatMul"]:
                return node
            consumers = self.graphdb.get_consumers(node)
            if len(consumers) == 0:
                return None
            return get_first_add_or_conv_or_matmul_op(consumers[0])
        def get_properties_of_interest(node):
            node = get_first_add_or_conv_or_matmul_op(node)
            if type(node) is bool:
                return True
            if node.type in ["Add", "MatMul"]:
                return {
                    "stride" : [1,1,1,1]
                }
            return {
                "stride" : node.get_attr("strides")
            }
        def properties_are_same(d1, d2):
            if d1 == True or d2 == True:
                return True
            if bool in [type(d1), type(d2)]:
                return True
            for k,v in d1.items():
                if k not in d2:
                    return False
                v2 = d2[k]
                if type(v) != type(v2): 
                    return False
                if type(v) is tuple:
                    v = list(v)
                    v2 = list(v2)
                if type(v) is list:
                    if len(v) != len(v2):
                        return False
                    for (i,i2) in zip(v, v2):
                        if i != i2:
                            return False
                    continue
                if type(v) is dict:
                    if properties_are_same(v, v2):
                        continue
                    return False
                if v != v2:
                    return False
                continue
            return True

        for op in self.op_list:
            # list(...) forces a copy that prevents
            # for node in nodes from iterating over added outputs
            nodes = list(op.get_outputs())
            for node in nodes:
                consumers = self.graphdb.get_consumers(node)
                if len(consumers) <= 1:
                    continue
                properties_to_match = [get_properties_of_interest(consumers[0])]
                nodes_for_properties_to_match = [node]
                copy_number = 0
                for c in consumers[1:]:
                    matched_index = -1
                    for i,p in enumerate(properties_to_match):
                        cp = get_properties_of_interest(c)
                        if properties_are_same(p, cp):
                            matched_index = i
                            break
                    if matched_index == -1:
                        node_copy = FakeOp(node)
                        node_copy.name = node_copy.name + "_copy_" + str(copy_number)
                        copy_number += 1
                        properties_to_match.append(get_properties_of_interest(c))
                        nodes_for_properties_to_match.append(node_copy)
                        op.add_fake_output(node_copy)
                        self.graphdb.add_fake_op(node_copy)
                    node_copy = nodes_for_properties_to_match[matched_index]
                    self.graphdb.replace_input(c, node, node_copy)
                    for op2 in self.op_list:
                        if c in op2.nodes:
                            op2_inputs = op2.get_inputs()
                            for op2_k, op2_is in op2_inputs.items():
                                for index, op2_i in enumerate(op2_is):
                                    if op2_i == node.name:
                                        op2_inputs[op2_k][index] = node_copy.name


    def _compute_correct_output_indices(self):
        last_usage_indices = {}
        for k, v in self.output_indices.items():
            for i, op in enumerate(reversed(self.op_list)):
                op_input_name_list = op.get_inputs().values()
                op_input_name_list = [v for l in op_input_name_list for v in l]
                if k in op_input_name_list:
                    index = len(self.op_list) - i - 1
                    last_usage_indices[k] = index
                    break
        free_list = [False]

        def allocate_first_free_index():
            for i, v in enumerate(free_list):
                if v:
                    free_list[i] = False
                    return i
            free_list.append(False)
            return len(free_list) - 1
        for i, op in enumerate(self.op_list):
            outputs = op.get_outputs()
            for output in outputs:
                index = allocate_first_free_index()
                self.output_indices[output.name] = index
            input_names = op.get_inputs().values()
            input_names = [v for l in input_names for v in l]
            for input_name in input_names:
                if input_name not in last_usage_indices.keys():
                    continue
                if last_usage_indices[input_name] == i:
                    free_list[self.output_indices[input_name]] = True
        self.max_output_buffers = len(free_list)

    def get_activation_buffer_count(self):
        return self.max_output_buffers

    def get_activation_index_for_name(self, name):
        if name not in self.output_indices.keys():
            return 0
        return self.output_indices[name]

    def get_op_list(self):
        return self.op_list

    def get_exported_outputs_and_names(self):
        # we want to give the outputs names that are relevant to the accelerator
        # so we will use a list to keep the op order, and we will use a hash
        # so that the actual ops can be looked up
        exported_outputs = {}
        name_list = []
        for i, op in enumerate(self.op_list):
            outputs = op.get_outputs()
            for output in outputs:
                output_name = "Operation_" + str(i) + "_" + output.type
                name_list.append(output_name)
                exported_outputs[output_name] = output
        return (name_list, exported_outputs)

    def get_accum_buffer_nodes_and_names(self):
        # we want to give the outputs names that are relevant to the accelerator
        # so we will use a list to keep the op order, and we will use a hash
        # so that the actual ops can be looked up
        accum_nodes = {}
        name_list = []
        for i, op in enumerate(self.op_list):
            conv2d_ops = op._get_ops_of_type("Conv2D")
            add_ops = op._get_ops_of_type("Add")
            if len(add_ops) > 0:
                op_to_dump = add_ops[0]
            elif len(conv2d_ops) > 0:
                op_to_dump = conv2d_ops[0]
            else:
            	continue
            output_name = "l" + str(i) + "_accum_out"
            name_list.append(output_name)
            accum_nodes[output_name] = op_to_dump
        return (name_list, accum_nodes)

    def get_fully_connected_nodes_and_names(self):
        fully_connected_nodes = {}
        name_list = []
        for i, op in enumerate(self.op_list):
            matmul_ops = op._get_ops_of_type("MatMul")
            for j, conv2d_op in enumerate(matmul_ops):
                output_name = "MatMul_" + str(i) + "_" + str(j)
                name_list.append(output_name)
                fully_connected_nodes[output_name] = conv2d_op
        return (name_list, fully_connected_nodes)

    def print_op_attributes(self):
        for op in self.op_list:
            print(op.__class__.__name__)
            op.print_node_attributes()

    def _dump_scnn_op_list_summary(self):
        op_summary_hash = {}
        for op in self.ops:
            op_summary_hash[op.__name__] = 0
        print("Ops execution order: ")
        for scnn_op in self.op_list:
            op_summary_hash[scnn_op.__class__.__name__] += 1
            print("  " + scnn_op.__class__.__name__)
        print("Op execution counts: ")
        for op in self.ops:
            print("  " + op.__name__ + ": " +
                  str(op_summary_hash[op.__name__]))

    def _op_list_matches_output_list(self, op_list, output_names):
        '''
        Iterates over ops in op list and marks any names that match
         an item in output_names as found, returns false if an op isn't
         in output_names, then checks to see if all output_names have 
         been marked as found

        Returns: True if all output_names exist in op_list and op_list
                 contains no ops that aren't in output_names
                 False otherwise
        '''
        found_outputs = []
        for i in range(len(output_names)):
            found_outputs.append(False)
        for op in op_list:
            try:
                index = output_names.index(op.name)
                found_outputs[index] = True
            except ValueError:
                return False
        for found in found_outputs:
            if not found:
                return False
        return True

    def _ops_all_computed(self, op_list):
        '''
        Iterates over all of the ops in op list and checks to make
         sure all the ops have been computed

        It ignores constants and placeholders since they don't need
         to be computed

        Returns: True if all of the ops have been computed, 
                         False if they haven't
        '''
        for op in op_list:
            if op.type not in {"Placeholder": 1, "Const": 1} and \
                    op.name not in self.computed_ops:
                return False
        return True

    def _compute_infinite_output_indices(self):
        for op in self.op_list:
            outputs = op.get_outputs()
            for output in outputs:
                self.output_indices[output.name] = self.output_index
                self.output_index += 1


    def _add_scnn_op(self, scnn_op):
        self.op_list.append(scnn_op)

    def _get_inputs_to_sub_graph(self, subgraph):
        '''
        Iterates over all of the nodes in the subgraph,
         gets all of their producer ops, then removes
         any ops that are in the subgraph

        Returns: A list of all of the producers for all of the nodes
                 in subgraph that aren't computed within the subgraph
        '''
        total_input_list = []
        for node in subgraph:
            producers = self.graphdb.get_producers(node)
            producers = [producer for producer in producers if producer.type not in [
                "Const", "Placeholder"]]
            total_input_list.extend(producers)

        # Now remove any inputs produced within the subgraph
        #
        inputs_to_subgraph = remove_items_from_list_if_they_exist(
            subgraph, total_input_list)
        return inputs_to_subgraph

    def _subgraph_producers_all_computed(self, subgraph):
        '''
        Returns: True if all of the inputs to subgraph have been
                 computed
                 False if they haven't
        '''
        input_nodes = self._get_inputs_to_sub_graph(subgraph)
        return self._ops_all_computed(input_nodes)

    def _get_subgraph_head_nodes(self, subgraph):
        '''
        Gets all of the inputs to the subgraph, then requests their
         consumers.  Returns the intersection of the subgraph and 
         the consumer list

        Returns: All nodes in the graph that have a non-Const or Placeholder
                 input that is not computed within the subgraph.
        '''
        subgraph_input_nodes = self._get_inputs_to_sub_graph(subgraph)
        consumer_list = {}
        for input_node in subgraph_input_nodes:
            consumers = self.graphdb.get_consumers(input_node)
            for consumer in consumers:
                consumer_list[consumer.name] = 1

        return [node for node in subgraph if node.name in consumer_list]

    def _get_input_names_for_head_nodes(self, head_nodes):
        input_names_for_head_nodes = {}
        for head_node in head_nodes:
            producers = self.graphdb.get_producers(head_node)
            input_names_for_head_nodes[head_node.name] = [
                producer.name for producer in producers]
        return input_names_for_head_nodes

    def _add_outputs_to_computed_ops(self, outputs):
        for output in outputs:
            self.computed_ops[output.name] = 1

    def _get_subgraph_consumers(self, subgraph):
        consumers = {}
        for node in subgraph:
            node_consumers = self.graphdb.get_consumers(node)
            for consumer in node_consumers:
                consumers[consumer.name] = consumer
        return list(consumers.values())

    def _print_head_match_status(self, matched_op, head, matched_nodes, matched_head_nodes, output_nodes):
        print("Found subgraph with head " + str(head.name) +
              " matching " + matched_op.__name__)

        print("  Inputs:")
        for match in matched_nodes:
            for producer_op in self.graphdb.get_producers(match):
                if producer_op in matched_nodes:
                    continue
                print("    " + match.name + ": " + producer_op.name)

        print("  Matched Heads:")
        for matched_head in matched_head_nodes:
            print("    " + matched_head.name)

        print("  Outputs:")
        for out in output_nodes:
            print("    " + out.name)

    def _subgraphs_are_disjoint(self, subgraph1, subgraph2):
        for node in subgraph1:
            if node in subgraph2:
                return False
        return True

    def _map_graph(self, graphdb, input_node_name, output_names):
        heads = [graphdb.get_node_by_name(input_node_name)]
        assert heads[0] is not None, "Node named " + \
            input_node_name + " not found in graph"
        found_at_least_one_subgraph = True
        full_graph_outputs = []
        all_matched_nodes = []
        while len(heads) > 0:

            # We need to insure that we are making progress.  If we didn't find any
            #  matches in the last iteration, we won't find any this iteration
            #
            assert found_at_least_one_subgraph, "Didn't find subgraphs matching head nodes: " + \
                str(heads)
            found_at_least_one_subgraph = False
            next_head_list = []
            iteration_matched_heads = []
            iteration_outputs = []

            # Iterate over all of the remaining outputs of the explored graph
            #  and attempt to match each one to a subgraph pattern
            #
            for head in heads:

                # skip any full graph outputs
                #
                if head.name in output_names:
                    full_graph_outputs.append(head)
                    continue

                # Iterate over all of the registered ops and attempt to match their
                #  subgraph patterns
                #
                for op in self.ops:

                    subgraph_matches, matched_nodes, output_nodes = try_match_subgraph(
                        graphdb, head, op.op_pattern())
                    if subgraph_matches:

                        # Only match subgraphs if all of their inputs have already been
                        # matched and their nodes have not already been matched
                        #
                        if not self._subgraph_producers_all_computed(matched_nodes) or \
                                not self._subgraphs_are_disjoint(matched_nodes, all_matched_nodes):
                            continue
                        all_matched_nodes.extend(matched_nodes)

                        # Get a list of nodes in the subgraph that accept a non-const
                        #  non-placeholder input that isn't computed in the subgraph
                        #
                        matched_head_nodes = self._get_subgraph_head_nodes(
                            matched_nodes)
                        # the first input will only have constant or placeholder inputs,
                        # so we need to add it to the list if it's not in there
                        if head not in matched_head_nodes:
                            matched_head_nodes.append(head)

                        input_names_for_head_nodes = self._get_input_names_for_head_nodes(
                            matched_head_nodes)

                        # Create an SCNNOp for the matched subgraph
                        # and add it to the accelerator op list
                        #
                        scnn_op = op(input_names_for_head_nodes,
                                     matched_nodes, output_nodes, graphdb, self)
                        self._add_scnn_op(scnn_op)

                        # Tell the DB which nodes are now being assigned to this scnn op
                        #
                        for node in matched_nodes:
                            self.graphdb.assign_scnn_op_to_tf_op(scnn_op, node)

                        # Keep track of all of the heads we have matched so that we
                        #  can remove them from the head list
                        #
                        iteration_matched_heads.extend(matched_head_nodes)

                        # Update our list of which operations have exported
                        #  outputs for use in later nodes
                        #
                        self._add_outputs_to_computed_ops(output_nodes)

                        # Keep track of the outputs we have matched in this iteration
                        #  so that we can update our head list with all of their consumers
                        #
                        iteration_outputs.extend(output_nodes)

                        # Print info about what we matched
                        #
                        self._print_head_match_status(
                            op, head, matched_nodes, matched_head_nodes, output_nodes)

                        found_at_least_one_subgraph = True
                        break

            # Remove any nodes that we matched
            #
            heads = remove_items_from_list_if_they_exist(
                iteration_matched_heads + full_graph_outputs, heads)

            # Get any consumers of ops we matched
            #
            consumers = self._get_subgraph_consumers(iteration_outputs)

            # Remove any consumers that have already been matched
            #
            consumers = remove_items_from_list_if_they_exist(
                all_matched_nodes, consumers)

            # Add remaining consumers to list of heads to match
            heads.extend(consumers)

        assert self._op_list_matches_output_list(
            full_graph_outputs, output_names)


class TFGraphDB():

    def __init__(self, nodes):
        self.nodes = nodes
        self._build_tensor_to_producer_op_map(nodes)
        self._build_tensor_to_consumer_op_map(nodes)
        # add code to duplicate nodes that output to multiple nodes and
        # have different properties
        self.scnn_op_for_tf_op = {}

    def _build_tensor_to_producer_op_map(self, nodes):
        self.tensor_to_producer_op = {}
        for node in nodes:
            if not isinstance(node, tf.Operation):
                continue
            for tensor in node.outputs:
                self.tensor_to_producer_op[tensor.name] = node

    def _build_tensor_to_consumer_op_map(self, nodes):
        self.tensor_to_consumer_ops = {}
        for node in nodes:
            if not isinstance(node, tf.Operation):
                continue
            for tensor in node.inputs:
                if tensor.name in self.tensor_to_consumer_ops.keys():
                    self.tensor_to_consumer_ops[tensor.name].append(node)
                    continue
                self.tensor_to_consumer_ops[tensor.name] = [node]

    def get_node_by_name(self, name):
        for node in self.nodes:
            if node.name == name:
                return node
        return None

    def get_consumers(self, op):
        consumers = []
        for tensor in op.outputs:
            consumers.extend(self.tensor_to_consumer_ops[tensor.name])
        return consumers

    def get_producers(self, op):
        producers = []
        for tensor in op.inputs:
            producers.append(self.tensor_to_producer_op[tensor.name])
        return producers

    def get_inputs(self):
        inputs = []
        for node in self.nodes:
            if node.type != "Placeholder":
                continue
            inputs.append(node)
        return inputs

    def get_input_tensors_with_indices(self, op, indices):
        input_ops = self.get_producers(op)
        tensors = [op.outputs[0]
                   for i, op in enumerate(input_ops) if i in indices]
        return tensors

    def get_second_input_tensor(self, op):
        return self.get_input_tensors_with_indices(op, [1])[0]

    def get_first_input_tensor(self, op):
        return self.get_input_tensors_with_indices(op, [0])[0]

    def assign_scnn_op_to_tf_op(self, scnn_op, tf_op):
        self.scnn_op_for_tf_op[tf_op.name] = scnn_op

    def get_scnn_op_for_tf_op(self, tf_op):
        return self.scnn_op_for_tf_op[tf_op.name]

    def add_fake_op(self, fake_op):
        for node in fake_op.inputs:
            self.tensor_to_consumer_ops[node.name].append(fake_op)

    def replace_input(self, c, node, node_copy):
        for t in c.inputs:
            producer = self.tensor_to_producer_op[t.name]
            if producer.name == node.name:
                self.tensor_to_producer_op[t.name] = node_copy


def parse_args(argv):
    try:
        opts, args = getopt.getopt(argv, "", [
            "graphdef=",
            "output_dir="])
        for i, opt in enumerate(opts):
            if len(opt) == 1:
                opt = (opt[0], 1)
                opts[i] = opt
        opts = dict(opts)
        if not "--graphdef" in opts and not "--output_dir" in opts:
            raise getopt.GetoptError
    except getopt.GetoptError:
        print("Usage: PYTHONPATH=$PYTHONPATH:/home/mat/tensorflow_models tf_graph_to_scnn.py --graphdef <path/to/graphdef.pb> --output_dir <path/to/files/for/accelerator/>")
        sys.exit(2)
    return opts


def find_op_index_with_output_in_graphdef(output, graphdef):
    for i, node in enumerate(graphdef.node):
        if isinstance(node, tf.Operation):
            for output in node.outputs:
                if output.name is output:
                    return i
    return None


def get_array_from_node(node):
    return tensor_util.MakeNdarray(node.attr['value'].tensor)


def get_model():
    return resnet_model.Model(
        resnet_size=50,
        bottleneck=True,
        num_classes=1001,
        num_filters=64,
        kernel_size=7,
        conv_stride=2,
        first_pool_size=3,
        first_pool_stride=2,
        block_sizes=[3, 4, 6, 3],
        block_strides=[1, 2, 2, 2],
        final_size=2048,
        resnet_version=2,
        data_format='channels_last',
        dtype=tf.float32)


def get_nodes(graphdef):
    returned_elements = []
    for node in graphdef.node:
        returned_elements.append(node.name)
    nodes = tf.import_graph_def(graphdef, return_elements=returned_elements)
    return nodes


def print_nodes(nodes):
    for node in nodes:
        if isinstance(node, tf.Operation):
            print(node.name + ": " + str(list(node.inputs)) +
                  "\n      " + str(node.outputs))


def get_positive_rgb_tensor_from_real_tensor(tensor):
    min_gez = np.min(tensor) >= 0

    r = np.clip(tensor[..., np.newaxis], 0, None)
    g = np.zeros_like(r)
    b = -1 * np.clip(tensor[..., np.newaxis], None, 0)
    if min_gez:
        rgb = np.concatenate([r, r, r], -1)
    else:
        rgb = np.concatenate([r, g, b], -1)
    rgb *= 255.0 / (np.max(rgb) + 0.000001)
    print(np.max(rgb))
    rgb = rgb.astype(np.uint8)
    return rgb


def get_integer_divisor_closest_to_sqrt_of_number(number):
    n_sqrt = math.sqrt(number)
    closest = n_sqrt - 1
    closest_divisor = 1
    divided = np.divide(number, np.arange(1, int(number / 2)))
    for i, div in enumerate(divided):
        if int(div) != div:
            continue
        if closest > abs(div - n_sqrt):
            closest = abs(n_sqrt - div)
            closest_divisor = div
    return closest_divisor


def get_normalized_hw(tensor):
    tensor = np.transpose(tensor, [0, 3, 1, 2])
    merged = np.reshape(tensor, tensor.shape[
                        :2] + (np.prod(tensor.shape[2:]),))
    mins = np.min(merged, axis=2, keepdims=True)
    positive = merged - mins
    positive_maximums = np.max(positive, axis=2, keepdims=True)
    normalized = np.divide(positive, 0.0000001 + positive_maximums)
    unmerged = np.reshape(normalized, tensor.shape)
    return np.transpose(unmerged, [0, 2, 3, 1])


def get_maximized_dynamic_range_hw(tensor):
    tensor = np.transpose(tensor, [0, 3, 1, 2])
    merged = np.reshape(tensor, tensor.shape[
                        :2] + (np.prod(tensor.shape[2:]),))
    mean = np.mean(merged, axis=2, keepdims=True)
    mean_squared = np.square(mean)
    squared_merged = np.square(merged)
    squared_mean = np.mean(squared_merged, axis=2, keepdims=True)
    variance = squared_mean - mean_squared
    zero_mean = merged - mean
    one_std = zero_mean / (np.sqrt(np.abs(variance)) + 0.000001)
    positive = (one_std + 1.0) / 2.0
    clipped = np.clip(positive, 0, 1.0)
    unmerged = np.reshape(clipped, tensor.shape)
    unmerged = np.multiply(
        unmerged, 1 - np.equal(tensor, np.zeros_like(tensor)))
    return np.transpose(unmerged, [0, 2, 3, 1])


def get_maximized_dynamic_range_hw_2(tensor):
    tensor = np.transpose(tensor, [0, 3, 1, 2])
    merged = np.reshape(tensor, tensor.shape[
                        :2] + (np.prod(tensor.shape[2:]),))
    positive = merged - np.min(merged, axis=2, keepdims=True)
    psorted = np.sort(positive)
    min_index = int(psorted.shape[-1] * 0.1)
    max_index = int(psorted.shape[-1] * 0.9)
    threshold_mins = psorted[:, :, min_index:min_index + 1]
    threshold_maxes = psorted[:, :, max_index:max_index + 1] - threshold_mins
    zero_min = positive - threshold_mins
    one_max = zero_min / (threshold_maxes + 0.0000001)
    clipped = np.clip(one_max, 0, 1.0)
    unmerged = np.reshape(clipped, tensor.shape)
    unmerged = np.multiply(
        unmerged, 1 - np.equal(tensor, np.zeros_like(tensor)))
    return np.transpose(unmerged, [0, 2, 3, 1])


def get_image_from_3d_activation(activation, normalize_hw=True):
    if normalize_hw:
        #activation = get_normalized_hw(activation)
        activation = get_maximized_dynamic_range_hw_2(activation)
    print(activation.shape[1:3])
    channels = activation.shape[-1]
    split_channels = []
    split_channels += [int(get_integer_divisor_closest_to_sqrt_of_number(channels))]
    split_channels += [int(channels / split_channels[0])]
    separated_channels = np.reshape(activation, list(
        activation.shape[1:3]) + split_channels)
    regrouped = np.transpose(separated_channels, [2, 0, 3, 1])
    flattened = np.reshape(
        regrouped, [c * d for c, d in zip(activation.shape[1:3], split_channels)])
    image = get_positive_rgb_tensor_from_real_tensor(flattened)
    return image


def mkdir_if_it_doesnt_exist(path):
    if not os.path.exists(path):
        os.makedirs(path)


def write_activations_to_output_dir(sess, path, accelerator, db):
    img = imread('individualImage.png') * 255
    _R_MEAN = 123.68
    _G_MEAN = 116.78
    _B_MEAN = 103.94
    _CHANNEL_MEANS = [_R_MEAN, _G_MEAN, _B_MEAN]
    quant_image = imagenet_preprocessing._mean_image_subtraction(
        tf.constant(img), _CHANNEL_MEANS, 3)
    quant_image = np.expand_dims(sess.run(
        tf.constant(img)), 0)
    placeholder_nodes = db.get_inputs()
    # better to compare to conv output images, but need layer outputs to correctly
    # execute the accelerator
    accelerator_output_names, accelerator_output_map = accelerator.get_exported_outputs_and_names()
    # for name in accelerator_output_names:
    #	print(name)

    ######################
    #accelerator_output_names, accelerator_output_map = accelerator.get_accum_buffer_nodes_and_names()
    ######################

    #fully_connected_output_names, fully_connected_output_map = accelerator.get_fully_connected_nodes_and_names()
    # accelerator_output_names.extend(fully_connected_output_names)
    #accelerator_output_map[fully_connected_output_names[-1]] = fully_connected_output_map[fully_connected_output_names[-1]]
    activations = sess.run([get_quant_op(
        accelerator_output_map[name].outputs[0]).output
        for name in accelerator_output_names],
        feed_dict={placeholder_nodes[0].outputs[0].name: quant_image})
    quant_image /= 16
    quant_image = sess.run(get_quant_op(quant_image).output)
#	activations = [activation.output for activation in activations]
    print("CLASS OUTPUT")
    print(activations[-1])
    print(activations[-1][0].shape)
    print(np.argsort(activations[-1][0]))
    # print("")
    activations_output_path = path + "/activations/"
    mkdir_if_it_doesnt_exist(activations_output_path)
    np.save(activations_output_path + "input.npy", quant_image, False)

    activation_data_vectors = {"Activation Values" : [], "Layer" : [], "Generator" : []}
    for i, (name, activation) in enumerate(zip(accelerator_output_names, activations)):
        is_2d = len(activation.shape) == 2
        if is_2d:
            activation = np.expand_dims(activation, 0)
            activation = np.expand_dims(activation, 0)
        np.save(activations_output_path + "with_bias_and_nonlinearity_" + str(i) + ".npy", activation, False)
        if i < 9:
            flat = activation.flatten().tolist()
            activation_data_vectors["Activation Values"].extend(flat)
            activation_data_vectors["Layer"].extend(["l" + str(i)] * len(flat))
            activation_data_vectors["Generator"].extend(["TensorFlow"] * len(flat))
        activation = activation.astype(float)
        #histogram = np.histogram(activation)
        # print(histogram)
        #plt.hist(histogram[0], bins=histogram[1])
        #plt.hist(activation.flatten(), bins='auto')
        # plt.show()
        is_2d = activation.shape[1:3] == (1, 1)
        sparsity = int(
            100 * (len(np.where(activation == 0)[0]) / np.prod(activation.shape)))
        image = get_image_from_3d_activation(activation, not is_2d)
        path = activations_output_path + name + ".png"
        imageio.imwrite(path, image)
        activations_output_path + name + "_" + str(sparsity) + ".png"
    return activation_data_vectors


def read_3d_numpy_file_and_write_as_image(path, output_path):
    arr = np.load(path)
    arr = arr.astype(float)
    #plt.hist(arr.flatten(), bins="auto")
    # plt.show()
    im = get_image_from_3d_activation(np.expand_dims(
        np.transpose(arr.astype(float), [1, 2, 0]), 0))
    imageio.imwrite(output_path, im)


def write_layer_params_to_output_dir(sess, path, accelerator, db):
    op_data_path = path + "/op_data/"
    mkdir_if_it_doesnt_exist(op_data_path)

    num_output_buffers_f = open(op_data_path + "num_output_buffers.txt", "w")
    num_output_buffers_f.write(str(accelerator.get_activation_buffer_count()))
    num_output_buffers_f.write("\n")
    num_output_buffers_f.close()

    filename = op_data_path + "ops.txt"
    ops_f = open(filename, "w")
    for i, scnn_op in enumerate(accelerator.get_op_list()):
        op_dir_name = str(i)  # + "_" + type(scnn_op).__name__
        status =scnn_op.write_op_description_to_directory(
            op_data_path + op_dir_name + "/", sess, i)
        if status == -1:
            break
        ops_f.write(op_dir_name)
        ops_f.write("\n")
        continue
        inputs = scnn_op.get_inputs()
        # input_indices = accelerator.
        layer_path = op_data_path + "/" + str(i)
        mkdir_if_it_doesnt_exist(layer_path)
        op_params = scnn_op.get_op_tensors(sess)

        filename = layer_path + "/layer_components.txt"
        layer_components_f = open(filename, "w")
        layer_components_f.write("\n".join(list(op_params.keys())))
        layer_components_f.close()

        filename = layer_path + "/layer_name.txt"
        layer_name_f = open(filename, "w")
        layer_name_f.write(scnn_op.__class__.__name__ + "\n")
        layer_name_f.close()

        for name, op_param in op_params.items():
            filename = layer_path + "/" + name + ".npy"
            print(name)
            print(np.array(op_param).shape)
            if name == "Conv2D" and i == 0:
                op_param *= 16
            #plt.hist(np.array(op_param).flatten(), bins='auto')
            # plt.show()
            np.save(filename, op_param, False)
        ops_f.write(str(i) + "\n")
        # break
    ops_f.close()


def main(argv):
    opts = parse_args(argv)
    with tf.Session() as sess:
        with tf.gfile.GFile(opts["--graphdef"], "rb") as f:
            gd = tf.GraphDef()
            gd.ParseFromString(f.read())
        nodes = get_nodes(gd)
        print_nodes(nodes)
        db = TFGraphDB(nodes)
        accelerator_mapping = SCNN(
            db, "import/resnet_model/Pad", ["import/resnet_model/final_dense"])
        #print_first_conv_weight_oc_sums(sess, accelerator_mapping)
        # accelerator_mapping.print_op_attributes()
        activation_data_vectors = write_activations_to_output_dir(
            sess, opts["--output_dir"], accelerator_mapping, db)
        write_layer_params_to_output_dir(
            sess, opts["--output_dir"], accelerator_mapping, db)

        accelerator_activations = []
        for i in range(9):
            activation = np.load("/home/mat/courses/first_year/second_semester/arch/scnn/tests/l"+str(i)+"_accum_out.npy")
            flat = activation.flatten().tolist()
            activation_data_vectors["Activation Values"].extend(flat)
            activation_data_vectors["Layer"].extend(["l" + str(i)] * len(flat))
            activation_data_vectors["Generator"].extend(["Accelerator"] * len(flat))

        #sns.set(style="whitegrid")
        #ax = sns.violinplot(
        #    x="Layer", y="Activation Values",
        #    hue="Generator", data=activation_data_vectors,
        #    split=True, inner="quartile")
        #ax.set(xlabel="Layer", ylabel="Activation Values")
        #plt.savefig("layer_distributions.pdf", bbox_inches="tight")




if __name__ == "__main__":
    main(sys.argv[1:])
