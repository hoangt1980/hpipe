#include "Vhpipe_tb_0.h"
#include "verilated.h"
#ifdef DUMP
#include "verilated_fst_c.h"
#endif
#include <iostream>

double main_time = 0.0;       // Current simulation time
// This is a 64-bit integer to reduce wrap over issues and
// allow modulus.  You can also use a double, if you wish.

double sc_time_stamp () {       // Called by $time in Verilog
    return main_time;           // converts to double, to match
                                // what SystemC does
}

int main(int argc, char** argv, char** env) {
	Verilated::commandArgs(argc, argv);
	Verilated::traceEverOn(true);
#ifdef DUMP
	VerilatedFstC* tfp = new VerilatedFstC;
#endif
	Vhpipe_tb_0* top = new Vhpipe_tb_0;
	top->clock_0 = 0;
	top->reset = 1;
#ifdef DUMP
	top->trace(tfp, 99);
	tfp->open("/home/mat/drive1/sim.fst");
#endif
	int reset_count = 0;
	std::cout << "Beginning hpipe simulation" << std::endl;
//	while (!Verilated::gotFinish()) {
	for (int i = 0; i < 6000000; ++i) {
#ifdef DUMP
		tfp->dump(main_time);
#endif
		if (++reset_count > 500)
			top->reset = 0;
		top->clock_0 = !top->clock_0;
		top->eval();
		main_time += 0.5;
	}
	top->final();
	delete top;
	exit(0);
}
