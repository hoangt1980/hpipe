import matplotlib.pyplot as plt
import numpy as np
import re

def get_time_map_from_file(file):
	with open(file) as fh:
		order = []
		times_1 = {}
		times_2 = {}
		for l in fh:
			l = l.split(" ")
			if len(l) < 2:
				continue
			name = l[0]
			#if "basic_conv" not in name:
			#	continue
			time = int(l[-1][:-1])
			if name not in times_1.keys():
				times_1[name] = time
				order.append(name)
			elif name not in times_2.keys():
				times_2[name] = time
				#times_1[name] = time - times_1[name]
	return (times_1, order)
times_1, order = get_time_map_from_file("../tensorflow_models/official/resnet_sparse/individual_layer_cycle_counts.txt")
times_2, order = get_time_map_from_file("../tensorflow_models/official/resnet_sparse/individual_layer_cycle_counts_fixed_dsp_5000.txt")
with open("resnet_50_conv_resource_utilization.log") as f:
	lines = [l.split(";") for l in f]
	get_number = re.compile(r'(\d+)')
	alms = [int(get_number.search(l[2]).group(1)) / 933120 for l in lines]
	regs = [int(get_number.search(l[8]).group(1)) / 3732480 for l in lines]
	m20ks = [int(get_number.search(l[11]).group(1)) / 11721 for l in lines]
	DSPs = [int(get_number.search(l[12]).group(1)) / 5760 for l in lines]
	for a,r,m,d in zip(alms, regs, m20ks, DSPs):
		print(", ".join([str(number) for number in [a,r,m,d]]))

differences = {}
new_order = []
for name in order:
	differences[name] = times_1[name] - times_2[name] #times_2[name] - times_1[name]
	print(name + ": " + str(differences[name]))

print(times_1)
print(times_2)
print("Max is: " + str(max(*list(differences.values()))))

x = np.arange(len(order)) * 3
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
ax1.bar(x, [times_1[name]*2 for name in order], width=2, color='#201060', label="Unbalanced")
ax1.bar(x, [times_2[name] for name in order], width=2, color='#D7E7A9', label="Balanced, DSP Target=5000")
ax2.annotate("Most Layers DSP\nUtilization is Highest", xytext=(-7, 0.039), xy=(0,0))
for i in [0, 5, 9]:
	ax2.annotate("", arrowprops=dict(width=0.1, headwidth=4, headlength=4), xy=(x[i], DSPs[i]), xytext=(x[5],0.0375))

for i,rtype in zip([26, 44, 51], [alms, m20ks, m20ks]):
	ax2.annotate("", arrowprops=dict(width=0.1, headwidth=4, headlength=4), xy=(x[i], rtype[i]), xytext=(90,0.05))
ax2.scatter(x, alms, s=5, marker="H", label="ALMs")
ax2.scatter(x, regs, s=5, marker="X", label="Registers")
ax2.scatter(x, m20ks, s=5, marker="1", label="M20Ks")
ax2.scatter(x, DSPs, s=5, marker="s", label="DSPs")
ax2.annotate("A Few Layers Have Higher\nALM or Memory Utilization", xytext=(55, 0.05), xy=(0,0), backgroundcolor="w")
ax1.legend(bbox_to_anchor=(0.775,1), loc="upper right", title="Cycles to Complete Image")
ax2.legend(title="Resource\nUtilization\n(Balanced)")
plt.xlabel(r"ResNet-50 v2 Layer")
ax1.set_ylabel(r"Cycles to Complete Image")
ax2.set_ylabel(r"Resource Utilization (Balanced)")
#plt.title(r"")

labels = []
for i,o in enumerate(order):
	if i % 10 == 0:
		labels.append(o.replace("_", r"\_"))
	else:
		labels.append("")
plt.xticks(x, labels)#, rotation=90)
plt.savefig("cycles_to_complete_image.pgf")