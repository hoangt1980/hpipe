from tf_graph_to_scnn import get_image_from_3d_activation
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
import compare_output_distributions as cod
from scipy.misc import imsave
from os import walk
import re
import math

files = []
for (_, _, filenames) in walk("verilog_images"):
	for f in filenames:
		if ".csv" not in f or "basic_conv_0" in f or "add_0" in f or "relu_0" in f or "placeholder" in f:
			continue
		files.append(f)
file_number_re = re.compile(r"_(\d+)\.csv")
file_number_pairs = [[file_number_re.search(f), f] for f in files]
for i,p in enumerate(file_number_pairs):
	if not p[0]:
		file_number_pairs[i][0] = -1
	else:
		file_number_pairs[i][0] = int(p[0].group(1))
file_number_pairs = sorted(file_number_pairs, key=lambda x: x[0])
files = [p[1] for p in file_number_pairs]


for count,f in enumerate(files):
	print("Generating " + f)
	with open("verilog_images/" + f, "r") as fh:
		values = [[v for v in l.split(",")] for l in fh]
	if len(values) == 0:
		print("No contents in " + f)
		continue
	channels = int(values[0][0])
	del values[0]
	if len(values) == 0:
		print("No contents in " + f)
		continue

	with open("verilog_images_saved/" + f, "r") as fh:
		other = [[v for v in l.split(",")] for l in fh]
	if len(other) == 0:
		print("No contents in " + f)
		continue
	if int(other[0][0]) != channels:
		continue
	del other[0]
	if len(other) == 0:
		print("No contents in " + f)
		continue

	length = len(values[0])
	max_height = length
	if len(values[-1]) != length:
		del values[-1]
	try:
		for i,l in enumerate(values):
			if i > max_height:
				break
			for j,v in enumerate(l):
				if "x" in v:
					values[i][j] = 0.0
				else:
					try:
						values[i][j] = float(v)
					except Exception as e:
						if i == (len(values) - 1):
							values[i][j] = 0.0
						else:
							raise e
	except Exception as e:
		print("Couldn't convert '" + str(values[i][j] + "' to float at (i,j) (" + str(i) + "," + str(j) + "), skipping " + f + "..."))
		continue

	length = len(other[0])
	max_height = length
	if len(other[-1]) != length:
		del other[-1]
	try:
		for i,l in enumerate(other):
			if i > max_height:
				break
			for j,v in enumerate(l):
				if "x" in v:
					other[i][j] = 0.0
				else:
					try:
						other[i][j] = float(v)
					except Exception as e:
						if i == (len(other) - 1):
							other[i][j] = 0.0
						else:
							raise e
	except Exception as e:
		print("Couldn't convert '" + str(other[i][j] + "' to float at (i,j) (" + str(i) + "," + str(j) + "), skipping " + f + "..."))
		continue
		#raise e
					

	"""
	for i,(vl,gl) in enumerate(zip(values, golden_values)):
		breaking = False
		for j,(v,g) in enumerate(zip(vl,gl)):
			if abs(v - g) > 150:
				print("Expected " + str(g) + " got " + str(v) + " in line " + str(i) + " at position " + str(j))
				breaking = True
				break
		if breaking:
			break
	"""


	"""
	np_values = np.array(values)
	np_golden = np.array(golden_values)
	np_golden = np_golden[:np_values.shape[0]]
	values = np_values - np_golden
	values = list(values)
	"""

	#values = golden_values

	height = len(values) // channels
	height = min(height, max_height)
	top_take = math.ceil(height * max(70-count,0) / 140.)
	print(top_take)
	print(height)
	bottom_take = height - top_take
	width = len(values[0])
	values = np.array(values[:channels * height])
	values = np.reshape(values, [1, height, channels, width])
	values = np.transpose(values, [0, 1, 3, 2]).astype(float)
	#values.shape[2] - values.shape[1]
	#values = np.pad(values, [[0,0], [0, 0], [0,0], [0, 0]], mode="constant", constant_values=0)

	other = np.array(other[:channels * height])
	other = np.reshape(other, [1, height, channels, width])
	other = np.transpose(other, [0, 1, 3, 2]).astype(float)
	other = np.roll(other, axis=-1, shift=1)

	output = np.concatenate([values[:,:top_take,:,:], other[:,top_take:,:,:]], axis=1)
	im = get_image_from_3d_activation(output)
	im = np.reshape(im[:,:,0], [im.shape[0], -1, width])
	im = np.reshape(im, [-1, height, im.shape[-2], width])
	im = np.transpose(im, [1,3,0,2])
	im = np.reshape(im, [height, width, -1])
	im = im[:,:,:16*3]
	im = np.transpose(im, [0,2,1])
	im = np.reshape(im, [height, 4*3, width * 4])
	im = np.transpose(im, [1,0,2])
	im = np.reshape(im, [3, height*4, width * 4])
	im = np.transpose(im, [1,2,0])

	no_ext = f[:-4]
	imsave("verilog_images/" + no_ext + ".png", im)

