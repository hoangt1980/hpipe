
Digital Logic Quick Start Guide
===============================================

This guide should show you how to use the digital logic python library to build and simulate
a simple circuit in verilator.

The example mostly walks through the boilerplate code that is currently necessary to begin
building circuits.  As such, it simply instatiates a RAM in a demo circuit that is 10 bits
wide and 1024 elements deep.  It then instantiates that RAM into a dummy top level module
that tells the library which signals should be exported from the demo module as inputs and
outputs.

Creating a circuit with Digital Logic
-----------------------------------------------
You should copy the following code into a python file we will call ``demo_circuit.py``
::
	import digitallogic as dl

	class DemoCircuit(dl.Module):
	  """
	  If we add a doc string to this demo module we will see that the generated
	  verilog will have a comment at the top with the same contents.
	  """
	  # you can add whatever arguments you want here if you want to insure
	  # you always get a reference to the current clock or the current reset,
	  # declare this as def __init__(self, clock, reset, **kwargs):
	  def __init__(self, **kwargs):
	    # when subclassing dl.Module you need to always call super __init__
	    # and you must pass a module_name, a module kind, and the kwargs
	    super(DemoCircuit, self).__init__("demo_circuit", kind="demo_circuit", **kwargs)
	    # eventually I want to get rid of this, but for now we need to call self.inst()
	    # to instantiate stuff, which happens so often that it's just a good idea to
	    # get another reference to this which is the much shorter inst
	    inst = self.inst

	    # Now we need to explicitly enter this module's scope.  When we call inst()
	    # from within this scope, signals and modules will be instatiated in this
	    # component
	    with dl.ModuleScope(self):
	      # now let us just instantiate a RAM we can write to and read from
	      self.ram = inst(dl.RAM, bits=10, depth=1024, name="ram")



	# We need a top level module that we won't actually generate for simulation
	# with verilator.  This will allow us to ensure inputs and outputs are correctly
	# set for the DemoCircuit module
	class SimulatorTopLevelModule(dl.Module):
	  def __init__(self, **kwargs):
	    super(SimulatorTopLevelModule, self).__init__("demo_circuit", kind="demo_circuit", **kwargs)
	    inst = self.inst
	    # we will set this property to prevent digitallogic from actually generating this module's verilog
	    # this is what we do for builds targeted at use in quartus or verilator.  
	    self.should_create_verilog_definition = False
	    # For builds targeting VCS we would set this to true, and design a module that has no inputs or outputs
	    # and generates resets, clocks, and test vectors.

	    with dl.ModuleScope(self):
	      # first let us create a clock and a reset
	      # the scope.
	      clock = inst(dl.Clock, name="clock")
	      reset = inst(dl.Logic, bits=1, name="reset")

	      # we will use dl.ParamScope to set the clock and reset for the children
	      # we can use dl.ParamScope to set parameters for anything instantiated in
	      with dl.ParamScope(clock=clock, reset=reset):
	        demo_circuit = inst(DemoCircuit, name="my_demo_circuit")

	        # now we need to copy the signals into the top level module to
	        # make dl propagate them up
	        demo_circuit.ram.r_addr.set_driver(inst(dl.Constant, value=0, bits=demo_circuit.ram.r_addr._bit_width, name="ram_r_addr"))
	        demo_circuit.ram.w_addr.set_driver(inst(dl.Constant, value=0, bits=demo_circuit.ram.w_addr._bit_width, name="ram_w_addr"))
	        demo_circuit.ram.r_en.set_driver(inst(dl.Constant, value=0, bits=1, name="ram_r_en"))
	        demo_circuit.ram.w_en.set_driver(inst(dl.Constant, value=0, bits=1, name="ram_w_en"))
	        demo_circuit.ram.r_data.copy()
	        demo_circuit.ram.w_data.set_driver(inst(dl.Constant, value=0, bits=demo_circuit.ram.w_data._bit_width, name="ram_w_data"))





	def main():
	  # we instantiate the top level module
	  stlm = SimulatorTopLevelModule()

	  # and we will now generate the output circuit
	  dl.Verilog.current_circuit.write_verilog_to_dir("demo_circuit_dir")



	if __name__ == "__main__":
	  main()



To run this and generate the circuit, run ``PYTHONPATH=</path/to/hpipe/git/repo> python3 demo_circuit.py``

Inspecting the Generated Verilog
-----------------------------------------------
After generation you should see the following directory structure
::
	demo_circuit_dir/ # this comes from the directory we specified in the python script
	 | - generated_constraints.sdc # this is a file that digitallogic will fill with timing constraints for quartus builds.  For this circuit is it empty.
	 | - verilog_paths.txt # This is a list of all of the verilog files in the current generated circuit.  You can take a look at it.
	 | - generated_quartus_settings.qsf # this is a file digitallogic will fill with quartus settings.  For this circuit it is empty.
	 | - CIRCUIT/ # This directory contains all of the generated verilog files in subdirectories matching the module instantiation hierarchy
	      | - demo_circuit_0/ # this is the directory that gets generated for the demo_circuit we instantiated in the dummy top level module.
	           | - my_demo_circuit.v # this module provides the implementation for our dummy module
	           | - my_demo_circuit/ # this directory containts the implementations of any modules instantiated in my_demo_circuit.v
	                | - ram.v # this contains the implementation of our RAM model.


.. highlight:: verilog
If we take a look at ``demo_circuit_dir/CIRCUIT/demo_circuit_0/my_demo_circuit.v`` we will see the following
::
	// 68d2999b2196ca58ed7e73e730ff2fbc
	module my_demo_circuit(
	  clock,
	  demo_circuit_0_ram_r_addr,
	  demo_circuit_0_ram_w_addr,
	  demo_circuit_0_ram_r_en,
	  demo_circuit_0_ram_w_en,
	  demo_circuit_0_ram_w_data,
	  ram_r_data
	);
	  // 
	  // 	If we add a doc string to this demo module we will see that the generated
	  // 	verilog will have a comment at the top with the same contents.
	  // 	

	  input [0:0] clock;
	  input [9:0] demo_circuit_0_ram_r_addr;
	  input [9:0] demo_circuit_0_ram_w_addr;
	  input [0:0] demo_circuit_0_ram_r_en;
	  input [0:0] demo_circuit_0_ram_w_en;
	  input [9:0] demo_circuit_0_ram_w_data;
	  output [9:0] ram_r_data;

	  ram ram_i(
	    // inputs
	    .clock(clock),
	    .demo_circuit_0_ram_r_addr(demo_circuit_0_ram_r_addr),
	    .demo_circuit_0_ram_w_addr(demo_circuit_0_ram_w_addr),
	    .demo_circuit_0_ram_r_en(demo_circuit_0_ram_r_en),
	    .demo_circuit_0_ram_w_en(demo_circuit_0_ram_w_en),
	    .demo_circuit_0_ram_w_data(demo_circuit_0_ram_w_data),
	    // outputs
	    .r_data(ram_r_data)
	  );

	endmodule // my_demo_circuit

We can see the RAM that we instantiated, and we can see that the clock, and ram ports that were connected to the top level module have been set as input and
output ports.  This is necessary for synthesis to a real circuit, and also necessary for simulation with verilator since verilator requires test vectors
and circuit control to happen in a C++ control file.

Simulating the Generated Verilog in Verilator
--------------------------------------------------------

.. highlight:: c++
Now make a testbench top level verilator file.
::
	// Most of this comes from the Verilator documentation, so please reference that
	// for explanations of what is going on here.
	#include "Vmy_demo_circuit.h" 
	#include "verilated.h"
	#ifdef DUMP
	#include "verilated_fst_c.h"
	#endif
	#include <iostream>
	#include <fstream>

	double main_time = 0.0;       // Current simulation time
	// This is a 64-bit integer to reduce wrap over issues and
	// allow modulus.  You can also use a double, if you wish.

	#ifdef DUMP
		VerilatedFstC* tfp;
	#endif

	double sc_time_stamp () {       // Called by $time in Verilog
	    return main_time / 100.0;           // converts to double, to match
	                                // what SystemC does
	}

	void eval_dump(Vmy_demo_circuit* top) {
	#ifdef DUMP
		tfp->dump(main_time);
	#endif
		top->eval();
	#ifdef DUMP
		tfp->dump(main_time);
	#endif
	}


	void tick(Vmy_demo_circuit* top) {
		top->clock = !top->clock;
		eval_dump(top);
		main_time += 0.5;
		top->clock = !top->clock;
		eval_dump(top);
		main_time += 0.5;
	}

	int main(int argc, char** argv, char** env) {


		Verilated::commandArgs(argc, argv);
		Verilated::traceEverOn(true);

	#ifdef DUMP
		tfp = new VerilatedFstC;
	#endif
		Vmy_demo_circuit* top = new Vmy_demo_circuit;
		top->clock = 0;
	#ifdef DUMP
		top->trace(tfp, 99);
		tfp->open("sim.fst");
	#endif
		std::cout << "Beginning simulation simulation" << std::endl;
	//	while (!Verilated::gotFinish()) {

		for (int i = 0; i < 1024; ++i) {
			top->demo_circuit_0_ram_w_en = 1;
			top->demo_circuit_0_ram_w_addr = i;
			top->demo_circuit_0_ram_w_data = i;
			tick(top);
		}
		top->demo_circuit_0_ram_w_en = 0;

		bool had_error = false;
		for (int i = 0; i < 1024; ++i) {
			top->demo_circuit_0_ram_r_en = 1;
			top->demo_circuit_0_ram_r_addr = i;
			tick(top);
			if (top->ram_r_data != i) {
				std::cout << "Iteration " << i << " read wrong data.  Should have been " << i << ", was " << top->ram_r_data << std::endl;
				had_error = true;
				break;
			}
		}

	#ifdef DUMP
		tfp->close();
	#endif
		top->final();
		delete top;
		if (had_error) {
			std::cout << "ERROR: Simulation unsuccessful.\n";
			exit(1);
		}
		std::cout << "PASS: Simulation successful!\n";
		exit(0);
	}


.. highlight:: sh
Copy this and save it in a file called testbench_top.cpp.  We can run our testbench with the following commands
::
	verilator --top-module my_demo_circuit -o verilator_sim --cc -f demo_circuit_dir/verilog_paths.txt --exe testbench_top.cpp
	make -C obj_dir -f Vmy_demo_circuit.mk
	obj_dir/verilator_sim

This just writes numbers 0-1023 to our memory at addresses 0-1023 and then reads them back and ensures that they are the same.

Dumping a Waveform from a Verilator Simulation
---------------------------------------------------
If you want to see a waveform showing this, build with the following commands:
::
	verilator --trace-fst --top-module my_demo_circuit -o verilator_sim --cc -f demo_circuit_dir/verilog_paths.txt --exe testbench_top.cpp
	make OPT_FAST=" -DDUMP=1 " -C obj_dir -f Vmy_demo_circuit.mk
	obj_dir/verilator_sim

Then open GTK Wave by running
::
	gtkwave &> /dev/null &

This should open GTK Wave.  From there you can go to File -> Open New Tab, open ``sim.fst``, select a module from the SST pane, then select a few
signals from the signals pane and click append.
