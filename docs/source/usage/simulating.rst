.. _simulating:
Running Simulations
========================================

This guide will explain how to simulate an entire HPIPE circuit, or part of an HPIPE circuit, in a verilator simulation.  This simulation will dump output activations into a directory.  This guide will show how to post-process those activations to view graphical representations of them and to compare them to the same post processed images from TensorFlow.

Building a Circuit
----------------------------
To build a test circuit, please follow the :doc:`Quick Start Guide <quickstart>`.  To build part of a circuit and then stop at a particular layer, you can set the ``"build_until"`` parameter in your ``.json`` config file to a string that partially matches the name of one of the layers in your circuit.  This will cause HPIPE to stop building your circuit when it reaches a layer that got a partial string match on the layer name.

You will also need to make sure ``"instantiate_quartus_ip"`` is set to ``false`` in your config file (our verilator simulations do not support actual quartus IP models).  Also make sure your ``"generated_circuit_dir"`` is set to ``"generated_files/circuits/quartus_circuit"``, and your ``"top_level_module_type"`` is set to ``"quartus"``.

Generating a Binary Input
----------------------------
A default binary input file containing multiple imagenet images can be found at:
::
	generated_files/binary_inputs/many_inputs_1.bin
	
If you would like to generate your own binary input file based off of a png image you can use the ``convert_png_to_bin.py`` script and then replace the aforementioned ``many_inputs_1.bin`` file. For example:
::
	python convert_png_to_bin.py --input_png A_large.png --output A_large.bin
	
You can also specify the interface width using the ``--interface_width`` option (default is 256). Note that the script will only work on RGB images with 8-bit pixels.


Dumping TensorFlow Activations
----------------------------
We will want to compare our simulation to a run of the neural network in TensorFlow.  To do this, we can tell HPIPE to dump the activations it got from the ``example_image`` in the config file.  To do this, we can invoke HPIPE by running:
::
	python3 -m hpipe.LayerComponents --param_file <path/to/your/param/file.json> --dump_tensorflow_activations

We also want to make sure the input image is the same as the one we will use for our simulation.  To ensure this, we will dump the first image from the bin file the simulation will be using as input:
::
	python3 export_image_from_bin.py --image_number 0 --bin_file_path generated_files/binary_inputs/many_inputs_1.bin --output_path exported_binary_inputs/imagenet_0.png

You should set ``example_image`` in your config file to ``exported_binary_inputs/imagenet_0.png`` when you generate your circuit.

Running Verilator
--------------------------
First, please install the latest version of verilator from source, and make sure the path to the executable is in your PATH.  Then you should be able to run the following command:
::
	make verilator && make verilator_run

This should cause verilator to build and run your circuit, dumping csv files into ``generated_files/layer_images/verilog``.  To process these, run:
::
	find generated_files/layer_images/verilog -name '*.csv' | xargs python3 csv_to_image.py --relative_path_to_tf_outputs ../tensorflow

This script will generate images of the activations from both tensorflow and the verilator simulation (if you omit the ``--relative_path_to_tf_outputs`` option it will just dump the images from your hpipe simulation).  They will be pngs and pdfs of the same name in the ``generated_files/layer_images/verilog`` directory.


If you are able to successfully run the post processing script you should see PDFs with contents like the following:

.. image:: /_static/output_distributions.png
	:alt: Distributions within output channels for one layer

This is a plot of the distributions of the TenorFlow activations in orange, next to the HPIPE activations in blue.  The distributions are broken down into output channels, so along the bottom you can see the last two distributions are from the 1,023rd output channel for this particular layer (the 27th convolution in in MobileNet-V1).  If the shapes of the distributions look approximately the same, that is what we want.  You should also see images (PNGs) that show visualizations of the activations, like the following from the first convolution operation in MobileNet-V1:

.. image:: /_static/basic_conv_0_verilog.png
	:alt: Activations from the HPIPE simulation

If you look in the ../tensorflow directory you should see corresponding activations from the TensorFlow run as well.  The following example is our TensorFlow output for the same network and image as our HPIPE simulation:

.. image:: /_static/basic_conv_0_tensorflow.png
	:alt: Activations from the TensorFlow run

We want these to look as similar as possible.
