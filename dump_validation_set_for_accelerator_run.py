from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

from absl import app as absl_app
from absl import flags
import tensorflow as tf  # pylint: disable=g-bad-import-order
import numpy as np
import sys
import getopt
import os
import re
from official.resnet_sparse.gen_pure_verilog.utils import BitArray

from official.utils.flags import core as flags_core
from official.utils.logs import logger
from official.resnet_sparse import imagenet_preprocessing
from official.resnet_sparse import resnet_model
from official.resnet_sparse import imagenet_main
from official.resnet_sparse import resnet_run_loop


def main(argv):
	input_fn = imagenet_main.input_fn(
	        is_training=False, data_dir="/home/mat/drive1/tf_imagenet",
	        batch_size=1,
	        num_epochs=1)
	with tf.gfile.GFile("/home/mat/drive1/different_sparsity_resnet_models/304811/frozen_graph.opt.swapped_max_pool_and_batch_norm.pb", "rb") as f:
		gd = tf.GraphDef()
		gd.ParseFromString(f.read())

	returned_elements = []
	input_map = {}
	placeholder_name = ""
	for node in gd.node:
		returned_elements.append(node.name)
	nodes = tf.import_graph_def(gd, return_elements=returned_elements, input_map=input_map)
	for node in nodes:
		#print(node.name)
		if node.name == "import/resnet_model/final_dense":
			logits = node.outputs[0]
		if node.type == "Placeholder":
			placeholder_name = node.outputs[0].name
			print(placeholder_name)

	debiased_image = tf.placeholder(dtype=tf.float32)
	mean_image_addition = debiased_image + tf.constant([[[[123.68, 116.779, 103.939]]]])
	with tf.Session() as sess:
		it = input_fn.make_one_shot_iterator()
		get_images_op = it.get_next()
		image_count = 0
		done = False
		group_count = 0
		classes_list = []
		predictions_list = []
		while not done:
			within_group_count = 0
			images_list = []
			while True:
				try:
					images, classes = sess.run(get_images_op)
					predictions = sess.run(logits, feed_dict={placeholder_name: images})
					images = images.astype(np.int16)
					classes = classes.astype(np.int16)
					images_list.append(images)
					classes_list.append(classes)
					predictions_list.append(predictions)
					image_count += 1
					within_group_count += 1
					print("Image Count: " + str(image_count) + " / 50000", end="\r")
					if within_group_count > 5000:
						break
				except tf.errors.OutOfRangeError:
					done = True
					break
			group_count += 1
			print()
			print("Concatenating lists")
			images = np.concatenate(images_list, axis=0)
			images = sess.run(mean_image_addition, feed_dict={debiased_image: images})
			
			del images_list
			BHCW = np.transpose(images, [0,1,3,2])
			interface_width = 256
			bits_per_scalar = 8
			interface_elements = interface_width // bits_per_scalar
			interface_bits = interface_elements * bits_per_scalar
			BHCWP = np.pad(BHCW, [[0,0],[0,0],[0,0],[0, 224 % interface_elements]], mode="constant", constant_values=0)
			del BHCW
			LI = np.reshape(BHCWP, [-1, interface_elements]).astype(np.uint8)
			print("Building BitArrays for each x index")
			test_in = [BitArray(bits=bits_per_scalar, value_vector=LI[:,l]) for l in range(LI.shape[-1])]
			del BHCWP
			del LI
			tmp = test_in[-1]
			print("Concatenating BitArrays")
			for t in reversed(test_in[:-1]):
				tmp += t
			del test_in
			print("Padding BitArray")
			if tmp.bits != 256:
				tmp += BitArray(bits=256 - tmp.bits, length=tmp.bit_array.shape[0])
			print("Dumping BitArray")
			with open("many_inputs_" + str(group_count) + ".bin", "wb") as fh:
				fh.write(tmp.get_byte_array())
			del tmp
		classes = np.concatenate(classes_list, axis=0)
		predictions = np.concatenate(predictions_list, axis=0)
		with open("classes.bin", "wb") as fh:
			fh.write(classes.flatten().tobytes())
		with open("tensorflow_predictions.csv", "w") as fh:
			for i in range(predictions.shape[0]):
				fh.write(",".join([str(j) for j in list(predictions[i])]) + "\n")





if __name__ == "__main__":
	main(sys.argv[1:])