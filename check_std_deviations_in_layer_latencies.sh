#!/bin/bash
bash -c "find verilator_layer_logs/ -type f $1" | xargs -I@ python -c 'import sys; import numpy as np; f = open(sys.argv[1],"r"); lines = [l for l in f]; f.close(); lines = lines[2:-1]; values = [int(l.split(" ")[4]) for l in lines]; v = np.array(values); d = np.diff(v); dev = np.std(d); print(sys.argv[1] + ": " + str(dev)) ' @
