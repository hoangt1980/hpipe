import tensorflow as tf
from tensorflow.python.framework import tensor_util
from tensorflow.core.framework import graph_pb2
from tensorflow.python.platform import gfile
from official.resnet_sparse import resnet_model

import numpy as np
from scipy.misc import imsave

import sys
import getopt
import os
import re

def find_op_index_with_output_in_graphdef(output, graphdef):
	for i,node in enumerate(graphdef.node):
		if isinstance(node, tf.Operation):
			for output in node.outputs:
				if output.name is output:
					return i
	return None

def parse_args(argv):
	try:
		opts, args = getopt.getopt(argv, "", [
			"model_dir=",
			"init_sparse_vars",
			"weight_image_directory=",
			"output_graph="])
		for i,opt in enumerate(opts):
			if len(opt) == 1:
				opt = (opt[0], 1)
				opts[i] = opt
		opts = dict(opts)
		if not "--model_dir" in opts:
			raise getopt.GetoptError
	except getopt.GetoptError:
		print("Usage: PYTHONPATH=$PYTHONPATH:/home/mat/tensorflow_models export_model.py --model_dir <path/to/ckpt/dir>")
		sys.exit(2)
	return opts

def compute_sparsity(tensor):
	return np.sum(np.equal(tensor, 0)) / float(np.prod(tensor.shape))

def get_max_min(tensor):
	return (np.max(tensor), np.min(tensor))

def get_quant_op(tensor):
	return tf.quantize(weight, -8-2**-12, 8, tf.qint16)

def get_positive_rgb_tensor_from_real_tensor(tensor):
	r = np.clip(tensor[..., np.newaxis], 0, None)
	g = np.zeros_like(r)
	b = np.clip(tensor[..., np.newaxis], None, 0)
	rgb = np.concatenate([r, g, b], -1)
	rgb *= 255.0 / np.max(rgb)
	rgb = rgb.astype(np.uint8)
	return rgb

def get_array_from_node(node):
	return tensor_util.MakeNdarray(node.attr['value'].tensor)

def write_weight_images_to_directory(directory, graphdef, identifier):
	subdir = directory + "/" + identifier
	os.mkdir(subdir)
	f = open(subdir+"/log.txt", "w+")
	weight_and_bias_pairs = {}
	last_weight_name = ""
	total_sparsity = 0.0
	total_weighting = 0.0
	for node in graphdef.node:
		if "masked_weight" in node.name:
			last_weight_name = node.name
			weight_and_bias_pairs[node.name] = {"weight": node}
		if "/moving_mean" in node.name and not "moving_mean/read" in node.name:
			weight_and_bias_pairs[last_weight_name]["bias"] = node
	for key, node in weight_and_bias_pairs.items():
		weight = get_array_from_node(node["weight"])
		if len(weight.shape) == 4:
			weight = weight.transpose(3,0,2,1).reshape(weight.shape[0]*weight.shape[3], weight.shape[1]*weight.shape[2])

		weight_image = get_positive_rgb_tensor_from_real_tensor(weight)
		imsave(subdir + "/" +key.replace('/', '_')+'.png', weight_image)

		sparsity = compute_sparsity(weight)
		sparsity_weighting = np.prod(weight.shape)
		total_sparsity += sparsity * sparsity_weighting
		total_weighting += sparsity_weighting

		f.write(key + ": " + str(weight_image.shape) + " " + str(sparsity) + "\n")

	f.write("Total sparsity: " + str(total_sparsity / total_weighting) + "\n")
	f.close()

def get_model():
	return resnet_model.Model(
        resnet_size=50,
        bottleneck=True,
        num_classes=1001,
        num_filters=64,
        kernel_size=7,
        conv_stride=2,
        first_pool_size=3,
        first_pool_stride=2,
        block_sizes=[3, 4, 6, 3],
        block_strides=[1, 2, 2, 2],
        final_size=2048,
        resnet_version=2,
        data_format='channels_last',
        dtype=tf.float32)

def get_model_op(model):
	test_input_image = tf.placeholder(tf.float32, shape=(1,224,224,3))
	model_op = model(test_input_image, False)
	return model_op

def load_latest_model_in_dir(directory, sess, init_sparsity=False):
	latest_checkpoint = tf.train.latest_checkpoint(directory)
	if latest_checkpoint is None:
		latest_checkpoint = directory
	if init_sparsity:
		variables_map = {
				v.name.split(":")[0] : v.name.split(":")[0] 
			for v in tf.trainable_variables() 
				if "pruning_variable" not in v.name and 
				"model_pruning" not in v.name}
		tf.train.init_from_checkpoint(directory, variables_map)
		sess.run(tf.initializers.global_variables())
	else:
		saver = tf.train.Saver()
		saver.restore(sess, latest_checkpoint)
	model_number_regex = re.compile(".*-(\d+)$")
	model_number = re_result = model_number_regex.match(latest_checkpoint).group(1)
	return model_number


def get_const_graphdef(sess):
	g = tf.get_default_graph()
	gd = g.as_graph_def()
	const_graph = tf.graph_util.convert_variables_to_constants(sess, gd, ["resnet_model/final_dense"])
	return const_graph

def get_pruned_graphdef_pb(const_graphdef):
	return tf.contrib.model_pruning.strip_pruning_vars_fn(const_graphdef, ["resnet_model/final_dense"])

def iterate_over_graphdef_pb(graphdef):
	returned_elements = []
	for node in graphdef.node:
		returned_elements.append(node.name)
	nodes = tf.import_graph_def(graphdef, return_elements=returned_elements)
	for node in nodes:
		if isinstance(node, tf.Operation):
			print(node.name + ": " + str(list(node.inputs)) + "\n      " + str(node.outputs))

def main(argv):
	opts = parse_args(argv)
	write_weight_images = "--weight_image_directory" in list(opts.keys())
	init_sparsity = "--init_sparse_vars" in list(opts.keys())
	model = get_model()
	model_op = get_model_op(model)
	with tf.Session() as sess:
		model_number = load_latest_model_in_dir(opts["--model_dir"], sess, init_sparsity)
		gd_pb = get_pruned_graphdef_pb(get_const_graphdef(sess))
		print(write_weight_images)
		if write_weight_images:
			write_weight_images_to_directory(opts["--weight_image_directory"], gd_pb, model_number)
		#iterate_over_graphdef_pb(gd_pb)
		if "--output_graph" in list(opts.keys()):
			with gfile.GFile(opts["--output_graph"], "wb") as f:
				f.write(gd_pb.SerializeToString())
	#finalized_sparse_graph = tf.import_graph_def(new_graph)



if __name__ == "__main__":
	main(sys.argv[1:])