import argparse
import json
import sys
import subprocess
import os
import multiprocessing
import time
import re
from termcolor import colored

parser = argparse.ArgumentParser(description="Reads a config file dumped by a full HPIPE build and runs unit tests on those modules")
parser.add_argument("--config", help="Pass in a json config file path here.", type=str)
parser.add_argument("--threads", help="Limit the number of threads.  Default of -1 means CPUs/2 (we assume SMT of 2 and don't want to leverage it).", type=int, default=-1)
parser.add_argument("--only_indices", help="Comma separated list of test indices to run (e.g. 1,3,21,30)", type=str, default="")
parser.add_argument("--override_parallelism", help="Override the parallelism configuration for input channel parallelism", type=str, default="")
parser.add_argument("--timeout", help="Set a maximum time for the subprocesses to run without one of them finishing.  The default is 240s.", type=int, default=240)
parser.add_argument("--image_count", help="Set the number of images to run through each layer during the test.  The default is 1.", type=str, default="1")
parser.add_argument("--margin", help="Set the error margin.  The tests will fail if the absolute difference of a single value and the tensorflow computed value is greater than 2**<this value>", type=str, default="9")
parser.add_argument("--print_test_numbers", help="Prints the test numbers along with the path to the input weights.", action="store_const", default=False, const=True)
parser.add_argument("--vcs", help="Use VCS as the simulator.", action="store_const", default=False, const=True)
parser.add_argument("--xprop", help="Run VRQ XPROP before running VCS (automatically uses VCS as the simulator since Verilator doesn't support 4-state simulations).", action="store_const", default=False, const=True)
parser.add_argument("--quartus_ip", help="Instantiate Quartus IP.", action="store_true", default=False)
parser.add_argument("--continuous_stdout", help="Instead of printing a summary, print stdout and stderr immediately to stdout and stderr.", action="store_true", default=False)
args = parser.parse_args(sys.argv[1:])
with open(args.config, "r") as fh:
	configs = json.load(fh)

conv_number_re = re.compile(r"_(\d+)")
def configs_sort(e):
	global conv_number_re
	return int(conv_number_re.search(e["aux"]["name"]).group(1))

configs.sort(key=configs_sort)

if args.print_test_numbers:
	for i,c in enumerate(configs):
		name = c["aux"]["name"]
		print(i, ": ", name)
	sys.exit(0)

if args.only_indices == "":
	args.only_indices = list(range(len(configs)))
else:
	args.only_indices = [int(s) for s in args.only_indices.split(",")]

if args.threads == -1:
	args.threads = multiprocessing.cpu_count() // 2
print("Running on " + str(args.threads) + " threads")
if args.threads < 1:
	args.threads = 1


if args.xprop:
	args.vcs = True

thread_status = [None] * args.threads
first_thread_status = [None] * args.threads
stages = [0] * args.threads
number = [0] * args.threads
running_configs = [None] * args.threads
names = [None] * args.threads
time_estimates = [None] * args.threads


env = os.environ
cwd = os.getcwd()
env["PYTHONPATH"] = cwd 
env["CUDA_VISIBLE_DEVICES"]=""

os.chdir("Unit_tests")

progress_counter = 0
success = 0
total = 0
start_number = 0
end = len(args.only_indices)
failing_configs = []
failing_last_lines = []
failing_indices = []
finish_times = []

subprocess_output = {"stdout": subprocess.PIPE, "stderr":subprocess.PIPE}
if args.continuous_stdout:
	subprocess_output = {}

while total < end and progress_counter < args.timeout:
	for i,(t,s,n,c) in enumerate(zip(thread_status, stages, number, running_configs)):
		if t is not None:
			done = t.poll()
			if t.returncode is None:
				continue
			progress_counter = 0
			circuit_path = f"CIRCUIT_PATH={cwd}/Unit_tests/conv_{i}"
			circuit_dir = f"conv_{i}"
			compile_dir = f"sim/verilator/conv_{i}/"
			if args.vcs:
				compile_dir = f"sim/vcs/conv_{i}/"
			if s == 0:
				command = ["make", circuit_path, "CIRCUIT_DIR="+circuit_dir, f"XPROP_DIR=conv_{i}_xprop", "xprop"]
				print(colored("Running " + " ".join(command), "yellow"))
				status = subprocess.run(command, **subprocess_output)
				if status.returncode != 0:
					print(colored("Failed XPROP Build", "red"))
					if not args.continuous_stdout:
						print(status.stdout)
						print(status.stderr)
					total += 1
					thread_status[i] = None
					t = None
					continue
				subprocess.run(["rm", "-rf", f"conv_{i}"], **subprocess_output)
				s = 1
				stages[i] = 1
			if args.xprop:
				circuit_path += "_xprop"
				circuit_dir += "_xprop"
			if s == 1:
				simulator = "verilator_no_wave"
				if args.vcs:
					simulator = "vcs_no_wave"
					compile_dir = f"VCS_COMPILE_DIR=" + compile_dir
				else:
					compile_dir = f"VERILATOR_OUTPUT_DIR=" + compile_dir
				command = ["make", circuit_path, "CIRCUIT_DIR="+circuit_dir, compile_dir, simulator]
				print(colored("Running " + " ".join(command), "yellow"))
				first_thread_status[i] = thread_status[i]
				if args.vcs:
					thread_status[i] = subprocess.Popen(command)#, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				else:
					thread_status[i] = subprocess.Popen(command, **subprocess_output)
				#if args.vcs:
				#	thread_status[i].communicate("q\n")
				running_configs[i] += " && " + " ".join(command) + "; cd .."
				stages[i] = 2
				continue
			success += int(t.returncode == 0)
			if t.returncode != 0:
				failing_indices.append(n)
				failing_configs.append(c)
				print(colored("FAILED: " + c, "red"))
				print(colored("Dumping STDOUT and STDERR:", "red"))
				if not args.continuous_stdout:
					print(first_thread_status[i].stdout.read().decode("utf-8"))
					print(first_thread_status[i].stderr.read().decode("utf-8"))
					run_out = t.stdout.read().decode("utf-8")
					print(run_out)
					run_err = t.stderr.read().decode("utf-8")
					print(run_err)
				failing_last_lines.append("\n".join(run_out.split("\n")[-5:]) + "\n" + "\n".join(run_err.split("\n")[-5:]))
				print(colored("Failed", "red"))
			else:
				if not args.continuous_stdout:
					fail = False
					try:
						output = t.stdout.read().decode("utf-8")
					except:
						print("ERROR: unable to decode output")
						fail = True
						
					if not fail:
						regex = re.compile(r'finished at time (\d+)')
						finish_time = regex.search(output).group(1)
						error = str(int(((100. * float(finish_time) / float(time_estimates[i])) - 100.) * 1000) / 1000.)
						finish_times.append((names[i], finish_time, time_estimates[i], error))
						print(finish_times[-1])
						#Marius: temp fix until estimator is updated for ConvWrapper and depthwiseconv (a couple of new Conv2D tests are also failing)
						if "conv_wrapper" not in names[i] and float(error) > 5:
							success -= 1
							failing_indices.append(n)
							failing_configs.append(c)
							print(colored(r"Failed because time estimate off by more than 5%. Command was:", "red"), "\n", running_configs[i])
							
						else:
							print(colored("Passed", "green"))
			subprocess.run(["rm", "-rf", circuit_dir], **subprocess_output)
			subprocess.run(["rm", "-rf", compile_dir], **subprocess_output)
			total += 1
			thread_status[i] = None
			t = None
			print(colored(f"Passed {success}/{total}", "green"))
		if t is None and start_number < end:
			config = configs[args.only_indices[start_number]]
			aux = config["aux"]
			config = config["command"]
			time_estimates[i] = aux["time_estimate"]
			if args.override_parallelism != "":
				try:
					index = config.index("--ic_parallelism")
					config[index + 1] = args.override_parallelism
				except ValueError:
					pass
			if args.vcs:
				config.append("--vcs")
			if args.quartus_ip:
				config.append("--instantiate_quartus_ip")
			index = config.index("-i")
			name = aux["name"]#re.search(r'/(basic_conv.*)_input.npy', config[index+1]).group(1)
			names[i] = name
			stages[i] = 1
			if args.xprop:
				stages[i] = 0
			number[i] = args.only_indices[start_number]
			output_csv_path = cwd + "/generated_files/layer_images/verilog/" + name + ".csv"
			command = ["python3", "HPIPE_Unit_Test.py",
				"--dir", f"conv_{i}",
				"--margin", args.margin,
				"--output_csv_path", output_csv_path,
				"--image_count", args.image_count] + config
			thread_status[i] = subprocess.Popen(command, env=env, **subprocess_output)
			running_configs[i] = "cd Unit_tests && " + " ".join(command)
			print(colored("Running " + running_configs[i], "yellow"))
			start_number += 1

	progress_counter += 1
	time.sleep(1)
if len(failing_configs) > 0:
	print(colored("Failing Configs:", "red"))
	for c,ll in zip(failing_configs, failing_last_lines):
		print(colored("   " + c, "red"))
		print(ll)
print(colored(f"Passed {success}/{total}", "green"))
if total < end:
	print(colored("Failed due to timeout", "red"))
	print("Should have run ", end, " configs.")
	print([s.returncode for s in thread_status if s is not None])
	for c,t in zip(running_configs, thread_status):
		if t is not None:
			print(c)
			if not args.continuous_stdout:
				print(t.stdout.read().decode("utf-8"))
				print(t.stderr.read().decode("utf-8"))

if len(failing_configs) > 0:
	print(colored("Failed configs: " + ",".join([str(i) for i in failing_indices]), "red"))


conv_number_re = re.compile(r"_(\d+)")
def f_time_sort(e):
	global conv_number_re
	return int(conv_number_re.search(e[0]).group(1))

if len(finish_times) > 0:
	finish_times.sort(key=f_time_sort)
	print("Dumping actual finish times compared to expected (from planner) layer latencies:")
	for (n,t,e,err) in finish_times:
		print(n, ": ", t, ", Estimate: ", e, ", Pct. Err.: ", err)

if (len(failing_configs) > 0) or (total < end):
	sys.exit(1)


