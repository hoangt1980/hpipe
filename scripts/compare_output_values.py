#This is just a quick script that goes through all the verilator csv files and compares the absolute difference between the values 
import os.path
import sys
import math
import numpy as np
import argparse


parser = argparse.ArgumentParser(description='Compares the tensorflow and verilog results by calculating the absolute difference for each layer')
parser.add_argument('--tensorflow_output_path', '-t', required=True, dest='tensorflow_output_path',
                   type=str,
                   help="The path of the tensorflow output csv files for the script to use")
parser.add_argument('--verilog_output_path', '-v', required=True, dest='verilog_output_path',
                   type=str,
                   help="The path of the Verilog simulation output csv files for the script to use")

#For now we do the first image only (as that is what the example outputs return)
final_line = sys.maxsize

def convert_csv_to_array(file_handle):
	global final_line
	
	t_file_values = []
	first_line = True
	cur_line = 0
	for line in file_handle:
		#Skip first line since it is the size
		if first_line:
			first_line = False
			continue
		elif cur_line >= final_line:
			break
		line = line.split(",")
		values = [int(num) for num in line]
		t_file_values.append(values)
		cur_line += 1
		
	final_line = cur_line
	return t_file_values

args = parser.parse_args()

if not os.path.isdir(args.tensorflow_output_path):
	print("ERROR: tensorflow_output_path is not valid!")

if not os.path.isdir(args.verilog_output_path):
	print("ERROR: tensorflow_output_path is not valid!")

onlyfiles = [f for f in os.listdir(args.tensorflow_output_path) if os.path.isfile(os.path.join(args.tensorflow_output_path, f))]
onlyfiles.sort()

#List of results where each element is (filename, average_diff, highest_diff, highest_diff_tf_value, highest_diff_verilog_value)
results = []


for filename in onlyfiles:
	t_file = open(args.tensorflow_output_path + "/" + filename, "r")
	v_file = open(args.verilog_output_path + "/" + filename, "r")
	
	t_file_values =  np.array(convert_csv_to_array(t_file))
	v_file_values =  np.array(convert_csv_to_array(v_file))
	
	try:
		difference_array = v_file_values -  t_file_values 
		difference_array = np.abs(difference_array)
		max_diff = np.amax(difference_array)
		max_diff_index =np.unravel_index(difference_array.argmax(), difference_array.shape)
		tf_max_diff_val = t_file_values[max_diff_index]
		verilog_max_diff_val = v_file_values[max_diff_index]
		average_diff = np.mean(difference_array)

		difference_array_copy = difference_array.astype(float)
		difference_array_copy[difference_array_copy == 0] = np.nan
		average_nonzero_diff = np.nanmean(difference_array_copy)

		results.append((filename, average_diff, max_diff, tf_max_diff_val, verilog_max_diff_val, max_diff_index))
	except:
		#This error is likely due to output channel parallelism messing up the Tensorflow vs Verilog output sizes for the basic_conv modules instantiated by the conv wrapper
		print("ERROR: could not process ", filename, " skipping file...")
	
	
	t_file.close()
	v_file.close()

print("(filename, average_diff, highest_diff, highest_diff_tf_value, highest_diff_verilog_value, max_diff_index)")
for row in results:
	print(row)


