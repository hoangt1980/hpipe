import subprocess
import os
import numpy as np
import time

env = os.environ
cwd = os.path.dirname(os.path.realpath(__file__)) + "/.."

class FakeProc():
	def __init__(self):
		self.returncode = 0

	def poll(self):
		return

dev_null = open("/dev/null", "w")

dsp_targets = [2500, 5000, 7500, 10000]
sparsity_values = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.75, 0.8, 0.85, 0.9]
for dsp_target in dsp_targets:
	progress_tracker = []
	processes = []
	done = [False for _ in sparsity_values]
	fhs = [None for _ in sparsity_values]


	for sparsity_value in sparsity_values:
		progress_tracker.append({"done_hpipe" : False, "done_verilator_build" : False, "done_verilator": False})

		circuit_path = f"generated_files/circuits/resnet_{dsp_target}_{sparsity_value}"
		hpipe_cmd = f"python3 -m hpipe.LayerComponents --param_file quartus_params.json --generated_circuit_path {circuit_path} --dsp_target {dsp_target} --make_fake_weights --fake_weights_sparsity {sparsity_value}".split(" ")
		print(hpipe_cmd)
		#processes.append(FakeProc())
		processes.append(subprocess.Popen(hpipe_cmd, stderr=dev_null, stdout=dev_null, env=env, cwd=cwd))

	while not np.all(done):
		for i,(p,s,progress) in enumerate(zip(processes, sparsity_values, progress_tracker)):
			if done[i]:
				continue
			p.poll()
			if p.returncode is not None:
				make_options = f"VERILATOR_DEFINES= VERILATOR_MAIN=quartus_component_verilator_top.cpp VERILATOR_THREADS=1 CIRCUIT_DIR=resnet_{dsp_target}_{s} VERILATOR_OUTPUT_DIR=sim/verilator/{s}/compile TOP=hpipe_0"
				if not progress["done_hpipe"]:
					progress["done_hpipe"] = True
					print(f"Starting Verilator Build for Sparsity {s}")
					cmd = f"make {make_options} verilator".split(" ")
					print(cmd)
					processes[i] = subprocess.Popen(cmd, env=env, cwd=cwd)
				elif not progress["done_verilator_build"]:
					progress["done_verilator_build"] = True
					print(f"Starting Verilator Run for Sparsity {s}")
					fhs[i] = open(f"{cwd}/verilator_sparsity_sweep/{dsp_target}/{s}", "w")
					processes[i] = subprocess.Popen(f"make {make_options} verilator_run".split(" "), stdout=fhs[i], env=env, cwd=cwd)
				else:
					progress["done_verilator"] = True
					done[i] = True
					fhs[i].close()
					fhs[i] = None
		time.sleep(60)

