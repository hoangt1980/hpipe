# This script converts a png image to a binary input file that can be used for verilator
# It has only been tested with images of size 224x224

import struct
import math
import os
import numpy as np
from PIL import Image
import argparse

# Set to None to skip resizing by default
RESIZE = None

parser = argparse.ArgumentParser(description='Convert a png to a bin input')
parser.add_argument('--input_png', required=True, dest='input_png',
                   type=str,
                   help='Specify the path of the input png image or directory')
parser.add_argument('--output_bin', required=True, dest='output_bin',
                   type=str,
                   help='Desired output bin path')
parser.add_argument('--interface_width', dest='interface_width', type=int,
        default=256, help='Interface width of input in bits (default=256)')
parser.add_argument('-s', '--output_image_size', dest='resize', type=int,
		help='Specify the final dimensions of image. Use 0 to keep the same dimensions. Currently only square sizes are supported, 224x224 is a common size', default=0)
parser.add_argument('--enable_crop', dest='enable_crop', action='store_true',
        help='Resizes only 1 image dimension and the crops the other. Reccomended for images where 1 dimension is much bigger than the other.')


args = parser.parse_args()

enable_crop = args.enable_crop
if args.resize != 0: RESIZE = args.resize

multiple_images = False
if os.path.isdir(args.input_png):  
	multiple_images = True

interface_width = args.interface_width	
bits_per_pixel = 8
pixels_per_transfer = interface_width // bits_per_pixel
	
if not multiple_images:
	#Open image and convert to 8-bit unsigned int
	image = Image.open(args.input_png)
	
	original_width, original_height = image.size
	if RESIZE != None:
		if enable_crop == False or original_width == original_height: image = image.resize((RESIZE, RESIZE), resample=Image.LANCZOS)
		else:
			#Resize so that smallest dimension is 224
			ratio = float(RESIZE)/float(min(original_width, original_height))
			new_width = int(original_width * ratio)
			new_height = int(original_height * ratio)
			if new_width < new_height and new_width != RESIZE: new_width = RESIZE
			elif new_height < new_width and new_height != RESIZE: new_height = RESIZE
			image = image.resize((new_width, new_height), resample=Image.LANCZOS)
				
			#Crop from sides so that final size is 224x224
			if new_width < new_height:
				size_difference = new_height - RESIZE
				starting_pixel = math.floor(size_difference/2.0)
				ending_pixel = new_height - starting_pixel
				if (size_difference % 2) != 0: starting_pixel += 1
				image = image.crop((0, starting_pixel, new_width, ending_pixel)) # left, top, right, bottom	
			else:
				size_difference = new_width - RESIZE
				starting_pixel = math.floor(size_difference/2.0)
				ending_pixel = new_width - starting_pixel
				if (size_difference % 2) != 0: starting_pixel += 1
				image = image.crop((starting_pixel, 0, ending_pixel, new_height)) # left, top, right, bottom		
					
			new_width, new_height = image.size
			if new_width != RESIZE or new_height != RESIZE: 
				print("ERROR: Image resized incorrectly from (" + str(original_width) + ', ' + str(original_height) + ") to (" + str(new_width) + ', ' + str(new_height) + ')')
				exit(1)
						
	#Convert to RGB if needed
	if(len(image.getbands()) != 3): image = image.convert('RGB')

	image = np.array(image,  dtype=np.uint8)

	#Variable definitions for determining number of pixels per transfer
	image_width = image.shape[0]
	image_height = image_width
	transfers_per_w = math.ceil(image_width / float(pixels_per_transfer))

	with open(args.output_bin, "wb") as fh:
		for row in range(image_width):
			for channel in range(image.shape[2]):
				cur_row = image[row,:,channel]
				#Order needs to be reversed for each transfer
				for transfer_num in range(transfers_per_w):
					start_index = transfer_num*pixels_per_transfer 
					end_index = transfer_num*pixels_per_transfer + pixels_per_transfer
					cur_pixels = np.flip(cur_row[start_index:end_index])
					cur_struct = struct.pack('>' + str(len(cur_pixels)) +'B', *cur_pixels)
					fh.write(cur_struct)
else:
	#Go through directory and convert multiple images
	with open(args.output_bin, "wb") as fh:
		for cur_image in sorted(os.listdir(args.input_png)):
			if not (cur_image.endswith(".png") or cur_image.endswith(".jpg") or cur_image.endswith(".JPEG")): continue
				
			print("Processing:", cur_image)

			image = Image.open(args.input_png + cur_image)
			original_width, original_height = image.size
			if RESIZE != None:
				if enable_crop == False or original_width == original_height: image = image.resize((RESIZE, RESIZE), resample=Image.LANCZOS)
				else:
					#Resize so that smallest dimension is 224
					ratio = float(RESIZE)/float(min(original_width, original_height))
					new_width = int(original_width * ratio)
					new_height = int(original_height * ratio)
					if new_width < new_height and new_width != RESIZE: new_width = RESIZE
					elif new_height < new_width and new_height != RESIZE: new_height = RESIZE
					image = image.resize((new_width, new_height), resample=Image.LANCZOS)
					
					#Crop from sides so that final size is 224x224
					if new_width < new_height:
						size_difference = new_height - RESIZE
						starting_pixel = math.floor(size_difference/2.0)
						ending_pixel = new_height - starting_pixel
						if (size_difference % 2) != 0: starting_pixel += 1
						image = image.crop((0, starting_pixel, new_width, ending_pixel)) # left, top, right, bottom	
					else:
						size_difference = new_width - RESIZE
						starting_pixel = math.floor(size_difference/2.0)
						ending_pixel = new_width - starting_pixel
						if (size_difference % 2) != 0: starting_pixel += 1
						image = image.crop((starting_pixel, 0, ending_pixel, new_height)) # left, top, right, bottom		
						
					new_width, new_height = image.size
					if new_width != RESIZE or new_height != RESIZE: 
						print("ERROR: Image resized incorrectly from (" + str(original_width) + ', ' + str(original_height) + ") to (" + str(new_width) + ', ' + str(new_height) + ')')
						exit(1)

			#Convert to RGB if needed
			if(len(image.getbands()) != 3): image = image.convert('RGB')

			image = np.array(image,  dtype=np.uint8)

			#Variable definitions for determining number of pixels per transfer
			image_width = image.shape[0]
			image_height = image_width
			transfers_per_w = math.ceil(image_width / float(pixels_per_transfer))
			
			for row in range(image_width):
				for channel in range(image.shape[2]):
					cur_row = image[row,:,channel]
					#Order needs to be reversed for each transfer
					for transfer_num in range(transfers_per_w):
						start_index = transfer_num*pixels_per_transfer 
						end_index = transfer_num*pixels_per_transfer + pixels_per_transfer
						cur_pixels = np.flip(cur_row[start_index:end_index])
						cur_struct = struct.pack('>' + str(len(cur_pixels)) +'B', *cur_pixels)
						fh.write(cur_struct)
		
		
			

			

