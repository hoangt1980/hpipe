# Creates a val_map.txt file for use with MLperf
# MLperf expects the images to have an associated file with <image_name> <output_class> on each line
import os
import argparse


output_file = "val_map.txt"

parser = argparse.ArgumentParser(description='Creates a val_map.txt file in current dir for use with MLperf. MLperf expects the images to have an associated file with <image_name> <output_class> on each line')
parser.add_argument('--output_classes', required=True, dest='class_file',
                   type=str,
                   help='Validation ground truth file. One output class on each line in the order of the images')
parser.add_argument('--image_folder', required=True,  dest='image_folder',
                   type=str,
                   help='The name of the folder which the actual images are in. Should be a subdirectory in regards to where the script is running. val_map.txt will be generated in regards to this path')
args = parser.parse_args()



output_classes = []
with open(args.class_file, 'r') as in_file:
	for line in in_file:
		output_classes.append(int(line))
		
image_folder = 	args.image_folder
if image_folder[-1] != '/': image_folder = image_folder + '/'

with open(output_file, "w") as out_file:
		i = 0;
		for cur_image in sorted(os.listdir(image_folder)):
			out_file.write(image_folder + cur_image + ' ' + str(output_classes[i]) + '\n')
			i += 1