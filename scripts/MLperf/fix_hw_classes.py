# Creates a mapping file to map Hw classes to classes from the actual dataset which MLperf uses
# This can either do the mapping directly if the original label file exists or it can attempt to infer the mapping using the output predictions from HW

#NOTE: this script has not been tested for anything other than Imagenet and will likely need modifications depending on the format of the input files

import operator
import argparse

number_images = 50000
number_classes = 1000

output_file = "HW_to_ImageNet_mapping.txt"

parser = argparse.ArgumentParser(description='Creates a mapping file to map Hw classes to classes from the actual dataset which MLperf uses. This can either do the mapping directly if the original label file exists or it can attempt to infer the mapping using the output predictions from HW')
parser.add_argument('--hw_label_file', required=True, dest='hw_label_file',
                   type=str,
                   help="The hardware file with the string labels. Assumes JSON format with  <class_id>: '<string_label>'")
parser.add_argument('--actual_label_file', required=True,  dest='actual_label_file',
                   type=str,
                   help='The actual label file. For Imagenet this file does not map directly to the class id but rather a different type of label. For example: n07717556	butternut squash')
parser.add_argument('--actual_labels_mapping', required=True,  dest='actual_labels_mapping',
                   type=str,
                   help='File that maps the actual_label_file mapping (e.g. n07717556) to an actual class id. For example: 829 n03775546')
parser.add_argument('--no_label_files', dest='no_label_files', action='store_true',
        help='Attempts to infer the mapping without the original label files. IMPORTANT: make sure you specify hardware_mapping_file and correct_mapping_file when using this option')
parser.add_argument('--hardware_mapping_file', dest='hardware_mapping_file',
                   type=str, default="hw_output_classes.txt",
                   help='The hardware output classes with <image_id> <class_id> on each line')
parser.add_argument('--correct_mapping_file',   dest='correct_mapping_file',
                   type=str, default="ILSVRC2012_validation_ground_truth.txt",
                   help='The correct_mapping_file to be used in conjuction with the hardware mapping file. Should just have 1 class on each line, image ID is assumed from line number')


args = parser.parse_args()


use_label_files = not(args.no_label_files)

if use_label_files:
	#If label files exist, use those to translate the classes
	hw_label_file = args.hw_label_file
	actual_label_file = args.actual_label_file
	actual_labels_mapping = args.actual_labels_mapping
	
	hw_labels = [0]
	with open(hw_label_file, 'r') as fh:
		for line in fh:
			line = line.replace("{", "").replace("}", "")
			line = line.split(':')[1]
			if '"' in line: line = line.split('"')[1]
			else: line = line.split("'")[1]
			#print(line)
			hw_labels.append(line)
			
	actual_labels_dict= {}
	with open(actual_label_file, 'r') as fh:
		for line in fh:
			line = line.split("	")
			line[1] = line[1].replace(" \n", "")
			actual_labels_dict[line[0]] = line[1]
			#print(line)
			
	actual_labels = [0]
	with open(actual_labels_mapping, 'r') as fh:
		for line in fh:
			line = line.split(" ")
			if int(line[0]) > number_classes: break
			line = line[1].replace("\n", "")
			actual_labels.append(actual_labels_dict[line])

	with open(output_file, 'w') as outfile:
		for i in range(1, number_classes+1):
			hw_class_label = hw_labels[i]
			matching_class = actual_labels.index(hw_class_label)
			outfile.write(str(i) + ' ' + str(matching_class) + '\n')
			#print(hw_class_label, matching_class)
	
	
else:
	hardware_mapping_file = args.hardware_mapping_file
	correct_mapping_file = args.correct_mapping_file

	hardware_mapping = [None] * number_images
	correct_mapping = []

	with open(correct_mapping_file, 'r') as fh:
		for line in fh: 
			correct_mapping.append(int(line))

	with open(hardware_mapping_file, 'r') as fh:
		for line in fh: 
			line = line.split(' ') 
			if int(line[0]) == 2: print(line[1])
			hardware_mapping[int(line[0])] = int(line[1])

	# Imagenet class is index, value is a dictionary of matching hw classes and # of occurences 
	mapping_dict = []
	for i in range(0, number_classes+1):
		mapping_dict.append({})

	# Match HW class with imagenet classes
	for i in range(0, number_images):
		hw_class = hardware_mapping[i]
		correct_class = correct_mapping[i]
		if hw_class in mapping_dict[correct_class]:
			mapping_dict[correct_class][hw_class] += 1
		else:
			mapping_dict[correct_class][hw_class] = 1

	#Create Imagenet -> HW class mapping
	imagenet_to_hw = [0]
	for i in range(1, number_classes+1):
		cur_dict = mapping_dict[i]
		#print(cur_dict)
		best_match, best_sum = max(cur_dict.items(), key=operator.itemgetter(1))
		imagenet_to_hw.append(best_match)

	#Check to make sure there are no duplicate mappings
	if len(imagenet_to_hw) != len(set(imagenet_to_hw)):
		print("WARNING: " + str(len(imagenet_to_hw) - len(set(imagenet_to_hw))) + " duplicates found in list")

	# Create HW - > Imagenet mapping
	hw_to_imagenet = [None] * (number_classes + 1)
	for i in range(1, number_classes+1):
		hw_to_imagenet[imagenet_to_hw[i]] = i


	#Write result to file
	with open(output_file, 'w') as outfile:
		for i in range(1, number_classes+1):
			if hw_to_imagenet[i] == None:  hw_to_imagenet[i] = 1 #Set random class if this happens
			outfile.write(str(i) + ' ' + str(hw_to_imagenet[i]) + '\n')