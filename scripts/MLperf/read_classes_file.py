#Converts a classes.bin file to a classes.txt file

with open("classes.txt", 'w') as outfile:
	with open("classes.bin", "rb") as f:
		byte = f.read(2)
		while byte:
			# Do stuff with byte.
			#print(byte,  int.from_bytes(byte, "little"))
			outfile.write(str(int.from_bytes(byte, "little")) +'\n')
			byte = f.read(2)