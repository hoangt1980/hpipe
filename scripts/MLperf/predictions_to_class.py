#Converts a tensorflow predictions file to just a text file that has the output classes

import operator

input_file_name = "tensorflow_predictions.csv"
output_file_name = "tensorflow_prediction_classes.txt"

with open(input_file_name, "r") as in_file:
	with open(output_file_name, 'w') as out_file:
		for line in in_file:
			line = line.split(',')
			line = [float(num) for num in line]
			max_index, max_value = max(enumerate(line), key=operator.itemgetter(1))
			out_file.write(str(max_index)+'\n')
			