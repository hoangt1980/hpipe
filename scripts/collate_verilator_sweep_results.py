import os
throughput_structure = {}
latency_structure = {}
throughput_structure_headings = []
structure = {"heading" : []}
name_order = []
have_name_order = False
d = "verilator_sparsity_sweep"
for dsp_target in os.walk(d):
	if dsp_target[0] == d:
		continue
	throughput_structure_headings.append(dsp_target[0].split("/")[-1])
	for sparsity in dsp_target[2]:
		if sparsity not in throughput_structure:
			throughput_structure[sparsity] = []
			latency_structure[sparsity] = []
		structure["heading"].append(dsp_target[0].split("/")[-1] + "_" + sparsity)
		with open(dsp_target[0] + "/" + sparsity, "r") as f:
			finished_lines = [l.split(" ") for l in f if "finished at time" in l]
			if not have_name_order:
				have_name_order = True
				for fl in finished_lines:
					if fl[0] not in structure and "conv" in fl[0]:
						structure[fl[0]] = []
						name_order.append(fl[0])
			local_struct = {k : [] for k in name_order}
			bias_add_49s = []
			for fl in finished_lines:
				if "conv" in fl[0]:
					local_struct[fl[0]].append(fl[4])
				if "basic_conv_0" in fl[0]:
					bias_add_49s.append(fl[4])
			throughput_structure[sparsity].append(str(1000000000./(int(bias_add_49s[1]) - int(bias_add_49s[0]))))
			latency_structure[sparsity].append(bias_add_49s[0])
			#for k,v in local_struct.items():
			#	if "conv" in k:
			#		structure[k].append(str(int(v[1]) - int(v[0])))

#name_order.insert(0, "heading")
#for n in name_order:
#	print(",".join([n] + structure[n]))
print(",".join(["sparsity"] + throughput_structure_headings))
sparsity_values = [float(k) for k in throughput_structure.keys()]
sparsity_values.sort()
sparsity_values = [str(s) for s in sparsity_values]
for sparsity in sparsity_values:
	#print(",".join([sparsity] + latency_structure[sparsity]))
	print(",".join([sparsity] + throughput_structure[sparsity]))