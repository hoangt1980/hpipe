import numpy as np
import seaborn as sns
import sys
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import os
import re

def get_layer_labels(l, arr):
	indices = np.arange(arr.shape[0])
	tiled = np.tile(indices, (np.prod(arr.shape[1:]), 1))
	transposed = np.transpose(tiled)
	return transposed.flatten()

def get_generator_labels(arr, name):
	return np.repeat(name, np.prod(arr.shape))

def get_violinplot_data(arr_dict):
	data = {
		"Generator": np.empty([0], dtype=np.dtype("<U5")),
		"Layer": np.empty([0], dtype=np.dtype("int64")),
		"Values": np.empty([0], dtype=np.dtype("int"))}
	sorted_layers_of_interest = list(arr_dict["Accelerator"].keys())
	sorted_layers_of_interest.sort()
	for k,a in arr_dict.items():
		for l in sorted_layers_of_interest:
			v = a[l]
			data["Values"] = np.concatenate([data["Values"], v.flatten().astype(int)])
			data["Layer"] = np.concatenate([
				data["Layer"],
				get_generator_labels(v, l)])
			data["Generator"] = np.concatenate([
				data["Generator"], 
				get_generator_labels(v, k)])
	return data

def check_args():
	if len(sys.argv) < 3:
		print("Usage:")
		print("  " + sys.argv[0] + " <accelerator_accum_output_dir> <tensorflow_accum_output_dir> [output_path]")
		print("  E.g. python compare_accum_distributions.py /home/mat/courses/first_year/second_semester/arch/scnn/tests/ conv_scnn_output/activations/")
		sys.exit(0)

def load_tf_acts(path):
	tf = np.load(path)
	tf = tf[0]
	tf = tf.transpose([2,0,1])
	return tf

def main():
	check_args()
	pattern = re.compile('l([0-9]+)_accum_out.npy')
	tf_pattern = re.compile('([0-9]+).npy')
	a_files = os.listdir(sys.argv[1])
	a_np = {}
	for f in a_files:
		m = pattern.match(f)
		if m:
			a_np[int(m.group(1))] = np.load(sys.argv[1] + f)
	tf_files = os.listdir(sys.argv[2])
	tf_np = {}
	for f in tf_files:
		m = tf_pattern.match(f)
		if m:
			tf_np[int(m.group(1))] = load_tf_acts(sys.argv[2] + f)
	vplot_data = get_violinplot_data({
		"Accelerator": a_np,
		"TensorFlow": tf_np})
	ax = sns.violinplot(data=vplot_data, hue="Generator", 
		x="Layer", y="Values", split=False, inner="quartile")
	if len(sys.argv) > 3:
		fig = plt.gcf()
		fig.set_size_inches(8.5,11)
		fig.savefig(sys.argv[3], bbox_inches="tight")
	else:
		plt.show()


if __name__ == "__main__":
    main()

