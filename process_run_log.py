import numpy as np
import operator
from pprint import pprint
with open("run_log.log", "r") as fh:
	lines = [l for l in fh]

times = {}
for l in lines:
	components = l.split(" ")
	if len(components) == 5:
		if components[0] not in times.keys():
			times[components[0]] = []
		times[components[0]].append(int(components[-1]))

for k,v in times.items():
	times[k] = np.diff(v)

outlier_indices = {}
for k,v in times.items():
	outlier_indices[k] = np.argmax(np.diff(v))

sorted_outliers = sorted(outlier_indices.items(), key=operator.itemgetter(1))
for (k,i) in sorted_outliers:
	print((k, i, *[t for t in times[k][:i+1]]))
