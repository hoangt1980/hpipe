import numpy as np
import sys
from IABuffer import IABuffer, IABufferModel

def main():
	np.random.seed(0)
	max_cycles = 3000000
	requesters = 1
	ia_shape = (1,224,224,3)
	kernel_shape = (7,7,3,64)
	stride = 2
	padding = "same"
	p_request = 1
	request_pattern_length = 100
	n_buffering = 1 # only 1 supported at the moment


	ic_orders = np.random.permutation(ia_shape[-1])
	ics_per_requester = ia_shape[-1] // requesters
	ics_for_requesters = [ics_per_requester for _ in range(requesters)]
	extra_ics = ia_shape[-1] - ics_per_requester * requesters
	for i in range(extra_ics):
		ics_per_requester[i] += 1
	ic_orders_by_requester = []
	ic_index = 0
	for ics in ics_for_requesters:
		ic_orders_by_requester.append(ic_orders[ic_index:ic_index+ics])
		ic_index += ics

	cycle_count = 0
	request_pattern = np.reshape(np.random.binomial(1, p_request, request_pattern_length * requesters), [request_pattern_length, requesters])
	pending_requests = np.array([False for _ in range(requesters)])
	requester_indices = np.array([0 for _ in range(requesters)])
	requester_end_indices = np.array([o.shape[-1] for o in ic_orders_by_requester])
	done = lambda: requester_indices == requester_end_indices
	all_done = lambda: done().all()
	requests = lambda g: ((np.invert(g)) and pending_requests) or (request_pattern[(cycle_count % request_pattern_length)] and np.invert(done()))

	ia = (np.random.normal(0., 1., ia_shape) * 20).astype(int)
	ia_buffer = IABuffer(ia_shape, kernel_shape, padding, stride, requesters, n_buffering)
	golden_oa = np.zeros(ia_buffer.output_spatial_shape)
	computed_oa = np.zeros(ia_buffer.output_spatial_shape)
	padded_ia = np.pad(ia, ia_buffer.padding, "constant", constant_values=0)
	for i in range(golden_oa.shape[0]):
		for j in range(golden_oa.shape[1]):
			golden_oa[i,j] = np.sum(padded_ia[0,i*stride:i*stride+kernel_shape[0],j*stride:j*stride+kernel_shape[1],:])
	buffer_model = IABufferModel(ia_buffer)

	oa_idx = 0
	oy = lambda: oa_idx // golden_oa.shape[0]
	ox = lambda: oa_idx % golden_oa.shape[1]

	ia_idx = 0
	iy = lambda: ia_idx // ia_shape[1]
	ix = lambda: ia_idx % ia_shape[2]

	end_oa_idx = np.prod(golden_oa.shape)
	end_ia_idx = np.prod(ia_shape[1:3]) - 1
	kernel_collection = np.zeros(np.prod(kernel_shape[:3]))
	finished = False
	while cycle_count < max_cycles and oa_idx != end_oa_idx:
		channel_indices = []
		d = done()
		for i,cs in enumerate(ic_orders_by_requester):
			if d[i]:
				channel_indices.append(0)
				continue
			channel_indices.append(cs[requester_indices[i]])
		data = ia[0,iy(),ix(),channel_indices]
		outputs = buffer_model.step(pending_requests, data, channel_indices, all_done() and not finished, [0 for _ in range(requesters)], False)
		if all_done():
			if ia_idx != (end_ia_idx):
				ia_idx += 1
				requester_indices.fill(0)
			else:
				finished = True

		if outputs["valid"]:
			kernel_collection[outputs["addr"]] = outputs["data"]
			computed_oa[oy(),ox()] += outputs["data"]

		gnt_indices = np.nonzero(outputs["gnts"])
		requester_indices[gnt_indices] += 1
		pending_requests = requests(outputs["gnts"])

		if outputs["increment_write_addr"]:
			if computed_oa[oy(),ox()] != golden_oa[oy(),ox()]:
				print(oy())
				print(ox())
				print(computed_oa[oy(),ox()])
				print(golden_oa[oy(),ox()])
				print(padded_ia[0,oy():oy()+kernel_shape[0],ox():ox()+kernel_shape[1],:])
				print(np.reshape(kernel_collection, kernel_shape[:3]))

				print()
				print(computed_oa)
				print(golden_oa)
				print((computed_oa == golden_oa))
				assert(computed_oa[oy(),ox()] == golden_oa[oy(),ox()])
			oa_idx += 1
		cycle_count += 1
	if cycle_count >= max_cycles:
		print("Reached max cycle count")
		print("ia_idx: " + str(ia_idx))
		print("oa_idx: " + str(oa_idx))
		sys.exit(-1)
	print(computed_oa)
	print(golden_oa)
	print((computed_oa == golden_oa))
	assert((computed_oa == golden_oa).all())

if __name__ == "__main__":
	main()