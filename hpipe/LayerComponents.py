import warnings
import math
import os
import sys
import json
import argparse
import pprint
import numpy as np
import digitallogic as dl
from pathlib import Path
from functools import reduce
from digitallogic.utils import clog2, get_padding, print_stage_header, v_comment_string, get_quantized, BitArray, cast_fixed, is_power_of_2
from hpipe.build_accelerator import get_plan
from bashplotlib.histogram import plot_hist
from hpipe.GraphBuilder import Node
import hpipe.ArchLoader as arch

modules_to_debug = []

class NNStageInput(dl.Module):
	def __init__(self, node, input_index, **kwargs):
		super(NNStageInput, self).__init__("nn_stage_input", kind="nn_stage_input", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):

			self.act_indices = {
				"batch"    : 0,
				"height"   : 1,
				"width"    : 2,
				"channels" : 3
			}

			self.input_index = input_index
			self.node = node
			self.ia_shape = node.inputs[input_index]._from.example_outputs[0].shape

			self.set_bit_widths(node)
			parallel_acts_per_ram = node.inputs[input_index]._from.get_parallel_acts_per_memory()
			input_width = self.get_input_act(dim="width")
			combined_count = math.ceil(float(input_width) / parallel_acts_per_ram)

			bits = self.bits
			signed = self.signed

			self.inputs = [inst(dl.Logic,
				bits,
				name="input_" + str(i),
				signed=signed) for i in range(input_width)]
			self.combined_inputs = []
			self.combined_writes = []
			self.writes = [inst(dl.Logic, 1, name="write_" + str(i)) for i in range(input_width)]
			for i in range(combined_count):
				lo = i * parallel_acts_per_ram
				hi = min(lo + parallel_acts_per_ram, combined_count*parallel_acts_per_ram)
				self.combined_inputs.append(inst(dl.Concat, *self.inputs[lo:hi], name="combined_input_" + str(i)))
				self.combined_writes.append(self.writes[lo])

			self.write = inst(dl.Logic, 1, name="write")
			self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")


	def set_bit_widths(self, node):
		a_precision_params = node.inputs[self.input_index]._from.precision_parameters
		bits = np.sum(np.array([int(a_precision_params[b]) for b in
			"a_sign_bits,a_exponent,a_int,a_f".split(",")]))
		signed = a_precision_params["a_sign_bits"] == 1

		self.signed          = signed
		self.exponent_bits   = a_precision_params["a_exponent"]
		self.int_bits        = a_precision_params["a_int"]
		self.fractional_bits = a_precision_params["a_f"]
		self.bits            = bits

	def get_input_act(self, dim=None):
		if dim is None:
			return self.node.inputs[self.input_index]._from.example_outputs[0]
		assert(dim in self.act_indices)
		return self.ia_shape[self.act_indices[dim]]

class NNStage(dl.Module):
	kind =  "nn_stage"
	def __init__(self, node, build_data_path=True, output_csv_path=None, depthwise_conv_parallelism=1, **kwargs):
		super(NNStage, self).__init__(self.__class__.kind, kind=self.__class__.kind, **kwargs)
		inst = self.inst
		self.output_csv_path = output_csv_path
		with dl.ModuleScope(self):
			print("Instantiating " + self.__class__.kind + " for tf op " + node.tf_op.name)

			if "clock" in kwargs:
				self.clock = kwargs["clock"]
			self.node = node
			self.act_indices = {
				"batch"    : 0,
				"height"   : 1,
				"width"    : 2,
				"channels" : 3
			}
			self.input_modules = [inst(NNStageInput, node, i) for i in range(len(node.inputs))]
			if len(self.input_modules) == 1:
				self.inputs = self.input_modules[0].inputs
				self.write = self.input_modules[0].write
				self.combined_inputs = self.input_modules[0].combined_inputs
				self.writes = self.input_modules[0].writes
				#self.writes[0].copy()
				self.combined_writes = self.input_modules[0].combined_writes
				self.space_to_write_line = self.input_modules[0].space_to_write_line
			self.set_act_shapes(node)
			
			#For depthwise convs we need to adjust the output channels since each conv module will only compute a certain subset 
			if depthwise_conv_parallelism > 1:
				list_oa_shape = list(self.oa_shape)
				list_oa_shape[self.act_indices["channels"]] //= depthwise_conv_parallelism
				self.oa_shape = tuple(list_oa_shape)

							
			self.set_bit_widths(node)
			output_width = self.get_output_act(dim="width")

			self.can_write_line = inst(dl.Logic, 1, name="can_write_line")
			self.output_valid = inst(dl.Logic, 1, name="control_output_valid")
			self.outputs_valid = [inst(dl.Logic, 1, name="output_valid_" + str(i)) for i in range(output_width)]

			_, _, output_bits = self.get_bit_widths()
			_, _, output_signed = self.get_signs()
			output_bit_spec = self.get_bit_spec("output")
			self.outputs = [inst(dl.Logic,
				output_bits,
				signed=output_signed,
			#	arithmetic_bit_spec=output_bit_spec,
				name="output_" + str(i)) for i in range(output_width)]

			with dl.ModuleScope(inst(dl.Module, "done_controller", kind="done_controller")):
				output_channel_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=self.get_output_act(dim="channels") - 1,
					name="output_channel_counter")
				output_channel_counter.set_increment_condition(self.output_valid.delay(1))
				output_lines_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=self.get_output_act(dim="height")-1,
					name="output_lines_counter")
				output_lines_counter.set_increment_condition(output_channel_counter.done_and_incrementing.delay(1))
				states = ["IDLE", "PROCESSING_IMAGE", "DONE"]
				edges = [
					["IDLE", "PROCESSING_IMAGE", self.output_valid.delay(1)],
					["PROCESSING_IMAGE", "DONE", output_lines_counter.done_and_incrementing.delay(1)],
					["DONE", "IDLE"]]
				state_machine = inst(dl.StateMachine, states, edges, None)
			self.done = inst(dl.Logic, bits=1, name="done")
			self.done.set_driver(state_machine.is_done.delay(1))

			self.utilization_estimates = {}
			self.control_valid_to_output_valid_delay = 0
			self.control_to_input_delay = 0
			self.guarantees_output_valid_low_in_cycles = True
			self.reset = inst(dl.Logic, bits=1, name="reset")
			self.reset.set_driver(kwargs["reset"])
			self.instantiate(node, inst, build_data_path, **kwargs)
			print()

	
	def get_custom_verilog_str(self, t="  "):
		if self.output_csv_path is None:
			self.output_csv_path = 'generated_files/layer_images/verilog/' + self.name + '.csv'
		dump_input = False #self.node.type != "Placeholder"
		input_csv_path = self.output_csv_path[:-4] + "_input.csv"
		fh = self.name + "_fh"
		fh_i = self.name + "_input_fh"
		verilog = t + "/*verilator no_inline_module*/\n"
		verilog += "`ifdef WRITE_IMAGES\n"
		verilog += "  integer " + fh + ";\n"
		verilog += "  integer " + fh_i + ";\n"
		verilog += "  initial begin\n"
		verilog += '    ' + fh + ' = $fopen("' + self.output_csv_path + '", "w");\n'
		verilog += '    $fwrite(' + fh + ', "' + str(self.get_output_act(dim="channels")) + '\\n");\n'
		if dump_input:
			verilog += '    ' + fh_i + ' = $fopen("' + input_csv_path + '", "w");\n'
			verilog += '    $fwrite(' + fh_i + ', "' + str(self.get_input_act(dim="channels")) + '\\n");\n'
		verilog += """  end
  final begin
"""
		verilog += '    $fclose(' + fh + ');\n'
		verilog += '    $fclose(' + fh_i + ');\n'
		verilog += '  end\n'
		verilog += "`endif\n"

		verilog += f"  always @(posedge {self.clock.name}) begin\n"
		verilog += "`ifdef WRITE_IMAGES\n"
		verilog += "    if (" + self.outputs_valid[0].name + " && !" + self.reset.name + ") begin\n"
		verilog += '      $fwrite(' + fh + ', "' + ",".join(['%d'] * len(self.outputs)) + '\\n", ' + ", ".join([d.name for d in self.outputs]) + " );\n"
		verilog += "    end\n"
		if dump_input:
			verilog += "    if (" + self.writes[0].name + " && !" + self.reset.name + ") begin\n"
			verilog += '      $fwrite(' + fh_i + ', "' + ",".join(['%d'] * len(self.inputs)) + '\\n", ' + ", ".join([d.name for d in self.inputs]) + " );\n"
			verilog += "    end\n"
		verilog += "`endif\n"
		verilog += "`ifdef DISPLAY_FINISH_TIMES\n"
		verilog += "    if (" + self.done.name + ") begin\n"
		verilog += '      $display("' + self.name + ' finished at time %0t", $time);\n'
		verilog += "    end\n"
		#verilog += "    if (!" + self.can_write_line.name + ") begin\n"
		#verilog += '      $display("' + self.name + ' cannot write");\n'
		#verilog += "    end\n"
		verilog += "`endif\n"
		verilog += "  end\n"
		return verilog

	def get_instance_comment(self, t="  "):
		header = "/" * 80 + "\n"
		comment = header
		padding = max((80-len(self.node.tf_op.name)) // 2, 0)
		comment += padding * " " + self.node.tf_op.name + "\n"
		comment += header
		comment += "Input Nodes:\n"
		for i in self.node.inputs:
			n = i._from
			comment += "  (" + n.tf_op.name
			if n.module is not None:
				comment += ", " + n.module.name
			comment += ")\n"
		comment += "Output Nodes:\n"
		for i in self.node.outputs:
			n = i._to
			comment += "  (" + n.tf_op.name
			if n.module is not None:
				comment += ", " + n.module.name
			comment += ")\n"
		comment += "Control valid to output valid delay: " + str(self.get_control_valid_to_output_valid_delay()) + "\n"
		comment += "Control to input delay: " + str(self.get_control_to_input_delay()) + "\n"
		if len(self.input_modules) > 0:
			comment += "Input shape: " + str(self.get_input_act().shape) + "\n"
		comment += "Output shape: " + str(self.get_output_act().shape) + "\n"
		comment += "Output Act Bit Spec: " + "\n"
		comment += "  " + pprint.pformat(self.get_bit_spec("output"), indent=len(t)).replace("\n", "\n  ") + "\n"
		comment += "Parameter Bit Spec: " + "\n"
		comment += "  " + pprint.pformat(self.get_bit_spec("parameters"), indent=len(t)).replace("\n", "\n  ") + "\n"
		return v_comment_string(comment, t)

	def set_act_shapes(self, node):
		self.oa_shape = node.example_outputs[0].shape

	def get_input_act(self, dim=None, input_index=0):		
		if dim is None:
			return self.node.inputs[input_index]._from.example_outputs[0]
		assert(dim in self.act_indices)
		return self.input_modules[input_index].ia_shape[self.act_indices[dim]]

	def get_output_act(self, dim=None):
		if dim is None:
			return self.node.example_outputs[0]
		assert(dim in self.act_indices)	
		return self.oa_shape[self.act_indices[dim]]

	def set_bit_widths(self, node):
		oa_precision_params = node.precision_parameters
		output_bits = np.sum(np.array([int(oa_precision_params[b]) for b in
			"a_sign_bits,a_exponent,a_int,a_f".split(",")]))
		osigned = oa_precision_params["a_sign_bits"] == 1

		self.output_signed = osigned
		self.output_exponent_bits = oa_precision_params["a_exponent"]
		self.output_int_bits = oa_precision_params["a_int"]
		self.output_fractional_bits = oa_precision_params["a_f"]
		self.output_bits = output_bits

		if len(node.values) > 0:
			p_bits = np.sum(np.array([int(oa_precision_params[b]) for b in
				"sign_bits,exponent,int,f".split(",")]))
			psigned = oa_precision_params["sign_bits"] == 1
			self.parameters_signed = psigned
			self.parameters_exponent_bits = oa_precision_params["exponent"]
			self.parameters_int_bits = oa_precision_params["int"]
			self.parameters_fractional_bits = oa_precision_params["f"]
			self.parameter_bits = p_bits
		else:
			self.parameters_signed = -1
			self.parameters_exponent_bits = -1
			self.parameters_int_bits = -1
			self.parameters_fractional_bits = -1
			self.parameter_bits = -1


	def get_from_input_module(self, index, f):
		if len(self.input_modules) <= index:
			return None
		return f(self.input_modules[index])

	def get_signs(self, input_index=0):
		return (self.get_from_input_module(input_index, lambda x: x.signed), self.parameters_signed, self.output_signed)

	def get_bit_widths(self, input_index=0):
		return (
			self.get_from_input_module(input_index, lambda x: x.bits),
			self.parameter_bits,
			self.output_bits)

	def get_int_part_widths(self, input_index=0):
		return (
			self.get_from_input_module(input_index, lambda x: x.int_bits),
			self.parameters_int_bits,
			self.output_int_bits)

	def get_frac_part_widths(self, input_index=0):
		return (self.get_from_input_module(input_index, lambda x: x.fractional_bits), self.parameters_fractional_bits, self.output_fractional_bits)

	def get_bit_spec(self, spec_name, input_index=0):
		assert(spec_name in "input,output,parameters".split(","))
		widths = self.get_bit_widths(input_index)
		int_parts = self.get_int_part_widths(input_index)
		frac_parts = self.get_frac_part_widths(input_index)
		signs = self.get_signs(input_index)
		index = {
			"input"     :0,
			"parameters":1,
			"output"    :2}[spec_name]
		return {
			"width" : widths[index],
			"int"   : int_parts[index],
			"frac"  : frac_parts[index],
			"sign"  : signs[index]
		}

	def get_control_valid_to_output_valid_delay(self):
		return self.control_valid_to_output_valid_delay

	def _set_control_valid_to_output_valid_delay(self, ctod):
		self.control_valid_to_output_valid_delay = ctod

	def get_control_to_input_delay(self):
		return self.control_to_input_delay

	def _set_control_to_input_delay(self, ctid):
		self.control_to_input_delay = ctid

	def get_aligned_for_add(self, i1, i2, i1_bit_spec, i2_bit_spec):
		new_spec = {}
		for k,v in i1_bit_spec.items():
			new_spec[k] = v
		def get_extended(_input, bits_to_extend, signed, extend_integer=True):
			if extend_integer:
				string = "integer"
			else:
				string = "fractional"
			# Only use the sign bit for extending the integer, otherwise we should use 0 for the fractional component
			# According to Matthew these bits shouldn't matter to much for non-quantized networks as we are adding precision and have no way of knowing what the fraction should be
			# For pre-quantized networks, however, this introduces error since a lot of the weights are already quantized to a certain number of bits, hence any additional ones should be 0
			if signed and extend_integer:
				to_repeat = _input[_input._bit_width-1]
			else:
				to_repeat = self.inst(dl.Constant, bits=1, value=0, name=_input.name + "_" + string + "_extend")
			if extend_integer:
				to_concat = [_input] + [to_repeat] * bits_to_extend
			else:
				# extend fractional
				to_concat = [to_repeat] * bits_to_extend + [_input]

			return self.inst(dl.Concat, *to_concat, signed=signed, name=_input.name + "_" + string + "_extended")
		def get_fractional_extend_amount(greater_spec, lesser_spec):
			nonlocal new_spec
			new_spec["frac"] = greater_spec["frac"]
			return greater_spec["frac"] - lesser_spec["frac"]
		def get_int_extend_amount(greater_spec, lesser_spec):
			nonlocal new_spec
			new_spec["int"] = greater_spec["int"]
			new_spec["sign"] = greater_spec["sign"]
			add_or_subtract_sign_bit = 0
			if greater_spec["sign"] and not lesser_spec["sign"]:
				add_or_subtract_sign_bit = 1
			elif not greater_spec["sign"] and lesser_spec["sign"]:
				add_or_subtract_sign_bit = -1
			return greater_spec["int"] - lesser_spec["int"] + add_or_subtract_sign_bit
		if i1_bit_spec["frac"] < i2_bit_spec["frac"]:
			e = get_fractional_extend_amount(i2_bit_spec, i1_bit_spec)
			i1 = get_extended(i1, e, i1_bit_spec["sign"], extend_integer=False)
		elif i1_bit_spec["frac"] > i2_bit_spec["frac"]:
			e = get_fractional_extend_amount(i1_bit_spec, i2_bit_spec)
			i2 = get_extended(i2, e, i2_bit_spec["sign"], extend_integer=False)

		if i1_bit_spec["int"] < i2_bit_spec["int"]:
			e = get_int_extend_amount(i2_bit_spec, i1_bit_spec)
			if e > 0:
				i1 = get_extended(i1, e, i1_bit_spec["sign"], extend_integer=True)
		elif i1_bit_spec["int"] > i2_bit_spec["int"]:
			e = get_int_extend_amount(i1_bit_spec, i2_bit_spec)
			if e > 0:
				i2 = get_extended(i2, e, i2_bit_spec["sign"], extend_integer=True)

		# extend one additional bit to allow two max integer quantities to overflow without
		# overflowing into the sign bit
		i1 = get_extended(i1, 1, i1_bit_spec["sign"], extend_integer=True)
		i2 = get_extended(i2, 1, i2_bit_spec["sign"], extend_integer=True)
		new_spec["int"] += 1
		new_spec["width"] = new_spec["int"] + new_spec["frac"] + int(new_spec["sign"])
		return (i1, i2, new_spec)

	def get_individual_values_from_combined(self, combined, use_output_act_bits=False, input_index=0):
		node = self.node
		if use_output_act_bits:
			input_width = self.get_output_act(dim="width")
		else:
			input_width = self.get_input_act(dim="width", input_index=input_index)
		combined_count = len(combined)
		bits, _, output_bits = self.get_bit_widths()
		if use_output_act_bits:
			bits = output_bits
		parallel_acts_per_ram = combined[0]._bit_width // bits

		individuals = []
		for i,c in enumerate(combined):
			for j in range(parallel_acts_per_ram):
				#if (i * parallel_acts_per_ram + j) >= input_width:
				#	break
				with dl.ParamScope(name=c.name + "_" + str(j)):
					individuals.append(c[j*bits+bits-1:j*bits])
		individuals = individuals[len(individuals)-input_width:]
		return individuals

	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		pass

	def get_test_module_string(self, t="  "):
		filehandles = [n.module.name + "_fh" for n in self.hpipe.nodes]
		verilog = "  integer " + filehandles[0]
		for fh in filehandles[1:]:
			verilog += ", " + fh
		verilog += ";\n"
		verilog += """
  initial begin
`ifndef VERILATOR
`ifndef DONTDUMP
  	$vcdplusfile("dump.vpd");
    $vcdpluson(0, hpipe_tb_0);
`endif
`endif
"""
		
		for fh,n in zip(filehandles, self.hpipe.nodes):
			verilog += '    ' + fh + ' = $fopen("generated_files/layer_images/verilog/' + fh[:-3] + '.csv", "w");\n'
			verilog += '    $fwrite(' + fh + ', "' + str(n.example_outputs[0].shape[-1]) + '\\n");\n'
		verilog += """  end
  reg [20:0] finish_counter;
  always @(posedge clock_0) begin
  	if (reset) begin
  		finish_counter <= '0;
  	end else begin
	  	finish_counter <= finish_counter + 1;
  		if (finish_counter == 200000) begin
"""
		for fh in filehandles:
			verilog += '  			$fclose(' + fh + ');\n'
		verilog += """
`ifndef VERILATOR
  			$finish;
`endif
  		end
  	end
  end
"""
		verilog += "  always @(posedge clock_0) begin\n"
		for fh,v,os in zip(filehandles, self.all_valids, self.all_outputs):
			verilog += "    if (" + v.name + ") begin\n"
			verilog += '      $fwrite(' + fh + ', "' + ",".join(['%d'] * len(os)) + '\\n", ' + ", ".join([d.name for d in os]) + " );\n"
			verilog += "    end\n"
		#for i,god in enumerate(self.golden_output_data):
		#	verilog += "      assert("+god.name+" == " + self.dsp_outputs[i].name + ");\n"
		verilog += "  end\n"
		return verilog

class Mean(NNStage):
	kind = "mean"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):

		bits, _, output_bits = self.get_bit_widths()
		int_bits, _, _ = self.get_int_part_widths()
		frac_bits, _, output_frac_bits = self.get_frac_part_widths()
		signed, _, output_signed = self.get_signs()
		input_channels = self.get_input_act(dim="channels")
		input_height = self.get_input_act(dim="height")
		input_width = self.get_input_act(dim="width")

		read_delay = 2
		reduction_stages = max(clog2(input_width), read_delay)
		mul_stages = 4
		mux_and_add_delay = 1
		constant_frac_bits = 26

		memory_width = min(40, bits + reduction_stages + input_height)

		piped_inputs = [i.delay(reduction_stages) for i in self.inputs]
		reduction_tree = inst(dl.ReduceAdd, *piped_inputs)
		in_mul_value = get_quantized(np.array([1. / float(input_width)]), int(signed), int_bits + reduction_stages, frac_bits)[0]
		in_mul_constant = inst(dl.Constant, value=in_mul_value, signed=signed, bits=bits+reduction_stages, name="in_mul_constant")
		out_mul_value = get_quantized(np.array([1. / float(input_width*input_height)]), 1, 0, constant_frac_bits)[0]
		out_mul_constant = inst(dl.Constant, value=out_mul_value, signed=True, bits=27, name="out_mul_constant")

		#with dl.ParamScope(name="input_mul_out", signed=signed):
		#	input_mul_out = inst(dl.MUL, reduction_tree.output.delay(mul_stages), in_mul_constant.delay(mul_stages), name="input_multiplier")[bits + reduction_stages + frac_bits - 1: frac_bits]

		ram = inst(dl.RAM, bits=memory_width, depth=input_channels)
		ram.r_en.set_driver(self.write.delay(reduction_stages - read_delay))
		read_data = inst(dl.Flop, bits=memory_width, signed=signed, driver=ram.r_data)
		channel_counter = inst(dl.Counter, reset_value=0, end_value=input_channels-1, increment_value=1, name="channel_counter")
		channel_counter.set_increment_condition(ram.r_en)
		ram.r_addr.set_driver(channel_counter.current_value)
		input_line_counter = inst(dl.Counter, reset_value=0, end_value=input_height-1, increment_value=1, name="input_line_counter")
		input_line_counter.set_increment_condition(channel_counter.done_and_incrementing)
		to_add = inst(dl.MuxV2, input_line_counter.is_reset_value.delay(read_delay+mux_and_add_delay), read_data.delay(mux_and_add_delay), inst(dl.Constant, value=0, bits=memory_width).delay(mux_and_add_delay), signed=signed)
		with dl.ParamScope(name="fully_reduced"):
			fully_reduced = (to_add + reduction_tree.output.delay(mux_and_add_delay))[memory_width-1:0]

		input_frac_bits, _, desired_fractional_bits = self.get_frac_part_widths()
		_, _, output_int_bits = self.get_int_part_widths()
		fractional_bits_in_output = input_frac_bits
		lo = fractional_bits_in_output - desired_fractional_bits
		hi = fractional_bits_in_output + output_int_bits - 1

		ram.w_data.set_driver(fully_reduced)
		ram.w_addr.set_driver(channel_counter.current_value.delay(read_delay + mux_and_add_delay))
		ram.w_en.set_driver(ram.r_en.delay(read_delay + mux_and_add_delay))

		with dl.ParamScope(name="output_mul_out", signed=signed):
			#output_mul_out = inst(dl.MUL, fully_reduced.delay(mul_stages), out_mul_constant.delay(mul_stages), name="output_multiplier")[output_bits + frac_bits-output_frac_bits+27 - 1: frac_bits-output_frac_bits+27]
			dsp = inst(dl.MultiplyAccumulate,
				[s._bit_width for s in [fully_reduced, out_mul_constant]], fully_reduced._bit_width+constant_frac_bits, 2, name="output_multiplier")
			dsp.inputs[0].set_driver(fully_reduced)
			dsp.inputs[1].set_driver(out_mul_constant)
			output_mul_out = dsp.output[output_bits + frac_bits-output_frac_bits+constant_frac_bits - 1: frac_bits-output_frac_bits+constant_frac_bits]

		self.space_to_write_line.set_driver(inst(dl.NOT, inst(dl.AND, inst(dl.EQ, input_line_counter.current_value, input_height-2), inst(dl.NOT, self.can_write_line))))

		self.outputs_valid[0].set_driver(inst(dl.AND, input_line_counter.is_done.delay(read_delay + mux_and_add_delay), ram.w_en).delay(mul_stages))
		self.output_valid.set_driver(self.outputs_valid[0])
		self.outputs[0].set_driver(output_mul_out)
		self._set_control_valid_to_output_valid_delay(0)
		self._set_control_to_input_delay(0)


class Add(NNStage):
	kind =  "add"
	def build_controller(self, inst, input_channels):
		control_to_data_path_delay = 3
		self.done_line = inst(dl.Logic, 1, name="done_line")
		self._done_line = inst(dl.Logic, 1, name="_done_line")
		states = ["IDLE", "READING_LINE"]
		edges = [
			["IDLE", "READING_LINE", self.go],
			["READING_LINE", "IDLE", self._done_line]
		]
		self.state_machine = inst(dl.StateMachine, states, edges, None)
		in_channel_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=input_channels-1,
			name="in_channel_counter")
		in_channel_counter.set_increment_condition(self.state_machine.is_reading_line)

		self._done_line.set_driver(in_channel_counter.is_done)
		self.done_line.set_driver(self._done_line.delay(control_to_data_path_delay))
		self.is_reading_line = self.state_machine.is_reading_line.delay(control_to_data_path_delay)

	def instantiate(self, node, inst, build_data_path=True, **kwargs):

		parallel_acts_per_ram = node.inputs[0]._from.get_parallel_acts_per_memory()

		input_channels = self.get_input_act(dim="channels")
		input_height = self.get_input_act(dim="height")
		input_width = self.get_input_act(dim="width")

		kernel_height = 1
		kernel_width = 1
		padding = [0,0]
		vertical_stride = 1
		horizontal_stride = 1

		bits, _, output_bits = self.get_bit_widths()
		signed, _, output_signed = self.get_signs()


		output_width = self.get_output_act(dim="width")
		output_height = self.get_output_act(dim="height")

		pre_modulo_addr_add_delay = 2
		address_computation_delay = pre_modulo_addr_add_delay
		memory_read_delay         = 2
		memory_model_delay        = 1
		added_read_delay          = memory_read_delay - memory_model_delay
		sr_write_delay            = 1
		single_addr_delay         = kernel_width
		single_pool_delay         = kernel_width * kernel_height

		self.go = inst(dl.Logic, 1, name="go")
		self.build_controller(inst, input_channels)

		self.ibcs = []
		self.ia_buffers = []
		self.read_data_groups = []
		have_full_lines = []
		per_ram_fanout_stages_list = []
		per_ram_write_stages_list = []
		is_reading_line_signals = []
		input_module_delayed_writes = []
		control_to_input_delays = []
		for i in range(len(node.inputs)):
			bits, _, output_bits = self.get_bit_widths(i)
			is_reading_line_input = inst(dl.Logic, bits=1, signed=False, name="is_reading_line_input_" + str(i))
			is_reading_line_signals.append(is_reading_line_input)
			input_module_delayed_write = inst(dl.Logic, bits=1, signed=False, name="input_module_delayed_write_" + str(i))
			input_module_delayed_writes.append(input_module_delayed_write)

			min_buffer_depth = max(node.planner_estimate.buffer_for_input[i], 2)
			input_channels = self.get_input_act(dim="channels", input_index=i)
			print("  Minimum buffer depth for input " + str(i) + ": " + str(min_buffer_depth))
			print("  Number of input channels for input " + str(i) + ": " + str(input_channels))
			#self, stride, kernel_height, channels, activation_height, padding, n_channel_splits, output_activation_height, min_buffer_depth=1, **kwargs):
			ibc = inst(InputBufferController,
				vertical_stride,
				kernel_height,
				input_channels,
				input_height,
				padding,
				1,
				output_height,
				min_buffer_depth=min_buffer_depth)
			self.input_modules[i].space_to_write_line.set_driver(ibc.space_to_write_line.delay(1))
			ibc.done_image.set_driver(self.done)
			ibc.write.set_driver(input_module_delayed_write)
			have_full_lines.append(ibc.has_full_kernel)
			ibc.finished_reading_line.set_driver(self.done_line)
			ibc.started_reading_line.set_driver(inst(dl.AND, self.go, self.state_machine.is_idle))
			self.ibcs.append(ibc)
			ia_buffer = inst(dl.FanoutRAM,
				bits * input_width,
				min_buffer_depth * input_channels)
			control_to_input_delays.append(ibc.write_counter_increment_delay)
			self.ia_buffers.append(ia_buffer)
			per_ram_fanout_stages_list.append(ia_buffer.read_delay)
			read_data_list = []
			w_data = inst(dl.Concat, *self.input_modules[i].inputs)
			ia_buffer.w_data.set_driver(w_data)
			select_base = 0			
			for j in self.input_modules[i].inputs:
				read_data_list.append(ia_buffer.r_data[select_base + j._bit_width - 1 : select_base])
				select_base += j._bit_width
			self.read_data_groups.append(read_data_list)

			addr_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=min_buffer_depth*input_channels-1,
				name="addr_counter_" + str(i))
			addr_counter.set_increment_condition(is_reading_line_input)

			ia_buffer.w_en.set_driver(ibc.write_outs[0])
			ia_buffer.w_addr.set_driver(ibc.write_address)
			ia_buffer.r_en.set_driver(is_reading_line_input)
			ia_buffer.r_addr.set_driver(addr_counter.current_value)

		max_control_to_input_delay = max(control_to_input_delays)
		max_per_ram_fanout_delay = max(per_ram_fanout_stages_list)
		for s,d in zip(is_reading_line_signals, per_ram_fanout_stages_list):
			s.set_driver(self.is_reading_line, delay=max_per_ram_fanout_delay-d)
		for im, imdw, d in zip(self.input_modules, input_module_delayed_writes, control_to_input_delays):
			imdw.set_driver(im.write.delay(max_control_to_input_delay-d))

		input_frac_bits, _, desired_fractional_bits = self.get_frac_part_widths()
		_, _, output_int_bits = self.get_int_part_widths()
		fractional_bits_in_output = input_frac_bits
		lo = fractional_bits_in_output - desired_fractional_bits
		hi = fractional_bits_in_output + output_int_bits - 1
		fractional_padding = 0
		if output_signed:
			hi += 1
		if lo < 0:
			fractional_padding = -lo
			lo = 0

		add_trees = []
		pipe_stages = 3
		output_valid_fanout = self.is_reading_line.delay(max_per_ram_fanout_delay+pipe_stages, max_fan=4, final_fan=output_width)
		for i,inputs in enumerate(zip(*self.read_data_groups)):
			if build_data_path:
				ram_index = i // parallel_acts_per_ram

				to_reduce = [p.delay(pipe_stages) for p in inputs]
				for tr in to_reduce:
					tr.signed = signed
				add_trees.append(inst(dl.ReduceAdd, *to_reduce))
				with dl.ModuleScope(add_trees[-1]):
					with dl.ParamScope(name="output_precision"):
						output = add_trees[-1].output[hi:lo]
						if fractional_padding > 0:
							output = inst(dl.Concat, inst(dl.Constant, bits=fractional_padding, value=0), output)
						self.outputs[i].set_driver(output)
			self.outputs_valid[i].set_driver(output_valid_fanout)
		self.output_valid.set_driver(self.is_reading_line)
		self.go.set_driver(inst(dl.AND, *have_full_lines, self.can_write_line))
		ibc_is_idle = [inst(dl.Logic, bits=1, name="ibc_is_idle") for ibc in self.ibcs]
		for i,ibc in zip(ibc_is_idle, self.ibcs):
			i.set_driver(ibc.state_machine.c["is_idle"])
		self.debug_stall = inst(dl.OR, *[inst(dl.AND,
			idle,
			inst(dl.NOT, self.is_reading_line),
			inst(dl.NOT, dl.Verilog.current_params.kwargs["reset"]),
			inst(dl.NOT, i.space_to_write_line),
			inst(dl.NOT, h)) for idle,i,h in zip(ibc_is_idle, self.input_modules, have_full_lines)])
		self._set_control_to_input_delay(max_control_to_input_delay)
		self._set_control_valid_to_output_valid_delay(pipe_stages + max_per_ram_fanout_delay)

	def get_custom_verilog_str(self, t="  "):
		verilog = super(Add, self).get_custom_verilog_str(t)
		'''
		verilog += "  always @(posedge clock_0) begin\n"
		verilog += "    if (" + self.debug_stall.name + ") begin\n"
		verilog += '      $display("' + self.name + ' deadlocked because there is no space and it does not have enough lines to write one out %0t", $time);\n'
		verilog += "    end\n"
		#verilog += "    if (!" + self.can_write_line.name + ") begin\n"
		#verilog += '      $display("' + self.name + ' cannot write");\n'
		#verilog += "    end\n"
		verilog += "  end\n"'''
		return verilog

class InputChannelBufferWriteController(dl.Module):
	def __init__(self, stride, kernel_height, channels, min_buffer_depth=1, **kwargs):
		super(InputChannelBufferWriteController, self).__init__("input_channel_buffer_write_controller", kind="input_channel_buffer_write_controller", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			self.write = inst(dl.Logic, 1, name="write")
			depth = (stride + kernel_height) * channels
			depth = max(depth, min_buffer_depth * channels)
			self.addr_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=depth-1)
			self.address = inst(dl.Logic,
				self.addr_counter.current_value._bit_width,
				self.addr_counter.current_value,
				name="address")
			self.addr_counter.set_increment_condition(self.write)
			self.requires_driver = [self.write]

class InputBufferController(dl.Module):
	"""
	InputBufferController (IBC)

	Background: The IBC controls writing to InputActivationBuffer modules (IABs).  The IAB
	            typically buffers kernel height (Kh) plus vertical stride (Sv) lines, but
	            the number of lines in the buffer can be overwritten by setting min_buffer_depth 
	            to something greater than stride + kernel_height.

	Components:
		input_channel_counter
	"""
	def __init__(self, stride, kernel_height, channels, activation_height, padding, n_channel_splits, output_activation_height, min_buffer_depth=1, **kwargs):
		super(InputBufferController, self).__init__("input_buffer_controller", kind="input_buffer_controller", **kwargs)
		inst = self.inst

		# store the input params so that we can dump them in the instance comment
		self.params = {
			"stride" : stride,
			"kernel_height" : kernel_height,
			"channels" : channels,
			"activation_height" : activation_height,
			"padding" : padding,
			"n_channel_splits" : n_channel_splits,
			"min_buffer_depth" : min_buffer_depth}


		with dl.ModuleScope(self):
			self.write = inst(dl.Logic, 1, name="write")
			self.finished_reading_line = inst(dl.Logic, 1, name="finished_reading_line")
			self.started_reading_line = inst(dl.Logic, 1, name="started_reading_line")
			self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")
			self.write_outs = [inst(dl.Logic, 1, name="write_out_" + str(i)) for i in range(n_channel_splits)]
			self.write_address = inst(dl.Logic, None, name="write_address")
			self.done_image = inst(dl.Logic, 1, name="done_image")
			self.increment_write_condition = inst(dl.Logic, 1, name="increment_write_condition")
			self.increment_write_condition.set_driver(inst(dl.Constant, value=1, bits=1, signed=False, name="default_increment_write_condition"))
			self.increment_lines_used = inst(dl.Logic, 1, name="increment_lines_used")
			
			n_channels = [math.ceil(channels / float(n_channel_splits)) for _ in range(channels % n_channel_splits)]
			n_channels += [channels // n_channel_splits for _ in range(n_channel_splits - (channels % n_channel_splits))]
			self.write_controllers = [inst(InputChannelBufferWriteController, stride, kernel_height, n, min_buffer_depth=min_buffer_depth) for n in n_channels]
			self.has_space_to_write = inst(dl.Logic, 1, name="has_space_to_write")
			self.input_channel_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=channels-1,
				name="input_channel_counter")
			
			input_line_counter_end_val = int((activation_height + np.sum(padding)-1))
			
			self.input_line_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=input_line_counter_end_val,
				name="input_line_counter")
			self.input_line_counter_next = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=input_line_counter_end_val,
				name="input_line_counter_next")

			total_lines_written = max(np.sum(padding),0) + activation_height
			lines_explicitly_read = stride * output_activation_height
			final_diff_value = lines_explicitly_read - total_lines_written
			print("Total lines written: ", total_lines_written, ", Lines explicitly read: ", lines_explicitly_read)
			done_line_decrement_value = -stride
			
			counter_end_value = max(kernel_height+stride, min_buffer_depth)
			
			self.lines_used_counter = inst(dl.MultiChangeCounter,
				reset_value=0,
				change_values=[
					1,
					done_line_decrement_value,
					final_diff_value],
				end_value=counter_end_value,
				name="lines_used_counter")
			self.lines_used_counter.change_signals[1].set_driver(self.finished_reading_line)
			self.lines_used_counter.change_signals[2].set_driver(self.done_image)
			self.lines_complete_counter = inst(dl.MultiChangeCounter,
				reset_value=0,
				change_values=[
					1,
					done_line_decrement_value,
					final_diff_value],
				end_value=counter_end_value,
				name="lines_complete_counter")
			with dl.ModuleScope(self.lines_complete_counter):
				started_reading_line_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=output_activation_height-1,
					name="started_reading_line_counter")
				started_reading_line_counter.set_increment_condition(self.started_reading_line)
			self.lines_complete_counter.change_signals[1].set_driver(self.started_reading_line)
			self.lines_complete_counter.change_signals[2].set_driver(started_reading_line_counter.done_and_incrementing)
			
			
			#For debugging only, shouldn't be used for actual control logic as it may slow things down
			self.state_machine_idle = inst(dl.Logic, 1, name="state_machine_idle")
						
			with dl.ModuleScope(self.lines_used_counter):			
				self.has_space_to_write =  inst(dl.NOT, self.lines_used_counter.is_done, name="has_space_to_write")
			with dl.ModuleScope(self.lines_complete_counter):
				if (stride > kernel_height):
					self.has_full_kernel = self.lines_complete_counter.current_value >= inst(dl.Constant, stride, bits=self.lines_used_counter.current_value._bit_width, name="stride")
				else:
					self.has_full_kernel = self.lines_complete_counter.current_value >= inst(dl.Constant, kernel_height, bits=self.lines_used_counter.current_value._bit_width, name="kernel_height")
				self.has_full_kernel = self.has_full_kernel.delay(4)
			self.has_padding = padding[0] != 0 or padding[1] != 0
			if self.has_padding:
				with dl.ModuleScope(self.input_line_counter):				
					if padding[0] > 0:
						padding_lower_bound = inst(dl.Constant, padding[0], signed=False, bits=self.input_line_counter.current_value._bit_width, name="padding_lower_bound")
						in_lower_padding = self.input_line_counter.current_value < padding_lower_bound
						in_lower_padding_next = self.input_line_counter_next.current_value < padding_lower_bound
					else:
						in_lower_padding = inst(dl.Constant, bits=1, value=0, name="in_lower_padding")
						in_lower_padding_next = inst(dl.Constant, bits=1, value=0, name="in_lower_padding_next")
					if padding[1] > 0:
						padding_upper_start = inst(dl.Constant, padding[0]+activation_height, signed=False, bits=self.input_line_counter.current_value._bit_width, name="padding_upper_start")
						in_upper_padding = self.input_line_counter.current_value >= padding_upper_start
						in_upper_padding_next = self.input_line_counter_next.current_value >= padding_upper_start
					else:
						in_upper_padding = inst(dl.Constant, bits=1, value=0, name="in_upper_padding")
						in_upper_padding_next = inst(dl.Constant, bits=1, value=0, name="in_upper_padding_next")
						
									
					self.in_padding_no_delay = inst(dl.OR, in_lower_padding, in_upper_padding, name="in_padding")
					self.in_padding_next_no_delay = inst(dl.OR, in_lower_padding_next, in_upper_padding_next, name="in_padding_next")
					
					self.in_padding = inst(dl.Flop, bits=1, driver=self.in_padding_no_delay, name="in_padding_d1")
					self.in_padding_next = inst(dl.Flop, bits=1, driver=self.in_padding_next_no_delay, name="in_padding_next_d1")
					
				#Marius: need to add corner case where we are doing our padding write soon but the buffer is almost full. Otherwise the module will request another line which will at the same time as our padding write
				#This should only happen on layers with a small number of input channels (e.g. the first one which has 3) and is most likely to occur when output channel parallelism is turned on 
				#Right now this is only implemented for upper padding. I'm not sure if it would be an issue for lower padding
				if padding[1] > 0:
					line_counter_almost_done_val = inst(dl.Constant, (input_line_counter_end_val-1), signed=False, bits=self.input_line_counter.current_value._bit_width, name="line_counter_almost_done_val")
					line_counter_almost_done = self.input_line_counter.current_value >= line_counter_almost_done_val
					self.pad_next_and_buffer_almost_full= inst(dl.AND, line_counter_almost_done, inst(dl.EQ, self.lines_used_counter.current_value, counter_end_value-1, name="buffer_almost_full"), name="pad_next_and_buffer_almost_full")
				else:
					self.pad_next_and_buffer_almost_full= inst(dl.Constant, bits=1, value=0, name="pad_next_and_buffer_almost_full")
									
				#self.has_space_and_not_in_padding_next = inst(dl.AND, self.has_space_to_write, inst(dl.NOT, self.in_padding_next_no_delay), self.state_machine_idle, inst(dl.NOT, self.pad_next_and_buffer_almost_full, name="not_pad_next_and_buffer_almost_full"))
				self.has_space_and_not_in_padding_next = inst(dl.AND, self.has_space_to_write, inst(dl.NOT, self.in_padding_next_no_delay), inst(dl.NOT, self.pad_next_and_buffer_almost_full, name="not_pad_next_and_buffer_almost_full"))
			else:
				self.has_space_and_not_in_padding_next = self.has_space_to_write

			self.space_to_write_line.set_driver(self.has_space_and_not_in_padding_next)
			self.done_line = inst(dl.Logic, 1, name="done_line")
			self.input_line_counter.set_increment_condition(self.done_line)

			states = ["IDLE", "WRITING_LINE", "DONE_WRITING_LINE"]
			edges = [
				["IDLE", "WRITING_LINE", self.write],
				["WRITING_LINE", "DONE_WRITING_LINE", self.done_line],
				["DONE_WRITING_LINE", "IDLE"]]
			if self.has_padding:
				edges += [["IDLE", "WRITING_PADDING", inst(dl.AND, self.has_space_to_write, self.in_padding)],
					["WRITING_PADDING", "DONE_WRITING_PADDING", self.done_line],
					["DONE_WRITING_PADDING", "IDLE"]]
				states += ["WRITING_PADDING", "DONE_WRITING_PADDING"]

			self.state_machine = inst(dl.StateMachine, states, edges, None)
			self.state_machine_idle.set_driver(self.state_machine.c["is_idle"])
			if self.has_padding:
				self.is_writing_padding = self.state_machine.c["is_writing_padding"]
				self.is_done_writing_padding = self.state_machine.c["is_done_writing_padding"]
			else:
				self.is_writing_padding = inst(dl.Constant, bits=1, value=0, name="is_writing_padding")
				self.is_done_writing_padding = inst(dl.Constant, bits=1, value=0, name="is_done_writing_padding")

			self.illegal_write = inst(dl.AND, self.write, inst(dl.NOT, self.state_machine.c["is_writing_line"]), inst(dl.NOT, inst(dl.AND, self.state_machine.c["is_idle"], self.has_space_to_write)))
			self.should_write_out = inst(dl.OR, self.is_writing_padding, self.write, name="should_write_out")
			self.base_counter_increment_condition = inst(dl.AND, self.should_write_out, self.increment_write_condition, name="base_counter_increment_condition")
			ic_active_channel_sr_reset = inst(dl.OR, kwargs["reset"], self.input_channel_counter.done_and_incrementing, name="is_active_channel_sr_reset")
			with dl.ParamScope(reset=ic_active_channel_sr_reset):
				ic_active_channel_sr = inst(dl.ShiftRegister, bits=1, depth=n_channel_splits)
				with dl.ModuleScope(ic_active_channel_sr):
					ic_active_channel_sr.d.set_driver(ic_active_channel_sr.q)
					ic_active_channel_sr.sr[0].set_reset_driver(inst(dl.Constant, bits=1, value=1, name="one"))
					z = inst(dl.Constant, bits=1, value=0, name="zero")
					for sr in ic_active_channel_sr.sr[1:]:
						sr.set_reset_driver(z)
					ic_active_channel_sr.should_shift.set_driver(self.input_channel_counter.increment_condition)
			self.channel_matches_split = ic_active_channel_sr.sr


			write_counter_increment_delay = min(max(0, len(self.write_outs)-2), 2)
			self.write_counter_increment_delay = write_counter_increment_delay
			for i,wo in enumerate(self.write_outs):
				wo.set_driver(inst(dl.AND, self.should_write_out, ic_active_channel_sr.sr[i]).delay(write_counter_increment_delay))
				# we can delay each of these by up to len(self.write_outs)-2 cycles since the
				# address will only need to be used again after we have written to all of the other
				# addresses
				self.write_controllers[i].write.set_driver(inst(dl.AND, wo, self.increment_write_condition.delay(write_counter_increment_delay)))


			self.write_addresses = [c.address for c in self.write_controllers]
			self.write_address.set_driver(inst(dl.OneHotMux, selects=ic_active_channel_sr.sr, values=[c.address for c in self.write_controllers]))
			self.write_address._bit_width = self.write_controllers[0].address._bit_width
			self.input_channel_counter.set_increment_condition(self.base_counter_increment_condition)
			
			self.increment_lines_used.set_driver(inst(dl.AND, self.state_machine.c["is_idle"], self.write))
			
			self.lines_used_counter.change_signals[0].set_driver(
				inst(dl.OR, self.is_done_writing_padding,self.increment_lines_used))
			self.lines_complete_counter.change_signals[0].set_driver(
				inst(dl.OR, self.is_done_writing_padding,
						self.state_machine.c["is_done_writing_line"], name="increment_lines_complete"))
			self.done_line.set_driver(self.input_channel_counter.done_and_incrementing)
			self.input_line_counter_next.set_increment_condition(inst(dl.OR, inst(dl.AND, self.state_machine.c["is_idle"].delay(2), self.write.delay(2)), self.is_done_writing_padding.delay(2)))

			self.requires_driver = [self.write, self.finished_reading_line, self.started_reading_line, self.done_image]

			self.dump_counter_values = self.input_line_counter.done_and_incrementing.copy()
			self.counter_values = {
				"input_channel_counter" : self.input_channel_counter.current_value.copy(),
				"input_line_counter" : self.input_line_counter.current_value.copy(),
				"input_line_counter_next" : self.input_line_counter_next.current_value.copy(),
				"lines_used_counter" : self.lines_used_counter.current_value.copy(),
				"lines_complete_counter" : self.lines_complete_counter.current_value.copy(),
				"started_reading_line_counter" : started_reading_line_counter.current_value.copy()
			}
			modules_to_debug.append(self)

	def get_instance_comment(self, t="  "):
		comment = "\n".join([k + ": " + str(v) for k, v in self.params.items()]) + "\n"
		return v_comment_string(comment, t)

	def get_custom_verilog_str(self, t="  "):
		verilog = super(InputBufferController, self).get_custom_verilog_str(t)
		"""
		module_path = ".".join([o.name for o in reversed([self] + self.get_owner_list()[:-1])])
		verilog += "  always @(posedge clock_0) begin\n"
		#verilog += "`ifdef TESTING\n"
		verilog += "    if (" + self.illegal_write.name + ") begin\n"
		verilog += '      $display("' + module_path + ' got written to in an state where it is not allowed to be written to %0t", $time);\n'
		verilog += "    end\n"
		verilog += "    if (" + self.dump_counter_values.name + ") begin\n"
		verilog += '      $display("' + module_path + ' finished input.  Counter values are:");\n'
		for k,v in self.counter_values.items():
			verilog += '      $display("  ' + k + ': %d", ' + v.name + ");\n"
		verilog += "    end\n"
		#verilog += "    if (!" + self.can_write_line.name + ") begin\n"
		#verilog += '      $display("' + self.name + ' cannot write");\n'
		#verilog += "    end\n"
		#verilog += "`endif\n"
		verilog += "  end\n"""
		return verilog

#Serialize N inputs into M channels, where N is a multiple of M
#This can be used if you only have a limited number of LUTs
class GenericSerializer(dl.Module):
	
	# Instantiates a copy of only the logic to drive the control output write; called by __init__
	# Since that write is global and arrives as an input before the actual data writes, I have no idea how else to implement this without duplicating a bunch of the logic 
	# Adding a set delay would not work because the output write patterns are dependent on how fast the input data arrives
	# NOTE: This must be updated for every change to control logic in the __init__ function.
	def instantiate_control_logic(self, input_channels, output_channels):
		inst = self.inst
		one = inst(dl.Constant, value=1, bits=1, name="control_one")
		zero =  inst(dl.Constant, value=0, bits=1, name="control_zero")
		
		input_fifo = inst(dl.FIFO, bits=1, depth=32, name="control_generic_serializer_FIFO") 
		input_fifo.w_en.set_driver(self.control_write)
		input_fifo.w_data.set_driver(one) #Not technically needed, since output is not hooked up it should be synthesized out in Quartus
		
		parallel_sr_depth = int(input_channels/output_channels)
		counter = inst(dl.Counter,  reset_value=0, increment_value=1, end_value=(parallel_sr_depth-1), name="sr_counter")
		
		sr_init = inst(dl.Flop, bits=1, reset_driver=0, name="control_sr_init_control")
		last_val_stored = inst(dl.Flop, bits=1, name="control_last_val_stored_control")
		previous_write_delay = inst(dl.Logic, 1, name=("control_previous_write_delay_control"))
		
		input_fifos_not_empty = inst(dl.NOT, input_fifo.empty, name="control_input_fifo_not_empty")
		read_fifo = inst(dl.AND, input_fifos_not_empty, inst(dl.OR, counter.is_done, inst(dl.NOT, sr_init)), name="control_read_fifo")
		input_fifo.r_en.set_driver(read_fifo) 
		sr_init.set_driver(inst(dl.MuxV2, read_fifo, sr_init, one, name="control_sr_init_mux"))
		counter.increment_condition.set_driver(inst(dl.AND, sr_init, inst(dl.OR, inst(dl.NOT, counter.is_done), inst(dl.AND, counter.is_done, input_fifos_not_empty)), name="control_sr_counter_increment_cond"))
		
		output_writes = (inst(dl.AND, sr_init.delay(1), inst(dl.NOT, last_val_stored), name="control_sr_output_write_"))
		
		#only actual need 2 values, but it's easier to maintain the code if we do it in an array similar to the actual __init__ function
		output_actual_writes = parallel_sr_depth * [None]
		
		output_actual_writes[parallel_sr_depth-1] = inst(dl.AND, output_writes, inst(dl.OR, inst(dl.EQ, self._constants[0], counter.current_value, name="control_counter_eq_0"), inst(dl.AND, counter.is_done, previous_write_delay)), name=("control_actual_write_last"))
		output_actual_writes[parallel_sr_depth-2] = inst(dl.AND, output_writes, inst(dl.EQ, self._constants[parallel_sr_depth-1], counter.current_value, name="control_counter_eq_last"), inst(dl.NOT, previous_write_delay), name="control_actual_write_second_last" )

		set_last_val_stored = inst(dl.MuxV2, inst(dl.AND, counter.is_done, output_actual_writes[parallel_sr_depth-1] ), last_val_stored, one, name="set_last_val_stored") 
		last_val_stored.set_driver(inst(dl.MuxV2, inst(dl.OR, counter.is_reset_value, inst(dl.NOT, sr_init)), set_last_val_stored, zero))
		previous_write_delay.set_driver(output_actual_writes[parallel_sr_depth-2], delay=1)
		
		
		return output_actual_writes[parallel_sr_depth-1]
		
	
	def __init__(self, bits, input_channels, output_channels, **kwargs):
		super(GenericSerializer, self).__init__("GenericSerializer", kind="GenericSerializer", **kwargs)
		inst = self.inst
		
		if input_channels % output_channels != 0:
			raise Exception("Error! " + str(input_channels) + " is not a multiple of " + str(output_channels) + ". Cannot instantiate GenericSerializer")
		
		with dl.ModuleScope(self):
			#Instantiate a FIFO for every input
			input_width = input_channels
			
			#Make sure to hook up the following inputs
			self.writes = []
			self.control_write = inst(dl.Logic, 1, name="control_write")
			self.input_fifos = [] # input_fifos[i].w_data

			#Use the following for outputs as needed
			self.outputs = []
			self.output_writes = [] # One write signal for each of the output channels
			self.output_actual_writes = [] # One output write signal for each of the input channels, this should be used if you are planning on splitting the output signals back up later
			
			# Batch valid differs from output_write in that it only goes high once the current batch of data from the input channels has passed through the serializer
			self.batch_valid = inst(dl.Logic, 1, name="batch_valid")
			self.control_batch_valid = inst(dl.Logic, 1, name="control_batch_valid") # Global output write signal, will output before the synchronized write signal
			
			self._constants = []
			fifo_outputs = []
			
			for i in range(input_width):
				self.writes.append(inst(dl.Logic, 1, name="write"))
				#Create FIFO for each input
				#Setting FIFO depth to 32 as that is the max depth of a single MLAB FIFO
				input_fifo = inst(dl.FIFO, bits=bits, depth=32, name="generic_serializer_input_" + str(i) + "_FIFO") 
				self.input_fifos.append(input_fifo)
				input_fifo.w_en.set_driver(self.writes[i])

				fifo_output = inst(dl.Logic, bits, name=("fifo_output_" + str(i)))
				fifo_output.set_driver(input_fifo.r_data)
				fifo_outputs.append(fifo_output)

			#Group up inputs and instantiate a parallel input serial output (PISO) shift register
			parallel_sr_depth = int(input_channels/output_channels)
			parallel_sr_list = []
			
			#Instantiate global counter so we know when PISO finishes each batch of data
			#NOTE: this assumes all input data arrives at the same time. Otherwise you need seperate counters for each individual PISO
			counter = inst(dl.Counter,  reset_value=0, increment_value=1, end_value=(parallel_sr_depth-1), name="sr_counter")
			
			#Create constants for counter values
			for j in range(0,parallel_sr_depth):
				tmp_const = inst(dl.Constant, value=j, bits=counter.get_bit_width(), name=("constant_" + str(j)))
				self._constants.append(tmp_const)
			
			#Need a flop to store init state otherwise we would end up needing an extra cycle in the counter (innefficient performance-wise)
			sr_init = inst(dl.Flop, bits=1, reset_driver=0, name="sr_init")
			one = inst(dl.Constant, value=1, bits=1, name="one")
			zero =  inst(dl.Constant, value=0, bits=1, name="zero")
			
			#This flop is needed because the counter might need to wait in the last stage for a new FIFO value. If that is the case, we need to ensure the last write only happens once while it is waiting
			last_val_stored = inst(dl.Flop, bits=1, name="last_val_stored")
			previous_write_delay = inst(dl.Logic, 1, name=("previous_write_delay"))
			
			#All inputs arrive at the same time so we can just check the first FIFO
			input_fifos_not_empty = inst(dl.NOT, self.input_fifos[0].empty, name="input_fifo_not_empty")
			#Only load if input FIFOs are ready and if either the previous data has been shifted out (i.e. counter is done) or we have not initialized yet
			read_fifo = inst(dl.AND, input_fifos_not_empty, inst(dl.OR, counter.is_done, inst(dl.NOT, sr_init)), name="read_fifo")
			
			#Set init reg and counter increment condition based on first shift register since other ones should all be the same:
			sr_init.set_driver(inst(dl.MuxV2, read_fifo, sr_init, one, name="sr_init_mux"))
			#Start incrementing the counter after input FIFOs are ready . Continue incrementing until counter is reset
			counter.increment_condition.set_driver(inst(dl.AND, sr_init, inst(dl.OR, inst(dl.NOT, counter.is_done), inst(dl.AND, counter.is_done, input_fifos_not_empty)), name="sr_counter_increment_cond"))

			#Instantate parallel load shift registers and output wires
			for i in range(0,input_channels,parallel_sr_depth):
				cur_num = int(i/parallel_sr_depth) #For naming
			
				parallel_sr = inst(dl.ParallelLoadShiftRegister, depth=parallel_sr_depth, bits=bits, name=("parallel_sr_" + str(cur_num)))
				parallel_sr_list.append(parallel_sr)
				
				cur_output=inst(dl.Logic, bits, name=("shift_reg_output_" + str(cur_num)))
				cur_output.set_driver(parallel_sr.sr[-1])
				self.outputs.append(cur_output)
				
				# We must also check if the last write has occured already since the counter could be waiting in the last state for a new value		   
				self.output_writes.append(inst(dl.AND, sr_init.delay(1), inst(dl.NOT, last_val_stored), name=("sr_output_write_"+str(cur_num))))

				for j in range(0,parallel_sr_depth):
					#Assign load values and write value
					parallel_sr.input_data[j].set_driver(fifo_outputs[i+(parallel_sr_depth-1)-j]) #Output is backwards so load input in backwards to ensure correct order
					self.input_fifos[i+j].r_en.set_driver(read_fifo) 
					parallel_sr.loads[j].set_driver(read_fifo, delay=1)

					#Write happens 1 cycle after it's associated counter value. The last write might happen when counter=0 or when the counter is in the last stage if its waiting on a new FIFO value 
					if j == (parallel_sr_depth-1): 
						self.output_actual_writes.append(inst(dl.AND, self.output_writes[cur_num], inst(dl.OR, inst(dl.EQ, self._constants[0], counter.current_value, name=("counter_eq_"+str(i+j))), inst(dl.AND, counter.is_done, previous_write_delay)), name=("actual_write_" + str(i+j)) ))
					else:
						self.output_actual_writes.append(inst(dl.AND, self.output_writes[cur_num], inst(dl.EQ, self._constants[j+1], counter.current_value, name=("counter_eq_"+str(i+j))), inst(dl.NOT, previous_write_delay), name=("actual_write_" + str(i+j)) ))
						

			#Set batch as valid if final value has been written
			self.batch_valid.set_driver(self.output_actual_writes[parallel_sr_depth-1])
			self.control_batch_valid.set_driver(self.instantiate_control_logic(input_channels, output_channels))
			
			#Set to 1 if the last value in each shift register has been stored/outputed 
			set_last_val_stored = inst(dl.MuxV2, inst(dl.AND, counter.is_done, self.output_actual_writes[parallel_sr_depth-1] ), last_val_stored, one, name="set_last_val_stored") 
			last_val_stored.set_driver(inst(dl.MuxV2, inst(dl.OR, counter.is_reset_value, inst(dl.NOT, sr_init)), set_last_val_stored, zero))
			previous_write_delay.set_driver(self.output_actual_writes[parallel_sr_depth-2], delay=1)

			
class ReLU(NNStage):
	kind = "relu"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		input_width = self.get_input_act(dim="width")
		bits, _, output_bits = self.get_bit_widths()
		signed, _, osigned = self.get_signs()
		input_act_bit_spec = self.get_bit_spec("input")
		output_act_bit_spec = self.get_bit_spec("output")
		input_bits = input_act_bit_spec["width"]
		relu_delay = 2
		zero = inst(dl.Constant, value=0, bits=input_bits, signed=osigned, name="zero")
		for i in range(input_width):
			#with dl.ParamScope(name="sign_bit_" + str(i)):
			less_than_0 = self.inputs[i][bits-1]
			non_clipped_output = self.inputs[i][input_bits-1:0]
			relu_mux = inst(dl.MuxV2,
				less_than_0,
				non_clipped_output,
				zero)
			self.outputs[i].set_driver(cast_fixed(relu_mux, input_act_bit_spec, output_act_bit_spec), delay=relu_delay)
			self.outputs_valid[i].set_driver(self.writes[i], delay=relu_delay)

		self.space_to_write_line.set_driver(self.can_write_line, delay=2)
		self.output_valid.set_driver(self.write, delay=relu_delay)
		self.requires_driver = self.writes + [self.write, self.can_write_line]
		self._set_control_valid_to_output_valid_delay(-1)
		self._set_control_to_input_delay(-1)
		
##################################################################################################################
class ReLU6(NNStage):
	kind = "relu6"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		input_width = self.get_input_act(dim="width")
		input_act_bit_spec = self.get_bit_spec("input")
		output_act_bit_spec = self.get_bit_spec("output")
		bits, _, output_bits = self.get_bit_widths()
		input_bits = input_act_bit_spec["width"]
		
		signed, _, osigned = self.get_signs()
		relu_delay = 2
		six = inst(dl.Constant, value=6<<input_act_bit_spec['frac'], bits=bits, signed=signed, name="six")
		zero = inst(dl.Constant, value=0, bits=input_bits, signed=osigned, name="zero")
		
		if input_act_bit_spec['int'] < 3:
			print("WARNING: Input has less than 3 int bits so ReLU6 cannot be instantiated. Using a Regular RELU instead.")

		for i in range(input_width):
			#If we have less than 3 input bits we can't represent 6 so instantiate a regular ReLU mux
			if input_act_bit_spec['int'] < 3:
				less_than_0 = self.inputs[i][bits-1]
				non_clipped_output = self.inputs[i][input_bits-1:0]
				relu_mux = inst(dl.MuxV2,
					less_than_0,
					non_clipped_output,
					zero)
			else:
				less_than_0_greater_than_6 = inst(dl.Concat, self.inputs[i][bits-1],
						self.inputs[i] > six)
				non_clipped_output = self.inputs[i][input_bits-1:0]
				relu_mux = inst(dl.MuxV2,
					less_than_0_greater_than_6,
					non_clipped_output,
					zero,
					six,
					zero)
			self.outputs[i].set_driver(cast_fixed(relu_mux, input_act_bit_spec, output_act_bit_spec), delay=relu_delay)
			self.outputs_valid[i].set_driver(self.writes[i], delay=relu_delay)

		self.space_to_write_line.set_driver(self.can_write_line, delay=2)
		self.output_valid.set_driver(self.write, delay=relu_delay)
		self.requires_driver = self.writes + [self.write, self.can_write_line]
		self._set_control_valid_to_output_valid_delay(-1)
		self._set_control_to_input_delay(-1)

#General class for Swish, Sigmoid and Tanh since they are all implemented in the same way; only the LUT values are different 
class ActivationFunction(NNStage):
	kind = "ActivationFunction"
	reduction_factor = 1 #How much to reduce the number of LUTs by. For example if there are 32 inputs and you reduce by 2, there will only be 16 LUTs (each shared by 2 inputs)
	
	#Source: https://stackoverflow.com/questions/6800193/what-is-the-most-efficient-way-of-finding-all-the-factors-of-a-number-in-python
	def find_factors(self, n):
		step = 2 if n % 2 else 1
		return set(reduce(list.__add__, ([i, n//i] for i in range(1, int(math.sqrt(n))+1, step) if n % i == 0)))
	
	def instantiate(self, node, inst, build_data_path=True, reduction_factor_override=None, **kwargs):
		function_type = node.get_type().lower()
		
		# Allow override of reduction factor for unit tests
		if reduction_factor_override != None: 
			self.reduction_factor = reduction_factor_override
		else:
			#Look for the previous convolution node. This should give us a lower bound for the cycles per values (i.e. max possible throughput)
			# The throughput from a max pooling or bias adds node is always going to be much higher than the convolution node
			prev_node = node.get_input() 
			while prev_node != None and prev_node.get_type() not in ["Conv2D", "DepthwiseConv2dNative"]:
				prev_node = prev_node.get_input()

			# Use the planner estimate to determine a lower bound for cycles per value
			# It may be possible to serialize even more since there usualy is a delay until the next row comes in, however this would be more complicated to compute since the max input FIFO size is 32
			if prev_node != None:
				cycles_per_value = float(prev_node.planner_estimate.row_cycles)/np.shape(prev_node.example_outputs)[-1] # Divide by the number of channels
				#print("cycles_per_value", cycles_per_value, "row_cycles", prev_node.planner_estimate.row_cycles, "shape", np.shape(prev_node.example_outputs) )
				factor_set = self.find_factors(self.get_input_act(dim="width"))
				# Find the maximum possible reduction factor for the given throughput
				cur_max = 1
				for num in factor_set:
					if num > cur_max and float(num) < cycles_per_value: 
						cur_max = num
				self.reduction_factor = 1
				print("Width", self.get_input_act(dim="width"), "Reduction factor", self.reduction_factor)
		
		#Change the bit spec in assign_activation_precisions in GraphBuilder.py
		output_act_bit_spec = self.get_bit_spec("output")
		input_act_bit_spec =  self.get_bit_spec("input")
		
		LUT_range = 2 ** input_act_bit_spec["int"]
		LUT_size = 2 ** input_act_bit_spec["width"]
		LUT_size_half = int(LUT_size/2)
		
		pos_range = np.arange(0,LUT_range,(LUT_range*2)/LUT_size)
		neg_range = np.arange(-LUT_range,0,(LUT_range*2)/LUT_size)
		z = np.zeros(LUT_size)
		z[0:LUT_size_half] = pos_range
		z[LUT_size_half:LUT_size] = neg_range
		activation_lookup_indices = z
		
		activation_lookup = []
		#Generate correct LUT values based on activation function
		if function_type== "sigmoid":
			activation_lookup = list(get_quantized(1 / (1 +  np.exp(-1 * activation_lookup_indices)),
				sign_bits=int(output_act_bit_spec["sign"]),
				integer_bits=output_act_bit_spec["int"],
				fractional_bits=output_act_bit_spec["frac"]))
		elif function_type == "tanh":
			# Tanh(x) = (e^x-e^-x)/(e^x+e^-x) = (e^2x - 1)/(e^2x + 1)
			activation_lookup = list(get_quantized((np.exp(2 * activation_lookup_indices) - 1) / (np.exp(2 * activation_lookup_indices) + 1),
				sign_bits=int(output_act_bit_spec["sign"]),
				integer_bits=output_act_bit_spec["int"],
				fractional_bits=output_act_bit_spec["frac"]))
		else: #Swish
			activation_lookup = list(get_quantized( z / (1 +  np.exp(-1 * activation_lookup_indices)),
			sign_bits=int(output_act_bit_spec["sign"]),
			integer_bits=output_act_bit_spec["int"],
			fractional_bits=output_act_bit_spec["frac"]))
		
		#Warning message since input/output may have different bit specs then preceding nodes
		input_max = str((2 ** input_act_bit_spec["int"]) - 1)
		output_max = str((2 ** output_act_bit_spec["int"]) - 1)
		warnings.warn(function_type + " implementation casts all inputs to " + str(input_act_bit_spec["width"]) + " bits with a range of -" + input_max + " to " + input_max +" and casts the output to " + str(output_act_bit_spec["width"]) + " bits with a range of -" + output_max + " to " + output_max) 
		
		input_width = self.get_input_act(dim="width")
		input_act_bit_spec = self.get_bit_spec("input")
		bits, _, output_bits = self.get_bit_widths()
		signed, _, osigned = self.get_signs()
		
		activation_delay = 2  #mem delay 2
		memory_model_delay = 1
		added_read_delay = activation_delay - memory_model_delay	
		
		#If we don't serialize the input to a smaller number of channels, use the old method for initializing ROM
		if self.reduction_factor == 1:
			for i in range(input_width):
				rom = inst(dl.ROM, bits=output_act_bit_spec["width"], content_list=activation_lookup)
				rom.r_en.set_driver(self.writes[i]) 
				rom.r_addr.set_driver(self.inputs[i])
				self.outputs[i].set_driver(rom.r_data, delay=added_read_delay)
				self.outputs_valid[i].set_driver(rom.r_en, delay=activation_delay)

			self.space_to_write_line.set_driver(self.can_write_line, delay=1)
			self.output_valid.set_driver(self.write, delay=activation_delay)
			self._set_control_valid_to_output_valid_delay(-1)
			self._set_control_to_input_delay(-1)	

		else:
			#serialize input first
			number_LUTs = int(input_width/self.reduction_factor)
			serial = inst(GenericSerializer, input_act_bit_spec["width"], input_width, number_LUTs)
			serial.control_write.set_driver(self.write)

			#Instantiate ROMs. We can use the serializer to reduce the number of ROMs required
			roms = []
			for j in range(number_LUTs):
				rom = inst(dl.ROM, bits=output_act_bit_spec["width"], content_list=activation_lookup)
				rom.r_en.set_driver(serial.output_writes[j]) 
				rom.r_addr.set_driver(serial.outputs[j])
				roms.append(rom)

			#Hook up all the inputs to the serializer and split up the outputs again after
			for i in range(input_width):
				serial.input_fifos[i].w_data.set_driver(self.inputs[i])
				serial.writes[i].set_driver(self.writes[i])

				#Save all outputs to flops first since the next stage expects all values to be written at the same time
				output_flop = inst(dl.Flop, bits=output_act_bit_spec["width"], name=("activation_output_flop_" + str(i)))	
				actual_write_delay = inst(dl.Logic, bits=1, name=("actual_write_delay_" + str(i)))
				actual_write_delay.set_driver(serial.output_actual_writes[i], delay=memory_model_delay)

				rom_id = int(math.floor(i/float(self.reduction_factor)))
				output_flop.set_driver(inst(dl.MuxV2, actual_write_delay, output_flop, roms[rom_id].r_data, name=("output_flop_mux_"+str(i)))) 

				#Hook up output to current flop and the valid signal to the global batch valid so everything is written to memory at once
				self.outputs[i].set_driver(output_flop)
				self.outputs_valid[i].set_driver(serial.batch_valid, delay=(activation_delay))

			self.space_to_write_line.set_driver(self.can_write_line, delay=1)
			self.output_valid.set_driver(serial.control_batch_valid, delay=(activation_delay)) 
			self._set_control_valid_to_output_valid_delay(0)
			self._set_control_to_input_delay(-1)			

		self.requires_driver = self.writes + [self.write, self.can_write_line] 


class BiasAdd(NNStage):
	kind = "bias_add"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		biases = node.values[0]
		input_width = self.get_input_act(dim="width")
		bias_bit_spec = self.get_bit_spec("parameters")
		stored_bits = clog2(max(abs(biases))*2+1)
		#Marius: technically the negative max value can be represented (e.g. for 8-bits it would be -128)
		if min(biases) == -2 ** (bias_bit_spec["int"]+bias_bit_spec["frac"]) and  abs(min(biases)) == max(abs(biases)):
			print("NOTE: the max absolute value stored was negative, and fits in the original bit spec (unlike the positive case)" )
			stored_bits = bias_bit_spec["width"]
		
		bias_bit_spec["int"] -= bias_bit_spec["width"] - stored_bits
		bias_bit_spec["width"] = stored_bits
		input_act_bit_spec = self.get_bit_spec("input")
		output_act_bit_spec = self.get_bit_spec("output")
		
		channels = self.get_input_act(dim="channels")
		physically_mapped = False
		if "physically_mapped_rams" in kwargs:
			physically_mapped = kwargs["physically_mapped_rams"]

		bias_add_cycles = 1
		cast_cycles = 2
		bias_rom = inst(dl.FanoutROM,
			content_list=biases,
			bits=stored_bits,
			physically_mapped=physically_mapped,
			name="bias_rom")

		address_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=channels-1)
		address_counter.set_increment_condition(self.write)
		bias_rom.r_addr.set_driver(address_counter.current_value)
		bias_rom.r_en.set_driver(self.write)

		bias_fanout = [inst(dl.Flop, bits=stored_bits, name="bias_fanout_" + str(i)) for i in range(input_width)]
		bias_fanout_pipe = inst(dl.PipelinedFanout,
			bias_rom.r_data,
			*bias_fanout,
			max_fanout=8,
			name="bias_fanout_pipe")
		self._set_control_to_input_delay(bias_rom.read_delay + bias_fanout_pipe.get_added_delay() + 1)
		self._set_control_valid_to_output_valid_delay(self.get_control_to_input_delay() + bias_add_cycles + cast_cycles)

		self.output_valid.set_driver(self.write)
		for i, (b, a) in enumerate(zip(bias_fanout, self.inputs)):
			b, a, aligned_spec = self.get_aligned_for_add(b, a, bias_bit_spec, input_act_bit_spec)
			added = a + b
			output = cast_fixed(added.delay(bias_add_cycles), aligned_spec, output_act_bit_spec).delay(cast_cycles)
			self.outputs[i].set_driver(output)
			self.outputs_valid[i].set_driver(self.writes[i], delay=bias_add_cycles+cast_cycles)

		self.space_to_write_line.set_driver(self.can_write_line, delay=1)
		self.requires_driver = self.writes + [self.write, self.can_write_line]


		
class MaxPool(NNStage):
	kind = "max_pool"
	def get_instance_comment(self, t="  "):
		comment = "Kernel shape: " + str(self.node.get_kernel_shape()) + "\n"
		comment += "Strides: " + str(self.node.get_strides()) + "\n"
		comment += "Padding: " + self.node.properties["padding"] + ": " + str(self.padding) + "\n"
		return super(MaxPool, self).get_instance_comment(t=t) + v_comment_string(comment, t)
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		parallel_acts_per_ram = node.inputs[0]._from.get_parallel_acts_per_memory()

		k_shape = node.get_kernel_shape()
		kernel_height = k_shape[0]
		kernel_width = k_shape[1]

		stride = node.get_strides()
		vertical_stride = stride[0]
		horizontal_stride = stride[1]

		bits, _, output_bits = self.get_bit_widths()
		input_act_bit_spec = self.get_bit_spec("input")
		output_act_bit_spec = self.get_bit_spec("output")
		signed, _, _ = self.get_signs()
		ia_shape = self.get_input_act().shape
		if type(node.properties["padding"]) is bytes:
			node.properties["padding"] = node.properties["padding"].decode("ascii")
		padding, _ = get_padding(
			ia_shape,
			k_shape,
			vertical_stride,
			node.properties["padding"],
			None)
		self.padding = padding

		input_channels = self.get_input_act(dim="channels")
		input_height = self.get_input_act(dim="height")
		input_width = self.get_input_act(dim="width")

		output_width = self.get_output_act(dim="width")
		output_height = self.get_output_act(dim="height")

		cast_delay = 1
		pre_modulo_addr_add_delay = 2
		address_computation_delay = pre_modulo_addr_add_delay
		sr_write_delay            = 1
		single_addr_delay         = kernel_width
		single_pool_delay         = kernel_width * kernel_height * 2
		fanout_levels = math.ceil(math.log(input_width, 4))

		self.go = inst(dl.Logic, 1, name="go")
		self.can_go = inst(dl.Logic, 1, name="can_go")

		self.ibc = inst(InputBufferController, vertical_stride, kernel_height, input_channels, input_height, padding[1], 1, output_height)
		self.ibc.write.set_driver(self.write)
		self.ibc.done_image.set_driver(self.done)
		self.can_go.set_driver(self.ibc.has_full_kernel)
		self.go.set_driver(inst(dl.AND, self.can_go, self.can_write_line))
		self.ia_buffer = inst(IABuffer,
			bits,
			kernel_height,
			vertical_stride,
			input_channels,
			input_width,
			build_data_path=build_data_path,
			write_port_width=parallel_acts_per_ram, ic_groups=1)
		memory_read_delay = self.ia_buffer.control_delay + self.ia_buffer.read_delay
		self.ia_buffer.w_addrs[0].set_driver(self.ibc.write_address)
		self.ia_buffer.writes[0].set_driver(self.ibc.write_outs[0])
		ram_inputs = self.inputs
		if self.ibc.has_padding:
			padding_zeros = inst(dl.Constant, bits=input_act_bit_spec["width"], value=1<<(input_act_bit_spec["width"]-1), signed=True, name="padding_zeros")
			ram_inputs = [inst(dl.MuxV2, w, padding_zeros, i, name="input_or_padding_"+str(j))for j,(i,w) in enumerate(zip(self.inputs, self.writes))]
		for ri, ia_buffer_w_data in zip(ram_inputs, self.ia_buffer.inputs):
			ia_buffer_w_data.set_driver(ri)

		with dl.ModuleScope(inst(dl.Module, "max_pool_read_controller", kind="max_pool_read_controller")):
			self.pool_multicycle_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=1,
				name="pool_multicycle_counter")
			self.channel_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=input_channels-1,
				name="channel_counter")
			self.x_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=kernel_width-1,
				name="x_counter")
			self.y_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=input_channels,
				end_value=(kernel_height-1) * input_channels,
				name="y_counter")

			self.done_line = inst(dl.AND,
				self.channel_counter.is_done,
				self.y_counter.is_done,
				self.x_counter.is_done,
				self.pool_multicycle_counter.is_done)

			# the read controller will keep track of the current output index
			# so that it knows when we finish an input image.
			output_line_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=output_height-1,
				name="output_line_counter")
			output_line_counter.set_increment_condition(self.done_line.delay(1))

			# The line address counter is the base address to which the pool kernel
			#  address is added.  Basically as the convolution window shifts down
			#  we increment this counter.  Our buffer has limited space though, so
			#  the counter decrements itself by the number of input lines we store
			#  whenever it exceeds the number of lines we store.
			# The second change value handles the edge case of the end of an input
			#  image where we need to add not the stride, but the size of the kernel.
			#  the stride will be added by the first change value, and the difference
			#  between the stride and the kernel size added by the second change 
			#  value.
			line_address_counter = inst(dl.ModuloMultiChangeCounter,
				reset_value=0,
				change_values=[
					input_channels * vertical_stride,
					input_channels * (kernel_height - vertical_stride)],
				modulo_value=input_channels * (vertical_stride + kernel_height),
				end_value=input_channels * (vertical_stride + kernel_height + 2))
			line_address_counter.change_signals[0].set_driver(self.done_line)
			line_address_counter.change_signals[1].set_driver(
				inst(dl.AND, self.done_line, output_line_counter.is_done.delay(1)))

			self.load_sr = inst(dl.AND,
				self.x_counter.is_reset_value,
				self.pool_multicycle_counter.is_reset_value).delay(address_computation_delay + memory_read_delay - fanout_levels)
			self.y_counter.set_increment_condition(
				inst(dl.AND, self.x_counter.is_done, self.pool_multicycle_counter.is_done))
			self.channel_counter.set_increment_condition(
				inst(dl.AND, self.y_counter.is_done, self.x_counter.is_done, self.pool_multicycle_counter.is_done))

			# This is where pre_modulo_addr_add_delay gets added
			# note that each of the inputs is delayed and then the output is delayed for a
			# total of 2 cycles added
			self.r_addr_pre_modulo = ((self.channel_counter.current_value.delay(1) +
				line_address_counter.current_value.delay(1) +
				self.y_counter.current_value.delay(1))).delay(1)
			self.r_addr_pre_modulo.name = "read_address_pre_modulo"

			modulo_value = inst(dl.Constant, input_channels * (vertical_stride + kernel_height), name="modulo_value")
			negative_modulo_value = inst(dl.Constant, bits=self.r_addr_pre_modulo._bit_width, value=-input_channels * (vertical_stride + kernel_height), name="negative_modulo_value")
			zeros = inst(dl.Constant, value=0, bits=negative_modulo_value._bit_width, name="read_addr_change_zeros")
			r_addr_ge_line_count = self.r_addr_pre_modulo >= modulo_value
			change = inst(dl.MuxV2, r_addr_ge_line_count, zeros, negative_modulo_value, name="read_addr_change")
			self.r_addr = (change + self.r_addr_pre_modulo)[self.ia_buffer.r_addrs[0]._bit_width-1:0]
			self.r_addr.name = "read_address"

			states = ["IDLE", "READING_LINE", "DONE_READING_LINE"]
			edges = [
				["IDLE", "READING_LINE", self.go],
				["READING_LINE", "DONE_READING_LINE", self.done_line],
				["DONE_READING_LINE", "IDLE"]]
			self.state_machine = inst(dl.StateMachine, states, edges, None)
			self.pool_multicycle_counter.set_increment_condition(self.state_machine.c["is_reading_line"])
			self.x_counter.set_increment_condition(inst(dl.AND, self.state_machine.c["is_reading_line"], self.pool_multicycle_counter.is_done))

		self.ia_buffer.r_addrs[0].set_driver(self.r_addr)
		self.ia_buffer.read.set_driver(inst(dl.AND, self.state_machine.is_reading_line, self.x_counter.is_reset_value).delay(address_computation_delay))
		self.ibc.finished_reading_line.set_driver(self.state_machine.c["is_done_reading_line"])
		self.ibc.started_reading_line.set_driver(inst(dl.AND, self.go, self.state_machine.c["is_idle"]))
		pooling_sr = inst(dl.ParallelLoadShiftRegister, bits, input_width + np.sum(padding[2]), signed=signed, name="pooling_sr")

		piped_shift_sr_fanouts = inst(dl.PipelinedFanout,
			inst(dl.AND, inst(dl.NOT, self.x_counter.is_reset_value), self.pool_multicycle_counter.is_reset_value).delay(address_computation_delay + memory_read_delay - fanout_levels),
			*[s for s in pooling_sr.shifts],
			levels=fanout_levels,
			name="shift_sr_fanout_pipe")
		piped_load_sr_fanouts = inst(dl.PipelinedFanout,
			self.load_sr,
			*[l for l in pooling_sr.loads],
			levels=fanout_levels,
			name="load_sr_fanout_pipe")

		self.new_pool = inst(dl.AND,
			self.state_machine.c["is_reading_line"],
			self.pool_multicycle_counter.is_reset_value,
			self.x_counter.is_reset_value,
			self.y_counter.is_reset_value).delay(address_computation_delay + memory_read_delay + sr_write_delay - fanout_levels)
		self.new_pool.name = "new_pool"
		zero = inst(dl.Constant, bits=1, value=0)
		with dl.ParamScope(reset_driver=zero):
			self.pool_valid = self.new_pool.delay(single_pool_delay + 1 + cast_delay)
		new_pool_fanout = [inst(dl.Logic, 1, name="new_pool_fanout") for _ in range(output_width)]
		pool_valid_fanout = [inst(dl.Logic, 1, name="pool_valid_fanout") for _ in range(output_width)]

		piped_new_pool_fanouts = inst(dl.PipelinedFanout,
			self.new_pool,
			*new_pool_fanout,
			levels=fanout_levels,
			name="new_pool_fanout_pipe")
		piped_pool_valid_fanouts = inst(dl.PipelinedFanout,
			self.pool_valid,
			*pool_valid_fanout,
			levels=fanout_levels,
			name="pool_valid_fanout_pipe")

		r_data = self.ia_buffer.outputs[0]

		for srdin, r in zip(pooling_sr.input_data[padding[2][0]:padding[2][0]+input_width], r_data):
			srdin.set_driver(r)
		with dl.ModuleScope(pooling_sr):
			for srdin in pooling_sr.input_data[:padding[2][0]] + pooling_sr.input_data[padding[2][0]+input_width:]:
				if signed:
					srdin.set_driver(inst(dl.Constant, value=1<<(srdin._bit_width-1), signed=True, bits=srdin._bit_width))
				else:
					srdin.set_driver(inst(dl.Constant, value=0, bits=srdin._bit_width))

		self.output_valid.set_driver(self.pool_valid)
		self.pooling_flops = []
		for i in range(output_width):
			input_index = kernel_width - 1 + i * stride[1]
			input_reg = inst(dl.Logic, pooling_sr.sr[input_index]._bit_width, pooling_sr.sr[input_index], signed=signed, name="max_input_" + str(i))
			load = pooling_sr.loads[input_index]
			new_load = new_pool_fanout[i]
			#Should have same bits as input otherwise it messes up the cast later on
			self.pooling_flops.append(inst(dl.Flop, bits=input_act_bit_spec["width"], signed=signed, name="pooling_flop"))
			can_load_gt = inst(dl.OR, pooling_sr.shifts[input_index], pooling_sr.loads[input_index]).delay(1)
			can_load_gt.name = "can_load_gt"
			select = inst(dl.OR, inst(dl.AND, can_load_gt, input_reg > self.pooling_flops[i]), new_load, name="select_max_" + str(i)).delay(1)

			self.pooling_flops[i].set_driver(inst(dl.MuxV2, select, self.pooling_flops[i], input_reg, signed=signed))
			# not sure why, but setting the driver is overriding the pooling flop signed property, so I am explicitly
			# resetting it here.
			self.pooling_flops[i].signed = True
			self.outputs[i].set_driver(cast_fixed(self.pooling_flops[i].delay(cast_delay), input_act_bit_spec, output_act_bit_spec))
			self.outputs_valid[i].set_driver(pool_valid_fanout[i])


		self._set_control_to_input_delay(self.ibc.write_counter_increment_delay + self.ia_buffer.control_to_input_delay)
		self._set_control_valid_to_output_valid_delay(fanout_levels)

		self.space_to_write_line.set_driver(self.ibc.space_to_write_line)
		self.requires_driver = self.inputs + [self.write, self.go]
		ibc_is_idle = inst(dl.Logic, bits=1, name="ibc_is_idle")
		ibc_is_idle.set_driver(self.ibc.state_machine.c["is_idle"])
		self.debug_stall = inst(dl.AND,
			ibc_is_idle,
			inst(dl.NOT, dl.Verilog.current_params.kwargs["reset"]),
			inst(dl.NOT, self.space_to_write_line),
			inst(dl.NOT, self.ibc.has_full_kernel),
			inst(dl.NOT, self.state_machine.c["is_reading_line"]))

	def get_custom_verilog_str(self, t="  "):
		verilog = super(MaxPool, self).get_custom_verilog_str(t)
		'''
		verilog += "  always @(posedge clock_0) begin\n"
		verilog += "    if (" + self.debug_stall.name + ") begin\n"
		verilog += '      $display("' + self.name + ' deadlocked because there is no space and it does not have enough lines to write one out %0t", $time);\n'
		verilog += "    end\n"
		#verilog += "    if (!" + self.can_write_line.name + ") begin\n"
		#verilog += '      $display("' + self.name + ' cannot write");\n'
		#verilog += "    end\n"
		verilog += "  end\n"'''
		return verilog

class Placeholder(NNStage):
	"""
	Components:
		we_shifters: This seems to be an absolutely ludicrous way to implement valid signals
		             should probably be re-written

	"""
	kind = "placeholder"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		should_deserialize = node.get_parallel_acts_per_memory(256) <= self.get_output_act(dim="width")
		input_clock = None
		input_reset = None
		if "input_clock" in kwargs:
			input_clock = kwargs["input_clock"]
		if "input_reset" in kwargs:
			input_reset = kwargs["input_reset"]
		#if should_deserialize:
		self._inst_deserializer_new(node, inst, input_clock, input_reset)
		#else:
		#	self._inst_serializer(node, inst)

	def _inst_deserializer_new(self, node, inst, input_clock=None, input_reset=None):
		if input_clock == None:
			input_clock = dl.Verilog.current_params.kwargs["clock"]
		if input_reset == None:
			input_reset = dl.Verilog.current_params.kwargs["reset"]
		output_clock = dl.Verilog.current_params.kwargs["clock"]
		output_reset = dl.Verilog.current_params.kwargs["reset"]

		interface_width = 256
		input_bit_spec = dl.Verilog.current_params.kwargs["input_bit_spec"]
		input_bits = np.sum(list(input_bit_spec.values()))
		parallel_acts_per_ram = interface_width // input_bits
		self.parallel_acts_per_ram = parallel_acts_per_ram

		act_width = self.get_output_act(dim="width")
		act_height = self.get_output_act(dim="height")
		act_channels = self.get_output_act(dim="channels")
		_, _, act_bits = self.get_bit_widths()
		placeholder_bit_spec = self.get_bit_spec("output")
		input_width = parallel_acts_per_ram * input_bits

		scalars_per_transfer = interface_width // input_bits
		shifter_depth = math.ceil(act_width / float(parallel_acts_per_ram))
		self.shifter_depth = shifter_depth


		self.write = inst(dl.Logic, bits=1, name="write")
		self.writes = [self.write]
		self.inputs = [inst(dl.Logic, bits=input_width, name="input_0")]
		self.input_lines = shifter_depth * act_channels * act_height
		self.write_addr = inst(dl.Logic, bits=clog2(self.input_lines-1), name="write_addr")
		input_lines = self.input_lines
		with dl.ParamScope(arithmetic_bit_spec=input_bit_spec):
			input_scalars = [self.inputs[0][input_bits*(i+1)-1:input_bits*i] for i in range(scalars_per_transfer)]

		ram_width = scalars_per_transfer * placeholder_bit_spec["width"]
		max_fan = 4
		end_fan = math.ceil((ram_width * input_lines * 2) / 20000)
		ram_fanout_depth = 0#math.ceil(math.log(end_fan, max_fan))

		with dl.ParamScope(clock=input_clock, reset=input_reset):

			self._build_input_bias_selection(inst, self.write, self.write_addr)
			input_adjustments = dl.Verilog.current_params.kwargs["input_adjustments"]
			pre_scaled_inputs = [self._scale_input(inst, i, input_adjustments["pre_scale"]) for i in input_scalars]
			biased_inputs = [p.delay(2) + self.input_bias for p in pre_scaled_inputs]
			post_scaled_inputs = [self._scale_input(inst, b.delay(1), input_adjustments["post_scale"]) for b in biased_inputs]

			adjusted_inputs = [p.cast(placeholder_bit_spec).delay(2) for p in post_scaled_inputs]
			write_base_selection_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=1,
				name="write_base_selection_counter")
			write_bases = [
				inst(dl.Constant, value=0, name="write_base_1"),
				inst(dl.Constant, value=self.input_lines, name="write_base_2")]

			current_write_base = inst(dl.MuxV2,
				write_base_selection_counter.current_value,
				*write_bases)
			ram_write_addr = current_write_base + self.write_addr.delay(1)

			ram_write = self.write.delay(5)
			ram_write_addr_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=self.input_lines*2-1,
				name="ram_write_addr_counter")
			ram_write_addr_counter.set_increment_condition(ram_write)
			"""full_input_being_written = inst(dl.AND, ram_write.delay(2),
				inst(dl.OR,
					inst(dl.EQ, ram_write_addr_counter.current_value.delay(2), input_lines-1),
					inst(dl.EQ, ram_write_addr_counter.current_value.delay(2), input_lines*2-1)))
			input_start_writing = inst(dl.AND, ram_write.delay(2),
				inst(dl.OR,
					inst(dl.EQ, ram_write_addr_counter.current_value.delay(2), 0),
					inst(dl.EQ, ram_write_addr_counter.current_value.delay(2), input_lines)))"""

			full_input_being_written = inst(dl.AND, self.write.delay(2),
					inst(dl.EQ, self.write_addr.delay(2), input_lines-1))
			input_start_writing = inst(dl.AND, self.write.delay(2),
					inst(dl.EQ, self.write_addr.delay(2), 0))
			write_base_selection_counter.set_increment_condition(full_input_being_written)

			ram_input = inst(dl.Concat, *adjusted_inputs, name="concatenated_inputs").delay(0)#, max_fan=max_fan, final_fan=end_fan)
			ram_write_addr = ram_write_addr.delay(4)
			#ram_write = ram_write.delay(ram_fanout_depth)#, max_fan=max_fan, final_fan=end_fan)
			#ram_write_addr = ram_write_addr_counter.current_value.delay(0)#, max_fan=max_fan, final_fan=end_fan)


		new_input_cdc = inst(dl.SingleCycleControlDomainCrossing, input_clock, output_clock, clock_a_reset=input_reset, clock_b_reset=output_reset)
		new_input_cdc.control_in.set_driver(full_input_being_written)
		have_input = new_input_cdc.control_out

		start_input_cdc = inst(dl.SingleCycleControlDomainCrossing, input_clock, output_clock, clock_a_reset=input_reset, clock_b_reset=output_reset)
		start_input_cdc.control_in.set_driver(input_start_writing)
		new_input_writing = start_input_cdc.control_out

		read_enable = inst(dl.Logic, bits=1, name="read_enable")
		read_address_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=self.input_lines*2-1,
			name="read_address_counter")
		read_address_counter.set_increment_condition(read_enable)

		input_counter = inst(dl.UpDownCounter,
			reset_value=0,
			increment_value=1,
			decrement_value=-1,
			end_value=2,
			name="input_counter")
		input_counter.set_increment_condition(have_input.delay(3))
		input_counter.set_decrement_condition(inst(dl.AND,
			read_enable.delay(1),
			inst(dl.OR,
				inst(dl.EQ, read_address_counter.current_value, self.input_lines - shifter_depth).delay(1),
				inst(dl.EQ, read_address_counter.current_value, self.input_lines*2 - shifter_depth).delay(1))))

		partial_input_counter = inst(dl.UpDownCounter,
			reset_value=0,
			increment_value=1,
			decrement_value=-1,
			end_value=2,
			name="partial_input_counter")
		partial_input_counter.set_increment_condition(new_input_writing.delay(3))
		partial_input_counter.set_decrement_condition(inst(dl.AND,
			read_enable.delay(1),
			inst(dl.OR,
				inst(dl.EQ, read_address_counter.current_value, self.input_lines - 1).delay(1),
				inst(dl.EQ, read_address_counter.current_value, self.input_lines*2 - 1).delay(1))))

		transfers_per_channel_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=shifter_depth-1,
			name="transfers_per_channel_counter")
		transfers_per_channel_counter.set_increment_condition(read_enable.delay(2))

		self.ram = inst(dl.FanoutRAM,
			depth=self.input_lines*2,
			bits=ram_input._bit_width,
			read_clock=output_clock,
			write_clock=input_clock,
			name="input_buffer")
		self.ram.w_en.set_driver(ram_write)
		self.ram.w_addr.set_driver(ram_write_addr)
		self.ram.r_addr.set_driver(read_address_counter.current_value)#, max_fan=max_fan, final_fan=end_fan, final_fan_of_1=True))
		self.ram.r_en.set_driver(read_enable)#, max_fan=max_fan, final_fan=end_fan, final_fan_of_1=True))
		self.ram.w_data.set_driver(ram_input)

		# Quartus removes the last stage of our address fanout and renames it to a register packed into the M20Ks, so we set
		# these settings to manually duplicate that register... which shockingly works :)
		#get_sdc_filter_str = lambda ab: "*|" + "|".join([o.name + "_i" for o in reversed(self.ram.get_owner_list()[:-2])]) + "|*|address_reg_" + ab + "[*]"
		#dl.Verilog.current_circuit.add_quartus_setting_lambda(
		#	lambda: "set_instance_assignment -name DUPLICATE_REGISTER " + str(math.ceil(end_fan/max_fan)) + " -to " + '"' + get_sdc_filter_str("a") + '"')
		#dl.Verilog.current_circuit.add_quartus_setting_lambda(
		#	lambda: "set_instance_assignment -name DUPLICATE_REGISTER " + str(math.ceil(end_fan/max_fan)) + " -to " + '"' + get_sdc_filter_str("b") + '"')

		output_data_shifter = inst(dl.BasicShiftRegister,
			bits=self.ram.r_data._bit_width,
			depth=shifter_depth,
			name="output_data_shifter")
		output_data_shifter.d.set_driver(self.ram.r_data)
		individuals = self.get_individual_values_from_combined(output_data_shifter.sr, True)
		for i, o in enumerate(reversed(individuals)):
			shifter_index = i // parallel_acts_per_ram
			self.outputs[i].set_driver(o)
		output_will_be_valid = inst(dl.EQ, transfers_per_channel_counter.current_value, 1).delay(self.ram.read_delay-2)
		self.output_valid.set_driver(output_will_be_valid)
		inst(dl.PipelinedFanout,
			output_will_be_valid,
			*self.outputs_valid,
			levels=shifter_depth-1,
			name="output_valid_fanout")


		self.have_enough_data_to_read_one_line = (input_counter.current_value > 0).delay(2)
		self.done_line = inst(dl.Logic, 1, name="done_line")
		self.within_line_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=act_channels*shifter_depth-1,
			name="within_line_counter")
		self.within_line_counter.set_increment_condition(read_enable)
		self.done_line.set_driver(self.within_line_counter.done_and_incrementing)

		# When the can_write_line signal is asserted and the placeholder has enough data to write a full
		# set of input channels to the next layer, it can move to WRITING_LINE, where it stays until it
		# finishes one full set of input channels.  It then goes to WAITING_PERIOD, which is simply a state
		# where it waits before it is allowed to check self.can_write_line again.  This waiting period
		# is necessary for Placeholder because there are usually far fewer input channels in the first
		# layer, thus there is not enough time to allow the can_write_line signal to update
		self.waiting_period_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=10,
			name="waiting_period_counter")
		states = ["IDLE", "WRITING_LINE", "WAITING_PERIOD"]
		edges = [
			["IDLE", "WRITING_LINE", inst(dl.AND, self.can_write_line, self.have_enough_data_to_read_one_line)],
			["WRITING_LINE", "WAITING_PERIOD", self.done_line],
			["WAITING_PERIOD", "IDLE", self.waiting_period_counter.is_done]]
		self.state_machine = inst(dl.StateMachine, states, edges, None)
		self.waiting_period_counter.set_increment_condition(self.state_machine.c["is_waiting_period"].delay(1))
		read_enable.set_driver(self.state_machine.c["is_writing_line"])

		self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")
		self.space_to_write_line.set_driver(inst(dl.NOT, inst(dl.EQ, partial_input_counter.current_value, 2)), delay=2)

		self._set_control_to_input_delay(0)
		self._set_control_valid_to_output_valid_delay(shifter_depth-1)
		self.guarantees_output_valid_low_in_cycles = False

		self.requires_driver = [self.write, self.can_write_line]#, self.write_address]


	def _scale_input(self, inst, _input, scale):
		input_adjustments = dl.Verilog.current_params.kwargs["input_adjustments"]
		scale_fractional_rescale = 2 ** input_adjustments["bit_spec"]["frac"]
		scale_signed = input_adjustments["bit_spec"]["sign"] == 1
		scale_bits = np.sum(list(input_adjustments["bit_spec"].values()))

		scale_constant = inst(dl.Constant,
			bits=scale_bits,
			value=int(scale * scale_fractional_rescale),
			signed=scale_signed,
			arithmetic_bit_spec=input_adjustments["bit_spec"],
			name="input_adjustment_scale")
		return inst(dl.MUL, _input, scale_constant)


	def _build_input_bias_selection(self, inst, new_input, input_address):
		interface_width = 256

		act_width = self.get_output_act(dim="width")
		act_height = self.get_output_act(dim="height")
		act_channels = self.get_output_act(dim="channels")

		input_bit_spec = dl.Verilog.current_params.kwargs["input_bit_spec"]
		input_bits = np.sum(list(input_bit_spec.values()))
		scalars_per_transfer = interface_width // input_bits
		transfers_per_channel = math.ceil(act_width / scalars_per_transfer)

		input_adjustments = dl.Verilog.current_params.kwargs["input_adjustments"]
		input_bias_ram_data = input_adjustments["channel_biases"]
		bias_fractional_rescale = 2 ** input_adjustments["bit_spec"]["frac"]
		bias_signed = input_adjustments["bit_spec"]["sign"] == 1
		bias_bits = np.sum(list(input_adjustments["bit_spec"].values()))

		is_p2 = is_power_of_2(len(input_bias_ram_data))
		if is_p2:
			hi = int(math.log2(float(len(input_bias_ram_data))))
			if hi == 0:
				current_bias_index = inst(dl.Constant, bits=1, value=0, name="current_bias_index")
			else:
				current_bias_index = input_address[hi-1:0].delay(2)
		else:
			content_list = np.arange(len(input_bias_ram_data))[...,np.newaxis]
			content_list = np.tile(content_list, [math.ceil(act_height * act_channels / len(input_bias_ram_data)), transfers_per_channel])
			content_list = np.reshape(content_list, [-1]).tolist()
			index_rom = inst(dl.ROM, content_list=content_list, bits=clog2(len(input_bias_ram_data)))

			index_rom.r_addr.set_driver(input_address)
			index_rom.r_en.set_driver(new_input)
			current_bias_index = index_rom.r_data.delay(1)

		with dl.ParamScope(arithmetic_bit_spec=input_adjustments["bit_spec"]):
			self.input_bias_mux = inst(dl.MuxV2,
				current_bias_index,
				*[inst(dl.Constant, value=int(v * bias_fractional_rescale), bits=bias_bits, signed=bias_signed, name="channel_bias_" + str(i))
					for i,v in enumerate(input_adjustments["channel_biases"])],
				name="input_bias_mux")

		with dl.ParamScope(name="input_bias", arithmetic_bit_spec=input_adjustments["bit_spec"]):
			self.input_bias = self.input_bias_mux.delay(0)





	def _inst_deserializer(self, node, inst, input_clock=None):
		if input_clock == None:
			input_clock = dl.Verilog.current_params.kwargs["clock"]
		parallel_acts_per_ram = node.get_parallel_acts_per_memory(256)
		act_width = self.get_output_act(dim="width")
		act_height = self.get_output_act(dim="height")
		act_channels = self.get_output_act(dim="channels")
		_, _, act_bits = self.get_bit_widths()
		buffer_width = parallel_acts_per_ram * act_bits

		shifter_depth = math.ceil(act_width / float(parallel_acts_per_ram))
		control_loop_cycles = (shifter_depth - 1) // 2
		read_delay = 2
		self.shifter_depth = shifter_depth
		self.parallel_acts_per_ram = parallel_acts_per_ram

		self.write = inst(dl.Logic, bits=1, name="write")
		self.writes = [self.write]
		self.inputs = [inst(dl.Logic, bits=buffer_width, name="input_0")]
		self.input_lines = shifter_depth * act_channels * act_height


		with dl.ParamScope(bits=1, signed=False):
			self.writing_line = inst(dl.Logic, name="writing_line")
			self.we_shifters  = [inst(dl.ShiftRegister, depth=(shifter_depth-i)+1, name="we_shifter") for i in range(shifter_depth)]
			self.next_we      = inst(dl.ShiftRegister, depth=shifter_depth, name="next_we")
			self.we_s         = [inst(dl.AND, self.writing_line, f, name="write_enable") for f in self.next_we.sr]
			self.next_we.d.set_driver(self.next_we.q)
			self.output_valid.set_driver(self.next_we.q)
			for shifter in self.we_shifters:
				with dl.ModuleScope(shifter):
					zero = inst(dl.Constant, 0, name="zero")
					for flop in shifter.sr:
						flop.set_reset_driver(zero)
			with dl.ModuleScope(self.next_we):
				one = inst(dl.Constant, 1, name="one")
				zero = inst(dl.Constant, 0, name="zero")
				for i,flop in enumerate(self.next_we.sr):
					if i == 0:
						flop.set_reset_driver(one)
					else:
						flop.set_reset_driver(zero)
		self.input_ram = inst(dl.FIFO,
			bits=buffer_width,
			depth=shifter_depth * act_channels * act_height * 2,
			almost_empty=shifter_depth*act_channels-1,
			almost_full=shifter_depth * act_channels * act_height-1,
			name="input_buffer")
		self.write_address = inst(dl.Logic, bits=self.input_ram.write_ptr.current_value._bit_width)
		self.write_address.set_driver(self.input_ram.write_ptr.current_value)
		self.input_ram.w_en.set_driver(self.write)
		self.input_ram.w_data.set_driver(self.inputs[0])

		with dl.ParamScope(bits=buffer_width):
			self.write_data = self.input_ram.w_data
			self.input_data_shifter = inst(dl.ShiftRegister, depth=shifter_depth, name="input_data_shifter")
		for we_shifter, we in zip(self.we_shifters, self.we_s):
			we_shifter.d.set_driver(we)

		individuals = self.get_individual_values_from_combined(self.input_data_shifter.sr, True)
		for i, o in enumerate(reversed(individuals)):
			shifter_index = i // parallel_acts_per_ram
			self.outputs[i].set_driver(o)
			self.outputs_valid[i].set_driver(self.we_shifters[shifter_index].q)

		self.have_enough_data_to_compute_one_line = inst(dl.NOT, self.input_ram.almost_empty).delay(2)
		self.done_line = inst(dl.Logic, 1, name="done_line")
		self.input_channel_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=act_channels-1, name="input_channel_counter")
		self.input_channel_counter.set_increment_condition(self.next_we.sr[shifter_depth-1-control_loop_cycles])
		self.done_line.set_driver(self.input_channel_counter.done_and_incrementing)

		states = ["IDLE", "WRITING_LINE"]
		edges = [
			["IDLE", "WRITING_LINE", inst(dl.AND, self.can_write_line, self.have_enough_data_to_compute_one_line)],
			["WRITING_LINE", "IDLE", self.done_line]]


		self.state_machine = inst(dl.StateMachine, states, edges, None)
		with dl.ParamScope(reset_driver=inst(dl.Constant, bits=1, value=0)):
			is_writing_line = self.state_machine.c["is_writing_line"].delay(control_loop_cycles)
		self.next_we.should_shift.set_driver(is_writing_line)
		self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")
		self.space_to_write_line.set_driver(inst(dl.NOT, self.input_ram.almost_full), delay=2)
		self.input_ram.r_en.set_driver(is_writing_line)
		self.input_data_shifter.d.set_driver(self.input_ram.r_data)
		self.writing_line.set_driver(is_writing_line)

		self._set_control_to_input_delay(read_delay)
		self._set_control_valid_to_output_valid_delay(read_delay)
		self.guarantees_output_valid_low_in_cycles = False

		self.requires_driver = [self.write, self.write_data, self.can_write_line]#, self.write_address]

	def _inst_serializer(self, node, inst):
		parallel_acts_per_ram = node.get_parallel_acts_per_memory(256)
		act_width = self.get_output_act(dim="width")
		parallel_acts_per_ram -= parallel_acts_per_ram % act_width
		act_height = self.get_output_act(dim="height")
		act_channels = self.get_output_act(dim="channels")
		_, _, act_bits = self.get_bit_widths()
		buffer_width = parallel_acts_per_ram * act_bits

		lines_per_group = parallel_acts_per_ram // act_width

		groups_per_oc_group = math.ceil(act_channels / float(lines_per_group))

		read_delay = 2
		self.shifter_depth = lines_per_group
		self.groups_per_oc_group = groups_per_oc_group
		self.parallel_acts_per_ram = parallel_acts_per_ram

		self.write = inst(dl.Logic, bits=1, name="write")
		self.writes = [self.write]
		self.inputs = [inst(dl.Logic, bits=buffer_width, name="input_0")]


		self.input_ram = inst(dl.FIFO,
			bits=buffer_width,
			depth=groups_per_oc_group * act_height * 2,
			almost_empty=groups_per_oc_group - 1,
			almost_full=groups_per_oc_group * act_height - 1,
			name="input_buffer")
		self.input_ram.w_en.set_driver(self.write)
		self.input_ram.w_data.set_driver(self.inputs[0])

		self.write_data = self.input_ram.w_data
		serialization_sr = inst(dl.ParallelLoadShiftRegister,
			depth=lines_per_group,
			bits=act_width*act_bits,
			name="serialization_sr")
		self.is_idle = inst(dl.Logic, bits=1, name="is_idle")
		self.is_writing_line = inst(dl.Logic, bits=1, name="is_writing_line")
		with dl.ParamScope(reset=self.is_idle):
			shift_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=len(serialization_sr.input_data)-1,
				name="shift_counter")
			shift_counter.set_increment_condition(self.is_writing_line)
		oc_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=act_channels-1,
			name="oc_counter")
		oc_counter.set_increment_condition(self.is_writing_line)

		self.should_read = inst(dl.AND, self.is_writing_line.delay(2), shift_counter.is_reset_value.delay(2))
		self.should_write_out = inst(dl.AND, self.is_writing_line, shift_counter.is_reset_value.delay(2))
		self.should_load_sr = self.should_read.delay(read_delay)
		line_width = act_width*act_bits
		for i,(s,l,d) in enumerate(zip(serialization_sr.shifts, serialization_sr.loads, serialization_sr.input_data)):
			d.set_driver(self.input_ram.r_data[(i+1)*line_width-1:i*line_width].delay(read_delay-1))
			l.set_driver(self.should_load_sr)


		self.have_enough_data_to_compute_one_line = inst(dl.NOT, self.input_ram.almost_empty).delay(2)
		self.done_line = inst(dl.Logic, 1, name="done_line")
		self.done_line.set_driver(oc_counter.is_done)

		states = ["IDLE", "WRITING_LINE"]
		edges = [
			["IDLE", "WRITING_LINE", inst(dl.AND, self.can_write_line, self.have_enough_data_to_compute_one_line)],
			["WRITING_LINE", "IDLE", self.done_line]]


		self.state_machine = inst(dl.StateMachine, states, edges, None)
		self.is_writing_line.set_driver(self.state_machine.c["is_writing_line"])
		self.is_idle.set_driver(self.state_machine.c["is_idle"])
		self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")
		self.space_to_write_line.set_driver(inst(dl.NOT, self.input_ram.almost_full), delay=2)
		self.input_ram.r_en.set_driver(self.should_read)

		self._set_control_to_input_delay(read_delay)
		self._set_control_valid_to_output_valid_delay(2+read_delay)
		self.guarantees_output_valid_low_in_cycles = False
		self.output_valid.set_driver(self.is_writing_line)
		for v in self.outputs_valid:
			v.set_driver(self.is_writing_line.delay(2+read_delay+1))
		for i,o in enumerate(self.outputs):
			o.set_driver(serialization_sr.sr[-1][(i+1)*act_bits-1:i*act_bits])

		self.requires_driver = [self.write, self.write_data, self.can_write_line]#, self.write_address]

	def get_instance_comment(self, t="  "):
		comment = "Shifter Depth: " + str(self.shifter_depth) + "\n"
		comment += "Parallel Acts Per RAM: " + str(self.parallel_acts_per_ram) + "\n"
		return super(Placeholder, self).get_instance_comment(t=t) + v_comment_string(comment, t=t)

	def get_custom_verilog_str(self, t="  "):
		verilog = super(Placeholder, self).get_custom_verilog_str(t)
		'''
		verilog += "  always @(posedge clock_0) begin\n"
		verilog += "    if (" + self.output_valid.name + "|| " + self.write.name + ") begin\n"
		verilog += '      $display("%d", ' + self.stored_lines_counter_current_value.name + ');\n'
		verilog += "    end\n"
		verilog += "  end\n"'''
		return verilog

class SequentialDistributedActivationBuffer(dl.Module):
	"""
	A type of input activation buffer that saves registers and routing
	resources by serializing the fanout of control and data signals
	to the individual buffers.

	Essentially this activation buffer takes control signals and
	fans them out like this:
	::
		R indicates a register
		M indicates a memory

		w_addr->-R--R--R--R--R--R
		         |  |  |  |  |  |
		         v  v  v  v  v  v
		         M  M  M  M  M  M
	
	Instead of like this:
	::
		                 |-R-->M
		                 |-R-->M
		            |-R--|-R-->M
		w_addr->-R--|
		            |-R--|-R-->M
		                 |-R-->M
		                 |-R-->M

	The sequential method is a much more efficient way to distribute inputs
	to the memories, but it can't be used in all situations.  The primary
	issue is that our memories receive inputs at different times.  In the
	context of our convolution module this means we can't use the sequential
	method if the memories have x muxes at the outputs.  It also means we
	need additional hardware (shift registers) at the outputs of the DSP
	chains to re-align the output data.

	In general this method is less flexible, so it should really only be
	used when we really need it.  In the case where our activation buffers
	are built with MLABs we see that we run out of registers and fast wiring
	resources around the activation buffers.  There are a lot of reasons why
	this happens.  Primarily:
		* We tend to use MLABs when we unroll a lot of input channels
		this spreads out the DSPs fed by memories that share the same
		control signals.  This means that we need more registers or
		more high speed wires.
		* The MLABs we use take up LABs that could previously have been
		used for registers
		* In some layers with a small number of weights (and in circuits
		that use almost all of the available memory blocks) the M20Ks
		get used as buffers for other layers, meaning that the region
		needs registers and wiring for completely unrelated control
		logic.

	"""
	def __init__(self, bits_per_act, kernel_height, stride, input_channels, activation_width, write_port_width, ic_groups=1, min_buffer_depth=1, activation_group_size=1, **kwargs):
		super(SequentialDistributedActivationBuffer, self).__init__("sequential_distributed_activation_buffer", kind="sequential_distributed_activation_buffer", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			num_buffered_lines = max(kernel_height + stride, min_buffer_depth)
			n_channels = [math.ceil(input_channels / float(ic_groups)) for _ in range(input_channels % ic_groups)]
			n_channels += [input_channels // ic_groups for _ in range(ic_groups - (input_channels % ic_groups))]
			total_buffer_depths = [n_channels[i] * num_buffered_lines for i in range(ic_groups)]
			buffer_specs = [dl.RAM.size_for_depth(bd) for bd in total_buffer_depths]
			buffer_depths = [[min(bd, (i * bs[0] % bd - 1) % bs[0] + 1) for i in range(math.ceil(bd / bs[0]))] for bd,bs in zip(total_buffer_depths, buffer_specs)]
			buffer_depths = [[np.sum(buffer_depth_list)] for buffer_depth_list in buffer_depths]
			buffer_widths = []
			has_greater_than_32 = max([bdl[0] for bdl in buffer_depths]) > 32
			if has_greater_than_32:
				output_delay = 1
			else:
				output_delay = 0
			for bs in buffer_specs:
				if bs[0] <= 64:
					buffer_widths.append([bits_per_act * activation_group_size] * activation_width)
				else:
					print("bs", bs, bs[0])
					print(buffer_depths)
					print(min_buffer_depth)
					print(buffer_specs)
					raise Exception("SequentialDistributedActivationBuffer only supports buffers 64 words deep or less.  Exiting.")
					warnings.warn("SequentialDistributedActivationBuffer only supports buffers 64 words deep or less.  Exiting.")
					sys.exit(2)

			max_offset = min(8, activation_width)
			self.max_offset = max_offset
			parallel_sequential_fanouts = math.ceil(activation_width / max_offset)
			self.parallel_sequential_fanouts = parallel_sequential_fanouts

			self.inputs = [inst(dl.Logic, bits=bits_per_act, name="ia_buffer_write_data_" + str(j)) for j in range(activation_width)]
			self.writes = [inst(dl.Logic, bits=1, name="write_ic_group_" + str(i)) for i in range(ic_groups)]
			self.read = inst(dl.Logic, bits=1, name="read")
			self.outputs = [[inst(dl.Logic, bits=bits_per_act, name="ia_buffer_read_data_ic_group_" + str(i) + "_" + str(j)) for j in range(activation_width)] for i in range(ic_groups)]

			address_bits = [clog2(bd[0]) for bd in buffer_depths]
			self.w_addrs = [inst(dl.Logic, bits=ab, name="w_addr_ic_group_" + str(i)) for i, ab in enumerate(address_bits)]
			self.r_addrs = [inst(dl.Logic, bits=ab, name="r_addr_ic_group_" + str(i)) for i, ab in enumerate(address_bits)]
			self.outputs = [[inst(dl.Logic, bits=bits_per_act, name="ia_buffer_read_data_ic_group_" + str(i) + "_" + str(j)) for j in range(activation_width)] for i in range(ic_groups)]

			#:
			#: The output of input buffer controller is a list of write signals
			#: with at most 1 single write signal set to 1, so we will select 0
			#: whenever the write signal is 0 and select the address whenever
			#: the write signal is 1.  This way we can reduce all of the address
			#: signals to a single write address that can be shared across
			#: multiple activation buffers that will not be written at the same
			#: time.
			#:
			#: w_addr_A  w_addr_B ... w_addr_G
			#:     |         |            |
			#:   0 |       0 |          0 |
			#:   |_|       |_|          |_|
			#:  \___/-w_A \___/-w_B    \___/-w_G
			#:    \         |            /
			#:     \_______ | __________/
			#:             |||
			#:             \OR/
			#:              |
			#:           w_addr, d=bor_stages
			#:              |
			#:              V

			bor_stages = 1
			selected_w_addrs = [inst(dl.MuxV2, w, 0, wa).delay(bor_stages) for i, (w, wa) in enumerate(zip(self.writes, self.w_addrs))]
			to_or = selected_w_addrs
			while len(to_or) > 1:
				next_to_or = []
				bor_stages += 1
				for i in range(0, len(to_or), 6):
					if i == (len(to_or) - 1):
						next_to_or.append(to_or[-1].delay(1))
					else:
						next_to_or.append(inst(dl.BOR, *to_or[i:i+6]).delay(1))
				to_or = next_to_or
			w_addr = to_or[0]

			#: Letters are input channel groups
			#: Numbers are activation x indices
			#: 
			#: Input Activation Labeled by Input Channel Group
			#:        _____________
			#:       |B B B B B B B|
			#:      _____________ B|
			#:     |B B B B B B B|B|
			#:    _____________ B|B|
			#:   |A A A A A A A|B|B|
			#:  _____________ A|B|B|
			#: |A A A A A A A|A|B|B|----/
			#: |A A A A A A A|A|B|     /--- input channel group B
			#: |A A A A A A A|A|B|----/
			#: |A A A A A A A|A|
			#: |A A A A A A A|A|----/
			#: |A A A A A A A|     /--- input channel group A
			#: |A A A A A A A|----/
			#:
			#:           w_addr, d=bor_stages
			#:              |
			#:              V
			#:              |-----------------------------|
			#:              |                             |
			#:  D1          |__        D2 ... DN          |
			#:  |              |        |     |           |
			#:  | r_addr_A-v---|--v-----|-...-|-------v   |
			#:  |          |   |  |     |     |       |   |
			#:  |         |A1| | |A2|   |     |      |AN| | <-w_A, w_data, d=1
			#:  v----------||--v--||----v     v-------||--v
			#:  |         |B1| | |B2|   |     |      |BN| | <-w_B, w_data, d=1
			#:  |          |   |  |     |     |       |   |
			#:  | r_addr_B-^---|--^-----|-...-|-------^   |
			#:  | r_addr_C-v---|--v-----|-...-|-------v   |
			#:  |          |   |  |     |     |       |   |
			#:  |         |C1| | |C2|   |     |      |CN| | <-w_C, w_data, d=2
			#:  v----------||--v--||----v     v-------||--v
			#:  |         |D1| | |D2|   |     |      |DN| | <-w_D, w_data, d=2
			#:  |          |   |  |     |     |       |   |
			#:  | r_addr_D-^---|--^-----|-...-|-------^   |
			#:  | r_addr_E-v---|--v-----|-...-|-------v   |
			#:  |          |   |  |     |     |       |   |
			#:  |         |E1| | |E2|   |     |      |EN| | <-w_E, w_data, d=3
			#:  v----------||--v--||----v     v-------||--v
			#:  |         |F1| | |F2|   |     |      |FN| | <-w_F, w_data, d=3
			#:  |          |   |  |     |     |       |   |
			#:  | r_addr_F-^---|--^-----|-...-|-------^   |
			#:  | r_addr_G-v---|--v-----|-...-|-------v   |
			#:  |          |   |  |     |     |       |   |
			#:  |         |G1| | |G2|   |     |      |GN| | <-w_G, w_data, d=4
			#:  v----------||--v--||----v     v-------||--v
			#:             
			#: With this structure we can share routing between unrelated
			#: write data, write addresses, and read addresses.  We cannot share write
			#: signals, so the write signals for each input channel group must be 
			#: fanned out to match the delay for each of those input channel groups.

			r_addr_to_chain_din_delay = 1
			r_addr_shift_registers = []
			for ic_group, r_addr in enumerate(self.r_addrs):
				parallel_chains = []
				for chain in range(parallel_sequential_fanouts):
					parallel_chains.append(inst(
						dl.BasicShiftRegister, r_addr._bit_width, max_offset, name="r_addr_"+str(ic_group)+"_pipe_"+str(chain)))
					parallel_chains[-1].d.set_driver(r_addr.delay(r_addr_to_chain_din_delay))
				r_addr_shift_registers.append(parallel_chains)

			n_w_addr_chains = activation_width
			fanout_per_w_addr_chain_reg = 6
			w_addr_chain_length = math.ceil(ic_groups / fanout_per_w_addr_chain_reg)
			w_addr_chain_dins = [inst(dl.Logic, bits=w_addr._bit_width, name="w_addr_chain_din_" + str(i)) for i in range(n_w_addr_chains)]
			w_addr_to_w_addr_chain_din_fanout_pipe = inst(dl.PipelinedFanout,
				w_addr,
				*w_addr_chain_dins,
				max_fanout=4,
				name="w_addr_to_w_addr_chain_din_fanout_pipe")
			w_addr_chains = []
			w_data_chains = []
			for act, w_data in enumerate(self.inputs):
				w_data_chains.append(inst(
					dl.BasicShiftRegister, w_data._bit_width, w_addr_chain_length, name="w_data_chain_"+str(act)))
				w_data_chains[-1].d.set_driver(w_data)
			for chain, din in enumerate(w_addr_chain_dins):
				w_addr_chains.append(inst(
					dl.BasicShiftRegister, w_addr._bit_width, w_addr_chain_length, name="w_addr_chain_"+str(chain)))
				w_addr_chains[-1].d.set_driver(din)

			w_addr_to_w_addr_chain_din_fanout_levels = w_addr_to_w_addr_chain_din_fanout_pipe.get_levels()
			write_enable_fanout_levels = w_addr_to_w_addr_chain_din_fanout_levels
			self.control_delay = r_addr_to_chain_din_delay + 4 + output_delay
			self.control_to_input_delay = write_enable_fanout_levels + bor_stages
			self.read_delay = max_offset - 2
			we_s = [[inst(dl.Flop, bits=1, name="write_ic_group_" + str(ic_group) + "_act_" + str(activation)) for activation in range(activation_width)] for ic_group in range(ic_groups)]
			for i, (we_group, we) in enumerate(zip(we_s, self.writes)):
				inst(dl.PipelinedFanout,
					we.delay(i // fanout_per_w_addr_chain_reg + bor_stages),
					*we_group,
					levels=write_enable_fanout_levels,
					name="write_enable_fanout_pipe_" + str(i))

			for ic_group in range(ic_groups):
				for act_number in range(activation_width):
					ram = inst(dl.RAM,
						bits=bits_per_act,
						depth=buffer_depths[ic_group][0],
						name="ram_icg_" + str(ic_group) + "_act_" + str(act_number))
					ram.w_en.set_driver(we_s[ic_group][act_number])
					ram.w_addr.set_driver(w_addr_chains[act_number].sr[ic_group//fanout_per_w_addr_chain_reg][ram.w_addr._bit_width-1:0])
					ram.r_addr.set_driver(r_addr_shift_registers[ic_group][act_number // max_offset].sr[act_number % max_offset].delay(1))
					ram.w_data.set_driver(w_data_chains[act_number].sr[ic_group//fanout_per_w_addr_chain_reg])
					self.outputs[ic_group][act_number].set_driver(ram.r_data.delay(output_delay))


class IABuffer(dl.Module):
	def __init__(self, bits_per_act, kernel_height, stride, input_channels, activation_width, write_port_width, ic_groups=1, min_buffer_depth=1, **kwargs):
		super(IABuffer, self).__init__("ia_buffer", kind="ia_buffer", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			num_buffered_lines = max(kernel_height + stride, min_buffer_depth)
			
			n_channels = [math.ceil(input_channels / float(ic_groups)) for _ in range(input_channels % ic_groups)]
			n_channels += [input_channels // ic_groups for _ in range(ic_groups - (input_channels % ic_groups))]
			total_buffer_depths = [n_channels[i] * num_buffered_lines for i in range(ic_groups)]
			buffer_specs = [dl.RAM.size_for_depth(bd) for bd in total_buffer_depths]
			buffer_depths = [[min(bd, (i * bs[0] % bd - 1) % bs[0] + 1) for i in range(math.ceil(bd / bs[0]))] for bd,bs in zip(total_buffer_depths, buffer_specs)]
			buffer_depths = [[np.sum(buffer_depth_list)] for buffer_depth_list in buffer_depths]
			buffer_widths = []
			for bs in buffer_specs:
				if bs[0] <= 64:
					buffer_widths.append([bits_per_act] * activation_width)
				else:
					total_width = activation_width * bits_per_act
					widths = [bs[1]] * math.floor(total_width / bs[1])
					remainder = total_width % bs[1]
					if remainder != 0:
						widths.append(remainder)
					assert(np.sum(widths) == total_width)
					buffer_widths.append(widths)

			max_fanout = 8
			added_read_delay = 1
			#if max([max(bds) for bds in buffer_depths]) <= 64:
				# for MLABs it hurts us to add output registers
			#	added_read_delay = 0

			self.inputs = [inst(dl.Logic, bits=bits_per_act, name="ia_buffer_write_data_" + str(j)) for j in range(activation_width)]
			self.writes = [inst(dl.Logic, bits=1, name="write_ic_group_" + str(i)) for i in range(ic_groups)]
			self.read = inst(dl.Logic, bits=1, name="read")
			self.outputs = [[inst(dl.Logic, bits=bits_per_act, name="ia_buffer_read_data_ic_group_" + str(i) + "_" + str(j)) for j in range(activation_width)] for i in range(ic_groups)]

			address_bits = [clog2(tbd) for tbd in total_buffer_depths]
			self.w_addrs = [inst(dl.Logic, bits=ab, name="w_addr_ic_group_" + str(i)) for i, ab in enumerate(address_bits)]
			self.r_addrs = [inst(dl.Logic, bits=ab, name="r_addr_ic_group_" + str(i)) for i, ab in enumerate(address_bits)]

			r_addr_fanouts = [[inst(dl.Logic, bits=r_addr._bit_width, name="r_addr_fanout_buffer_" + str(i)) for w in widths for d in depths]
				for i,(r_addr, widths, depths) in enumerate(zip(self.r_addrs, buffer_widths, buffer_depths))]

			physical_ram_addr_bits = [min(ab, clog2(bs[0])) for ab,bs in zip(address_bits, buffer_specs)]
			total_write_fanout = np.sum([len(bdl) for bdl in buffer_depths])
			total_ram_tile_count = np.sum([len(bdl) * len(bwl) for bdl, bwl in zip(buffer_depths, buffer_widths)])
			needed_din_fanout_delay = math.ceil(math.log(total_write_fanout, 10))
			needed_addr_fanout_delay = math.ceil(math.log(total_ram_tile_count, max_fanout))
			self.control_delay = max(needed_addr_fanout_delay, 4)
			self.data_in_delay = needed_din_fanout_delay
			self.control_to_input_delay = self.control_delay - self.data_in_delay
			memory_model_read_delay = 1
			ram_read_delay = memory_model_read_delay + added_read_delay
			self.read_delay = memory_model_read_delay + math.ceil(math.log(max([len(bdl) for bdl in buffer_depths]), max_fanout)) + ram_read_delay

			self.concatenated_inputs = inst(dl.Concat, *self.inputs, name="concatenated_inputs")
			#delayed_write_data = self.concatenated_inputs.delay(self.data_in_delay, max_fan=max_fanout, final_fan=total_write_fanout)
			delayed_write_data = [inst(dl.Logic, bits=self.concatenated_inputs._bit_width, name="delayed_write_data_" + str(i)) for i in range(total_write_fanout)]
			write_data_pipe = inst(dl.PipelinedFanout,
				self.concatenated_inputs,
				*delayed_write_data,
				levels=self.data_in_delay,
				name="write_data_pipe")

			r_addr_fanout_pipes = [inst(dl.PipelinedFanout, r_addr, *r_addr_l, levels=self.control_delay-2) for i, (r_addr, r_addr_l) in enumerate(zip(self.r_addrs, r_addr_fanouts))]


			self.ram_ic_group_module_list = [inst(dl.Module, "ic_group_ram", kind="ic_group_ram", name="ic_group_ram_" + str(i)) for i in range(ic_groups)]
			ic_group_read_data = []
			mux_stages = []
			cumulative_depth_counter = 0
			for input_channel_group, (
				ic_buffer_width_list, ic_buffer_depths_list, ic_group_ram, pb, w_addr, r_addrs, ic_group_write) in enumerate(
				zip(buffer_widths, buffer_depths, self.ram_ic_group_module_list, physical_ram_addr_bits, self.w_addrs, r_addr_fanouts, self.writes)):
				with dl.ModuleScope(ic_group_ram):
					ic_group_ram_tile_count = len(ic_buffer_width_list) * len(ic_buffer_depths_list)
					read_muxes = []
					wr_addr = w_addr.delay(self.control_delay-2, max_fan=max_fanout, final_fan=ic_group_ram_tile_count, final_fan_of_1=False)

					if len(ic_buffer_depths_list) > 1:
						write_chip_select_bits = w_addr[w_addr._bit_width-1:pb]
						decoded_writes = [inst(dl.AND, inst(dl.EQ, write_chip_select_bits, j), ic_group_write).delay(
							self.control_delay, max_fan=max_fanout, final_fan=ic_group_ram_tile_count) for j,r in enumerate(ic_buffer_depths_list)]

						read_chip_select_bits = r_addr[r_addr._bit_width-1:pb]
						decoded_reads = [inst(dl.AND, inst(dl.EQ, read_chip_select_bits, j), self.read).delay(
							self.control_delay, max_fan=max_fanout, final_fan=ic_group_ram_tile_count) for j,r in enumerate(ic_buffer_depths_list)]
					else:
						#decoded_writes = [ic_group_write.delay(self.control_delay, max_fan=max_fanout, final_fan=len(ic_buffer_width_list))]
						#if self.control_delay > 1:
						#	fanout_type = dl.Flop
						#else:
						fanout_type = dl.Logic
						with dl.ParamScope(bits=1):
							decoded_reads = [inst(fanout_type, name="ic_group_" + str(input_channel_group) + "_read_fanout_" + str(i)) for i in range(len(ic_buffer_width_list))]
							decoded_writes = [inst(fanout_type, name="ic_group_" + str(input_channel_group) + "_write_fanout_" + str(i)) for i in range(len(ic_buffer_width_list))]
						write_fanout_pipe = inst(dl.PipelinedFanout,
							ic_group_write,
							*decoded_writes,
							levels=self.control_delay-1,
							name="ic_group_" + str(input_channel_group) + "_write_fanout_pipe")
						read_fanout_pipe = inst(dl.PipelinedFanout,
							self.read,
							*decoded_reads,
							levels=self.control_delay-1,
							name="ic_group_" + str(input_channel_group) + "_read_fanout_pipe")

					select_signals = []
					buffer_width_start = 0
					r_addr_index = 0
					for x, (buffer_width, read, write) in enumerate(zip(ic_buffer_width_list, decoded_reads, decoded_writes)):
						depth_ram_list = []
						for y, buffer_depth in enumerate(ic_buffer_depths_list):
							ram = inst(dl.RAM,
								bits=buffer_width,
								depth=buffer_depth,
								name="ram_x" + str(x) + "_y" + str(y))
							ram.w_en.set_driver(write.delay(1))
							ram.r_en.set_driver(read.delay(1))
							ram.w_data.set_driver(delayed_write_data[y+cumulative_depth_counter][buffer_width_start+buffer_width-1:buffer_width_start])
							ram.w_addr.set_driver(wr_addr[ram.w_addr._bit_width-1:0].delay(2))
							ram.r_addr.set_driver(r_addrs[r_addr_index][ram.r_addr._bit_width-1:0].delay(2))
							r_addr_index += 1
							depth_ram_list.append(ram)
						read_mux = inst(dl.PipelinedMux,
							bits=depth_ram_list[-1].r_data._bit_width,
							max_select_bits_per_stage=clog2(max_fanout),
							total_mux_size=len(depth_ram_list),
							name="read_mux_" + str(x))
						read_muxes.append(read_mux)
						if len(select_signals) == 0:
							ra_base_select = pb
							for stage,(select,mux_count) in enumerate(zip(read_mux.select_signals, read_mux.muxes_per_stage)):
								next_ra_base_select = ra_base_select + select._bit_width
								select_signals.append(r_addr[next_ra_base_select-1:ra_base_select].delay(self.control_delay + ram_read_delay + stage, max_fan=max_fanout, final_fan=len(ic_buffer_width_list)))
								ra_base_select = next_ra_base_select

						for select,m_select in zip(select_signals, read_mux.select_signals):
							m_select.set_driver(select)
						for _input, ram in zip(read_mux.inputs, depth_ram_list):
							_input.set_driver(ram.r_data, delay=added_read_delay)
						buffer_width_start += buffer_width
					cumulative_depth_counter += len(ic_buffer_depths_list)
					concatenated_read_data = inst(dl.Concat, *[mux.output for mux in read_muxes], name="concatenated_read_data")
					mux_stages.append(read_muxes[0].stages)
					ic_group_read_data.append(concatenated_read_data)

			self.params_to_print = {
				"control_delay": self.control_delay,
				"data_in_delay": self.data_in_delay,
				"control_to_input_delay" : self.control_to_input_delay,
				"read_delay" : self.read_delay,
				"n_channels" : n_channels,
				"buffer_depths" : buffer_depths,
				"total_buffer_depths" : total_buffer_depths,
				"num_buffered_lines" : num_buffered_lines}

			delayed_read_data = [c.delay(self.read_delay - (ram_read_delay + stages)) for c,stages in zip(ic_group_read_data, mux_stages)]
			for i in range(ic_groups):
				for j in range(activation_width):
					self.outputs[i][j].set_driver(delayed_read_data[i][(j+1)*bits_per_act-1:j*bits_per_act])

			"""for rl in self.ram_list:
				for r in rl:
					r.has_fanout_control = True
					tile_estimate = r.tile_estimate
					fanout = math.ceil(tile_estimate/max_fanout)
					if fanout > 1:
						get_sdc_filter_str = lambda reg_name, m=r: "*|" + "|".join([o.name + "_i" for o in reversed(m.get_owner_list()[:-2])]) + "|" + m.name + "_i|*|" + reg_name + "[*]"
						dl.Verilog.current_circuit.add_quartus_setting_lambda(
							lambda te=fanout, fs=get_sdc_filter_str: "set_instance_assignment -name DUPLICATE_REGISTER " + str(te) + " -to " + '"' + fs("address_reg_a") + '"')
						dl.Verilog.current_circuit.add_quartus_setting_lambda(
							lambda te=fanout, fs=get_sdc_filter_str: "set_instance_assignment -name DUPLICATE_REGISTER " + str(te) + " -to " + '"' + fs("address_reg_b") + '"')
						dl.Verilog.current_circuit.add_quartus_setting_lambda(
							lambda te=fanout, fs=get_sdc_filter_str: "set_instance_assignment -name DUPLICATE_REGISTER " + str(te) + " -to " + '"' + fs("rdaddr_reg") + '"')"""


	def get_instance_comment(self, t="  "):
		return v_comment_string("\n".join([k + ": " + str(v) for k,v in self.params_to_print.items()]), t=t)

class BasicConvDSPs(dl.Module):
	def __init__(self,
		count,
		bits,
		signed,
		parallel_weights_per_dsp=2,
		parallel_weights=2,
		build_data_path=True,
		delay_for_chain_break=4,
		staggered_weight_info=None,
		round_const=0, **kwargs):

		# standard module initialization steps
		super(BasicConvDSPs, self).__init__("basic_conv_dsps", kind="basic_conv_dsps", **kwargs)
		assert(staggered_weight_info is not None)
		kwargs = self.kwargs
		inst = self.inst

		# build inputs to be driven by instantiating module
		self.inputs = [inst(dl.Logic, bits=bits[i%len(bits)], signed=signed[i%len(signed)], name="input_" + str(i)) for i in range(count * parallel_weights * 2)]
		self.accumulate_signals = [inst(dl.Logic, bits=1, signed=False, name="accumulate") for _ in range(count)]

		# A fully registered S10 DSP block has 3 input pipeline stages plus one output register
		input_pipe_depth = 3
		output_stages = 1
		dsp_mac_delay = input_pipe_depth + output_stages
		self.added_delay = dsp_mac_delay
		self.count = 0

		# Having a bunch of long DSP chains causes quartus to fail placement, so we break
		# long chains into multiple smaller ones.  The result output of one DSP chain will then
		# be fed into the input pipeline of the next DSP block, so we need at least 3 pipe stages
		assert(delay_for_chain_break >= input_pipe_depth)
		additional_delay_between_chain_breaks = delay_for_chain_break - input_pipe_depth
		with dl.ModuleScope(self):
			if not build_data_path:
				self.outputs = [inst(dl.Logic, bits=36) for i in range(count)]
				self.dsp_scopes = [self for i in range(count)]
			else:
				self.outputs = []
				self.dsp_scopes = []
				for i in range(count):
					if sys.stdout.isatty():
						print("   Chains " + str(i+1) + "/" + str(count), end="\r")
					dsps_for_output = []
					self.dsp_scopes.append(inst(dl.Module, "dsp_output_group", kind="dsp_output_group", name="dsp_output_group_" + str(i)))
					with dl.ModuleScope(self.dsp_scopes[-1]):
						remaining_weights = parallel_weights
						multipliers_built = 0
						for chain_index, chain_length in enumerate(staggered_weight_info["multipliers_per_chain"]):
							inputs = self.inputs[parallel_weights*2*i + multipliers_built*2: parallel_weights*2*i + multipliers_built*2+chain_length*2]
							chain_data = self._build_dsp_chain(
								chain_length,
								bits[multipliers_built*2:multipliers_built*2+chain_length*2],
								[_input.signed for _input in inputs],
								chain_index > 0,
								parallel_weights_per_dsp,
								inputs)
							multipliers_built += chain_length
							if chain_index > 0:
								chain_data["carry_in"].set_driver(chain_output, delay=additional_delay_between_chain_breaks)
							chain_output = chain_data["output"]
						chain_data["dsps"][-1].inputs[-1].set_driver(self.accumulate_signals[i])
						chain_data["dsps"][-1].load_const = round_const
						self.outputs.append(chain_data["output"])
				print()


	def _build_dsp_chain(
		self,
		num_multipliers,
		bits,
		signed,
		build_carry_in,
		parallel_weights_per_dsp,
		inputs):
		inst = self.inst
		# instantiate DSP blocks until we have built enough multipliers for the chain
		remaining_multipliers = num_multipliers
		multipliers_built = 0

		# Build a small data structure with data necessary for the caller to connect
		#  the chain to other logic
		chain_data = {
			"carry_in" : None,
			"output" : None,
			"dsps" : None
		}

		is_chain_start = True
		dsps = []
		while remaining_multipliers > 0:
			start_chain_carry_in = is_chain_start and build_carry_in
			# Number of weight inputs for the DSP we are about to instantiate
			#  if the number of multipliers is not evenly divisible by the number
			#  of parallel multiplications per DSP then the first DSP will use fewer
			#  inputs.
			num_weight_inputs = remaining_multipliers % parallel_weights_per_dsp
			if num_weight_inputs == 0:
				num_weight_inputs = parallel_weights_per_dsp
			# If there is a carry in to the chain, the DSP will have only one multiplication
			# and one carry in input.
			if start_chain_carry_in:
				num_weight_inputs = 1

			# Each weight input will have a corresponding activation input, so the total
			# inputs is num_weight_inputs * 2 (chain in and out do not count here)
			inputs_for_this_dsp = num_weight_inputs * 2
			bits_for_this_dsp = bits[multipliers_built*2:multipliers_built*2 + inputs_for_this_dsp]
			input_signals = inputs[multipliers_built*2:multipliers_built*2 + inputs_for_this_dsp]
			dsp = inst(dl.MultiplyAccumulate,
				input_widths=bits_for_this_dsp,
				signed=signed,
				accumulate_width=64,
				parallel_reduction_count=inputs_for_this_dsp,
				external_carry_in=start_chain_carry_in)
			for i,input_signal in enumerate(input_signals):
				dsp.inputs[i].set_driver(input_signal)
			dsps.append(dsp)
			self.count += 1
			if start_chain_carry_in:
				chain_data["carry_in"] = dsp.external_carry_in
			if not is_chain_start:
				dsp.carry_in.set_driver(dsps[-2].carry_out)
			is_chain_start = False
			remaining_multipliers -= num_weight_inputs
			multipliers_built += num_weight_inputs

		chain_data["output"] = dsps[-1].output
		chain_data["dsps"] = dsps

		return chain_data


				

class OCController(dl.Module):
	"""
	OCController

	Controls accumulation in the multipliers, run-length accumulation, and line address
	for the input activation buffer read address calculations

	Background: Each output channel can have a different number of weights since the
	weights can be sparse.  Activations corresponding to these weights must be multiplied
	and accumulated, and the accumulation must reset whenever we begin to process a new
	set of weights and activations for a new output channel.  To compute the cycles when
	we need to reset the accumulator we need a ROM that stores how many cycles we accumulate
	for each output channel, and a counter that counts down the number of cycles for each
	output channel.  The ROM is called ``weights_per_oc``, and the counter is called
	``in_oc_counter``.  All of the other components are just control logic for these two components
	that provide an address for the ROM, compute when to reset the counter, and track
	whether we are currently processing output channels or waiting for data.

	Args:
		weights_per_oc (list): a list containing the maximum number of weights for an output channel
			processing group (so if the processing is split along the input channel
			dimension 3 times, and the partitions have [3,7,5] nonzero weights for
			output channel #1, then ``weights_per_oc`` for output channel #1 should be 7,
			and the weights for the other input channels should be padded to 7 weights
			with 0 weights)
		weights_per_oc_bits (int): this should really just be ``clog2(max(weights_per_oc)+1)``
		done_delay (int): If weights are staggered for systolic accumulation, then ``done_delay`` should
			be the maximum amount of staggering.

	Attributes:
		weights_per_oc (:class:`digitallogic.digitallogic.ROM`)
			ROM containing the number of weights for each output channel

			Depth
				# of Output Channels in layer
			Values
				Number of weights for the output channel with an index matching the address
			Address
				index of output channel

		weights_per_oc_latency_hider (:class:`digitallogic.digitallogic.ROMLatencyHider`):
			Controller for ``weights_per_oc`` that allows for single cycle reads with a ROM that 
			takes two cycles to read

		current_output_channel_counter (:class:`digitallogic.digitallogic.Counter`):
			Stores the index of the output channel
			being processed.  When it finishes and
			in_oc_counter is done it causes the
			state machine to go from ``PROCESSING_OCS``
			to ``DONE``

			Increment Condition
				``in_oc_counter`` is done and in state ``PROCESSING_OCS``

			Done Condition
				counter equals ``output_channels - 1``

		weights_per_oc_addr (:class:`digitallogic.digitallogic.Counter`):
			Like ``current_output_channel_counter``,
			``weights_per_oc_addr`` stores the current
			output channel, but it does so for the
			``weights_per_oc_latency_hider``.  The
			latency hider reads from the ROM before
			values are actually needed, so this counter
			will be slightly ahead of 
			``current_output_channel_counter`` and is
			used as the address for the ROM
			(whereas the ``current_output_channel_counter``)
			is used for control.

			Increment Condition
				the latency hider reads from the ROM

			Done Condition
				counter equals the number of
				output channels.
				NOTE: this intentionally counts to 1 greater
				than the highest address in the ROM.  This is
				because the condition to get a new value from
				the ROM (``get_next_oc_counter_value``) fires a total
				of # output channels + 1 times.  By having this
				counter count to # output channels it will be reset
				to 0 when we transition to the DONE state.
				We might load an X into in_oc_counter as a result,
				but this is fine since we don't use in_oc_counter
				once we transition out of ``PROCESSING_OCS``.  When
				we enter ``START`` again the X value will be overwritten
				with the correct value at index 0 of the ROM.

		in_oc_counter (:class:`digitallogic.digitallogic.DownCounter`):
			Counter that counts down from the number of
			weights in an output channel to zero.  It
			loads the number of weights in an output
			channel from the ``weights_per_oc`` ROM

			Decrement Condition
				state is ``PROCESSING_OCS``

			Reset Condition
				``get_next_oc_counter_value``

			Reset Value
				the number of weights in an output channel, from ``weights_per_oc`` ROM


	"""
	def __init__(self, weights_per_oc, weights_per_oc_bits, done_delay=0, **kwargs):
		super(OCController, self).__init__(
			"oc_controller", kind="oc_controller", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			internal_to_external_control_delay = 2
			self.go = inst(dl.Logic, 1, name="go")
			max_weights_per_oc = np.max(np.array(weights_per_oc) - 1)

			#: We will use a counter that counts down the total
			#: number of weights to track processing of one line.
			#: This reduces loading on the the in_oc_counter
			total_weights = np.sum(np.array(weights_per_oc))
			self.processing_ocs_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=total_weights-1,
				name="processing_ocs_counter")

			#: Instantiate a ROM that has the number of weights in 
			#: each output channel.
			self.weights_per_oc = inst(dl.ROM, 
				content_list=list(np.array(weights_per_oc) - 1),
				bits=weights_per_oc_bits,
				name="weights_per_oc_buffer")

			#: For layers with many output channels this ROM will be in a block RAM that
			#: requires two cycles to read.  The FIFO will allow us to read from a LUTRAM
			#: with fast single cycle control latency
			self.weights_per_oc_fifo = inst(
				dl.FIFO,
				bits=self.weights_per_oc.r_data._bit_width,
				depth=16,
				almost_full=8,
				use_lutram=True,
				dont_allow_x_write=False,
				name="weights_per_oc_latency_hiding_fifo")


			#: We will maintain two counters that track the current output channel
			#: 'current_output_channel_counter' is one that tracks the output channel
			#: so that we know the progress of a layer
			#: 'weights_per_oc_addr' is one that tracks the read location in the ROM
			self.current_output_channel_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=len(weights_per_oc)-1,
				name="current_output_channel_counter")
			self.weights_per_oc_addr = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=len(weights_per_oc),
				name="weights_per_oc_addr")

			read_weights_per_oc_rom = inst(dl.AND,
				inst(dl.NOT, self.weights_per_oc_fifo.almost_full),
				inst(dl.NOT, kwargs["reset"]),
				name="read_weights_per_oc_rom").delay(1)
			self.weights_per_oc_fifo.w_data.set_driver(self.weights_per_oc.r_data.delay(1))
			self.weights_per_oc_fifo.w_en.set_driver(read_weights_per_oc_rom.delay(2))
			self.weights_per_oc.r_en.set_driver(read_weights_per_oc_rom)
			#: We increment the ROM read address whenever the latency hider reads 
			#:  from the memory
			self.weights_per_oc_addr.set_increment_condition(
				read_weights_per_oc_rom)

			#: We set it to be the driver for the ROM Read address
			self.weights_per_oc.r_addr.set_driver(
				self.weights_per_oc_addr.current_value[self.weights_per_oc.r_addr._bit_width-1:0])

			#: In OC counter tracks how many weights we have processed in an OC
			#: it gets reset to the value we read from the ROM and counts down to
			#: zero
			self.in_oc_counter = inst(dl.DownCounter,
				max_weights_per_oc, name="in_oc_counter")
			self.in_oc_counter.reset_value.set_driver(
				self.weights_per_oc_fifo.r_data)

			#: row here means that we are done a set of output channels
			#self.done_row = inst(dl.AND,
			#	self.current_output_channel_counter.is_done,
			#	self.in_oc_counter.is_done, name="done_row")
			self.done_row = self.processing_ocs_counter.is_done.copy()
			self.done_row.name = "done_row"

			#: there is a bug in the state machine module
			#: that means we can't drive edge conditions with
			#: signals with a name like is_<state_name>
			can_finish = inst(dl.Logic, bits=1, name="can_finish")

			states = ["IDLE", "START", "PROCESSING_OCS", "DONE"]
			edges = [
				["IDLE", "START", self.go],
				["START", "PROCESSING_OCS"],
				["PROCESSING_OCS", "DONE", self.done_row],
				["DONE", "IDLE", can_finish]]
			
			self.state_machine = inst(dl.StateMachine, states, edges, None, name="state_machine")
			self.processing_ocs_counter.set_increment_condition(self.state_machine.is_processing_ocs)
			self.should_read_weights = inst(dl.Logic, bits=1, name="should_read_weights")
			
			#Marius: if we are using a ConvWrapper (for output channel parallelism) we may need to stall the next row depending on sparsity in other convolution modules 
			#These signals should be passed up to the ConvWrapper
			self.fsm_idle = self.state_machine.is_idle
			self.fsm_has_started = self.state_machine.is_start

			#:  Done delay allows for weight staggering (which allows for systolic multiplier chaining).
			#:  Basically weight staggering causes computation of all output channels to take an additional
			#:  number of cycles equal to the maximum amount of staggering.  A done delay of greater than 0
			#:  will cause a counter to be created that keeps us in the DONE state an extra done_delay cycles.
			#:
			#:  If you want to see why this is necessary, the following should explain the weight staggering:
			#:  Let's say we have 3 multipliers each connected in a chain like: ([R] is a register)
			#:   a1\
			#:      x -->[R]
			#:   b1/      |
			#:   a2\\      V
			#:      x --> + -> [R]
			#:   b2/            |
			#:   a3\\            V
			#:      x --------> + -> [R]-|->
			#:   b3/            ^--------|
			#:
			#:  The idea here is that we are using 3 multipliers to accelerate the computation of
			#:  a single output channel.  To make this fast the output of each multiplier is registered
			#:  prior to being added to the output of the next multiplier.  This means that we need
			#:  to delay the inputs to each multiplier to get them to align properly, like this:
			#:  ___________________________
			#:  Time:     1 2 3 4 5 6 7 8 9
			#:  ---------------------------
			#:  a1 input: 1 1 1 2 2 2 2
			#:  a2 input:   1 1 1 2 2 2 2
			#:  a3 input:     1 1 1 2 2 2 2
			#:  ---------------------------
			#:  Here the numbers in the table indicate which output channel the input corresponds to,
			#:  so a1 gets inputs for OC#1 from time 1-3, and gets inputs for OC#2 from time 4-7, while
			#:  a2 gets inputs for OC#1 from time 2-4. One cycle after a3 gets its final OC#1 input (cycle 6), the
			#:  output register for the third multiplier will contain the accumulation of all the multiplications
			#:  for OC#1.
			#:  For this example, OCController would finish and go back to idle after time 7 if done_delay was 0,
			#:  but we don't want that, so we delay going back to IDLE until all of the multipliers have finished
			#:  with done_delay_counter.  In this example, done_delay_counter should delay going back to IDLE by 2
			#:  cycles.

			if done_delay > 0:
				done_delay_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=done_delay,
					name="done_delay_counter")
				with dl.ModuleScope(done_delay_counter):
					done_delay_counter_almost_done_value = inst(dl.Constant, bits=done_delay_counter.current_value._bit_width, value=done_delay-1, name="almost_done_value")
					done_delay_counter_almost_done = inst(dl.EQ, done_delay_counter.current_value, done_delay_counter_almost_done_value, name="almost_done")
					can_finish.set_driver(inst(dl.AND, self.state_machine.is_done, done_delay_counter_almost_done), delay=1)
					done_delay_counter.set_increment_condition(self.state_machine.is_done)
				self.should_read_weights.set_driver(
					inst(dl.OR,
						self.state_machine.is_processing_ocs,
						inst(dl.AND,
							inst(dl.NOT, can_finish),
							self.state_machine.is_done)), delay=internal_to_external_control_delay)
			else:
				can_finish.set_driver(self.state_machine.is_done)
				self.should_read_weights.set_driver(self.state_machine.is_processing_ocs, delay=internal_to_external_control_delay)


			self.current_output_channel_counter.set_increment_condition(
				inst(dl.AND, 
					self.state_machine.is_processing_ocs,
					self.in_oc_counter.is_done))
			self._done_oc = inst(dl.AND, self.state_machine.is_processing_ocs, self.in_oc_counter.is_done, name="_done_oc")
			self.done_oc = self._done_oc.delay(internal_to_external_control_delay)
			self.get_next_oc_counter_value = inst(dl.OR,
					self.state_machine.is_start,
					self._done_oc,
					name="get_next_oc_counter_value")
			self.in_oc_counter.reset_signal.set_driver(
				self.get_next_oc_counter_value)
			self.in_oc_counter_reset = self.in_oc_counter.reset_signal.delay(internal_to_external_control_delay)
			self.done = inst(dl.Logic, 1)
			self.done.set_driver(can_finish, delay=internal_to_external_control_delay)
			self.weights_per_oc_fifo.r_en.set_driver(self.get_next_oc_counter_value)
			self.in_oc_counter.enable.set_driver(self.state_machine.is_processing_ocs)

			self.requires_driver = [self.go]

class WeightBufferController(dl.Module):
	def __init__(self, buffer, depth, **kwargs):
		super(WeightBufferController, self).__init__("weight_buffer_controller", kind="weight_buffer_controller", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			self.load_weights = inst(dl.Logic, 1, name="load_weights")
			self.weight_buffer_addr_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=depth-1,
				name="weight_buffer_addr_counter")

			self.weight_buffer_addr_counter.set_increment_condition(self.load_weights)

			self.weight_buffer_addr = inst(dl.Logic, bits=self.weight_buffer_addr_counter.current_value._bit_width)
			self.weight_buffer_addr.set_driver(self.weight_buffer_addr_counter.current_value)

			self.requires_driver = [self.load_weights]

class RLProcessor(dl.Module):
	def __init__(self, rl_width, addr_width, **kwargs):
		super(RLProcessor, self).__init__("rl_processor", kind="rl_processor", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			self.rl = inst(dl.Logic, rl_width, name="rl")
			self.rl_valid = inst(dl.Logic, 1, name="rl_valid")
			self.clear = inst(dl.Logic, 1, name="clear")
			self.address = inst(dl.Flop, bits=addr_width, name="address")

			cumulative_mux = inst(dl.MuxV2,
				self.clear,
				self.address,
				inst(dl.Constant, bits=addr_width, value=0, name="zeros"),
				name="cumulative_mux")
			rl_mux = inst(dl.MuxV2,
				self.rl_valid,
				inst(dl.Constant, bits=rl_width, value=0),
				self.rl)

			with dl.ParamScope(name="next_address_wide"):
				next_address_wide = cumulative_mux + rl_mux
			with dl.ParamScope(name="next_address"):
				next_address = next_address_wide[addr_width-1:0]

			self.address.set_driver(next_address)

			self.requires_driver = [self.rl, self.rl_valid, self.clear]

#For printing a more detail assertion message in FIFO overflow assertion
ConvWrapper_counter = 0

class ConvWrapper(NNStage):
	"""
	ConvWrapper

	Wraps multiple BasicConv module instantiations to enable unrolling along output channels

	"""
	kind =  "conv_wrapper"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		fifo_to_mux_delay = 3
		n_output_channel_groups = node.planner_estimate.n_output_channel_groups
		print(f"  n_output_channel_groups: {n_output_channel_groups}")
		group_ocs = [[] for _ in range(n_output_channel_groups)]
		group_cycles = [0 for _ in range(n_output_channel_groups)]
		cycles_per_oc = []
		oc_order = []
		buffer_depths = [0 for _ in range(n_output_channel_groups)]
		max_buffer_depths = [4 for _ in range(n_output_channel_groups)]
		is_depthwise = (node.type == "DepthwiseConv2dNative")
		oc_order_fifo_depth = 16
		
		#Choose the correct dimension for the Depthwise conv
		output_channel_dimension = node.values[0].shape[-1]
		if is_depthwise:
			output_channel_dimension = node.values[0].shape[-2]
		
		group_ocs_cycles = [[] for _ in range(n_output_channel_groups)]
		for oc in range(output_channel_dimension):
			if is_depthwise:
				cycles_for_oc = np.nonzero(node.values[0][:,:,oc,:])[0].size
			else:
				cycles_for_oc = np.nonzero(node.values[0][:,:,:,oc])[0].size						
			min_index = np.argmin(group_cycles)
			group_cycles[min_index] += cycles_for_oc
			cycles_per_oc.append(group_cycles[min_index])
			group_ocs[min_index].append(oc)
			group_ocs_cycles[min_index].append(cycles_for_oc)
			buffer_depths[min_index] += 1
			oc_order.append(min_index)
			if min_index != np.argmin(group_cycles):
				max_buffer_depths[min_index] = max(2 * buffer_depths[min_index], max_buffer_depths[min_index])
				buffer_depths[min_index] = 0

						
		global ConvWrapper_counter
		# For the unit tests
		example_input = node.input_nodes()[0].example_outputs[0]
		self.example_input = example_input
		self.weights = self.node.values[0]
		if node.explicit_padding is not None:
			self.example_input = np.pad(example_input, node.explicit_padding, "constant", constant_values=0)
		
		#: Instantiate a ROM that has the order in which the BasicConv instances
		#: will produce output channels
		self.oc_order_rom = inst(dl.ROM, 
			content_list=oc_order,
			bits=clog2(n_output_channel_groups),
			name="oc_order_rom")
				
		# For a depthwise conv we don't want to send the entire activation depth to each conv module
		# Essentially we only want to send the ones each conv module needs for its filters
		# Need to use a FIFO similar to what we did with the OC 
		if is_depthwise:
			#Need a second ROM for depthwise convolutions since we need to know which conv to send the current value to
			self.oc_order_rom_2 = inst(dl.ROM, 
				content_list=oc_order,
				bits=clog2(n_output_channel_groups),
				name="oc_order_rom_2")
			
			self.oc_order_rom_addr_2 = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=len(oc_order) - 1,
			name="oc_order_rom_addr_2")
			
			oc_order_fifo_depthwise = inst(dl.FIFO,
				bits=self.oc_order_rom_2.r_data._bit_width,
				depth=oc_order_fifo_depth,
				use_lutram=True,
				almost_full=oc_order_fifo_depth//2,
				name="oc_order_fifo_depthwise")

			oc_order_fifo_depthwise_control = inst(dl.FIFO,
				bits=self.oc_order_rom_2.r_data._bit_width,
				depth=oc_order_fifo_depth,
				use_lutram=True,
				almost_full=oc_order_fifo_depth//2,
				name="oc_order_fifo_depthwise_control")	
			
			oc_order_rom_2_read = inst(dl.AND, inst(dl.NOT, kwargs["reset"]), inst(dl.NOT, inst(dl.AND, oc_order_fifo_depthwise.almost_full, oc_order_fifo_depthwise_control.almost_full), name="oc_order_fifos_depthwise_alomst_full")).delay(1)
			self.oc_order_rom_2.r_en.set_driver(oc_order_rom_2_read)
			
			self.oc_order_rom_2.r_addr.set_driver(self.oc_order_rom_addr_2.current_value)
			self.oc_order_rom_addr_2.set_increment_condition(oc_order_rom_2_read)
							
			oc_order_fifo_depthwise.w_en.set_driver(oc_order_rom_2_read.delay(2))
			oc_order_fifo_depthwise.w_data.set_driver(self.oc_order_rom_2.r_data.delay(1))
			oc_order_fifo_depthwise.r_en.set_driver(self.writes[0])
			oc_order_fifo_depthwise_control.w_en.set_driver(oc_order_rom_2_read.delay(2))
			oc_order_fifo_depthwise_control.w_data.set_driver(self.oc_order_rom_2.r_data.delay(1))
			oc_order_fifo_depthwise_control.r_en.set_driver(self.write)
							
		bc_insts = []
		output_fifo_lists = []
		buffered_output_counters = []
		buffered_output_counters_gt_0 = []
		read_fifo_list = []
		conv_fsm_idle_signals = []
		
		all_convs_idle = inst(dl.Logic, bits=1, name="all_convs_idle")
		
		for i, (bd, gocs) in enumerate(zip(max_buffer_depths, group_ocs)):
			dummy_node = Node(node.tf_op, node.example_outputs, node.precision_parameters)
			dummy_node.type = node.type
			dummy_node.planner_estimate = node.planner_estimate
			dummy_node.inputs = node.inputs
			dummy_node.outputs = node.outputs
			dummy_node.properties = node.properties
			dummy_node.explicit_padding = node.explicit_padding
			if is_depthwise:			
				values = node.values[0][:,:,gocs,:]
			else:
				values = node.values[0][:,:,:,gocs]	
				
				
			dummy_node.values = [values]

			if is_depthwise:
				bc_inst = self.inst(BasicConv, dummy_node, depthwise_conv_parallelism=n_output_channel_groups)
			else:
				bc_inst = self.inst(BasicConv, dummy_node)
				
			if i == 0:
				self.utilization_estimates = bc_inst.utilization_estimates
			else:
				for k in bc_inst.utilization_estimates.keys():
					self.utilization_estimates[k] += bc_inst.utilization_estimates[k]
			bc_insts.append(bc_inst)
			for sink, source in zip(bc_inst.inputs, self.inputs):
				sink.set_driver(source, delay=1)
				
			#Writes to conv module are depndent on which output channel was sent in for depthwise convolutions
			if is_depthwise:
				actual_write_en = inst(dl.AND, inst(dl.EQ, i, oc_order_fifo_depthwise_control.r_data), self.write)
				bc_inst.write.set_driver(actual_write_en, delay=1)
			else:
				bc_inst.write.set_driver(self.write, delay=1)
				
			for sink, source in zip(bc_inst.writes, self.writes):
				if is_depthwise:
					actual_write_en = inst(dl.AND, inst(dl.EQ, i, oc_order_fifo_depthwise.r_data), source)
					sink.set_driver(actual_write_en, delay=1)
				else:
					sink.set_driver(source, delay=1)
				
				
			# Marius: Some groups may take less cycles than other due to spartsity
			# If this is the case then they need to wait for the group with the longest cycle latency to complete 
			# Otherwise the output FIFOs may eventually overflow as we could have an extra value that needs to be stored after each row
			# ====================
			# We may need a delay on all the idle signals for timing
			conv_fsm_idle_signals.append(bc_inst.oc_controller.fsm_idle.delay(1))
			
			# We want to store all_convs_idle until the module actually starts
			# For depthwise convolutions some modules might start earlier. Reset driver can be 0 or 1, it shouldn't matter
			conv_ready_to_start = inst(dl.Flop, bits=1, reset_driver=1, name="control_sr_init_control")
			tmp_reg_input = inst(dl.MuxV2, conv_ready_to_start, all_convs_idle, inst(dl.NOT, bc_inst.oc_controller.fsm_has_started), name="all_convs_idle_mux_oc_group_" + str(i))
			conv_ready_to_start.set_driver(tmp_reg_input)
			
			bc_inst.can_write_line.set_driver(inst(dl.AND, self.can_write_line.delay(1), conv_ready_to_start))
			# ==================				
			
			output_fifos = []
			for j,(output, valid) in enumerate(zip(bc_inst.outputs, bc_inst.outputs_valid)):
				output_fifo = inst(dl.FIFO, bits=output._bit_width, depth=bd, name="oc_group_" + str(i) + "_output_fifo_" + str(j)) 
				output_fifos.append(output_fifo)
				output_fifo.w_data.set_driver(output.delay(2))
				output_fifo.w_en.set_driver(valid.delay(2))
				
				out_fifo_full = inst(dl.Logic, bits=1, name="oc_group_" + str(i) + "_output_fifo_full_" + str(j))
				out_fifo_full.set_driver(inst(dl.AND, output_fifo.full, valid.delay(2)))
				
				#Checks if FIFOs have overflowed or not, should not affect Quartus build (should be optimized out)
				m = self
				def full_condition(m, t="  ", fifo_condition=out_fifo_full):
					condition = "(" + fifo_condition.name + " !== 1)"
					return condition
					
				inst(dl.Assertion,
					name="output_valid_delay_check",
					condition_f=full_condition,
					condition_f_args=m,
					comment=out_fifo_full.name + " is full! " + str(ConvWrapper_counter),
					error_string=out_fifo_full.name + " is full! " + str(ConvWrapper_counter))
				
			output_fifo_lists.append(output_fifos)

			read_fifo_list.append(inst(dl.Logic, bits=1, name="read_fifo_oc_group_" + str(i)))
			buffered_output_counter = inst(dl.UpDownCounter,
				reset_value=0,
				increment_value=1,
				decrement_value=-1,
				end_value=bd,
				name="buffered_output_counter_oc_group_" + str(i))
			buffered_output_counters.append(buffered_output_counter)
			buffered_output_counters_gt_0.append(inst(dl.NOT, inst(dl.EQ, buffered_output_counter.current_value, 0)))
			buffered_output_counter.set_increment_condition(bc_inst.output_valid)
			buffered_output_counter.set_decrement_condition(read_fifo_list[-1])

		all_convs_idle.set_driver(inst(dl.AND, *conv_fsm_idle_signals))
				
		#: The ROM could be deep and may require multiple cycles to read
		#: to hide this latency we will use a FIFO
		oc_order_fifo = inst(dl.FIFO,
			bits=self.oc_order_rom.r_data._bit_width,
			depth=oc_order_fifo_depth,
			use_lutram=True,
			almost_full=oc_order_fifo_depth//2)
		oc_order_rom_read = inst(dl.AND, inst(dl.NOT, kwargs["reset"]), inst(dl.NOT, oc_order_fifo.almost_full)).delay(1)
		self.oc_order_rom.r_en.set_driver(oc_order_rom_read)
		oc_order_fifo.w_en.set_driver(oc_order_rom_read.delay(2))
		oc_order_fifo.w_data.set_driver(self.oc_order_rom.r_data.delay(1))

		#: For layers with many output channels this ROM will be in a block RAM that
		#: requires two cycles to read.  The latency hider buffers two reads in registers
		#: so that we can get the next one in one cycle
		self.oc_order_rom_addr = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=len(oc_order) - 1,
			name="oc_order_rom_addr")
		self.oc_order_rom.r_addr.set_driver(self.oc_order_rom_addr.current_value)
		self.oc_order_rom_addr.set_increment_condition(
			oc_order_rom_read)
		oc_order_fifo.r_en.set_driver(inst(dl.OR, *read_fifo_list))
		next_oc_group = oc_order_fifo.r_data
		for i, (read, has_enough_values) in enumerate(zip(read_fifo_list, buffered_output_counters_gt_0)):
			read.set_driver(inst(dl.AND, has_enough_values, inst(dl.EQ, next_oc_group, i)))

		any_read = inst(dl.OR, *read_fifo_list)

		max_control_to_output_delay = max([bc.get_control_valid_to_output_valid_delay() for bc in bc_insts]) + 3

		delayed_fifo_read_lists = []
		for read, ofifol in zip(read_fifo_list, output_fifo_lists):
			delayed_fifo_read_data = []
			fifo_count = len(ofifol)
			r_en = read.delay(max_control_to_output_delay, max_fan=4, final_fan=fifo_count)
			for fifo in ofifol:
				fifo.r_en.set_driver(r_en)
				delayed_fifo_read_data.append(fifo.r_data.delay(fifo_to_mux_delay-1)) #Need to subtract 1 here since FIFO output comes 1 cycles after read enable (control signals below stay the same) 
			delayed_fifo_read_lists.append(delayed_fifo_read_data)

		delayed_mux_select = next_oc_group.delay(max_control_to_output_delay + fifo_to_mux_delay, max_fan=4, final_fan=len(self.outputs))
		delayed_any_read = any_read.delay(max_control_to_output_delay + fifo_to_mux_delay, max_fan=4, final_fan=len(self.outputs))
		for i,(o,ov) in enumerate(zip(self.outputs, self.outputs_valid)):
			mux_inputs = [l[i] for l in delayed_fifo_read_lists]
			o.set_driver(inst(dl.MuxV2, delayed_mux_select, *mux_inputs))
			ov.set_driver(delayed_any_read)

		self._set_control_to_input_delay(bc_insts[0].get_control_to_input_delay())
		self._set_control_valid_to_output_valid_delay(max_control_to_output_delay + fifo_to_mux_delay)
		self.output_valid.set_driver(any_read)
		#Potential issue here if unbalanced so we have to AND all the signals
		space_to_write_line_list = [cur_inst.space_to_write_line for cur_inst in bc_insts]
		all_space_to_write_line = inst(dl.AND, *space_to_write_line_list)
		self.space_to_write_line.set_driver(all_space_to_write_line)
		

class BasicConv(NNStage):
	"""
	BasicConv

	Implements convolutions of variable kernel size and stride.  Does not support dilated
	convolutions yet, and support for depthwise convolutions is not validated.

	Attributes:
		ibc (:class:`InputBufferController`):
			controls write addresses for the input activation buffers

		iab (:class:`IABuffer`):
			buffers input activation lines

		dsps (:class:`BasicConvDSPs`):
			A set of DSP blocks potentially chained together that implement multiplications
			and accumulations
		
		oc_controller (:class:`OCController`):
			The output channel controller.  This controller and buffer tell us
			what cycles we should accumulate both runlengths and weight-activation
			products.

		activation_address_computers (list of privately defined modules):
			This is a set of controllers that compute read addresses
			for each of the activation buffers.  There is one
			address computer for each input channel split.

		x_muxes (list of :class:`digitallogic.digitallogic.MuxV2`):
			One mux for each activation input along the x dimension.  These allow
			us to shift the input activations to the correct DSP to for kernels with
			a width greater than 1.

			The following diagram is an example showing these x muxes in a layer with
			a kernel_width of 3, an input width of 7, and ``padding="SAME"``.
			The input activation buffer is 7 activations wide (a-g), and 
			``(kernel_height + stride) * input_channels`` deep.  We load all activations
			from the same address, and apply the same weight, but in order to accumulate
			in place we need to have each multiplier select the activation along the
			x dimension that matches the weight.
			::

			  xsel1 --|
			          |
			     0  -|\\
			     a1 -| |------\\
			     b1 -|/        x---- oa
			          | w1 ---/
			     a1 -|\\
			     b1 -| |------\\
			     c1 -|/        x---- ob
			          | w1 ---/
			     b1 -|\\
			     c1 -| |------\\
			     d1 -|/        x---- oc
			          | w1 ---/
			     c1 -|\\
			     d1 -| |------\\
			     e1 -|/        x---- od
			          | w1 ---/
			     d1 -|\\
			     e1 -| |------\\
			     f1 -|/        x---- oe
			          | w1 ---/
			     e1 -|\\
			     f1 -| |------\\
			     g1 -|/        x---- of
			          | w1 ---/
			     f1 -|\\
			     g1 -| |------\\
			     0  -|/        x----og
			            w1 ---/

			Continuing this example, if we look at a 3x3 slice of the weights:
			::
				w1 w2 w3
				w4 w5 w6
				w7 w8 w9

			The corresponding xsel values are:
			::
				0  1  2
				0  1  2
				0  1  2

			and the corresponding input activations for oa are:
			::
				0 a0 b0
				0 a1 b1
				0 a2 b2

			NOTE: technically the above input activation indices should
			all be multiplied by the number of input channels since
			the input activations are stored with the channel dimension
			contiguous and the height dimension as the outer dimension

	"""
	kind = "basic_conv"
	def get_instance_comment(self, t="  "):
		comment = "Kernel shape: " + str(self.node.get_kernel_shape()) + "\n"
		comment += "Strides: " + str(self.node.get_strides()) + "\n"
		comment += "Number of input channel divisions: " + str(self.node.planner_estimate.n_channel_splits) + "\n"
		comment += "Number of parallel acts in a RAM: " + str(self.node.inputs[0]._from.get_parallel_acts_per_memory()) + "\n"
		comment += "Padding: " + self.node.properties["padding"] + ": " + str(self.padding) + "\n"
		return super(BasicConv, self).get_instance_comment(t=t) + v_comment_string(comment, t)

	def instantiate(self, node, inst, build_data_path=True, clock_2x=None, **kwargs):

		physically_mapped = False
		if "physically_mapped_rams" in kwargs:
			physically_mapped = kwargs["physically_mapped_rams"]

		estimate = self.node.planner_estimate

		parallel_acts_per_ram = node.inputs[0]._from.get_parallel_acts_per_memory()
		input_signed, weights_signed, output_signed = self.get_signs()
		signed = [weights_signed, input_signed]

		input_bits, weight_bits, output_bits = self.get_bit_widths()
		planned_bits = [weight_bits, input_bits]
		
		is_depthwise = node.type == "DepthwiseConv2dNative"
		input_act_bit_spec = self.get_bit_spec("input")
		output_act_bit_spec = self.get_bit_spec("output")

		n_channel_splits = estimate.n_channel_splits
		self.n_channel_splits = n_channel_splits
		parallel_weights_per_dsp = min(2, n_channel_splits)

		break_chain_every_n_weights = 57 # Adds a delay of delay_for_chain_break every N weights (for breaking long DSP chains to allow placement)
		delay_for_chain_break = 9        # Delay added whenever a DSP chain is broken.  The default is the number of input pipeline stages for a DSP plus the output DSP delay
		stagger_every_n_weights = 2
		reduce_with_carry_out = stagger_every_n_weights > 0

		input_channels = self.get_input_act(dim="channels")	
		input_height = self.get_input_act(dim="height")
		input_width = self.get_input_act(dim="width")
		output_width = self.get_output_act(dim="width")
		output_height = self.get_output_act(dim="height")
		k_shape = node.get_kernel_shape()
		kernel_height = k_shape[0]
		kernel_width = k_shape[1]
		vertical_stride = node.get_strides()[1]
		horizontal_stride = node.get_strides()[2]
		
		input_frac_bits, weight_frac_bits, desired_fractional_bits = self.get_frac_part_widths()
		_, _, output_int_bits = self.get_int_part_widths()
		fractional_bits_in_output = weight_frac_bits + input_frac_bits	
		
		if is_depthwise:
			input_channels = k_shape[2]

		from_spec = {
			"frac" : fractional_bits_in_output,
			"sign" : 1,
			"int" : 64 - 1 - fractional_bits_in_output
		}
		to_spec = {
			"frac" : desired_fractional_bits,
			"int" : output_int_bits
		}
		if output_signed:
			to_spec["sign"] = 1
		else:
			to_spec["sign"] = 0

		round_const = -1
		if fractional_bits_in_output > desired_fractional_bits:
			round_const = (fractional_bits_in_output - desired_fractional_bits - 1)

		# I've added this for compatibility because the code originally used a 
		# single stride but over time I do hope to migrate it over to use 
		# independent strides (so new code should use the correct stride
		#  above, but old code still limits us to a single stride)
		stride = vertical_stride
	
		f_max_channels_in_input_buffer_line = lambda: math.ceil(input_channels / n_channel_splits)
		f_max_ia_buffer_depth = lambda: f_max_channels_in_input_buffer_line()  * (max(kernel_height, vertical_stride) + vertical_stride)

		while f_max_ia_buffer_depth() > 64 and f_max_ia_buffer_depth() < 70:
			n_channel_splits += 2
		self.n_channel_splits = n_channel_splits
		parallel_weights_per_dsp = min(2, n_channel_splits)
		max_channels_in_input_buffer_line = f_max_channels_in_input_buffer_line()
		max_ia_buffer_depth = f_max_ia_buffer_depth()
		min_ia_buffer_depth = max_ia_buffer_depth
		ia_buffer_has_lutrams = False
		if min_ia_buffer_depth <= 32:
			ia_buffer_has_lutrams = True
			min_ia_buffer_depth = math.floor(32 / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line
		elif min_ia_buffer_depth <= 64:
			ia_buffer_has_lutrams = True
			min_ia_buffer_depth = math.floor(64 / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line
		else:
			if min_ia_buffer_depth < 128:
				print("WARNING: IABuffer depth greater than 64, less than 128 ", min_ia_buffer_depth)
			target_buffer_depth = math.ceil(max_ia_buffer_depth / 512) * 512
			min_ia_buffer_depth = math.floor(target_buffer_depth / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line

		lines_in_ia_buffer = min_ia_buffer_depth // max_channels_in_input_buffer_line 
		self.lines_in_ia_buffer = lines_in_ia_buffer

		sorted_x_accumulation = estimate.sorted_x_accumulation(node)
		#sorted_x_accumulation = False
		if sorted_x_accumulation:
			post_dsp_x_mux_delay = 1
			dsp_to_x_mux_delay = math.ceil(math.sqrt(n_channel_splits) / 3)
			has_stride_counter = min(stride, kernel_width) > 1
			in_dsp_round_const = -1
			if round_const == -1:
				round_const = 0
			else:
				round_const = 1 << round_const
		else:
			post_dsp_x_mux_delay = 0
			has_stride_counter = False
			dsp_to_x_mux_delay = 0
			in_dsp_round_const = round_const

		# Delay scheme
		# OC Controller controls which Weight Buffer Address to read from
		# + 2 Cycles for read
		# + 1 Cycle for RLProcessor
		# + 1 Cycle to add RL address to line address
		# + 1 Cycle to MOD added RL Address by the memory depth
		# + 2 Cycles for read from Activation Buffer
		# + 2 Cycles to mux the right x
		# + 4 cycles to multiply and accumulate + external_reduction delay (computed in BasicConvDSPs)

		memory_model_delay        = 1
		memory_read_delay         = 2
		if min_ia_buffer_depth < 64:
			memory_read_delay = 1
		added_read_delay          = memory_read_delay - memory_model_delay

		weight_buffer_to_rl_processor_delay = 1
		rl_processor_delay        = 1
		line_addr_add_delay       = 1
		address_mod_delay         = 1
		address_computation_delay = weight_buffer_to_rl_processor_delay + rl_processor_delay + line_addr_add_delay + address_mod_delay
		overflow_detection_delay  = 2

		mux_delay                 = 2
		if kernel_width <= 4:
			mux_delay = 1
		if kernel_width <= 2:
			mux_delay = 0
		mux_to_dsp_delay          = 1 #math.ceil(math.sqrt(n_channel_splits * output_width) / 4.)
		if ia_buffer_has_lutrams and kernel_width == 1:
			mux_to_dsp_delay = 0

		if sorted_x_accumulation:
			mux_to_dsp_delay = 0
			mux_delay = 0

		# The in_oc_counter counts down from the number of weights in the output channel minus 1 to zero
		#  We use the reset signal from this to determine when to accumulate.  We don't want to accumulate
		#  for the first element because we need to start the accumulation from zero, but the reset signal
		#  happens one cycle earlier than this so we need to add a delay to align them.
		in_oc_reset_to_counter_start_delay = 1

		done_to_controller_delay = 4
		reset_fanout_delay = 0

		ia_shape = node.inputs[0]._from.example_outputs[0].shape
		if type(node.properties["padding"]) is bytes:
			node.properties["padding"] = node.properties["padding"].decode("ascii")
		padding, _ = get_padding(
			ia_shape,
			k_shape,
			stride,
			node.properties["padding"],
			node.explicit_padding)
		self.padding = padding
		dsp_chain_count = output_width
		if sorted_x_accumulation and horizontal_stride > 1:
			total_input_width = np.sum(padding[2]) + ia_shape[2]
			used_input_width = output_width * horizontal_stride
			if kernel_width > horizontal_stride:
				used_input_width += kernel_width - horizontal_stride
			dsp_chain_count = math.ceil(used_input_width / horizontal_stride)


		self.ibc = inst(InputBufferController,
			vertical_stride,
			kernel_height,
			input_channels,
			input_height,
			padding[1],
			n_channel_splits,
			output_height, min_buffer_depth=lines_in_ia_buffer)
		self.space_to_write_line.set_driver(self.ibc.space_to_write_line)
		self.ibc.write.set_driver(self.write)
		self.ibc.done_image.set_driver(self.done.delay(done_to_controller_delay))
		
		use_ia_buffer = max_ia_buffer_depth > 64 or ((kernel_width > 1 or horizontal_stride > 1) and not sorted_x_accumulation) or has_stride_counter
		#use_ia_buffer = True
	
		if use_ia_buffer:
			self.ia_buffer = inst(IABuffer,
				planned_bits[1],
				kernel_height,
				stride,
				k_shape[2],
				input_width,
				write_port_width=parallel_acts_per_ram, ic_groups=n_channel_splits, min_buffer_depth=lines_in_ia_buffer)
		else:
			assert(not has_stride_counter)
			self.ia_buffer = inst(SequentialDistributedActivationBuffer,
				planned_bits[1],
				kernel_height,
				stride,
				k_shape[2],
				input_width,
				write_port_width=parallel_acts_per_ram, ic_groups=n_channel_splits, min_buffer_depth=lines_in_ia_buffer)

		# drive write control signals for IABuffer (write addrs, write enables, and write data)
		# Addrs:
		for ibc_w_addr, ia_buffer_w_addr in zip(self.ibc.write_addresses, self.ia_buffer.w_addrs):
			ia_buffer_w_addr.set_driver(ibc_w_addr)
		# Enables:
		for wo, ia_buffer_write in zip(self.ibc.write_outs, self.ia_buffer.writes):
			ia_buffer_write.set_driver(wo)
		# Data:
		ram_inputs = self.inputs
		if self.ibc.has_padding:
			padding_zeros = inst(dl.Constant, bits=planned_bits[1], value=0, name="padding_zeros")
			ram_inputs = [inst(dl.MuxV2, w, padding_zeros, i, name="input_or_padding_"+str(j))for j,(i,w) in enumerate(zip(self.inputs, self.writes))]

			"""
			padding_zeros = inst(dl.Constant, bits=planned_bits[1], value=0, name="padding_zeros")
			ram_inputs = []
			for i in range(math.ceil(len(self.inputs) / 2)):
				with dl.ParamScope(clock=clock_2x):
					ab_input_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=1)
				pad_mux_sel = inst(dl.Concat, self.writes, ab_input_counter, name="pad_mux_sel")
				ram_inputs.append(inst(dl.MuxV2,
					pad_mux_sel, padding_zeros, *self.inputs[i*2:i*2+2], name=f"input_or_padding_{i}"))"""

		for ri, ia_buffer_w_data in zip(ram_inputs, self.ia_buffer.inputs):
			ia_buffer_w_data.set_driver(ri)

		staggered_weight_info = BasicConv.get_staggered_weight_info(
			parallel_weights=n_channel_splits,
			stagger_every_n_weights=stagger_every_n_weights,
			break_chain_every_n_weights=break_chain_every_n_weights,
			delay_for_chain_break=delay_for_chain_break)
		max_stagger = max(staggered_weight_info["staggers"])

		(weights_per_oc,
			weights_per_oc_bits,
			serial_weights,
			serial_weights_bits,
			numpy_combined_weights) = self._get_serialized_weights_and_rls(
				parallel_weights=n_channel_splits, stagger_every_n_weights=stagger_every_n_weights,
				is_depthwise=is_depthwise, staggered_weight_info=staggered_weight_info,
				group_x_dim=sorted_x_accumulation,
				delay_for_chain_break=delay_for_chain_break)
		example_input = node.input_nodes()[0].example_outputs[0]
		self.numpy_combined_weights = numpy_combined_weights
		self.weights = self.node.values[0]
		self.weights_per_oc = weights_per_oc
		self.example_input = example_input
		if node.explicit_padding is not None:
			self.example_input = np.pad(example_input, node.explicit_padding, "constant", constant_values=0)
		if padding[2][0] >= 0:
			self.padded_example_input = np.pad(example_input, padding, "constant", constant_values=0)
		else:
			self.padded_example_input = example_input[0,:padding[1][0],:padding[2][0],:]

		#print("  Serial weights bits " + str(serial_weights_bits))
		bits = []
		for i in range(n_channel_splits):
			bits.append(serial_weights_bits[i * 3])
			bits.append(planned_bits[1])

		dsps = inst(BasicConvDSPs,
			dsp_chain_count, bits, [input_signed, weights_signed],
			parallel_weights=n_channel_splits,
			parallel_weights_per_dsp=parallel_weights_per_dsp,
			reduce_with_carry_out=reduce_with_carry_out,
			build_data_path=build_data_path,
			staggered_weight_info=staggered_weight_info,
			delay_for_chain_break=delay_for_chain_break,
			round_const=in_dsp_round_const,
			name="dsps")
		mac_delay = dsps.added_delay
		self.dsps = dsps
		self.utilization_estimates["DSPs"] = self.dsps.count

		weight_bit_divisions = np.pad(np.cumsum(serial_weights_bits), pad_width=[1, 0], mode="constant", constant_values=0)
		self.weight_buffer = inst(dl.FanoutROM,
			content_list=serial_weights,
			bits=np.sum(serial_weights_bits),
			physically_mapped=physically_mapped,
			name="weight_buffer")
		#print("  Weight bit divisions: " + str(weight_bit_divisions))
		self.utilization_estimates["Weight Buffer Lines"] = len(serial_weights)
		self.utilization_estimates["Weight Buffer Width"] = np.sum(serial_weights_bits)
		
		self.weight_buffer_read_enable = inst(dl.Logic, 1, name="weight_buffer_read_enable")
		self.weight_buffer.r_en.set_driver(self.weight_buffer_read_enable)
		self.weight_buffer_addr_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=len(serial_weights)-1,
			name="weight_buffer_read_addr")
		self.weight_buffer_addr_counter.set_increment_condition(self.weight_buffer_read_enable)
		self.weight_buffer.r_addr.set_driver(self.weight_buffer_addr_counter.current_value)

		self.oc_controller = inst(OCController,
			weights_per_oc,
			weights_per_oc_bits,
			done_delay=max_stagger,
			name="output_channel_controller")
		normal_go = inst(dl.AND, self.can_write_line, self.ibc.has_full_kernel.delay(2))
		self.oc_controller.go.set_driver(normal_go)
		self.ibc.finished_reading_line.set_driver(self.oc_controller.done.delay(self.weight_buffer.read_delay+weight_buffer_to_rl_processor_delay + rl_processor_delay + line_addr_add_delay + address_mod_delay))
		self.ibc.started_reading_line.set_driver(self.oc_controller.state_machine.is_start.delay(2))
	
		self.weight_buffer_read_enable.set_driver(self.oc_controller.should_read_weights)

		# Stamp out multiple state machines for the weight buffer since the FIFO could be very wide
		"""oc_controllers = [self.oc_controller]
		if isinstance(self.weight_buffer.rom, dl.FIFORingROM):
			for i in range(0, len(self.weight_buffer.rom.fifo.fifos) // 6, 6):
				ibc = inst(InputBufferController, vertical_stride, kernel_height, input_channels, input_height, padding[1], n_channel_splits, output_height, min_buffer_depth=lines_in_ia_buffer)
				ibc.write.set_driver(self.write)
				ibc.done_image.set_driver(self.done.delay(done_to_controller_delay))

				oc_controller = inst(OCController,
					weights_per_oc,
					weights_per_oc_bits,
					done_delay=max_stagger,
					name="output_channel_controller")
				oc_controllers.append(oc_controller)
				oc_controller.go.set_driver(inst(dl.AND, self.can_write_line, ibc.has_full_kernel, inst(dl.NOT, self.weight_buffer.rom.fifo.fifos[i].empty)))
				ibc.finished_reading_line.set_driver(oc_controller.done)
				ibc.started_reading_line.set_driver(oc_controller.state_machine.c["is_start"])
				for fifo in self.weight_buffer.rom.fifo.fifos[i:i+6]:
					fifo.r_en.set_driver(oc_controller.should_read_weights)"""


		self.rls = []
		self.read_addresses = []
		with dl.ModuleScope(inst(dl.Module, "activation_address_computers",
				kind="activation_address_computers", name="activation_address_computers")):
			for i in range(n_channel_splits):
				stagger_amount = staggered_weight_info["staggers"][i]

				# Each address computer computes addresses for a group of input channels,
				# so group channels is the number of channels in this group
				group_channels = self.node.values[0][0,0,i::n_channel_splits,0].shape[0]

				# to minimize the load on individual drivers we will take our done line
				# signal from one of the oc controller copies
				#oc_controller_index = int((i / n_channel_splits) * (len(oc_controllers)-1) + 0.5)
				occ = self.oc_controller#oc_controllers[oc_controller_index]
				done_line = occ.done.delay(self.weight_buffer.read_delay+stagger_amount+weight_buffer_to_rl_processor_delay)

				# each address computer will keep track of the current output index
				# so that it knows when we finish an input image.
				output_line_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=output_height-1,
					name="output_line_counter")
				output_line_counter.set_increment_condition(done_line)

				# The line address counter is the base address to which the decoded RL
				#  values are added.  Basically as the convolution window shifts down
				#  we increment this counter.  Our buffer has limited space though, so
				#  the counter decrements itself by the number of input lines we store
				#  whenever it exceeds the number of lines we store.
				# The second change value handles the edge case of the end of an input
				#  image where we need to add not the stride, but the size of the kernel.
				#  the stride will be added by the first change value, and the difference
				#  between the stride and the kernel size added by the second change 
				#  value.
				# The third change value is for Depthwise convolutions.  Since the input
				#  channel is no longer computed by runlengths in depthwise convolutions,
				#  we have added the input channel component to the line_address_counter
				line_addr_counter_reset_value = 0
				total_lines_written = max(np.sum(padding[1]),0) + input_height
				target_second_start_value = (group_channels * total_lines_written + line_addr_counter_reset_value)
				value_after_output_lines_read = (group_channels * stride * output_height)
				start_end_difference = target_second_start_value - value_after_output_lines_read
				# The modulo only works if we go over the modulo value. If our change value
				# is negative and we are at address 0, then we go negative and end up with
				# something totally wrong
				if start_end_difference < 0:
					start_end_difference += group_channels * lines_in_ia_buffer
				line_address_change_values = [
						group_channels * stride,
						start_end_difference]
				if is_depthwise:
					line_address_change_values.append(1)
					line_address_change_values[0] -= group_channels
				line_address_counter = inst(dl.ModuloMultiChangeCounter,
					reset_value=line_addr_counter_reset_value,
					change_values=line_address_change_values,
					modulo_value=group_channels * lines_in_ia_buffer,
					end_value=group_channels * (lines_in_ia_buffer + 2),
					tolerable_delay=1, # this only changes when we finish a line, so we can
					                   # tolerate at least 2 cycles to perform the modulo operation.
					                   # This optimization is for timing.
					name="line_address")
				line_address_counter.change_signals[0].set_driver(done_line)
				line_address_counter.change_signals[1].set_driver(inst(dl.AND, done_line, output_line_counter.is_done.delay(1)))
				if is_depthwise:
					line_address_counter.change_signals[2].set_driver(occ.done_oc.delay(stagger_amount),
						delay=self.weight_buffer.read_delay + weight_buffer_to_rl_processor_delay+rl_processor_delay)

				with dl.ParamScope(name="runlength"):
					
					upper_index = weight_bit_divisions[i*3+2] - 1
					lower_index = weight_bit_divisions[i*3+1]
					
					if upper_index < lower_index: 
						raise Exception("Error! The runlengths for this input channel group", i, "have a width of 0. This is a bug and the hardware generated if we continued would be incorrect. Please see https://gitlab.com/mathewkhall/hpipe/-/issues/4 for more information")
					
					self.rls.append(self.weight_buffer.r_data[upper_index:lower_index].delay(weight_buffer_to_rl_processor_delay))
							
				rl = self.rls[-1]
				if is_depthwise:
					rl = inst(dl.MUL, rl, inst(dl.Constant, value=group_channels), extend_to_match_bits=False)
				rl_bits = rl._bit_width

				# this is where rl_processor_delay comes from
				rl_processor = inst(RLProcessor,
					rl_bits,
					self.ia_buffer.r_addrs[i]._bit_width,
					name="rl_processor_c_" + str(i))

				# technically the chip supports reads with a single cycle delay, so that's what the memory models,
				# but that only works for a low FMAX, so we need to add an additional cycle
				#
				rl_processor.rl.set_driver(rl)
				# delay the read signal by the memory read delay to create a valid signal
				rl_processor.rl_valid.set_driver(self.weight_buffer_read_enable, delay=self.weight_buffer.read_delay + weight_buffer_to_rl_processor_delay)
				rl_processor.clear.set_driver(occ.in_oc_counter_reset.delay(stagger_amount),
					delay=self.weight_buffer.read_delay + weight_buffer_to_rl_processor_delay + rl_processor_delay)

				# This is where line_addr_add_delay comes from
				line_address_plus_rl_address = inst(dl.Flop,
					driver=line_address_counter.current_value + rl_processor.address,
					name="line_address_plus_rl_address")

				to_add = inst(dl.MuxV2,
					line_address_plus_rl_address >= inst(dl.Constant,
						bits=line_address_plus_rl_address._bit_width,
						value=group_channels * (lines_in_ia_buffer), name="last_address"),
					inst(dl.Constant,
						bits=line_address_plus_rl_address._bit_width,
						value=0, name="dont_subtract_buffer_depth_zeros"),
					inst(dl.Constant,
						bits=line_address_plus_rl_address._bit_width,
						value=-group_channels * (lines_in_ia_buffer),
						name="negative_buffer_depth"))

				# This is where address_mod_delay comes from
				address = inst(dl.Flop,
					driver=(line_address_plus_rl_address + to_add)[self.ia_buffer.r_addrs[i]._bit_width-1:0],
					name="read_address_" + str(i))
				self.ia_buffer.r_addrs[i].set_driver(address)
				self.read_addresses.append(address)
		self.ia_buffer.read.set_driver(self.weight_buffer_read_enable.delay(self.weight_buffer.read_delay+address_computation_delay))

		act_indices = None
		if serial_weights_bits[2] != 0:
			with dl.ParamScope(name="act_index"):
				act_indices = [self.weight_buffer.r_data[weight_bit_divisions[i*3+3]-1:weight_bit_divisions[i*3+2]] for i in range(n_channel_splits)]
			act_index_fanouts = [[inst(dl.Logic, act_indices[i]._bit_width, name="act_index_" + str(i) + "_fanout") for _ in range(dsp_chain_count)] for i in range(n_channel_splits)]
		else:
			mux_delay = 0
		if sorted_x_accumulation:
			mux_delay = 0
		with dl.ParamScope(name="weight"):
			weights = [self.weight_buffer.r_data[weight_bit_divisions[i*3+1]-1:weight_bit_divisions[i*3]] for i in range(n_channel_splits)]
		weight_fanouts_in = [[inst(dl.Logic, bits=weights[i]._bit_width, name="weight_" + str(i) + "_fanout") for _ in range(dsp_chain_count)] for i in range(n_channel_splits)]
		weight_spread_fanout_delay = min(2, math.floor(((n_channel_splits**2) * dsp_chain_count)**(0.25)/3))
		weight_fanout_pipe_levels = max(math.ceil(math.log(dsp_chain_count, 1 + 32 / n_channel_splits)), 2)
		if not use_ia_buffer:
			weight_spread_fanout_delay = 0
			weight_fanout_pipe_levels = 1
		weight_fanouts = [[wf.delay(weight_spread_fanout_delay) for wf in wfil] for wfil in weight_fanouts_in]

		accumulate_fanouts_in = [inst(dl.Logic, bits=1, synthesis_attributes="synthesis dont_merge", name="accumulate") for _ in range(dsp_chain_count)]
		accumulate_fanouts = [af.delay(weight_spread_fanout_delay) for af in accumulate_fanouts_in]

		weight_shift_delay = address_computation_delay + self.ia_buffer.control_delay + self.ia_buffer.read_delay + mux_delay + mux_to_dsp_delay - weight_fanout_pipe_levels
		if use_ia_buffer:
			piped_accumulate_fanouts = inst(dl.PipelinedFanout,
				inst(dl.NOT, self.oc_controller.in_oc_counter_reset, name="accumulate").delay(
					in_oc_reset_to_counter_start_delay + 
					self.weight_buffer.read_delay + 
					address_computation_delay + 
					self.ia_buffer.control_delay +
					self.ia_buffer.read_delay +
					mux_delay +
					mux_to_dsp_delay +
					max_stagger - weight_fanout_pipe_levels - weight_spread_fanout_delay),
				*accumulate_fanouts_in,
				levels=weight_fanout_pipe_levels,
				name="accumulate_fanout_pipe")

			piped_weight_fanouts = [inst(dl.PipelinedFanout,
				w.delay(weight_shift_delay - weight_spread_fanout_delay, pack_to_memory=True),
				*weight_fanouts_in[i],
				levels=weight_fanout_pipe_levels,
				name="weight_" + str(i) + "_fanout_pipe") for i,w in enumerate(weights)]
		else:
			print(
				in_oc_reset_to_counter_start_delay,
				self.weight_buffer.read_delay,
				address_computation_delay,
				self.ia_buffer.control_delay,
				mux_delay,
				mux_to_dsp_delay,
				max_stagger,
				weight_fanout_pipe_levels,
				weight_spread_fanout_delay)
			# NOTE: Need to insure delay accounts for stride group size for pre muxes
			weight_fanout_chains = []
			accumulate_fanout_chains = []
			delayed_a = inst(dl.NOT, self.oc_controller.in_oc_counter_reset, name="accumulate").delay(
				in_oc_reset_to_counter_start_delay + 
				self.weight_buffer.read_delay + 
				address_computation_delay + 
				self.ia_buffer.control_delay +
				mux_delay +
				mux_to_dsp_delay -
				weight_fanout_pipe_levels +
				max_stagger - 1)
			for chain_number in range(math.ceil(dsp_chain_count / self.ia_buffer.max_offset)):
				accumulate_fanout_chains.append(inst(dl.BasicShiftRegister,
						1, self.ia_buffer.max_offset,
						name="accumulate_chain_" + str(chain_number)))
				accumulate_fanout_chains[-1].d.set_driver(delayed_a)

			for wn, w in enumerate(weights):
				delayed_w = w.delay(weight_shift_delay - self.ia_buffer.read_delay - 2, pack_to_memory=True)
				parallel_chains = []
				for chain_number in range(self.ia_buffer.parallel_sequential_fanouts):
					parallel_chains.append(inst(dl.BasicShiftRegister,
						w._bit_width, self.ia_buffer.max_offset,
						name="weight_" + str(wn) + "_chain_" + str(chain_number)))
					parallel_chains[-1].d.set_driver(delayed_w)
				weight_fanout_chains.append(parallel_chains)

			for i,afi in enumerate(accumulate_fanouts_in):
				afi.set_driver(accumulate_fanout_chains[i//self.ia_buffer.max_offset].sr[i%self.ia_buffer.max_offset])

			for ic_group, (wfl, wcl) in enumerate(zip(weight_fanouts_in, weight_fanout_chains)):
				for activation_number, wf in enumerate(wfl):
					wf.set_driver(wcl[activation_number//self.ia_buffer.max_offset].sr[activation_number%self.ia_buffer.max_offset].delay(1))


		control_to_output_valid_delay = (
			self.weight_buffer.read_delay +
			address_computation_delay +
			self.ia_buffer.control_delay +
			self.ia_buffer.read_delay +
			mux_delay +
			mux_to_dsp_delay +
			mac_delay +
			dsp_to_x_mux_delay +
			post_dsp_x_mux_delay +
			overflow_detection_delay)
		output_valid_fanout_levels = math.ceil(math.log(dsp_chain_count, 4))
		piped_output_valid_fanouts = inst(dl.PipelinedFanout,
			self.output_valid.delay(
				control_to_output_valid_delay - 
				output_valid_fanout_levels),
			*self.outputs_valid,
			levels=output_valid_fanout_levels,
			name="output_valid_fanout_pipe")

		if act_indices is not None:
			total_delay = address_computation_delay + self.ia_buffer.control_delay + self.ia_buffer.read_delay
			fanout_component = min(total_delay, math.ceil(math.log(n_channel_splits * dsp_chain_count, 4)))
			piped_act_index_fanouts = [inst(dl.PipelinedFanout,
				ai.delay(total_delay - fanout_component),
				*act_index_fanouts[i],
				levels=fanout_component,
				name="act_index_" + str(i) + "_fanout_pipe") for i,ai in enumerate(act_indices)]

		self._set_control_to_input_delay(self.ibc.write_counter_increment_delay + self.ia_buffer.control_to_input_delay)
		self._set_control_valid_to_output_valid_delay(control_to_output_valid_delay)

		r_data = []
		for rdl in self.ia_buffer.outputs:
			r_data_l = []
			for rd in rdl:
				r_data_l.append(rd.delay(mux_delay))
			r_data.append(r_data_l)
		if sorted_x_accumulation:
			pre_dsp_mux_size = min(stride, kernel_width)
			pre_dsp_mux_name = "stride_muxes"
		else:
			pre_dsp_mux_size = kernel_width
			pre_dsp_mux_name = "x_muxes"
		pre_dsp_muxes = inst(dl.Module, pre_dsp_mux_name, kind=pre_dsp_mux_name, name=pre_dsp_mux_name)
		if not sorted_x_accumulation or has_stride_counter:
			with dl.ModuleScope(pre_dsp_muxes):
				padding_data = inst(dl.Constant, bits=r_data[0][0]._bit_width, value=0, name="x_padding_zeros")
			for i in range(n_channel_splits):
				if padding[2][0] >= 0:
					r_data[i] = [padding_data] * padding[2][0] + r_data[i] + [padding_data] * padding[2][1]

		self.dsp_valid = inst(dl.Logic, bits=1, name="dsp_valid")
		self.not_accumulate = inst(dl.Logic, bits=1, name="not_accumulate")
		self.dsp_valid_no_stagger = inst(dl.Logic, bits=1, name="dsp_valid_no_stagger")
		self.not_accumulate_no_stagger = inst(dl.Logic, bits=1, name="not_accumulate_no_stagger")
		if sorted_x_accumulation:
			kernel_x_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=kernel_width-1, name="kernel_x_counter")
			kernel_x_counter.set_increment_condition(inst(dl.AND, self.dsp_valid, self.not_accumulate))
			post_dsp_x_count = kernel_x_counter
			total_delay = (
				self.weight_buffer.read_delay +
				address_computation_delay +
				self.ia_buffer.control_delay +
				self.ia_buffer.read_delay)
			if has_stride_counter:
				stride_kernel_x_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=kernel_width-1, name="stride_kernel_x_counter")
				stride_kernel_x_counter.set_increment_condition(inst(dl.AND, self.dsp_valid_no_stagger, self.not_accumulate_no_stagger))
				stride_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=min(stride, kernel_width)-1, name="stride_counter")
				stride_counter.set_increment_condition(inst(dl.AND, self.dsp_valid_no_stagger, self.not_accumulate_no_stagger))
				stride_counter.reset_signal.set_driver(inst(dl.OR, kwargs["reset"], stride_counter.done_and_incrementing, stride_kernel_x_counter.done_and_incrementing))
				stride_counter_fanout = [[inst(dl.Logic, bits=stride_counter.current_value._bit_width, name=f"stride_counter_fanout_{i}_{j}") for j in range(dsp_chain_count)] for i in range(n_channel_splits)]

				fanout_component = min(total_delay, math.ceil(math.log(dsp_chain_count, 4)))
				previous_stagger = 0
				staggered_stride_counter = stride_counter.current_value
				for i in range(n_channel_splits):
					stagger_amount = staggered_weight_info["staggers"][i]
					to_stagger = stagger_amount - previous_stagger
					previous_stagger = stagger_amount
					staggered_stride_counter = staggered_stride_counter.delay(to_stagger)
					inst(dl.PipelinedFanout,
						staggered_stride_counter.delay(total_delay - fanout_component),
						*stride_counter_fanout[i],
						levels=fanout_component,
						name="stride_counter_fanout_pipe")
				kernel_from_stride_column_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(kernel_width / stride)-1)
				kernel_from_stride_column_counter.set_increment_condition(stride_counter.done_and_incrementing.delay(max_stagger))
				kernel_from_stride_column_counter.reset_signal.set_driver(inst(dl.OR, kwargs["reset"], kernel_from_stride_column_counter.done_and_incrementing.delay(max_stagger), stride_kernel_x_counter.done_and_incrementing.delay(max_stagger)))
				post_dsp_x_count = kernel_from_stride_column_counter

			fanout_count = dsp_chain_count
			post_dsp_x_count_fanout = [inst(dl.Flop,
				bits=post_dsp_x_count.current_value._bit_width,
				name="post_dsp_x_count_fanout_" + str(i)) for i in range(fanout_count)]
			total_delay += mux_delay + mux_to_dsp_delay + mac_delay + dsp_to_x_mux_delay - 1
			fanout_component = min(total_delay, math.ceil(math.log(fanout_count, 4)))
			inst(dl.PipelinedFanout,
				post_dsp_x_count.current_value.delay(total_delay - fanout_component),
				*post_dsp_x_count_fanout,
				levels=fanout_component,
				name="post_dsp_x_counter_fanout_pipe")


		with dl.ModuleScope(pre_dsp_muxes):
			for i in range(dsp_chain_count):
				dsp_base_index = i*n_channel_splits*2
				muxes = []
				for j in range(n_channel_splits):
					if has_stride_counter:
						mux = inst(dl.MuxV2, stride_counter_fanout[j][i], *r_data[j][i*stride:i*stride+pre_dsp_mux_size]).delay(mux_to_dsp_delay)
						muxes.append(mux)
					elif act_indices is not None:
						mux = inst(dl.MuxV2, act_index_fanouts[j][i].delay(mux_delay), *r_data[j][i*stride:i*stride+pre_dsp_mux_size]).delay(mux_to_dsp_delay)
						muxes.append(mux)
					else:
						mux = r_data[j][i*stride].delay(mux_to_dsp_delay)
						muxes.append(mux)
				for j in range(n_channel_splits):
					dsps.inputs[dsp_base_index+j*2+1].set_driver(muxes[j])
					dsps.inputs[dsp_base_index+j*2].set_driver(weight_fanouts[j][i])
				dsps.accumulate_signals[i].set_driver(accumulate_fanouts[i])

		self.not_accumulate.set_driver(self.oc_controller.in_oc_counter_reset.delay(max_stagger))
		self.dsp_valid.set_driver(self.weight_buffer_read_enable.delay(max_stagger))
		self.not_accumulate_no_stagger.set_driver(self.oc_controller.in_oc_counter_reset)
		self.dsp_valid_no_stagger.set_driver(self.weight_buffer_read_enable)
		if sorted_x_accumulation:
			x_counter_increment = inst(dl.AND, self.dsp_valid, self.not_accumulate)
			x_counter = inst(dl.Counter, reset_value=0, end_value=kernel_width-1, increment_value=1, name="x_counter")
			x_counter.set_increment_condition(x_counter_increment)
			external_accumulate_clear_fanout = [inst(dl.Flop, bits=1, name="external_accumulate_clear_fanout_" + str(i)) for i in range(output_width)]
			external_accumulate_add_fanout = [inst(dl.Flop, bits=1, name="external_accumulate_add_fanout_" + str(i)) for i in range(output_width)]

			total_delay = (
				self.weight_buffer.read_delay +
				address_computation_delay +
				self.ia_buffer.control_delay +
				self.ia_buffer.read_delay +
				mux_delay +
				mux_to_dsp_delay +
				mac_delay +
				dsp_to_x_mux_delay -
				1)
			fanout_count = output_width
			fanout_component = min(total_delay, math.ceil(math.log(fanout_count, 4)))
			external_accumulate_add_fanout_pipe = inst(dl.PipelinedFanout,
				x_counter_increment.delay(total_delay - fanout_component),
				*external_accumulate_add_fanout,
				levels=fanout_component)
			external_accumulate_clear_fanout_pipe = inst(dl.PipelinedFanout,
				x_counter.done_and_incrementing.delay(total_delay - fanout_component + 1),
				*external_accumulate_clear_fanout,
				levels=fanout_component)
			self.output_valid.set_driver(inst(dl.AND, x_counter_increment, x_counter.is_done))
		else:
			self.output_valid.set_driver(inst(dl.AND, self.dsp_valid, self.not_accumulate))
		self.requires_driver = [self.can_write_line]


		if use_ia_buffer:
			realign_delays = [0 for _ in range(dsp_chain_count)]
		else:
			realign_delays = [self.ia_buffer.max_offset - 1 - i % self.ia_buffer.max_offset for i in range(dsp_chain_count)]
		outputs = self.dsps.outputs
		output_cast_modules = self.dsps.dsp_scopes
		if sorted_x_accumulation:
			external_accumulator_bits = 36
			delayed_accumulated_data = [o[external_accumulator_bits-1:0].delay(d, pack_to_memory=True).delay(dsp_to_x_mux_delay) for o,d in zip(self.dsps.outputs, realign_delays)]
			realign_delays = [0 for _ in range(len(self.outputs))]
			if not has_stride_counter:
				padding_data = inst(dl.Constant, bits=delayed_accumulated_data[0]._bit_width, value=0, name="x_padding_zeros")
				delayed_accumulated_data = [padding_data] * padding[2][0] + delayed_accumulated_data + [padding_data] * padding[2][1]
			chains_per_output = math.ceil(kernel_width / stride)
			outputs = []
			output_cast_modules = self
			for i in range(output_width):
				accumulation_register = inst(dl.Flop, bits=external_accumulator_bits, signed=True, name="external_accumulation_register_" + str(i))
				round_constant = inst(dl.Constant, bits=external_accumulator_bits, value=round_const, signed=True, name="round_const")
				accumulation_register.set_reset_driver(round_constant)
				accumulation_register_or_round_const = inst(dl.MuxV2, external_accumulate_clear_fanout[i], accumulation_register, round_constant, signed=True, name="accumulation_register_or_round_const")
				kernel_select_mux = inst(dl.MuxV2, post_dsp_x_count_fanout[i], *delayed_accumulated_data[i:i+chains_per_output], signed=True, name="kernel_select_mux")
				accumulate_mux = inst(dl.MuxV2, external_accumulate_add_fanout[i], inst(dl.Constant, bits=external_accumulator_bits, signed=True, value=0), kernel_select_mux, name="accumulate_mux")
				accumulation_register.set_driver((accumulation_register_or_round_const + accumulate_mux)[external_accumulator_bits-1:0])
				outputs.append(accumulation_register)
			from_spec = {
				"frac" : fractional_bits_in_output,
				"sign" : 1,
				"int" : external_accumulator_bits - 1 - fractional_bits_in_output
			}




		with dl.ParamScope(name="conv_mac_output"):
			for i,(o, realign_delay) in enumerate(zip(outputs, realign_delays)):
				if type(output_cast_modules) is list:
					output_cast_module = output_cast_modules[i]
				else:
					output_cast_module = output_cast_modules
				with dl.ModuleScope(output_cast_module):
					output = o
					self.outputs[i].set_driver(cast_fixed(o.delay(overflow_detection_delay), from_spec, to_spec).delay(realign_delay, pack_to_memory=True))
		ibc_is_idle = inst(dl.Logic, bits=1, name="ibc_is_idle")
		ibc_is_idle.set_driver(self.ibc.state_machine.c["is_idle"])
		"""self.debug_stall = inst(dl.AND,
			ibc_is_idle,
			inst(dl.NOT, dl.Verilog.current_params.kwargs["reset"]),
			inst(dl.NOT, self.space_to_write_line),
			inst(dl.NOT, self.ibc.has_full_kernel),
			inst(dl.NOT, self.oc_controller.state_machine.is_processing_ocs))"""

	@classmethod
	def get_staggered_weight_info(cls,
		parallel_weights=2,
		stagger_every_n_weights=0,
		break_chain_every_n_weights=57,
		delay_for_chain_break=4):

		staggers = []
		multiplications_per_dsp = []
		chain_lengths = []
		if stagger_every_n_weights > 0:
			if break_chain_every_n_weights == 0 or parallel_weights <= (break_chain_every_n_weights + stagger_every_n_weights - 1):
				chains = 1
				total_systolic_delay = (parallel_weights - 1) // stagger_every_n_weights
				chain_lengths = [total_systolic_delay + 1]
				multipliers_per_chain = [parallel_weights]
				multiplications_per_dsp = [parallel_weights]
			else:
				chain_breaks = parallel_weights // break_chain_every_n_weights
				# We can only fit one multiplication in the first DSP in a chain if we split it,
				# but if we don't split it, we can fit parallel_weights.  In the case that
				# parallel_weights % break_chain_every_n_weights is less than parallel_weights, we can actually pack the last
				# broken chain into the second last chain (since there's an additional parallel_weights multipliers if we don't break it)
				first_chain_length = (parallel_weights % break_chain_every_n_weights)
				combine_first_two_chains = first_chain_length < stagger_every_n_weights
				if combine_first_two_chains:
					chain_breaks -= 1
					first_chain_length += break_chain_every_n_weights

				last_chain_delay = (first_chain_length - 1) // stagger_every_n_weights

				break_delay = delay_for_chain_break * chain_breaks
				systolic_delay_per_chain = (break_chain_every_n_weights - 1) // stagger_every_n_weights
				nonbreak_delay = chain_breaks * systolic_delay_per_chain + last_chain_delay
				total_systolic_delay = nonbreak_delay + break_delay
				chains = chain_breaks + 1
				chain_lengths = [last_chain_delay + 1] + [systolic_delay_per_chain + 1] * chain_breaks
				multipliers_per_chain = [first_chain_length] + [break_chain_every_n_weights] * chain_breaks

			# Compute the total systolic delay of the multiplier chain (excludes DSP input register delays)
			stagger_amount = 0
			accumulated_index = 0
			for chain_index, chain_length in enumerate(chain_lengths):
				multipliers_remaining_in_chain = multipliers_per_chain[chain_index]
				for i in range(chain_length):
					multipliers_for_this_dsp = multipliers_remaining_in_chain % stagger_every_n_weights
					if multipliers_for_this_dsp == 0:
						multipliers_for_this_dsp = stagger_every_n_weights
					if chain_index != 0 and i == 0:
						multipliers_for_this_dsp = 1
					multiplications_per_dsp.append(multipliers_for_this_dsp)
					staggers += [stagger_amount] * multipliers_for_this_dsp
					accumulated_index += multipliers_for_this_dsp
					multipliers_remaining_in_chain -= multipliers_for_this_dsp
					stagger_amount += 1
				stagger_amount += delay_for_chain_break
		else:
			chain_lengths = [parallel_weights]
			staggers = [0] * parallel_weights
			multipliers_per_chain = [parallel_weights]

		return {
			"staggers" : staggers,
			"multipliers_per_chain": multipliers_per_chain,
			"multipliers_per_dsp": multiplications_per_dsp,
			"chain_lengths" : chain_lengths
		}


	@classmethod
	def get_serialized_weights_and_rls(cls,
		weights,
		parallel_weights=2,             # Number of multiplications per DSP
		stagger_every_n_weights=0,      # Adds a delay of 1 cycle every N weights for systolic chaining of DSPs
		delay_for_chain_break=4,        # Delay added whenever a DSP chain is broken.  The default is the number of input pipeline stages for a DSP plus the output DSP delay
		staggered_weight_info=None,
		group_x_dim=False,
		is_depthwise=False):            # Depthwise is a little different since each output channel has only one input channel
		assert(staggered_weight_info is not None)
		weight_order = {"H" : 0, "W" : 1, "I" : 2, "O" : 3}
		
		if is_depthwise:
			weights = np.reshape(weights, [weights.shape[0], weights.shape[1], 1, -1])
			assert(weights.shape[-1] != 1)
			
		def get_oc_weight_list(w):
			"""
			 Gets a tuple of:
			  1. List of lists of weight values for each output channel
			  2. The corresponding number of input channels and y positions that should be skipped 
			      to decode the position of the weight in the input group
			  3. The corresponding x positins for each weight
			 Args:
				w: a 4 dimensional tensor.  This tensor should be a group of weights that should
				   be computed serially.  The number of parallel weights can be increased by
				   dividing the input channels, so this should be a group of weights corresponding
				   to a group of input channels
			"""
			nonlocal group_x_dim
			ws = []
			rls = []
			x_sels = []
			if group_x_dim:
				x_range = w.shape[-3]
			else:
				x_range = 1
			for oc in range(w.shape[-1]):
				for x in range(x_range):
					if group_x_dim:
						_w = np.transpose(w[:,x:x+1,:,oc], [0,2,1])
					else:
						_w = np.transpose(w[:,:,:,oc], [0,2,1])
					nz = np.nonzero(_w)
					# need at least one weight in each output channel
					if nz[0].size == 0:
						nz = (*[np.array([0]) for _ in _w.shape],)
					x_sels.append(nz[-1])
					nz_w = _w[nz]
					ws.append(nz_w)
					indices = w.shape[-2] * nz[0] + nz[1]
					_rls = indices - np.pad(indices[:-1], [1,0], mode="constant", constant_values=0)
					rls.append(_rls)
					assert(_rls.size == nz_w.size)
			return (ws, rls, x_sels)

		# The following lists are lists of lists of serial weights, runlengths, and x weight indices.
		#  The outer list contains lists for each input channel group
		#  The list within that contains lists for each output channel
		ws = []
		rls = []
		x_act_indices = []
		lists = [ws, rls, x_act_indices]
		bits = []
		for j in range(parallel_weights):
			w = weights[:,:,j::parallel_weights,:]
			w_bits = math.floor(math.log2(np.max(np.abs(w)))) + 2
			w_s, rl_s, x_sels = get_oc_weight_list(w)
			rl_bits = clog2(1+np.max(np.array([i for l in rl_s for i in l])))
			x_sels_bits = clog2(1+np.max(np.array([i for l in x_sels for i in l])))
			bits.extend([w_bits, rl_bits, x_sels_bits])
			ws.append(w_s)
			rls.append(rl_s)
			x_act_indices.append(x_sels)


		# pad each of the input channel groups to be the same length
		for i in range(len(ws[0])):
			counts = [w_group_list[i].size for w_group_list in ws]
			max_count = max(counts)
			for j in range(len(ws)):
				current_group_length = ws[j][i].size
				if current_group_length < max_count:
					zp_count = max_count - current_group_length
					args = {"pad_width" : [0, zp_count], "mode" : "constant", "constant_values" : 0}
					for l in lists:
						l[j][i] = np.pad(l[j][i], **args)
			for j in range(1,len(ws)):
				assert(ws[j][i].size == ws[j-1][i].size)

		# Now we need to get the groups of acts, rls, and indices in the order that we want them
		#  to be concatenated into the memory
		# We want a repetition of weights[0], rls[0], indices[0], weights[1], rls[1], indices[1], ...
		#  so we will iterate over the group index in the outer loop and then iterate over the list
		#  type in the inner loop, appending to an array
		output_channels = len(ws[0])
		input_channel_groups = len(ws)
		num_element_types = len(lists)
		combined = [[] for _ in range(num_element_types * input_channel_groups)]
		for i in range(output_channels):
			concatenate_elements = []
			for j in range(input_channel_groups):
				# now go through every type of element (weight values, rl values, x indices)
				for k,l in enumerate(lists):
					combined[k + j * num_element_types].extend(list(l[j][i]))


		# Now we will optionally stagger every n lists
		max_stagger = 0
		if stagger_every_n_weights > 0:
			combined_index = 0
			max_stagger = max(staggered_weight_info["staggers"])
			for i,stagger in enumerate(staggered_weight_info["staggers"]):
				for j in range(num_element_types):
					combined[combined_index] = [0] * stagger + combined[combined_index] + [0] * (max_stagger - stagger)
					combined_index += 1

		numpy_combined = combined
		bit_arrays = [BitArray(bits=b, value_vector=ns) for b, ns in zip(bits, combined)]
		combined = bit_arrays[0]
		for ba in bit_arrays[1:]:
			combined += ba

		weights_per_oc = [len(l) for l in ws[0]]
		weights_per_oc_bits = clog2(np.max(weights_per_oc))
		return (weights_per_oc, weights_per_oc_bits, combined, bits, numpy_combined)

	def _get_serialized_weights_and_rls(self,
		parallel_weights=2,             # Number of multiplications per DSP
		stagger_every_n_weights=0,      # Adds a delay of 1 cycle every N weights for systolic chaining of DSPs
		delay_for_chain_break=4,        # Delay added whenever a DSP chain is broken.  The default is the number of input pipeline stages for a DSP plus the output DSP delay
		staggered_weight_info=None,
		group_x_dim=False,
		is_depthwise=False):            # Depthwise is a little different since each output channel has only one input channel
		weights = self.node.values[0]
		return BasicConv.get_serialized_weights_and_rls(weights, parallel_weights, stagger_every_n_weights, delay_for_chain_break, staggered_weight_info, group_x_dim, is_depthwise)

	def get_custom_verilog_str(self, t="  "):
		verilog = super(BasicConv, self).get_custom_verilog_str(t)
		'''
		verilog += "  always @(posedge clock_0) begin\n"
		verilog += "    if (" + self.debug_stall.name + ") begin\n"
		verilog += '      $display("' + self.name + ' deadlocked because there is no space and it does not have enough lines to write one out %0t", $time);\n'
		verilog += "    end\n"
		#verilog += "    if (!" + self.can_write_line.name + ") begin\n"
		#verilog += '      $display("' + self.name + ' cannot write");\n'
		#verilog += "    end\n"
		verilog += "  end\n"'''
		return verilog

class HPipe(dl.Module):
	def __init__(self, plan, debug_individual_stage=None, build_until=None, build_from=None, **kwargs):
		super(HPipe, self).__init__("hpipe", kind="hpipe", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			self.alternate_reset = inst(dl.Logic, bits=1, name="alternate_reset")
			self.alternate_reset.set_driver(inst(dl.Constant, value=0, bits=1, name="alternate_reset_default_driver"))
			combined_reset = inst(dl.OR, self.alternate_reset, kwargs["reset"])
			def build_reset(combined_reset, count_value=100, name="reset"):
				nonlocal inst
				reset_counter = inst(dl.Counter, reset=combined_reset, reset_value=0, end_value=count_value, increment_value=1, name=name + "_counter")
				reset_counter.set_increment_condition(inst(dl.NOT, reset_counter.is_done))
				reset = inst(dl.NOT, reset_counter.is_done)
				# I don't use delay here because the DUPLICATE_HIERARCHY_DEPTH
				# assignment requires our pipe to be in the module from which
				# it fans out.
				reset_pipe = [inst(dl.Flop, name="reset_pipe_" + str(i))for i in range(12)]
				prior_stage = [reset] + reset_pipe
				for rp,prior in zip(reset_pipe, prior_stage):
					rp.set_driver(prior)
				reset_pipe[-1].name=name
				reset = reset_pipe[-1]
				# The DUPLICATE_HIERARCHY_DEPTH assignment doesn't support wildcards even though it
				# explicitly says it does.  As a result, this needs the full path :(
				dl.Verilog.current_circuit.add_quartus_setting_lambda(
					lambda: "set_instance_assignment -name DUPLICATE_HIERARCHY_DEPTH 10 -to " + '"u0|generic_component_0' + reset.get_sdc_filter_string()[1:] + '"')
				return reset
			self.input_reset = None
			self.reset = inst(dl.Logic, bits=1, name="system_reset")
			if "input_clock" in kwargs:
				input_clock = kwargs["input_clock"]
				with dl.ParamScope(clock=input_clock):
					reset_control_counter = inst(dl.Counter, reset=combined_reset, reset_value=0, end_value=20, increment_value=1, name="reset_control_counter")
					reset_control_counter.set_increment_condition(inst(dl.NOT, reset_control_counter.is_done))
					# Since the input clock is slower than the system clock, 
					delayed_combined_reset = inst(dl.NOT, reset_control_counter.is_done).delay(3)
					second_delayed_combined_reset = inst(dl.NOT, reset_control_counter.is_done).delay(1)
					input_reset = build_reset(delayed_combined_reset, name="input_reset")
					self.input_reset = input_reset
				combined_reset_cdc = inst(dl.SynchronizationChain, input_clock, kwargs["clock"])
				combined_reset_cdc.control_in.set_driver(second_delayed_combined_reset)
				combined_reset = combined_reset_cdc.control_out
			self.reset.set_driver(build_reset(combined_reset, name="system_reset"))
			with dl.ParamScope(reset=self.reset, input_reset=self.input_reset):
				nodes = []
				self.nodes = nodes
				done = False
				last_node = None
				add_count = 0
				found_start = True
				if build_from is not None:
					found_start = False
				def process_node(n):
					nonlocal last_node
					nonlocal done
					nonlocal nodes
					nonlocal add_count
					nonlocal found_start
					nonlocal debug_individual_stage
					nonlocal build_until
					nonlocal build_from
					if done:
						return
					if build_from is not None and build_from in n.tf_op.name:
						if not found_start:
							nodes.append(n.inputs[0]._from)
						found_start = True
					if found_start:
						nodes.append(n)
						last_node = n
					if build_until is not None and build_until in n.tf_op.name:
						add_count += 1
						done = True
						#if add_count == 3:
					if debug_individual_stage is not None and debug_individual_stage in n.tf_op.name:
						nodes = [n]
						self.nodes = nodes
						done = True
					#if n.type == "Conv2D":
					#	done = True
						#add_count += 1
						#if add_count == 10:
						#	done = True"""
				plan.graph.walk_graph(f_node=process_node)
				if debug_individual_stage is not None:
					fake_placeholder = nodes[0].inputs[0]._from
					fake_placeholder.type = "Placeholder"
					fake_placeholder.inputs = []
					dl.Verilog.current_params.kwargs["input_bit_spec"] = {
						"int" : fake_placeholder.precision_parameters["a_int"],
						"frac" : fake_placeholder.precision_parameters["a_f"],
						"sign" : 1
					}
					nodes.append(fake_placeholder)

				for n in nodes:
					if n.type == "Placeholder":
						first_placeholder = n
						break
				for n in nodes:
					if n.type == "Conv2D":
						first_conv = n
						break
				for n in nodes:
					if n.type == "MaxPool":
						first_max_pool = n
						break
				for n in nodes:
					if n.type == "BiasAdd":
						first_bias_add = n
						break
				for n in nodes:
					if n.type == "Relu":
						first_relu = n
						break

				instances = []
				module_map = {
					"Placeholder" : Placeholder,
					"Conv2D" : BasicConv,
					"DepthwiseConv2dNative" : BasicConv,
					"MaxPool": MaxPool,
					"BiasAdd": BiasAdd,
					"Relu"   : ReLU,
					"Relu6"  : ReLU6,
					"Swish"  : ActivationFunction,
					"Add"    : Add,
					"AddV2"    : Add,
					"Sigmoid"  : ActivationFunction,
					"Tanh"	 : ActivationFunction,
					"Mean"   : Mean
				}
				edges_to_connect = []
				#with dl.ParamScope(extend_to_match_bits=True):
				override_args = {}
				if build_from is not None:
					override_args["input_bit_spec"] = {
						"sign" : first_placeholder.precision_parameters["a_sign_bits"],
						"int" : first_placeholder.precision_parameters["a_int"],
						"frac" : first_placeholder.precision_parameters["a_f"]
					}

				for n in nodes:
					if n.type == "Conv2D" or n.type == "DepthwiseConv2dNative":
						if n.planner_estimate.n_output_channel_groups > 1:
							instances.append(inst(ConvWrapper, n))
						else:
							instances.append(inst(module_map[n.type], n))
					else:
						instances.append(inst(module_map[n.type], n))
					n.module = instances[-1]
					edges_to_connect.extend(list(enumerate(n.inputs)))

				self.placeholder = first_placeholder.module

				for i,e in edges_to_connect:
					_to = e._to
					_from = e._from
					i_driven = _to.module.input_modules[i]
					o_driven = _to.module
					driver = _from.module
					if driver is None:
						continue
					ctoo = driver.get_control_valid_to_output_valid_delay()
					ctoi = o_driven.get_control_to_input_delay()
					o_to_i_delay = ctoi - ctoo
					c_to_c_delay = ctoo - ctoi
					if ctoi == -1:
						o_driven._set_control_valid_to_output_valid_delay(driver.get_control_valid_to_output_valid_delay())
						o_driven._set_control_to_input_delay(driver.get_control_valid_to_output_valid_delay())
						o_to_i_delay = 0
						c_to_c_delay = 0
					#if build_from is not None:
					#	ctoo = 0
					#	driver._set_control_to_input_delay(0)
					#	driver._set_control_valid_to_output_valid_delay(0)
					#else:
					assert(ctoo != -1)


					max_fan = None
					final_fan = None
					standard_pipe_delay = 2
					added_pipe_fanout_delay = 0
					if isinstance(o_driven, Add):
						input_index = _to.inputs.index(e)
						min_buffer_depth = max(_to.planner_estimate.buffer_for_input[input_index], 2)
						standard_pipe_delay = min(min_buffer_depth, 5)

					c_to_c_delay = max(standard_pipe_delay, c_to_c_delay + standard_pipe_delay)
					o_to_i_delay = max(standard_pipe_delay, o_to_i_delay + standard_pipe_delay)
					with dl.ModuleScope(o_driven):
						i_driven.write.set_driver(driver.output_valid, delay=c_to_c_delay)
						for _input,o in zip(i_driven.inputs, driver.outputs):
							_input.set_driver(o.delay(o_to_i_delay))
						for _input,o in zip(i_driven.writes, driver.outputs_valid):
							_input.set_driver(o.delay(o_to_i_delay))

						"""
						outputs_and_outputs_valid = driver.outputs[0].get_delay(driver.outputs + driver.outputs_valid + [driver.output_valid], hyperpipe=True, delay=5)
						output_valid = outputs_and_outputs_valid[-1]
						outputs_and_outputs_valid = outputs_and_outputs_valid[:-1]
						i_driven.write.set_driver(output_valid.delay(c_to_c_delay, auto_shift_register_recognition=False))
						for i,o in zip(i_driven.inputs + i_driven.writes, outputs_and_outputs_valid):
							i.set_driver(o.delay(o_to_i_delay))"""

				for n in nodes:
					m = n.module
					def valid_condition(m, t="  ", n=2):
						control_valid_to_output_valid_delay = m.get_control_valid_to_output_valid_delay()
						condition = "(" + m.output_valid.name + " === 1 |-> ##" + str(control_valid_to_output_valid_delay) + " " + m.outputs_valid[0].name + " === 1)"
						if m.guarantees_output_valid_low_in_cycles:
							condition += " and\n"
							condition += t * n + "(" + m.output_valid.name + " === 0 |-> ##" + str(control_valid_to_output_valid_delay) + " " + m.outputs_valid[0].name + " === 0)"
						return condition
					def write_condition(m, t="  ", n=2):
						assert(isinstance(m, NNStageInput))
						control_to_input_delay = m.owner.get_control_to_input_delay()
						condition = "(" + m.write.name + " === 1 |-> ##" + str(control_to_input_delay) + " " + m.writes[0].name + " === 1)"
						return condition


					"""for im in m.input_modules:
						with dl.ModuleScope(im):
							clocked_logic = inst(dl.Flop, bits=1, name="just_need_to_get_a_clock_in_here")
							clocked_logic.set_driver(inst(dl.Constant, bits=1, value=0))
							inst(dl.Assertion,
								name="output_valid_delay_check",
								condition_f=write_condition,
								condition_f_args=im,
								comment=n.tf_op.name + " specifies the delay from write to writes[0] should be " + str(m.get_control_to_input_delay()) + " cycles",
								error_string=n.tf_op.name + " writes[0] should have had a delay of " + str(m.get_control_to_input_delay()) + " from write")"""

					with dl.ModuleScope(m):
						"""inst(dl.Assertion,
							name="output_valid_delay_check",
							condition_f=valid_condition,
							condition_f_args=m,
							comment=n.tf_op.name + " specifies the delay from output_valid to outputs_valid[0] should be " + str(m.get_control_valid_to_output_valid_delay()) + " cycles",
							error_string=n.tf_op.name + " outputs_valid[0] should have had a delay of " + str(m.get_control_valid_to_output_valid_delay()) + " from output_valid")"""
						space_to_write_line_signals = []
						for o in n.outputs:
							mod = o._to.module
							if mod is None:
								continue
							input_mod = mod.input_modules[o._to.inputs.index(o)]
							space_to_write_line_signals.append(input_mod.space_to_write_line.delay(3))
						if len(space_to_write_line_signals) > 1:
							m.can_write_line.set_driver(inst(dl.AND, *space_to_write_line_signals).delay(3))
						elif len(space_to_write_line_signals) == 1:
							m.can_write_line.set_driver(space_to_write_line_signals[0])

				# temporary for testing
				self.output_modules = []
				for n in nodes:
					m = n.module
					if len(m.can_write_line.drivers) == 0:
						m.can_write_line.set_driver(inst(dl.Constant, bits=1, value=1))
						self.output_modules.append(m)

				#self.bc = inst(BasicConv, first_conv)
				#self.iwc.finished_reading_line.set_driver(self.bc.oc_controller.done)
				#self.bc.oc_controller.go.set_driver(inst(dl.AND, self.iwc.has_full_kernel, self.max_pool.can_write_line.delay(5)))

				# we will make a shift register that shifts the input and control data to each memory
				"""shifter_depth = math.ceil(len(self.bc.ia_buffer.ram_list)/2.)
				with dl.ModuleScope(self.bc.ia_buffer):
					self.input_data_shifter = inst(dl.ShiftRegister, bits=self.bc.ia_buffer.ram_list[0].w_data._bit_width, depth=shifter_depth, name="input_data_shifter")
					self.w_addr_shifter = inst(dl.ShiftRegister, bits=self.iwc.write_addr._bit_width, depth=shifter_depth+1, name="w_addr_shifter")
					self.w_addr_shifter.d.set_driver(self.iwc.write_addr)
					self.ic_lsb_shifter = inst(dl.ShiftRegister, bits=1, depth=shifter_depth+1, name="ic_lsb_shifter")
					self.ic_lsb_shifter.d.set_driver(self.iwc.input_channel_counter.current_value[0:0])
					self.we_shifters = [inst(dl.ShiftRegister, bits=1, depth=i+2, name="we_shifter") for i in range(len(self.iwc.we_s))]
					for we_shifter, we in zip(self.we_shifters, self.iwc.we_s):
						we_shifter.d.set_driver(we)

				for i, r in enumerate(self.bc.ia_buffer.ram_list):
					with dl.ModuleScope(r):
						target_ic = i % 2
						shifter_index = i // 2
						r.w_en.set_driver(
							inst(dl.AND,
								inst(dl.EQ,
									inst(dl.Constant, target_ic, bits=1),
									self.ic_lsb_shifter.sr[shifter_index+1]),
								self.we_shifters[shifter_index].q))
						r.w_addr.set_driver(self.w_addr_shifter.sr[shifter_index+1])
						r.w_data.set_driver(self.input_data_shifter.sr[shifter_index])"""
	def get_custom_verilog_str(self, t="  "):
		return t + "/*verilator no_inline_module*/\n"

	def write_stats_to_dir(self, directory):
	 #Marius:  Create output directory if it does not exist 
		if not os.path.exists(directory):
			Path(directory).mkdir(parents=True, exist_ok=True)
		
		path = directory + "/utilization_estimates.txt"
		print("Writing Utilization Estimates to " + path)
		with open(path, "w") as fh:
			for n in self.nodes:
				if len(list(n.module.utilization_estimates.keys())) > 0:
					fh.write(n.tf_op.name + ": " + pprint.pformat(n.module.utilization_estimates) + "\n")


class HPIPETLM(dl.Module):
	def __init__(self, module_name, kind, **kwargs):
		super(HPIPETLM, self).__init__(module_name, kind=kind, **kwargs)

	def dump_tensorflow_activations(self):
		print_stage_header("Writing TensorFlow Outputs For Nodes in Graph.\nIf multiple example images were specified, defaulting to the first one.")
		os.makedirs("generated_files/layer_images/tensorflow", exist_ok=True)
		for n in self.hpipe.nodes:
			output_image = n.example_outputs[0]
			#print("NODE OF INTEREST SHAPE: " + str(output_image.shape))
			output_shape = output_image.shape
			output_image = np.reshape(output_image[0], output_image.shape[1:])
			output_image = np.transpose(output_image, [0,2,1])
			output_image = np.reshape(output_image, [np.prod(output_image.shape[:2]),output_image.shape[-1]])
			f_name = "generated_files/layer_images/tensorflow/" + n.module.name + ".csv"
			print("  Generating " + f_name)
			with open(f_name, "w") as f:
				f.write(str(output_shape[-1]) + "\n")
				for i in range(output_image.shape[0]):
					f.write(",".join([str(d) for d in list(output_image[i])]) + "\n")


class QuartusTop(HPIPETLM):
	def __init__(self, plan, build_from=None, build_until=None, debug_individual_stage=None, **kwargs):
		super(QuartusTop, self).__init__("quartus_top", kind="quartus_top", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			clock = inst(dl.Clock, name="sys_clock")
			clock2x = inst(dl.Clock, name="sys_clock_2x")
			reset = inst(dl.Logic, name="reset", bits=1, signed=False)
			self.clock = clock
			self.reset = reset
			with dl.ParamScope(clock=clock, reset=reset, write_port_width=4, clock2x=clock2x):
				hpipe = inst(HPipe, plan, build_from=build_from, build_until=build_until, debug_individual_stage=debug_individual_stage)
				self.hpipe = hpipe
				write = inst(dl.Constant, value=1, bits=1)
				input_data = inst(dl.Constant, value=1, bits=hpipe.placeholder.inputs[0]._bit_width)
				with dl.ModuleScope(hpipe):
					hpipe.placeholder.inputs[0].set_driver(input_data.delay(10))
					hpipe.placeholder.write.set_driver(write.delay(10))
					if len(hpipe.output_modules[0].outputs) == 1:
						output_data = hpipe.output_modules[0].outputs[0].delay(10)
					else:
						output_sr = inst(dl.ParallelLoadShiftRegister, bits=hpipe.output_modules[0].outputs[0]._bit_width, depth=len(hpipe.output_modules[0].outputs))
						for l,v in zip(output_sr.loads, hpipe.output_modules[0].outputs_valid):
							l.set_driver(v)
						for i,d in zip(output_sr.input_data, hpipe.output_modules[0].outputs):
							i.set_driver(d)
						output_data = output_sr.sr[-1].delay(10)
					output_valid = hpipe.output_modules[0].outputs_valid[0].delay(10)
					space_to_write_line = hpipe.placeholder.space_to_write_line.delay(10)
				top_output_data = output_data.delay(1)
				top_output_valid = output_valid.delay(1)
				space_to_write_line_top = space_to_write_line.delay(1)

class SystemBuilderComponent(HPIPETLM):
	def __init__(self, plan, build_from=None, build_until=None, debug_individual_stage=None, **kwargs):
		super(SystemBuilderComponent, self).__init__("system_builder_component", kind="system_builder_component", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			clock = inst(dl.Clock, name="sys_clock")
			clock2x = inst(dl.Clock, name="sys_clock_2x")
			io_clock = inst(dl.Clock, name="io_clock")
			reset = inst(dl.Logic, name="reset", bits=1, signed=False)
			self.clock = clock
			self.io_clock = io_clock
			self.reset = reset
			with dl.ParamScope(clock=clock, clock2x=clock2x, reset=reset, write_port_width=4, extend_to_match_bits=True): #, rom_path_modifier="quartus_circuit/"
				self.instantiate(inst, plan, build_from, build_until, debug_individual_stage, **kwargs)

	def compute_transfer_info(self, h, w, c, b, transfer_bits):
		width_bits = w * b
		is_deserialized = True
		if width_bits >= transfer_bits:
			is_deserialized = False

		lines_per_width = math.ceil(width_bits/float(transfer_bits))
		acts_per_line = transfer_bits//width_bits
		if is_deserialized:
			total_lines = math.ceil(c*h/float(acts_per_line))
		else:
			total_lines = c*h*lines_per_width
		return (total_lines * (transfer_bits // 8), total_lines)

	def instantiate(self, inst, plan, build_from, build_until, debug_individual_stage, directly_propagate_outputs_to_top=False, **kwargs):
		hpipe = inst(HPipe, plan, build_from=build_from, build_until=build_until, debug_individual_stage=debug_individual_stage, input_clock=self.io_clock, **kwargs)
		self.hpipe = hpipe
		bits_per_output = hpipe.output_modules[0].outputs[0]._bit_width
		outputs_per_line = 256 // bits_per_output
		ram_bit_width = outputs_per_line * bits_per_output

		input_act_width = hpipe.placeholder.get_output_act(dim="width")
		input_act_height = hpipe.placeholder.get_output_act(dim="height")
		input_act_channels = hpipe.placeholder.get_output_act(dim="channels")
		input_bit_spec = dl.Verilog.current_params.kwargs["input_bit_spec"]
		input_act_bits = np.sum(list(input_bit_spec.values()))

		output_act_width = hpipe.output_modules[0].get_output_act(dim="width")
		output_act_height = hpipe.output_modules[0].get_output_act(dim="height")
		output_act_channels = hpipe.output_modules[0].get_output_act(dim="channels")
		output_act_bits = hpipe.output_modules[0].get_bit_spec("output")["width"]

		output_bytes, output_lines = self.compute_transfer_info(output_act_height, output_act_width, output_act_channels, output_act_bits, 256)
		input_bytes, input_lines = self.compute_transfer_info(input_act_height, input_act_width, input_act_channels, input_act_bits, 256)

		width_lines = math.ceil(hpipe.output_modules[0].get_output_act(dim="width") / float(outputs_per_line))
		shifter_depth = width_lines
		if width_lines == 1:
			shifter_depth = outputs_per_line
		output_buffer_depth = output_lines
		parallel_out_count = min(width_lines, output_act_width)
		shifter_bit_width = min(ram_bit_width, bits_per_output*output_act_width)

		self.interface = {
			"addr" : inst(dl.Logic, bits=20, name="addr"),
			"data" : inst(dl.Logic, bits=256, name="data"),
			"valid" : inst(dl.Logic, bits=1, name="valid"),
			"chipselect" : inst(dl.Logic, bits=1, name="chipselect"),
			"waitrequest" : inst(dl.Logic, bits=1, name="waitrequest"),

			"addr2" : inst(dl.Logic, bits=max(1,clog2(output_buffer_depth)), name="addr2"),
			"data2" : inst(dl.Logic, bits=256, name="data2"),
			"valid2" : inst(dl.Logic, bits=1, name="valid2"),
			"chipselect2" : inst(dl.Logic, bits=1, name="chipselect2"),
			"waitrequest2" : inst(dl.Logic, bits=1, name="waitrequest2"),

			"addr3" : inst(dl.Logic, bits=clog2(10), name="addr3"),
			"data3" : inst(dl.Logic, bits=32, name="data3"),
			"valid3" : inst(dl.Logic, bits=1, name="valid3"),
			"chipselect3" : inst(dl.Logic, bits=1, name="chipselect3")
		}

		with dl.ModuleScope(hpipe):
			with dl.ParamScope(reset=hpipe.reset):
				hpipe.alternate_reset.set_driver(inst(dl.AND, self.interface["chipselect3"], self.interface["valid3"], inst(dl.EQ, self.interface["addr3"], 8)))
				with dl.ParamScope(clock=self.io_clock):
					hpipe.placeholder.inputs[0].set_driver(self.interface["data"][hpipe.placeholder.inputs[0]._bit_width-1:0].delay(10))
					hpipe.placeholder.write.set_driver(inst(dl.AND, self.interface["valid"], self.interface["chipselect"]).delay(10))
					hpipe.placeholder.write_addr.set_driver(self.interface["addr"], delay=10)
					address_copy_to_keep_in_port_list = inst(dl.Logic, bits=20)
					address_copy_to_keep_in_port_list.set_driver(self.interface["addr"], delay=10)

				with dl.ParamScope(reset_driver=inst(dl.Constant, value=0, bits=1), clock=self.io_clock, reset=hpipe.input_reset):
					status_register_components = {
						"has_space_to_write" : inst(dl.Flop, bits=1, name="has_space_to_write"),
						"output_data_ready" : inst(dl.Flop, bits=1, name="output_data_ready")
					}
				status_register_order = [
					"has_space_to_write",
					"output_data_ready"
				]
				status_register = inst(dl.Concat, *[status_register_components[k] for k in status_register_order], name="status_register")
				self.status_register = status_register
				if status_register._bit_width < 32:
					status_register = inst(dl.Concat,
						status_register,
						inst(dl.Constant, value=0, bits=32-status_register._bit_width),
						name="padded_status_register")
				with dl.ParamScope(clock=self.io_clock, reset=hpipe.input_reset):
					status_register_select = inst(dl.MuxV2,
						self.interface["addr3"].delay(5),
						inst(dl.Constant, value=output_act_height, name="output_act_height"),
						inst(dl.Constant, value=output_act_width, name="output_act_width"),
						inst(dl.Constant, value=output_act_channels, name="output_act_channels"),
						inst(dl.Constant, value=output_act_bits, name="output_act_bits"),
						inst(dl.Constant, value=input_act_height, name="input_act_height"),
						inst(dl.Constant, value=input_act_width, name="input_act_width"),
						inst(dl.Constant, value=input_act_channels, name="input_act_channels"),
						inst(dl.Constant, value=input_act_bits, name="input_act_bits"),
						inst(dl.Constant, bits=32, value=0, name="reset_register"),
						status_register,
						extend_to_match_bits=True,
						name="status_register_select")
					status_register_pcie_read = status_register_select.delay(5).copy()
					status_register_pcie_read.name = "status_register_pcie_read"
					self.interface["data3"].set_driver(status_register_pcie_read)

				if directly_propagate_outputs_to_top:
					for output in hpipe.output_modules:
						backpressure = inst(dl.Logic, bits=1, name=output.name + "_backpressure")
						about_to_be_valid = inst(dl.Logic, bits=1, name=output.name + "_about_to_be_valid")
						output.can_write_line.set_driver(backpressure)
						about_to_be_valid.set_driver(output.output_valid)
						dl.Verilog.current_circuit.propagate_to_top[backpressure] = "input"
						dl.Verilog.current_circuit.propagate_to_top[about_to_be_valid] = "output"
						all_outputs = inst(dl.Concat, *output.outputs, name=output.name + "_data_outputs")
						all_data_valid_signals = inst(dl.Concat, *output.outputs_valid, name=output.name + "_data_valid_signals")
						dl.Verilog.current_circuit.propagate_to_top[all_outputs] = "output"
						dl.Verilog.current_circuit.propagate_to_top[all_data_valid_signals] = "output"
					return

				output_fifo = inst(dl.FIFO,
					bits=len(hpipe.output_modules[0].outputs) * hpipe.output_modules[0].outputs[0]._bit_width,
					depth=2*hpipe.output_modules[0].get_output_act(dim="channels"),
					almost_empty=0,
					almost_full=hpipe.output_modules[0].get_output_act(dim="channels"))
				output_fifo.w_en.set_driver(hpipe.output_modules[0].outputs_valid[0])
				output_fifo.w_data.set_driver(inst(dl.Concat, *hpipe.output_modules[0].outputs))

				shift_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=shifter_depth-1, name="shift_counter")
				output_channel_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=output_act_height * output_act_channels-1, name="output_channel_counter")
				shift_to_idle_control = shift_counter.is_done
				if width_lines == 1:
					shift_to_idle_control = inst(dl.Constant, bits=1, value=1, name="constant_1")

				wait_for_state_update_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=3,
					name="wait_for_state_update_counter")

				waiting_for_readback_to_finish = inst(dl.Logic, bits=1, name="waiting_for_readback_to_finish")
				states = ["IDLE", "READ_FIFO", "SHIFT", "DRAIN_LAST_ELEMENTS", "WAIT_FOR_STATE_UPDATE"]
				edges = [
					["IDLE", "READ_FIFO", inst(dl.AND,
						inst(dl.NOT, output_fifo.empty),
						inst(dl.NOT, waiting_for_readback_to_finish))],
					["READ_FIFO", "SHIFT"],
					["SHIFT", "IDLE", shift_to_idle_control],
					["SHIFT", "DRAIN_LAST_ELEMENTS", inst(dl.AND, output_channel_counter.is_reset_value, inst(dl.NOT, shift_counter.is_done))],
					["DRAIN_LAST_ELEMENTS", "WAIT_FOR_STATE_UPDATE", shift_counter.is_done],
					["WAIT_FOR_STATE_UPDATE", "IDLE", wait_for_state_update_counter.is_done]]

				with dl.ParamScope(clock=self.io_clock):
					last_read_occured = inst(dl.AND, 
						self.interface["chipselect2"],
						self.interface["valid2"],
						inst(dl.EQ, self.interface["addr2"], output_buffer_depth-1),
						name="last_read_occured").delay(4)
				last_read_occured_cross_clock = inst(dl.SingleCycleControlDomainCrossing, self.io_clock, self.clock, clock_a_reset=hpipe.input_reset, clock_b_reset=hpipe.reset)
				last_read_occured_cross_clock.control_in.set_driver(last_read_occured)
				last_read_occured_system_clock = last_read_occured_cross_clock.control_out
				
				state_machine = inst(dl.StateMachine, states, edges, None, name="output_fifo_state_machine")
				wait_for_state_update_counter.set_increment_condition(state_machine.c["is_wait_for_state_update"])
				done_writing_out = inst(dl.AND,
					output_channel_counter.is_reset_value,
					shift_counter.is_done,
					inst(dl.OR, state_machine.c["is_shift"],
						state_machine.c["is_drain_last_elements"]),
					name="done_writing_out")
				done_writing_out_cross_clock = inst(dl.SingleCycleControlDomainCrossing, self.clock, self.io_clock, clock_a_reset=hpipe.reset, clock_b_reset=hpipe.input_reset)
				done_writing_out_cross_clock.control_in.set_driver(done_writing_out)
				done_writing_out_io_clock = done_writing_out_cross_clock.control_out

				with dl.ParamScope(clock=self.io_clock, reset=hpipe.input_reset):
					done_state_machine = inst(dl.StateMachine,
						["OUTPUT_NOT_READY", "OUTPUT_READY"],
						[
							["OUTPUT_NOT_READY", "OUTPUT_READY", done_writing_out_io_clock],
							["OUTPUT_READY", "OUTPUT_NOT_READY", inst(dl.AND, 
																		self.interface["chipselect2"],
																		self.interface["valid2"]).delay(4)]], None, name="done_state_machine")
				status_register_components["output_data_ready"].set_driver(done_state_machine.c["is_output_ready"])

				# I delay this here just so that we have a register instantiated in the HPipe module to add the timing constraint.
				# If you try to use the get_registers tcl command on a wire driven by a register, it doesn't find the register :(
				# Since the hpipe.placeholder.space_to_write_line signal is actually a wire driven by a register I instantiate
				# a new register in HPIPE just so that we have a clean signal to reference with our timing constraints.
				self.placeholder_space_pre_domain_cross = hpipe.placeholder.space_to_write_line.delay(1)
				self.placeholder_space_pre_domain_cross.synthesis_attributes = "synthesis keep"
				with dl.ParamScope(clock=self.io_clock):
					with dl.ParamScope(intended_clock_crossing=True):
						self.placeholder_space_clock_cross_flop = self.placeholder_space_pre_domain_cross.delay(1)
					self.placeholder_space_clock_cross_flop.synthesis_attributes = "synthesis keep"
					status_register_components["has_space_to_write"].set_driver(self.placeholder_space_clock_cross_flop.delay(3))
					# This TLM does not actually get generated.  It instantiates signals both inside and outside the HPIPE module
					# which makes the necessary input and output signals appear in the HPIPE port list.
					# As a result, its get_sdc_constraints_string function never gets called, so have manually added it
					# to a global list of lambdas to call to get more sdc constraints
					dl.Verilog.current_circuit.add_sdc_lambda(lambda: self.get_sdc_constraints_string())

				states = ["WAITING_FOR_OUTPUT_TO_BE_READY", "WAITING_FOR_READBACK_TO_FINISH"]
				edges = [["WAITING_FOR_OUTPUT_TO_BE_READY", "WAITING_FOR_READBACK_TO_FINISH", done_writing_out.delay(1)],
					["WAITING_FOR_READBACK_TO_FINISH", "WAITING_FOR_OUTPUT_TO_BE_READY", last_read_occured_system_clock]]
				system_output_backpressure_state_machine = inst(dl.StateMachine, states, edges, None, name="system_output_backpressure_state_machine")
				waiting_for_readback_to_finish.set_driver(system_output_backpressure_state_machine.c["is_waiting_for_readback_to_finish"])


				should_shift = inst(dl.OR, state_machine.c["is_shift"], state_machine.c["is_drain_last_elements"], name="should_shift")
				shift_counter.set_increment_condition(should_shift)
				output_fifo.r_en.set_driver(state_machine.c["is_read_fifo"], delay=1)
				output_channel_counter.set_increment_condition(state_machine.c["is_read_fifo"])
				output_sr = inst(dl.ParallelLoadShiftRegister, bits=shifter_bit_width, depth=shifter_depth)
				for i in range(width_lines):
					output_sr.loads[i].set_driver(state_machine.c["is_read_fifo"], delay=3)
					hi = min((i+1)*shifter_bit_width-1, bits_per_output*output_act_width-1)
					input_data = output_fifo.r_data[hi:i*shifter_bit_width]
					if hi < ((i+1)*shifter_bit_width-1):
						input_data = inst(dl.Concat, input_data, inst(dl.Constant, value=0, bits=((i+1)*shifter_bit_width-1)-(bits_per_output*output_act_width-1)))
					output_sr.input_data[width_lines-i-1].set_driver(input_data, delay=1)
				for i in range(width_lines, shifter_depth):
					output_sr.loads[i].set_driver(inst(dl.Constant, value=0, bits=1))
					output_sr.input_data[width_lines - i - 1].wave_needs_driver = True
				for i in range(shifter_depth):
					output_sr.shifts[i].set_driver(should_shift, delay=3)


				write_to_output_ram = should_shift
				if width_lines == 1:
					write_to_output_ram = inst(dl.AND, should_shift, shift_counter.is_done)

				output_ram = inst(dl.RAM,
					write_clock=self.clock,
					read_clock=self.io_clock,
					bits=ram_bit_width,
					depth=output_buffer_depth,
					name="output_ram")
				self.output_ram = output_ram
				counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=output_buffer_depth-1,
					name="output_ram_write_addr")
				counter.set_increment_condition(write_to_output_ram.delay(1))
				output_ram.w_addr.set_driver(counter.current_value.delay(4))
				output_ram.w_en.set_driver(write_to_output_ram.delay(5))
				output_ram.w_data.set_driver(inst(dl.Concat, *list(reversed(output_sr.sr)))[ram_bit_width-1:0].delay(2))

				with dl.ParamScope(clock=self.io_clock):
					output_ram.r_addr.set_driver(self.interface["addr2"].delay(4))
					output_ram.r_en.set_driver(inst(dl.AND, self.interface["chipselect2"], self.interface["valid2"]).delay(4))
					output_data = output_ram.r_data.delay(4).copy()
					if output_data._bit_width < self.interface["data2"]._bit_width:
						output_data = inst(dl.Concat, output_data, inst(dl.Constant, value=0, bits=self.interface["data2"]._bit_width-output_data._bit_width))
					output_data.name="pcie_output_data"
					self.interface["data2"].set_driver(output_data)

				hpipe.output_modules[0].can_write_line.set_driver(inst(dl.NOT, output_fifo.almost_full))
				self.interface["waitrequest"].set_driver(inst(dl.Constant, bits=1, value=0))
				self.interface["waitrequest2"].set_driver(inst(dl.Constant, bits=1, value=0))

	def get_sdc_constraints_string(self):
		pre_cross_filter_string = self.placeholder_space_pre_domain_cross.get_sdc_filter_string() + "[*]"
		post_cross_filter_string = self.placeholder_space_clock_cross_flop.get_sdc_filter_string() + "[*]"
		string = ""

		# The assumption we are making here is that space to write will change faster than the cpu
		# can start to write input data and then check again if it is able to write more data
		# The other assumption we are making is that the signal will deassert for many cycles, then
		# re-assert for many cycles, not toggling at anywhere near the rate of either clock
		# the flops are
		string += "set_max_delay -from [get_registers {" + pre_cross_filter_string + "}]"
		string += " -to [get_registers {" + post_cross_filter_string + "}] 10.000\n"
		# We hold the driving signal for multiple cycles, so there shouldn't be any hold
		# constraint
		string += "set_min_delay -from [get_registers {" + pre_cross_filter_string + "}]"
		string += " -to [get_registers {" + post_cross_filter_string + "}] -2.000\n"
		return string


class HPIPETB2(HPIPETLM):
	def __init__(self, plan, build_from=None, build_until=None, debug_individual_stage=None, wait_for_output=False, **kwargs):
		super(HPIPETB2, self).__init__("hpipe_tb", kind="hpipe_tb", **kwargs)
		inst = self.inst
		self.plan = plan
		with dl.ModuleScope(self):
			clock = inst(dl.Clock)
			reset = inst(dl.ForcedSignal, name="reset", bits=1, signed=False)
			self.clock = clock
			self.reset = reset
			reset.add_value(1, 0)
			reset.add_value(0, 100)
			with dl.ParamScope(clock=clock, reset=reset):
				sbc = inst(SystemBuilderComponent, plan, build_from, build_until, debug_individual_stage, **kwargs)
				sbc.reset.set_driver(reset)
				sbc.clock.set_driver(self.clock)
				sbc.io_clock.set_driver(self.clock)
			# the reset we defined above is an external reset that only resets a reset controller within HPIPE.
			# After that reset is deasserted, an internally generated reset is asserted for some number of cycles
			reset = sbc.hpipe.reset
			self.hpipe = sbc.hpipe
			interface = sbc.interface

			# At the moment we only support one placeholder.  The plan is to support multiple
			# and use a memory mapped interface where address 0 is the first placeholder,
			# address 1 is the second, etc.  The placeholders use a FIFO interface, so they don't
			# actually accept an address.

			with dl.ParamScope(clock=clock, reset=reset):
				images_to_run = 2
				serial_inputs = self.get_serialized_inputs(build_from is not None)
				writing_input = inst(dl.Logic, bits=1, name="writing_input")
				for n, si in serial_inputs.items():
					input_rom = inst(dl.ROM, name="test_input_image", bits=si.bits, content_list=si)
					input_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=len(si)-1)
					input_rom.r_addr.set_driver(input_counter.current_value)
					input_rom.r_en.set_driver(writing_input)
					input_counter.set_increment_condition(writing_input)

				if input_counter.current_value._bit_width < interface["addr"]._bit_width:
					padded_counter = inst(dl.Concat, input_counter.current_value, inst(dl.Constant, value=0, bits=interface["addr"]._bit_width-input_counter.current_value._bit_width))

				written_image_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=images_to_run, name="written_image_counter")
				written_image_counter.set_increment_condition(input_counter.is_done)

				interface["addr"].set_driver(padded_counter)
				start_writing = inst(dl.Logic, bits=1, name="start_writing")
				output_ready = interface["data3"][1]
				ready_to_accept_input = interface["data3"][0]
				states = ["IDLE", "WRITING_INPUT"]
				edges = [
					["IDLE", "WRITING_INPUT", inst(dl.AND, start_writing, inst(dl.NOT, written_image_counter.is_done))],
					["WRITING_INPUT", "IDLE", input_counter.is_done]]
				state_machine = inst(dl.StateMachine, states, edges, None)
				output_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=sbc.output_ram.depth-1,
					name="output_counter")

				readback_states = ["IDLE", "READING_BACK", "DONE_READING_BACK"]
				readback_edges = [
					["IDLE", "READING_BACK", output_ready],
					["READING_BACK", "DONE_READING_BACK", output_counter.is_done],
					["DONE_READING_BACK", "IDLE"]]
				readback_state_machine = inst(dl.StateMachine, readback_states, readback_edges, None, name="readback_state_machine")
				output_counter.set_increment_condition(readback_state_machine.is_reading_back)
				writing_input.set_driver(state_machine.c["is_writing_input"])
				in_pipe_counter = inst(dl.MultiChangeCounter,
					reset_value=0,
					change_values=[1,-1],
					end_value=15,
					name="in_pipe_counter")
				in_pipe_counter.change_signals[0].set_driver(input_counter.is_done)
				in_pipe_counter.change_signals[1].set_driver(readback_state_machine.c["is_done_reading_back"])

				if wait_for_output:
					start_writing.set_driver(inst(dl.AND, in_pipe_counter.is_reset_value, ready_to_accept_input, state_machine.c["is_idle"]))
				else:
					start_writing.set_driver(inst(dl.AND, ready_to_accept_input, state_machine.c["is_idle"]))


				interface["addr3"].set_driver(inst(dl.Constant, value=9, bits=interface["addr3"]._bit_width))
				interface["addr"].set_driver(inst(dl.Concat, input_counter.current_value, inst(dl.Constant, bits=10, value=0)).delay(1))
				interface["valid"].set_driver(writing_input.delay(1))
				interface["chipselect"].set_driver(writing_input.delay(1))
				interface["data"].set_driver(input_rom.r_data)
				interface["valid2"].set_driver(readback_state_machine.is_reading_back)
				interface["chipselect2"].set_driver(readback_state_machine.is_reading_back)
				interface["addr2"].set_driver(output_counter.current_value)
				done_image_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=images_to_run-1, name="done_image_counter")
				done_image_counter.set_increment_condition(readback_state_machine.is_done_reading_back)

				interface["data2"].copy()
				self.done_simulation = done_image_counter.done_and_incrementing.copy()

	def get_serialized_inputs(self, build_from_not_none):
		interface_width = 256
		example_inputs = []
		inputs_per_transfer = []
		used_bits_per_transfer = []
		bits_per_input = []
		placeholder_names = []

		for n in self.plan.graph.nodes:
			if n.type == "Placeholder":
				placeholder_names.append(n.tf_op.name)
				example_inputs.append(n.example_outputs[0])
				bits = n.get_bits(is_act=True)
				bits_per_input.append(bits)
				inputs_per_transfer.append(interface_width // bits)
				used_bits_per_transfer.append(inputs_per_transfer[-1] * bits)
		if build_from_not_none:
			p = [n for n in self.plan.graph.nodes if n.tf_op.name == placeholder_names[0]][0]
			input_bit_spec = p.precision_parameters
			example_inputs = [get_quantized(self.plan.graph.example_input, *[input_bit_spec[k] for k in ["a_sign_bits", "a_int", "a_f"]])]
		else:
			input_bit_spec = dl.Verilog.current_params.kwargs["input_bit_spec"]
			bits = np.sum(list(input_bit_spec.values()))
			inputs_per_transfer = [interface_width // bits]
			used_bits_per_transfer = [inputs_per_transfer[0] * bits]
			bits_per_input = [bits]
			example_inputs = [get_quantized(self.plan.graph.example_input, *[input_bit_spec[k] for k in ["sign", "int", "frac"]])]


		byte_arrays = {}
		for i,p,u,b,n in zip(example_inputs, inputs_per_transfer, used_bits_per_transfer, bits_per_input, placeholder_names):
			# Convert example image to byte array and serialize it to fit over the interface width
			BHCW = np.transpose(i, [0,1,3,2])
			H = BHCW.shape[1]
			C = BHCW.shape[2]
			W = BHCW.shape[3]

			# This pads the width dimension so that the width can be evenly divisible by the number of elements per transfer
			# the additional zeros get discarded by the Placeholder deserializer
			#
			BHCWP = np.pad(BHCW, [[0,0],[0,0],[0,0],[0, W % p]], mode="constant", constant_values=0)
			# This merges the BHCW dimensions so that we have a fully serialized image that is of shape 
			# [PADDED_ELEMENT_COUNT / inputs_per_transfer, inputs_per_transfer]
			LI = np.reshape(BHCWP, [BHCWP.size // p, p])

			# Now we create a BitArray for each vector in LI and combine them to get the full BitArray
			test_in = [BitArray(bits=b, value_vector=LI[:,l]) for l in range(LI.shape[-1])]
			tmp = test_in[-1]
			for t in reversed(test_in[:-1]):
				tmp += t
			test_in = tmp#BitArray(bits=tmp.bits, bit_array=tmp)
			if test_in.bits != interface_width:
				test_in += BitArray(bits=interface_width - test_in.bits, length=test_in.bit_array.shape[0])

			byte_arrays[n] = test_in
		return byte_arrays




	def get_custom_verilog_str(self, t="  "):
		verilog = """
  initial begin
`ifndef VERILATOR
`ifdef DUMP
  	$vcdplusfile("dump.vpd");
"""
		'''for m in modules_to_debug:
			owner_list = m.get_owner_list()
			verilog += "    $vcdpluson(0, " + ".".join([o.name + "_i" for o in reversed(owner_list[:-2])] + [m.name + "_i"]) + ");\n"'''
		verilog += """
    $vcdpluson(0, hpipe_tb_0);
    //$vcdplusmemon(0, hpipe_tb_0);
    //$vcdpluson(0, hpipe_0_i.basic_conv_24_i);
`endif
`endif
"""
		
		verilog += f"""  end
  reg [20:0] finish_counter;
  always @(posedge clock_0) begin
  	if (reset) begin
  		finish_counter <= '0;
  	end else begin
	  	finish_counter <= finish_counter + 1;
  		if (finish_counter == 10000000 || {self.done_simulation.name}) begin
"""
		verilog += """
`ifndef VERILATOR
  			$finish;
`endif
  		end
  	end
  end
"""
		return verilog

	def get_custom_verilog_port_list(self, t="  "):
		verilog = "`ifdef VERILATOR\n"
		verilog += t + self.clock.name + ",\n"
		verilog += t + self.reset.name + "\n"
		verilog += "`endif\n"
		return verilog


class HPipeTB(HPIPETLM):
	def __init__(self, plan, build_from=None, build_until=None, debug_individual_stage=None, **kwargs):
		super(HPipeTB, self).__init__("hpipe_tb", kind="hpipe_tb", **kwargs)
		inst = self.inst
		interface_width = 256
		interface_elements = interface_width // 9
		interface_bits = interface_elements * 9
		with dl.ModuleScope(self):
			clock = inst(dl.Clock)
			reset = inst(dl.ForcedSignal, name="reset", bits=1, signed=False)
			self.clock = clock
			self.reset = reset
			reset.add_value(1, 0)
			reset.add_value(0, 500)
			example_inputs = []
			example_input_precisions = []
			for n in plan.graph.nodes:
				print(n.tf_op.name + " " + n.type)
			for n in plan.graph.nodes:
				if not debug_individual_stage and not build_from and n.type == "Placeholder":
					example_inputs.append(n.example_outputs[0])
					example_input_precisions.append(n.precision_parameters)
				elif (debug_individual_stage is not None and debug_individual_stage in n.tf_op.name) or (build_from is not None and build_from in n.tf_op.name):
					for i in n.inputs:
						example_inputs.append(i._from.example_outputs[0])
					example_input_precisions.append(n.precision_parameters)
					break

			conv_count = 0
			for n in plan.graph.nodes:
				if n.type == "Conv2D":
					conv_count += 1
					if conv_count == 3:
						first_conv = n
						break
			for n in plan.graph.nodes:
				if n.type == "BiasAdd":
					first_bias = n
					break
			for n in plan.graph.nodes:
				if n.type == "MaxPool":
					first_max_pool = n
					break
			for n in plan.graph.nodes:
				if n.type == "Relu":
					first_relu = n
					break
			for n in plan.graph.nodes:
				if n.type in ["Add","AddV2"]:
					first_add = n
					break

			"""
			output_image = [np.sum(np.multiply(input_image[0,i*2:i*2+7,j*2:j*2+7,:], weights[:,:,:,c])) for i in range(112) for j in range(112) for c in range(weights.shape[-1])]
			output_image = np.array(output_image)
			output_image = np.reshape(output_image, [112,112,weights.shape[-1]])
			output_image = np.transpose(output_image, [0,2,1])
			output_image = np.reshape(output_image, [112*weights.shape[-1], 112])
			output_image //= 2**4
			output_image = [np.max(output_image[i*2:i*2+3,j*2:j*2+3,c]) for i in range(56) for j in range(56) for c in range(weights.shape[-1])]
			output_image = np.array(output_image)
			output_image = np.reshape(output_image, [56,56,weights.shape[-1]])
			output_image += biases[np.newaxis, np.newaxis, :] * 100
			output_image *= (output_image > 0).astype(int)
			"""
			node_of_interest = first_conv
			if len(node_of_interest.values) > 0:
				weights = node_of_interest.values[0]
				np.save("first_layer_weights.npy", weights)
				input_image = node_of_interest.inputs[0]._from.example_outputs[0]
				np.save("input_image.npy", input_image)

			#im = Image.fromarray((example_inputs[0][0]+128).astype(np.uint8), 'RGB')
			#im.show()
			if debug_individual_stage is None and build_from is None:
				BHCW = np.transpose(example_inputs[0], [0,1,3,2])
				BHCWP = np.pad(BHCW, [[0,0],[0,0],[0,0],[0, 224 % interface_elements]], mode="constant", constant_values=0)
				LI = np.reshape(BHCWP, [224 * 3 * 224 // interface_elements, interface_elements])
				test_in = [BitArray(bits=9, value_vector=LI[:,l]) for l in range(LI.shape[-1])]
				tmp = test_in[-1]
				for t in reversed(test_in[:-1]):
					tmp += t
				test_in = tmp#BitArray(bits=tmp.bits, bit_array=tmp)
				tmp = BitArray(bits=tmp.bits, bit_array=tmp.bit_array)
				if tmp.bits != 256:
					tmp += BitArray(bits=256 - tmp.bits, length=tmp.bit_array.shape[0])
					with open("input.bin", "wb") as fh:
						fh.write(tmp.get_byte_array())
			else:
				test_inputs = [np.reshape(np.transpose(i, [0,1,3,2]), [np.prod(np.array(list(i.shape))[[0,1,3]]), i.shape[2]]) for i in example_inputs]

			with dl.ParamScope(clock=clock, reset=reset, write_port_width=4, extend_to_match_bits=True):
				hpipe = inst(HPipe, plan, debug_individual_stage, build_until, build_from=build_from)
				self.hpipe = hpipe
			with dl.ParamScope(clock=clock, reset=hpipe.reset, write_port_width=4):
				for n in plan.graph.nodes:
					if "conv2d_25" in n.tf_op.name:
						continue
						np.save("first_layer_weights.npy", n.values[0])
						exit(0)

				if debug_individual_stage is None and build_from is None:
					input_rom = inst(dl.ROM, name="test_input_image", bits=(256 // 9) * 9, content_list=test_in)
					input_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=len(test_in)-1)
					input_rom.r_addr.set_driver(input_counter.current_value)
					# inst(dl.NOT, input_counter.is_done)
					read_input = inst(dl.AND, inst(dl.NOT, reset), hpipe.placeholder.space_to_write_line)
					input_rom.r_en.set_driver(read_input)
					input_counter.set_increment_condition(read_input)
					hpipe.placeholder.write.set_driver(read_input.delay(1))
					hpipe.placeholder.inputs[0].set_driver(input_rom.r_data)
					input_rom.r_en.set_driver(hpipe.placeholder.space_to_write_line)
					write_counter = inst(dl.Counter,
						reset_value=0,
						increment_value=1,
						end_value=hpipe.placeholder.input_lines,
						name="write_counter")
					write_counter.set_increment_condition(hpipe.placeholder.write)
				else:
					input_precision,_,_ = self.hpipe.nodes[0].module.get_bit_widths()
					width = len(test_inputs[0][0])
					
					for i,t in enumerate(test_inputs):
						t = [BitArray(bits=input_precision, value_vector=t[:,l]) for l in range(t.shape[-1])]
						tmp = t[-1]
						for _t in reversed(t[:-1]):
							tmp += _t
						test_inputs[i] = tmp
					input_roms = [inst(dl.ROM, bits=input_precision*width, content_list=t) for t in test_inputs]
					input_counters = [inst(dl.Counter, reset_value=0, increment_value=1, end_value=len(t)-1) for t in test_inputs]
					channel_counters = [inst(dl.Counter, reset_value=0, increment_value=1, end_value=example_inputs[i].shape[-1]-1) for i,t in enumerate(test_inputs)]
					m = hpipe.nodes[0].module
					for i,(r,c) in enumerate(zip(input_roms, input_counters)):
						states = ["IDLE", "WRITING_LINE", "DONE"]
						edges = [["IDLE", "WRITING_LINE", m.input_modules[i].space_to_write_line],
							["WRITING_LINE", "IDLE", channel_counters[i].is_done],
							["WRITING_LINE", "DONE", inst(dl.AND, channel_counters[i].is_done, c.is_done)],
							#["DONE", "IDLE"]
							]
						state_machine = inst(dl.StateMachine, states, edges, None, name="input_state_machine_" + str(i))
						with dl.ModuleScope(state_machine):
							f = inst(dl.Flop, bits=1, name="phantom_flop")
							f.set_reset_driver(inst(dl.Constant, bits=1, value=0))
						read_input = state_machine.c["is_writing_line"]
						channel_counters[i].set_increment_condition(state_machine.c["is_writing_line"])
						r.r_addr.set_driver(c.current_value)
						r.r_en.set_driver(read_input)
						c.set_increment_condition(read_input)
						m.input_modules[i].write.set_driver(read_input.delay(1))
						delayed_write = read_input.delay(1 + m.get_control_to_input_delay())
						delayed_read_data = r.r_data.delay(m.get_control_to_input_delay())
						for j,inp in enumerate(m.input_modules[i].inputs):
							inp.set_driver(delayed_read_data[(j+1)*input_precision-1:j*input_precision])
							m.input_modules[i].writes[j].set_driver(delayed_write)



				"""with dl.ModuleScope(hpipe.bc.ia_buffer, True):
					input_mux = inst(dl.Mux, input_rom.r_data._bit_width,
						hpipe.iwc.state_machine.c["is_writing_padding"].delay(1),
						input_rom.r_data,
						inst(dl.Constant, bits=input_rom.r_data._bit_width, value=0))"""


				"""
				self.all_outputs = []
				self.all_valids = []
				self.done_signals = []
				for n in self.hpipe.nodes:
					output_signals = []
					self.all_valids.append(inst(dl.Logic, bits=1))
					self.all_valids[-1].set_driver(n.module.outputs_valid[0])
					self.done_signals.append(inst(dl.Logic, bits=1))
					self.done_signals[-1].set_driver(n.module.done)
					for o in n.module.outputs:
						output_signals.append(inst(dl.Logic, bits=o._bit_width, signed=o.signed))
						output_signals[-1].set_driver(o)
					self.all_outputs.append(output_signals)"""

			print_stage_header("Writing TensorFlow Outputs For Nodes in Graph")
			for n in self.hpipe.nodes:
				output_image = n.example_outputs[0]
				#print("NODE OF INTEREST SHAPE: " + str(output_image.shape))
				output_shape = output_image.shape
				output_image = np.reshape(output_image, output_image.shape[1:])
				output_image = np.transpose(output_image, [0,2,1])
				output_image = np.reshape(output_image, [np.prod(output_image.shape[:2]),output_image.shape[-1]])
				f_name = "generated_files/layer_images/tensorflow/" + n.module.name + ".csv"
				if not os.path.isfile(f_name) or True:
					print("  Generating " + f_name)
					with open(f_name, "w") as f:
						f.write(str(output_shape[-1]) + "\n")
						for i in range(output_image.shape[0]):
							f.write(",".join([str(d) for d in list(output_image[i])]) + "\n")
				else:
					print("  Not touching " + f_name + " since it already exists")


				"""with dl.ModuleScope(inst(dl.Module, "test_output_mux", kind="test_output_mux", name="test_output_mux")):
					self.output_roms = [inst(dl.ROM, name="golden_output_image", bits=36, content_list=list(output_image[i])) for i in range(output_image.shape[0])]
					for otr in self.output_roms:
						otr.r_en.set_driver(inst(dl.Constant, bits=1, value=1, signed=False))
						otr.r_addr.set_driver(output_data_counter.current_value)
				self.golden_output_data = [inst(dl.Logic, bits=36, signed=True, name="golden_output_data") for _ in self.output_roms]
				for god, otr in zip(self.golden_output_data, self.output_roms):
					god.set_driver(otr.r_data)"""



	def get_custom_verilog_str(self, t="  "):
		verilog = """
  initial begin
`ifndef VERILATOR
`ifdef DUMP
  	$vcdplusfile("max_pool_debug.vpd");
    $vcdpluson(0, hpipe_tb_0);
    //$vcdpluson(0, hpipe_0_i.basic_conv_24_i);
`endif
`endif
"""
		
		verilog += """  end
  reg [20:0] finish_counter;
  always @(posedge clock_0) begin
  	if (reset) begin
  		finish_counter <= '0;
  	end else begin
	  	finish_counter <= finish_counter + 1;
  		if (finish_counter == 2000000) begin
"""
		verilog += """
`ifndef VERILATOR
  			$finish;
`endif
  		end
  	end
  end
"""
		return verilog

	def get_custom_verilog_port_list(self, t="  "):
		verilog = "`ifdef VERILATOR\n"
		verilog += t + self.clock.name + ",\n"
		verilog += t + self.reset.name + "\n"
		verilog += "`endif\n"
		return verilog

def get_argparser():
	parser = argparse.ArgumentParser(description="Generate verilog that implements a neural network exported from TensorFlow")
	parser.add_argument("--param_file_path", type=str, default="params_file.json")
	parser.add_argument("--quartus", const=True, default=False, action="store_const")
	parser.add_argument("--dont_dump_verilog", const=True, default=False, action="store_const")
	parser.add_argument("--dump_tensorflow_activations", const=True, default=False, action="store_const")
	parser.add_argument("--print_graph_nodes", const=True, default=False, action="store_const")
	parser.add_argument("--make_fake_weights", const=True, default=False, action="store_const")
	parser.add_argument("--print_resource_estimates", const=True, default=False, action="store_const")
	parser.add_argument("--fake_weights_sparsity", default=0.0, type=float)
	parser.add_argument("--generated_circuit_path", default="", type=str)
	parser.add_argument("--dsp_target", default=0, type=int)
	parser.add_argument("--arch", default="architectures/s10.json")
	parser.add_argument("--device", default="devices/s10_2800.json")
	return parser

def process_params_file(args):
	params_file = args.param_file_path
	generated_circuit_dir = "generated_circuit"
	top_level_module_type = "tb"
	
	if args.quartus:
		generated_circuit_dir = "quartus_circuit"
		top_level_module_type = "quartus"

	params = {
		"pb_path" : "/home/mat/drive1/different_sparsity_resnet_models/304811/frozen_graph.opt.swapped_max_pool_and_batch_norm.pb",
		"sample_image" : "individualImage.png",
		"output_names" : ["import/resnet_model/final_dense"],
		"quartus" : args.quartus,
		"debug_individual_stage" : None,
		"build_from" : None,
		"build_until" : None,
		"target_act_bits" : 16,
		"target_parameter_bits" : 16,
		"oc_unroll_threshold" : 32,
		"input_adjustments" : {
			"pre_scale" : 1.0,
			"channel_biases" : [0.0, 0.0, 0.0],
			"post_scale" : 1.0,
			"bit_spec" : {
				"sign" : 0,
				"int" : 1,
				"frac" : 0
			},
		},
		"input_bit_spec" : {
			"sign" : 0,
			"int" : 0,
			"frac" : 8
		},
		"example_input_only_scale" : 1.0,
		"apply_input_adjustments_to_example_input" : True,
		"dsp_target" : 5000,
		"mem_target" : None,
		"instantiate_quartus_ip" : False,
		"physically_mapped_rams" : True,
		"op_precision_annotations_path" : "default_precisions.annotations",
		"generated_circuit_dir" : generated_circuit_dir,
		"build_data_path" : True,
		"top_level_module_type" : top_level_module_type,
		"import_mobilenet_ssd" : False,
		"propagate_outputs_to_top": False
	}
	default_input_adjustments = params["input_adjustments"]
	
	if os.path.isfile(params_file):
		required_keys = list(params.keys())
		with open(params_file, "r") as fh:
			new_params = json.load(fh)
			for k in required_keys:
				if k not in new_params:
					print("Param file " + params_file + " doesn't have required key '" + k + "'.  Adding it with default value.")
					new_params[k] = params[k]
			params = new_params
	with open(params_file, "w") as fh:
		json.dump(params, fh, sort_keys=True, indent=4)
		
	# In case we want to use seperate bit specs for Bias and activation functions we can use target_bias_and_act_func_bits 
	# (e.g. Stratix 10 NX has 8-bit fixed-point limit on tensor blocks but we can have larger precision for the bias add after)
	# If this isn't specified match it up with the activation precision
	if "target_bias_and_act_func_bits" not in params:
		params["target_bias_and_act_func_bits"] = params["target_act_bits"]
		
	#Make sure generated circuit directory is correct if using VCS	
	if params["top_level_module_type"] == "tbV2":
		params["generated_circuit_dir"] = "generated_files/circuits/generated_circuit"

	#if params["build_from"] is not None:
	#	params["input_adjustments"] = default_input_adjustments
	return params

def main(argv):
	warnings.simplefilter('once', UserWarning)
	parser = get_argparser()
	args = parser.parse_args(argv[1:])
	arch.Architecture(args.arch, args.device)

	dump_tensorflow_activations = args.dump_tensorflow_activations
	params = process_params_file(args)
	pb_path = params["pb_path"]
	image_path = params["sample_image"]
	output_names = params["output_names"]
	generate_quartus_project = params["quartus"]
	debug_individual_stage = params["debug_individual_stage"]
	build_from = params["build_from"]
	build_until = params["build_until"]
	input_adjustments = params["input_adjustments"]
	input_bit_spec = params["input_bit_spec"]
	dsp_target = params["dsp_target"]
	mem_target = params["mem_target"]
	target_act_bits = params["target_act_bits"]
	target_parameter_bits = params["target_parameter_bits"]
	target_bias_and_act_func_bits = params["target_bias_and_act_func_bits"]
	instantiate_quartus_ip = params["instantiate_quartus_ip"]
	generated_circuit_dir = params["generated_circuit_dir"]
	op_precisions_path = params["op_precision_annotations_path"]
	build_data_path = params["build_data_path"]
	oc_unroll_threshold = params["oc_unroll_threshold"]
	physically_mapped_rams = params["physically_mapped_rams"]
	import_mobilenet_ssd = params["import_mobilenet_ssd"]
	apply_input_adjustments_to_example_input = params["apply_input_adjustments_to_example_input"]
	example_input_only_scale = params["example_input_only_scale"]
	propagate_outputs_to_top = params["propagate_outputs_to_top"]
	top_level_module_types = {
		"quartus" : SystemBuilderComponent,
		"tb" : HPipeTB,
		"tbV2" : HPIPETB2
	}
	if args.generated_circuit_path != "":
		generated_circuit_dir = args.generated_circuit_path
	if args.dsp_target != 0:
		dsp_target = args.dsp_target

	top_level_module_type = params["top_level_module_type"]
	if top_level_module_type not in top_level_module_types:
		print("Parameter top_level_module_type must be on of: " + ", ".join(top_level_module_types.keys()) + ". You specified " + top_level_module_type)
	top_level_module_type = top_level_module_types[top_level_module_type]


	
	print_stage_header("Optimizing Graph and Forming Plan")
	from hpipe.build_accelerator import get_plan
	plan = get_plan(
		pb_path,
		image_path,
		output_names,
		oc_unroll_threshold=oc_unroll_threshold,
		op_precision_annotations_path=op_precisions_path,
		dsp_target=dsp_target,
		mem_target=mem_target,
		input_adjustments=input_adjustments,
		build_from=build_from,
		target_act_bits=target_act_bits,
		target_parameter_bits=target_parameter_bits,
		target_bias_and_act_func_bits=target_bias_and_act_func_bits,
		print_graph_nodes=args.print_graph_nodes,
		make_fake_weights=args.make_fake_weights,
		fake_weights_sparsity=args.fake_weights_sparsity,
		print_resource_estimates=args.print_resource_estimates,
		import_mobilenet_ssd=import_mobilenet_ssd,
		example_input_only_scale=example_input_only_scale,
		apply_input_adjustments_to_example_input=apply_input_adjustments_to_example_input)
	if args.print_resource_estimates:
		return

	cs_list = []
	for n in plan.graph.nodes:
		pe = n.planner_estimate
		if pe.multiplier_count > 0:
			cs_list.append(pe.n_channel_splits)

	print_stage_header("Channel Splits Histogram")
	plot_hist(cs_list, xlab=True, showSummary=True)


	os.makedirs(generated_circuit_dir, exist_ok=True)
	with open(generated_circuit_dir + "/conv_cycle_estimates.txt", "w") as fh:
		for n in plan.graph.nodes:
			if n.type not in ["Conv2D", "DepthwiseConv2dNative"]:
				continue
			fh.write(n.tf_op.name + ", " + str(n.planner_estimate.n_channel_splits) + ", " + str(n.planner_estimate.time_weight) + "\n")

	print_stage_header("Building Circuit")
	circuit = dl.Circuit()
	tlm_args = {
		"circuit" : circuit,
		"build_until" : build_until,
		"debug_individual_stage" : debug_individual_stage,
		"build_data_path" : build_data_path,
		"directly_propagate_outputs_to_top" : propagate_outputs_to_top}
	with dl.ModuleScope(circuit):
		with dl.ParamScope(circuit=circuit, implement_with_megafunction=instantiate_quartus_ip,
			input_bit_spec=input_bit_spec, input_adjustments=input_adjustments,
			physically_mapped_rams=physically_mapped_rams):
			netlist = top_level_module_type(plan, **tlm_args)
			if top_level_module_type == SystemBuilderComponent:
				netlist.should_create_verilog_definition = False
	print("M20K Estimate: ", circuit.m20k_estimates())
	mif_additional_path = ""
	#if generate_quartus_project:
	#	mif_additional_path = "/generated_circuit"
	numpy_dump_dir = generated_circuit_dir + "/numpy_weights_and_inputs/"
	os.makedirs(numpy_dump_dir, exist_ok=True)
	unit_test_configs = []

	for n in plan.graph.nodes:
		if (n.type != "Conv2D" and n.type != "DepthwiseConv2dNative") or n.module is None:
			continue
		m = n.module
		
		#Not needed for ConvWrapper
		if isinstance(m, BasicConv):
			np.save(numpy_dump_dir + m.name + "_weights_per_oc.npy", m.weights_per_oc)
			np.save(numpy_dump_dir + m.name + "_weights.npy", m.numpy_combined_weights)
			
		np.save(numpy_dump_dir + m.name + "_input.npy", m.example_input)
		np.save(numpy_dump_dir + m.name + "_input_weights.npy", m.weights)
		
		#cwd = os.getcwd() + "/"
		#File paths should be relative to Unit Test folder
		cwd = "test_configs/mobilenet-v2/"
		unit_test_configs.append({
			"aux" : {
				"name" : m.name,
				"time_estimate" : str(n.planner_estimate.time_weight)
			},
			"command" : [
			"--sy" , str(n.get_strides()[1]),
			"--sx" , str(n.get_strides()[2]),
			#"-i" , cwd + numpy_dump_dir + m.name + "_input.npy",
			"-i" , cwd + m.name + "_input.npy",
			#"--weights" , cwd + numpy_dump_dir + m.name + "_input_weights.npy",
			"--weights" , cwd + m.name + "_input_weights.npy",
			"--iwbf" , str(n.precision_parameters["f"]),
			"--iibf" , str(n.input_nodes()[0].precision_parameters["a_f"]),
			"--wbf" , str(n.precision_parameters["f"]),
			"--wbi" , str(n.precision_parameters["int"]),
			"--ibf" , str(n.input_nodes()[0].precision_parameters["a_f"]),
			"--ibi" , str(n.input_nodes()[0].precision_parameters["a_int"]),
			"--obf" , str(n.precision_parameters["a_f"]),
			"--obi" , str(n.precision_parameters["a_int"]),
			"--type", n.type,	
			"--padding", n.properties["padding"],
			"--ic_parallelism", str(n.planner_estimate.n_channel_splits),
			"--oc_parallelism", str(n.planner_estimate.n_output_channel_groups)
			]})
	with open(numpy_dump_dir + "unit_test_configs.json", "w") as fh:
		json.dump(unit_test_configs, fh)

	if dump_tensorflow_activations:
		netlist.dump_tensorflow_activations()
	if args.dont_dump_verilog:
		sys.exit(0)
	netlist.hpipe.write_stats_to_dir(generated_circuit_dir)
	circuit.write_verilog_to_dir(generated_circuit_dir)#, mif_additional_path=mif_additional_path)
	os.makedirs("generated_files/layer_images/verilog", exist_ok=True)

	#print_stage_header("Finished Verilog Generation")
	"""
	print_stage_header("Generating Filelists and Info For Modular Verilator Flow")
	with open(generated_circuit_dir + "/verilog_paths.txt", "r") as fh:
		paths = [l for l in fh]
	for n in netlist.hpipe.nodes:
		m = n.module
		paths_for_module = []
		for p in paths:
			if ("/" + m.name + "/") in p or ("/" + m.name + ".v") in p:
				paths_for_module.append(p)
		paths_for_module_str = "".join(paths_for_module)
		paths_path = generated_circuit_dir + "/CIRCUIT/" + netlist.name + "/" + netlist.hpipe.name + "/" + m.name + "/verilog_paths.txt"
		file_matches = False
		if os.path.isfile(paths_path):
			file_matches = True
			with open(paths_path, "r") as pp:
				ppe_str = [l for l in "".join(pp)]
				if ppe_str != paths_for_module_str:
					file_matches = False
		if not file_matches:
			with open(paths_path, "w") as pp:
				pp.write(paths_for_module_str)"""


if __name__ == "__main__":
	main(sys.argv)


#	node_precision_file = open("op_precisions.annotations", "w")
#	node_precision_file.write("node_name,scale,sign_bits,exponent,int,f,a_sign_bits,a_exponent,a_int,a_f\n")
#	def print_node_precision_file(node):
#		node_precision_file.write(",".join([node.tf_op.name, "1.", "1","0","4","11","1","0","4","11"]) + "\n")
#	plan.graph.walk_graph(f_node=print_node_precision_file)
#	node_precision_file.close()

