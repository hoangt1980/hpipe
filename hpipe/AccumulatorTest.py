import numpy as np
import sys
from Accumulator import Accumulator, AccumulatorModel
from SparseWeights import SparseWeights
from WeightBinner import WeightBinner

def main():
	np.random.seed(0)
	bins = 16
	max_cycles = 10000
	parallel_reduction = 2
	weight_shape = (3,3,128,256)
	ia_shape = (3,3,128,1)
	ia = (np.random.normal(0., 1., ia_shape) * 20).astype(int)
	weight_count = np.prod(weight_shape)
	weights = (np.reshape(np.random.binomial(1, 0.15, weight_count), weight_shape) * np.random.normal(0.,1.,weight_shape) * 20).astype(int)
	golden_oa = np.sum(ia * weights, axis=(0,1,2))
	sparse_weights = SparseWeights(weights)
	binner = WeightBinner(bins, sparse_weights)
	accumulators = []
	accumulator_models = []
	for i in range(bins):
		accumulators.append(Accumulator(weight_shape, 3, 2, parallel_reduction))
		for swd in reversed(binner.bins[i]):
			accumulators[-1].add_oc_group(*swd)
		accumulator_models.append(AccumulatorModel(accumulators[-1], 1))

	collected_oa = np.zeros_like(golden_oa)
	collected    = np.zeros_like(golden_oa)
	flat_ia = ia.flatten()
	reqs = [[False] for i in range(bins)]
	cycle_count = 0
	total_req_count = 0
	while cycle_count < max_cycles and collected.size != np.sum(collected):
		already_granted = False
		valid = False
		v = 0
		addr = 0
		req_count = 0
		if cycle_count < flat_ia.size:
			valid = True
			v = flat_ia[cycle_count]
			addr = cycle_count
		cycle_count += 1
		for i,m in enumerate(accumulator_models):
			granting = reqs[i][0] and not already_granted
			if reqs[i][0] and already_granted:
				total_req_count += 1
			outputs = m.step(valid, addr, v, [granting])
			reqs[i] = outputs["reqs"]
			if granting:
				already_granted = True
				collected[outputs["oc"]] = 1
				collected_oa[outputs["oc"]] = outputs["data"]
				indices = list(np.nonzero(weights[:,:,:,outputs["oc"]:outputs["oc"]+1]))
				acts = ia[indices]
				indices[-1] += outputs["oc"]
				ws = weights[indices]
				assert(collected_oa[outputs["oc"]] == golden_oa[outputs["oc"]])
	print(collected_oa == golden_oa)
	print(cycle_count)
	print(float(total_req_count) / float(cycle_count))
	assert((collected_oa == golden_oa).all())



if __name__ == "__main__":
	main()