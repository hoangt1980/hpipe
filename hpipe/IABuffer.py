import numpy as np
import math
import digital_logic as dl
from utils import LogicModel, MuxModel, CounterModel, clog2

class IABuffer():
	def __init__(self, input_activation_shape, kernel_shape, padding, stride, max_consumer_count, input_n_buffering, activation_bits=16
				 explicit_padding=[[0, 0], [0, 0], [0, 0], [0, 0]]):
		input_height = input_activation_shape[1]
		input_width = input_activation_shape[2]
		input_channels = input_activation_shape[3]

		kernel_height = kernel_shape[0]
		kernel_width = kernel_shape[1]

		input_spatial_shape = [s + np.sum(p) for s, p in zip(input_activation_shape[1:3], explicit_padding[1:3])]
		spatial_filter_shape = [s for s in kernel_shape[0:2]]
		if padding.lower() == "same":
			output_spatial_shape = [math.ceil(float(s) / stride) for s in input_spatial_shape]
		else:
			output_spatial_shape = [math.ceil((s - (k - 1)) / float(stride))
									for s, k in zip(input_spatial_shape, spatial_filter_shape)]
		self.output_spatial_shape = output_spatial_shape
		padded_input_spatial_shape = [(o - 1) * stride + k for o, k in zip(output_spatial_shape, spatial_filter_shape)]
		padded_input_spatial_shape = [max(p,i) for p,i in zip(padded_input_spatial_shape, input_spatial_shape)]
		spatial_padding = [p - i for p, i in zip(padded_input_spatial_shape, input_spatial_shape)]
		divided_spatial_padding = [[math.ceil(p / 2.), p // 2] for p in spatial_padding]
		self.padding = [[0, 0]] + divided_spatial_padding + [[0, 0]]

		self.activation_bits = activation_bits
		self.input_activation_shape = list(input_activation_shape[0:1]) + padded_input_spatial_shape + list(
			input_activation_shape[3:4])
		self.kernel_shape = kernel_shape
		self.stride = stride
		self.num_lines = 2**clog2(kernel_height + stride)
		self.memory_depth = 2**clog2(padded_input_spatial_shape[1]) * self.num_lines * 2**clog2(input_channels)
		self.consumer_count = max_consumer_count
		self.input_n_buffering = input_n_buffering
		if kernel_width == 1 and kernel_height == 1:
			self.num_lines = 1
			self.memory_depth = 2 * kernel_shape[2]

class IABufferController(dl.Module):
	def __init__(self, ia_buffer, kind="ia_buffer_controller", **kwargs):
		super(IABufferController, self).__init__("ia_buffer_controller", kind=kind, **kwargs)
		kwargs = self.kwargs

		input_height = ia_buffer.input_activation_shape[1]
		input_width = ia_buffer.input_activation_shape[2]
		input_channels = ia_buffer.input_activation_shape[3]
		channel_bits = clog2(input_channels)
		x_bits = clog2(input_width)

		kernel_height = ia_buffer.kernel_shape[0]
		kernel_width = ia_buffer.kernel_shape[1]

		stride = ia_buffer.stride

		self.print_header_count = 0
		self.memory_depth = ia_buffer.memory_depth
		self.num_lines = ia_buffer.num_lines

		self.kernel_height = kernel_height
		self.kernel_width = kernel_width

		self.input_n_buffering = ia_buffer.input_n_buffering

		self.x_padding_left = ia_buffer.padding[2][0]
		self.x_padding_right = input_width - ia_buffer.padding[2][1]
		self.y_padding_top = ia_buffer.padding[1][0]
		self.y_padding_bottom = input_height - ia_buffer.padding[1][1]

		self.have_reset_write_idx_without_read_idx_reset = False

		self.inputs = {
			"req": dl.Logic(1, name="req", **kwargs),
			"data": dl.Logic(ia_buffer.activation_bits, name="input_data", **kwargs),
			"channel": dl.Logic(channel_bits, name="input_channel", **kwargs),
			"x": dl.Logic(x_bits, name="input_x", **kwargs),
			"increment_y": dl.Logic(1, name="increment_y", **kwargs),
			"waits": [dl.Logic(1, name="wait", **kwargs) for _ in range(ia_buffer.consumer_count)]
		}

		counter_properties = [
			["write_channel", 0, 1, input_channels - 1],
			["write_x", 0, 1, input_width - 1],
			["write_y", 0, 1, input_height - 1],
			["read_channel", 0, 1, input_channels - 1],
			["read_base_x", 0, stride, ((input_width - kernel_width) // stride) * stride],
			["read_base_y", 0, stride, ((input_height - kernel_height) // stride) * stride],
			["read_kernel_x", 0, 1, kernel_width - 1],
			["read_kernel_y", 0, 1, kernel_height - 1],
			["kernel_index", 0, 1, input_channels*kernel_width*kernel_height - 1]
		]
		self.counters = {}
		for p in counter_properties:
			self.counters[p[0]] = dl.Counter(
				reset_value=p[1],
				increment_value=p[2],
				end_value=p[3],
				name=p[0], **kwargs)

		self.logic = {}
		self.logic["write_channel_mux"] = dl.Mux(
			lambda: self.c["in_pad"],
			lambda: [self.logic["input_channel_mux"], self.counters["write_channel"]],
			clog2(input_channels),
			"write_channel_mux")

		self.derived_counters = {}
		def add_derived_counters(l):
			nonlocal self
			for d in l:
				self.derived_counters[d[0]] = d[1]
				d[1].name = d[0]
		derived_counter_list = [
			["mod_write_y", self.counters["write_y"][:clog2(ia_buffer.num_lines)-1]],
			["read_y", (self.counters["read_base_y"] + self.counters["read_kernel_y"])],
			["read_x", self.counters["read_base_x"] + self.counters["read_kernel_x"]]
		]
		add_derived_counters(derived_counter_list)

		derived_counter_list = [
			["mod_read_y", self.derived_counters["read_y"][:clog2(ia_buffer.num_lines)-1]],
		]
		add_derived_counters(derived_counter_list)
		derived_counter_list = [
			["write_addr", CounterModel.concat([
				self.derived_counters["mod_write_y"],
				self.counters["write_x"],
				self.logic["write_channel_mux"]])],
			["read_addr", CounterModel.concat([
				self.derived_counters["mod_read_y"],
				self.derived_counters["read_x"],
				self.counters["read_channel"]])],
			["write_position", CounterModel.concat([
				self.counters["write_y"],
				self.counters["write_x"]])],
			["read_position", CounterModel.concat([
				self.counters["read_base_y"],
				self.counters["read_base_x"]])]
		]
		add_derived_counters(derived_counter_list)

		self.buffer = np.zeros(math.ceil(ia_buffer.memory_depth / 1024.) * 1024)
		self.write_buffer_idx = 0
		self.read_buffer_idx = 0

		self._build_c()
		self.accept_reqs = not self.c["in_pad"]

		self.counters["write_channel"].set_increment_condition(lambda: self.c["write"])
		self.counters["write_x"].set_increment_condition(lambda: self.counters["write_channel"].reset_condition())
		self.counters["write_y"].set_increment_condition(self.counters["write_x"].reset_condition)

		self.counters["read_channel"].set_increment_condition(lambda: self.c["read"])
		self.counters["read_kernel_x"].set_increment_condition(self.counters["read_channel"].reset_condition)
		self.counters["read_kernel_y"].set_increment_condition(self.counters["read_kernel_x"].reset_condition)
		self.counters["read_base_x"].set_increment_condition(self.counters["read_kernel_y"].reset_condition)
		self.counters["read_base_y"].set_increment_condition(self.counters["read_base_x"].reset_condition)
		self.counters["kernel_index"].set_increment_condition(lambda: self.c["read"])


class IABufferModel():
	def __init__(self, ia_buffer):
		input_height = ia_buffer.input_activation_shape[1]
		input_width = ia_buffer.input_activation_shape[2]
		input_channels = ia_buffer.input_activation_shape[3]

		kernel_height = ia_buffer.kernel_shape[0]
		kernel_width = ia_buffer.kernel_shape[1]

		stride = ia_buffer.stride

		self.print_header_count = 0
		self.memory_depth = ia_buffer.memory_depth
		self.num_lines = ia_buffer.num_lines

		self.kernel_height = kernel_height
		self.kernel_width = kernel_width

		self.input_n_buffering = ia_buffer.input_n_buffering

		self.x_padding_left = ia_buffer.padding[2][0]
		self.x_padding_right = input_width - ia_buffer.padding[2][1]
		self.y_padding_top = ia_buffer.padding[1][0]
		self.y_padding_bottom = input_height - ia_buffer.padding[1][1]

		self.have_reset_write_idx_without_read_idx_reset = False
		self.gnt_idx = 0

		self.inputs = {
			"reqs": [False for _ in range(ia_buffer.requester_count)],
			"data": 0,
			"channels": [0 for _ in range(ia_buffer.requester_count)],
			"increment_write_addr": False,
			"write_buffer_idx": 0,
			"waits": []
		}

		counter_properties = [
			["write_channel", 0, 1, input_channels - 1],
			["write_x", 0, 1, input_width - 1],
			["write_y", 0, 1, input_height - 1],
			["read_channel", 0, 1, input_channels - 1],
			["read_base_x", 0, stride, ((input_width - kernel_width) // stride) * stride],
			["read_base_y", 0, stride, ((input_height - kernel_height) // stride) * stride],
			["read_kernel_x", 0, 1, kernel_width - 1],
			["read_kernel_y", 0, 1, kernel_height - 1],
			["kernel_index", 0, 1, input_channels*kernel_width*kernel_height - 1]
		]
		self.counters = {}
		for p in counter_properties:
			self.counters[p[0]] = CounterModel(
				reset_value=p[1],
				increment_value=p[2],
				end_value=p[3],
				name=p[0])

		self.logic = {}
		self.logic["input_channel_mux"] = MuxModel(
			lambda: self.gnt_idx,
			lambda: self.inputs["channels"],
			clog2(input_channels),
			"input_channel_mux")
		self.logic["write_channel_mux"] = MuxModel(
			lambda: self.c["in_pad"],
			lambda: [self.logic["input_channel_mux"], self.counters["write_channel"]],
			clog2(input_channels),
			"write_channel_mux")

		self.derived_counters = {}
		def add_derived_counters(l):
			nonlocal self
			for d in l:
				self.derived_counters[d[0]] = d[1]
				d[1].name = d[0]
		derived_counter_list = [
			["mod_write_y", self.counters["write_y"][:clog2(ia_buffer.num_lines)-1]],
			["read_y", (self.counters["read_base_y"] + self.counters["read_kernel_y"])],
			["read_x", self.counters["read_base_x"] + self.counters["read_kernel_x"]]
		]
		add_derived_counters(derived_counter_list)

		derived_counter_list = [
			["mod_read_y", self.derived_counters["read_y"][:clog2(ia_buffer.num_lines)-1]],
		]
		add_derived_counters(derived_counter_list)
		derived_counter_list = [
			["write_addr", CounterModel.concat([
				self.derived_counters["mod_write_y"],
				self.counters["write_x"],
				self.logic["write_channel_mux"]])],
			["read_addr", CounterModel.concat([
				self.derived_counters["mod_read_y"],
				self.derived_counters["read_x"],
				self.counters["read_channel"]])],
			["write_position", CounterModel.concat([
				self.counters["write_y"],
				self.counters["write_x"]])],
			["read_position", CounterModel.concat([
				self.counters["read_base_y"],
				self.counters["read_base_x"]])]
		]
		add_derived_counters(derived_counter_list)

		self.buffer = np.zeros(math.ceil(ia_buffer.memory_depth / 1024.) * 1024)
		self.gnts = np.array([False for _ in range(ia_buffer.requester_count)])
		self.write_buffer_idx = 0
		self.read_buffer_idx = 0

		self._build_c()
		self.accept_reqs = not self.c["in_pad"]

		self.counters["write_channel"].set_increment_condition(lambda: self.c["write"])
		self.counters["write_x"].set_increment_condition(lambda: self.counters["write_channel"].reset_condition())
		self.counters["write_y"].set_increment_condition(self.counters["write_x"].reset_condition)

		self.counters["read_channel"].set_increment_condition(lambda: self.c["read"])
		self.counters["read_kernel_x"].set_increment_condition(self.counters["read_channel"].reset_condition)
		self.counters["read_kernel_y"].set_increment_condition(self.counters["read_kernel_x"].reset_condition)
		self.counters["read_base_x"].set_increment_condition(self.counters["read_kernel_y"].reset_condition)
		self.counters["read_base_y"].set_increment_condition(self.counters["read_base_x"].reset_condition)
		self.counters["kernel_index"].set_increment_condition(lambda: self.c["read"])

	def _print_c_header(self):
		for k in self.c.keys():
			print("%5s" % k + " ", end="")

	def _print_c(self):
		for k,v in self.c.items():
			print(("%" + str(max(len(k), 5)) + "s") % str(v) + " ", end="")

	def _print_counters_header(self):
		for k in self.counters.keys():
			print("%5s" % k + " ", end="")
		for k in self.derived_counters.keys():
			print("%5s" % k + " ", end="")

	def _print_counters(self):
		for k,v in self.counters.items():
			print(("%" + str(max(len(k), 5)) + "s") % str(v()) + " ", end="")
		for k,v in self.derived_counters.items():
			print(("%" + str(max(len(k), 5)) + "s") % str(v()) + " ", end="")

	def _print_logic(self):
		for k,v in self.logic.items():
			print(("%" + str(max(len(k), 5)) + "s") % str(v()) + " ", end="")

	def _print_logic_header(self):
		for k in self.logic.keys():
			print("%5s" % k + " ", end="")

	def _print_inputs_header(self):
		for k in self.inputs.keys():
			print("%9s" % k + " ", end="")

	def _print_inputs(self):
		for k in self.inputs.keys():
			print(("%" + str(max(len(k), 9)) + "s") % str(self.inputs[k]) + " ", end="")

	def _print_headers(self):
		self._print_c_header()
		self._print_counters_header()
		self._print_inputs_header()
		self._print_logic_header()
		print("%9s" % "Grants", end="")
		print()

	def _print_state(self):
		if self.print_header_count % 40 == 0:
			self._print_headers()
		self.print_header_count += 1
		self._print_c()
		self._print_counters()
		self._print_inputs()
		self._print_logic()
		print("%9s" %  str(self.gnts), end="")
		print()


	def _update_c(self):
		for k,v in self._c.items():
			self.c[k] = v()
		#self._print_state()

	def _build_c(self):
		self._c = {}
		self.c = {}
		for k,v in self.counters.items():
			self._c[k] = v.done
		self._c["in_top_pad"] = lambda: self.counters["write_y"]() < self.y_padding_top
		self._c["in_bottom_pad"] = lambda: self.counters["write_y"]() >= self.y_padding_bottom
		self._c["in_left_pad"] = lambda: self.counters["write_x"]() < self.x_padding_left
		self._c["in_right_pad"] = lambda: self.counters["write_x"]() >= self.x_padding_right
		self._c["in_pad"] = lambda: self._c["in_top_pad"]() or self._c["in_bottom_pad"]() or self._c["in_left_pad"]() or self._c["in_right_pad"]()
		self._c["write_zeros"] = lambda: self._space_to_write() and self._c["in_pad"]() and not self.have_reset_write_idx_without_read_idx_reset
		self._c["write"] = lambda: self.gnts.any() or self._c["write_zeros"]()
		self._c["read"] = lambda: self.derived_counters["read_y"]() < self.counters["write_y"]() or self.derived_counters["read_y"]() == self.counters["write_y"]() and self.derived_counters["read_x"]() < self.counters["write_x"]() or self.have_reset_write_idx_without_read_idx_reset
		self._c["accept_reqs"] = lambda: not self._c["in_pad"]() and self._space_to_write() and not self.have_reset_write_idx_without_read_idx_reset
		self._update_c()

	def _space_to_write(self):
		if self.kernel_height == 1 and self.kernel_width == 1:
			return (self.derived_counters["write_position"]() - 2) != self.derived_counters["read_position"]()
		return (self.counters["write_y"]() - self.num_lines) != self.counters["read_base_y"]()

	def _step_counters(self):
		for counter in self.counters.values():
			counter.step()
		for counter in self.counters.values():
			counter.update_c()

	def step(self, reqs, data, channels, increment_write_addr, write_buffer_idx, waits):
		self.inputs["reqs"] = reqs
		self.inputs["data"] = data
		self.inputs["channels"] = channels
		self.inputs["increment_write_addr"] = increment_write_addr
		self.inputs["write_buffer_idx"] = write_buffer_idx
		self.inputs["waits"] = waits
		rvalue = {
			# upstream feedback
			"gnts": self.gnts,

			# downstream outputs
			"addr": 0,
			"valid": False,
			"data": 0,
			"increment_write_addr": False
		}
		self.gnts.fill(False)
		self.gnt_idx = 0
		if self._c["accept_reqs"]():
			for i, req in enumerate(reqs):
				if req:
					self.gnt_idx = i
					self.gnts[i] = True
					break
		self._update_c()
		if self.gnts.any():
			self._step_write(data[self.gnt_idx], write_buffer_idx[self.gnt_idx])
		elif self.c["write_zeros"]:
			self._step_write(0, self.write_buffer_idx)

		(rvalue["valid"],
		 rvalue["addr"],
		 rvalue["data"],
		 rvalue["increment_write_addr"]) = self._step_read()

		if self.counters["write_y"].reset_condition():
			self.have_reset_write_idx_without_read_idx_reset = True
		elif self.c["read_kernel_y"] and self.c["read_kernel_x"] and self.c["read_channel"] and self.c["read_base_x"] and self.c["read_base_y"]:
			self.have_reset_write_idx_without_read_idx_reset = False

		self._step_counters()
		return rvalue

	def _step_write(self, data, write_buffer_idx):
		write_address = 0
		if self.write_buffer_idx != write_buffer_idx:
			write_address += self.write_address.get_increment_value()
		self.buffer[self.derived_counters["write_addr"]() + write_address] = data

		if not self.c["write_zeros"]:
			self.write_buffer_idx += 1
			self.write_buffer_idx = self.write_buffer_idx % self.input_n_buffering

		#print(self.write_address() + channel + write_address)


	def _step_read(self):
		if not self.c["read"]:
			return (False, 0, 0, False)
		address = self.derived_counters["read_addr"]()
		data = self.buffer[address]
		valid = True
		addr = self.counters["kernel_index"]()
		increment_write_addr = self.c["read_kernel_y"] and self.c["read_kernel_x"] and self.c["read_channel"]

		return (valid, addr, data, increment_write_addr)
