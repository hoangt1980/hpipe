import sys
import numpy as np
import math

from matplotlib.image import imread
from hpipe.GraphBuilder import GraphBuilder
from hpipe.Plan import Plan
from PIL import Image
from copy import copy

def check_args(argv):
	if len(argv) < 5:
		print("Error, please specify a path to a graphdef, a path to a test input image, and the input node name")
		print(argv[0] + " <path/to/graphdef.pb> <path/to/input_image.png> <input_node_name> <output_node_name>")
		print("For example:")
		print("  " + argv[0] + " /home/mat/drive1/different_sparsity_resnet_models/304811/frozen_graph.opt.swapped_max_pool_and_batch_norm.pb individualImage.png import/resnet_model/Pad import/resnet_model/final_dense")
		sys.exit(-1)

def get_plan(pb_path, image_paths, output_names,
		dsp_target=5000,mem_target=None, input_adjustments=None, example_input_only_scale=1.0,
		apply_input_adjustments_to_example_input=True, **kwargs):
	if (input_adjustments is None) or (apply_input_adjustments_to_example_input == False):
		input_adjustments = {
			"pre_scale" : 1.0,
			"channel_biases" : [0.0, 0.0, 0.0],
			"post_scale" : 1.0,
		}
	original_image = None
	
	
	if image_paths is not None:
		if type(image_paths) != list:
			print("Warning: single image specified. Automatic quantization may not be as precise. Using a list of images is recommended")
			image_paths = [image_paths]
	
		#Apply adjustments to all input images. You could probably do this ouside a loop but keeping it this way for now in case we want different adjustments for each image
		images = None
		for image_path in image_paths:
			img = imread(image_path)
			img = img.astype(float) * example_input_only_scale
			original_image = copy(img)[np.newaxis, ...]
			img *= input_adjustments["pre_scale"]
			#im = Image.fromarray(img.astype(np.uint8), 'RGB')
			#im.show()

			biases = np.array(input_adjustments["channel_biases"])
			while len(biases.shape) < len(img.shape):
				biases = biases[np.newaxis, ...]
			#_R_MEAN = 123.68
			#_G_MEAN = 116.78
			#_B_MEAN = 103.94
			img += biases
			img *= input_adjustments["post_scale"]
			img = np.expand_dims(img, 0)
			
			#Stack all the images
			if type(images) is not np.ndarray:
				images=img.copy()
			else:
				images = np.vstack((images,img))
	else:
		images = None
		
	gb = GraphBuilder()
	graph = gb.load_graph(pb_path, images, output_names, **kwargs)
	if original_image is not None:
		graph.example_input = original_image
	plan = Plan(graph, mem_target, dsp_target, **kwargs)
	return plan


def main(argv):
	check_args(argv)
	plan = get_plan(argv[1], argv[2], [argv[4]])

	total_weight_m20ks = 0
	def print_node_number_and_resource_allocation(n):
		nonlocal total_weight_m20ks
		if n.planner_estimate.multiplier_count == 0:
			return
		print("%37s" % n.tf_op.name + "," + 
			"%5s" % str(n.planner_estimate.multiplier_count) + "," + 
			"%5s" % str(n.planner_estimate.ia_m20k_count) + ",", end="")
		if n.type in ["Conv2D", "DepthwiseConv2D"]:
			nz = np.nonzero(n.values[0])
			count = math.ceil(nz[0].size / 1024.)
			total_weight_m20ks += count
			print("%5s" % str(count), end=",")
		else:
			print(" " * 4 + "0", end=",")

		print(",".join(["%7s" % str(i) for i in [
			n.planner_estimate.row_cycles,
			n.planner_estimate.initialization_cycles,
			n.planner_estimate.sustained_cycles_between_rows,
			n.planner_estimate.sustained_cycles_between_rows - n.planner_estimate.row_cycles]]))

	plan.graph.walk_graph(print_node_number_and_resource_allocation)
	print("Total DSPs: " + str(plan.mul_count))
	print("Estimated IA Buffer M20ks: " + str(plan.ia_m20k_count))
	print("Weight M20Ks: " + str(total_weight_m20ks))


if __name__ == "__main__":
	main(sys.argv)