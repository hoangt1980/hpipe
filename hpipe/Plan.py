import math
import numpy as np
#from hpipe.LayerComponents import BasicConv
from hpipe.GraphBuilder import Graph
from digitallogic.utils import clog2
from multiprocessing import Process, Value
import hpipe.ArchLoader as arch
import os
import tensorflow as tf


#Turning on multithreading (actually multiprocessing in this case) may either increase or decrease the speed of calculating the first cost estimate depending on the design
ENABLE_MULTITHREADING = 1
# Higher CPU core counts degrade performance due to process creation overhead. Setting this to 1 will still introduce some processes creation overhead; it's better to just use the flag above
CORE_LIMIT = 4

class ProgressStatusBar():
	def __init__(self, target, start_value):
		self.terminal_columns, _ = os.get_terminal_size(0)
		self.start_value = start_value
		self.previous_progress = 0
		self.target = target
		self.target_minus_start = target - start_value
		self.increment_size = target / self.terminal_columns

	def update_progress(self, progress):
		if (progress - self.previous_progress) < self.increment_size:
			return
		self.previous_progress = progress
		p = (progress - self.start_value) / (self.target_minus_start)
		previous_progress = 0

		num_p = int((self.terminal_columns - 8) * p)
		pct = int(p * 100)
		pct_str = "% 3d%%" % pct
		s = self.terminal_columns - num_p - 8
		print("[" + ("=" * num_p) + ">" + (" " * s) + "] " + pct_str, end="\r")

class Plan():
	def __init__(self, graph, mem_bound, dsp_bound, oc_unroll_threshold=10000, print_resource_estimates=False, **kwargs):
		print("  Devising Plan")
		self.graph = graph
		print("    Initializing throughput and resource estimates for each operation")
		for n in graph.nodes:
			n.planner_estimate = PlannerEstimate(n)

		# compute lines of buffer for each node input
		def get_min_buffer_for_node(n):
			if n.type in ["Conv2D", "DepthwiseConv2dNative", "MaxPool"]:
				return n.get_strides()[1] + n.get_kernel_shape()[0]
			if n.type in ["Add", "Mean", "AddV2"]:
				return 1
			return 0
		def set_buffering_for_node(n):
			input_cumulative_buffering = [i._from.planner_estimate.cumulative_buffered_lines for i in n.inputs]
			try:
				max_input_buffering = max(input_cumulative_buffering)
				min_input_buffering = get_min_buffer_for_node(n)
				for i in input_cumulative_buffering:
					n.planner_estimate.buffer_for_input.append(max(max_input_buffering-i+min_input_buffering, min_input_buffering))
				to_add = min(n.planner_estimate.buffer_for_input)
				n.planner_estimate.cumulative_buffered_lines = max_input_buffering + to_add
			except ValueError as e:
				n.planner_estimate.cumulative_buffered_lines = 0

		print("    Computing buffering for skip paths in the graph")
		graph.walk_graph(f_node=set_buffering_for_node)

		# for some reason our first cost estimation works the best even though it's not as balanced
		print("    Tuning operation throughput")
		self.tune_throughput(graph, dsp_bound, oc_unroll_threshold)

	def initialize_planner_parameters(self, graph):
		def init_channel_splits(n):
			if n.type == "Conv2D":
				n.planner_estimate.n_channel_splits = 2
				n.planner_estimate.multiplier_count = n.example_outputs[0].shape[2]
			else:
				n.planner_estimate.n_channel_splits = 1
		for n in graph.nodes:
			init_channel_splits(n)
			n.planner_estimate.compute_node_cycle_estimates(n, -1)


	def initial_estimates(self, graph):
		total_time_cost = 0
		fixed_mul_count = 0
		total_mem_cost = 0
		time_weights = []
		for n in graph.nodes:
			total_time_cost += n.planner_estimate.time_weight
			time_weights.append(n.planner_estimate.time_weight)
			total_mem_cost += n.planner_estimate.memory_weight
			if n.planner_estimate.requires_multiplier:
				fixed_mul_count += 1

		return (total_time_cost, fixed_mul_count, total_mem_cost, time_weights)

	def tune_throughput(self, graph, dsp_bound, oc_unroll_threshold):
		self.initialize_planner_parameters(graph)
		(total_time_cost, fixed_mul_count,
			total_mem_cost, time_weights) = self.initial_estimates(graph)
		
		time_weights = np.array([n.planner_estimate.time_weight for n in graph.nodes])
		self.mul_count = np.sum([n.example_outputs[0].shape[2] for n in graph.nodes if n.type == "Conv2D" or n.type == "DepthwiseConv2dNative"])
		status_bar = ProgressStatusBar(dsp_bound, self.mul_count)
		
		done = False
		while not done:		
			# Find the two nodes with the highest time cost
			top_two_indices = np.argpartition(time_weights, -2)[-2:]

			# find which of these two has the highest time cost
			sorted_top_two_indices = top_two_indices[np.argsort(time_weights[top_two_indices])]

			# Get the node with the highest time cost
			top_time_cost = time_weights[sorted_top_two_indices[-1]]
			top_node = graph.nodes[sorted_top_two_indices[-1]]
			
			# Get the node with the second highest time cost
			second_top_time_cost = time_weights[sorted_top_two_indices[0]]
						
			# Our depthwise convolution implementation has no
			# nobs available to turn at the moment.  If one of
			# these ends up being the slowest node we need to exit
			depthwise_conv = False
			if top_node.type == "DepthwiseConv2dNative":
				depthwise_conv = True
				
			(done, top_time_cost) = self.optimize_node(top_node, top_time_cost, second_top_time_cost, dsp_bound, oc_unroll_threshold, status_bar, depthwise_conv)
			time_weights[sorted_top_two_indices[-1]] = top_time_cost
			

		for n in graph.nodes:
			n.planner_estimate.compute_node_cycle_estimates(n)
		print("  Target DSP Count was: " + str(dsp_bound))
		print("  Achieved DSP Count is: " + str(self.mul_count))
		self.mul_count = 0
		self.ia_m20k_count = 0
		for i,n in enumerate(graph.nodes):
			self.mul_count += n.planner_estimate.multiplier_count
			self.ia_m20k_count += n.planner_estimate.ia_m20k_count

	def optimize_node(self, node, top_time_cost, second_top_time_cost, dsp_bound, oc_unroll_threshold, status_bar, depthwise_conv=False):
		input_channels = node.get_kernel_shape()[-2]
		output_channels = node.get_kernel_shape()[-1]
		output_width = node.example_outputs[0].shape[2]
		
		#For depthwise convolutions each filter covers 1 input channel. The output channels match up with the number of input channels
		if depthwise_conv:
			output_channels = input_channels
			input_channels = 1
			
		while top_time_cost >= second_top_time_cost:
			pi_next, po_next = node.planner_estimate.next_parallelism_settings(oc_unroll_threshold, input_channels, output_channels)

			#Marius: Temporary fix for OCP in Mobilenet V2
			#There is a FPGA hang on the first layer thar I am unable to fix right now
			if po_next > 1 and depthwise_conv == False and input_channels == 3:
				po_next = 1
				pi_next = 3
				print("WARNING: Limiting output channel parallelism for first layer to 1 to prevent FPGA hang. See Gitlab issue for more details")
			
			node_dsps_next = math.ceil(pi_next / 2) * po_next * output_width
			node_dsps = node.planner_estimate.multiplier_count

			next_mul_count = self.mul_count + node_dsps_next - node_dsps
			if next_mul_count > dsp_bound:
				return (True, top_time_cost)

			self.mul_count = next_mul_count
			status_bar.update_progress(self.mul_count)
			node.planner_estimate.n_channel_splits = pi_next
			node.planner_estimate.n_output_channel_groups = po_next
			node.planner_estimate.multiplier_count = node_dsps_next
			node.planner_estimate.compute_node_cycle_estimates(node, -1)
			top_time_cost = node.planner_estimate.time_weight
			if node.planner_estimate.n_channel_splits == input_channels and node.planner_estimate.n_output_channel_groups == output_channels:
				return (True, top_time_cost)
			
		return (False, top_time_cost)

	def _print_resource_estimates(self, time_weights, graph, dsp_bound, oc_unroll_threshold):
		total_ram_consumption = None
		for n in graph.nodes:
			if n.type not in ["DepthwiseConv2dNative", "Conv2D"]:
				continue
			if total_ram_consumption is None:
				total_ram_consumption = [a for a in n.planner_estimate.weight_ram_counts]
			else:
				for i in range(len(total_ram_consumption)):
					total_ram_consumption[i] += n.planner_estimate.weight_ram_counts[i]

		original_ram_consumption = [0 for _ in total_ram_consumption]
		done = False
		while True:
			top_two_indices = np.argpartition(time_weights, -2)[-2:]
			sorted_top_two_indices = top_two_indices[np.argsort(time_weights[top_two_indices])]
			top_time_cost = time_weights[sorted_top_two_indices[-1]]
			top_node = graph.nodes[sorted_top_two_indices[-1]]
			second_top = time_weights[sorted_top_two_indices[0]]
			if top_node.type == "DepthwiseConv2dNative":
				done = True
				break
			added_multipliers = 0
			top_kernel_shape = top_node.get_kernel_shape()
			original_mul_count = top_node.planner_estimate.multiplier_count
			for i in range(len(original_ram_consumption)):
				original_ram_consumption[i] = top_node.planner_estimate.weight_ram_counts[i]
			channel_splits_increment = 1
			while top_time_cost >= second_top:
				channel_splits_increment = top_node.planner_estimate.channel_splits_increment()
				multipliers_to_add = top_node.example_outputs[0].shape[2] * top_node.planner_estimate.n_output_channel_groups * channel_splits_increment
				added_multipliers = (top_node.planner_estimate.multiplier_count + multipliers_to_add) - original_mul_count
				if (added_multipliers + self.mul_count) > dsp_bound:
					added_multipliers -= top_node.example_outputs[0].shape[2]
					done = True
					break
				top_node.planner_estimate.n_channel_splits += min(top_kernel_shape[-2]-top_node.planner_estimate.n_channel_splits, 2)
				if top_node.planner_estimate.n_channel_splits > oc_unroll_threshold or top_node.planner_estimate.n_channel_splits == top_kernel_shape[-2]:
					previous_n_channel_splits = top_node.planner_estimate.n_channel_splits
					top_node.planner_estimate.n_channel_splits = math.ceil(top_node.planner_estimate.n_channel_splits * top_node.planner_estimate.n_output_channel_groups / (top_node.planner_estimate.n_output_channel_groups + 1))

					top_node.planner_estimate.n_output_channel_groups += 1
				top_node.planner_estimate.multiplier_count = math.ceil(top_node.planner_estimate.n_channel_splits // 2) * top_node.planner_estimate.n_output_channel_groups * top_node.example_outputs[0].shape[2]
				top_node.planner_estimate.compute_node_cycle_estimates(top_node, -1)
				for i,v in enumerate(top_node.planner_estimate.weight_ram_counts):
					total_ram_consumption[i] += v - original_ram_consumption[i]
					original_ram_consumption[i] = v
				print(added_multipliers, *total_ram_consumption)
				top_time_cost = top_node.planner_estimate.time_weight
				if top_node.planner_estimate.n_channel_splits == top_kernel_shape[-2]:
					break
			time_weights[sorted_top_two_indices[-1]] = top_time_cost
			self.mul_count += added_multipliers
			if done:
				print(top_time_cost)
				break

	def dump_parallelism_sweep(self):
		in_order_groups = [1, 2, 4, -1]
		data = []
		conv_nodes = [n for n in self.graph.nodes if n.type in ["DepthwiseConv2dNative", "Conv2D"]]

		num_nodes = len(conv_nodes)
		with open("parallelism_sweep_resnet.csv", "w") as fh:
			for i,n in enumerate(conv_nodes):
				print(f"{i}/{num_nodes}", end="\r")
				kernel_shape = n.get_kernel_shape()
				input_channels = kernel_shape[2]
				output_channels = kernel_shape[3]
				max_p = input_channels * output_channels
				in_order_groups[-1] = input_channels
				baseline = n.planner_estimate.time_weight
				for iogs in in_order_groups:
					for ocp in range(1,output_channels//2):
						for icp in range(min(2*ocp, input_channels),input_channels+1):
							n.planner_estimate.n_channel_splits = icp
							n.planner_estimate.n_output_channel_groups = ocp
							n.planner_estimate.compute_node_cycle_estimates(n, iogs)
							fh.write(",".join([str(asd) for asd in [max_p, ocp, icp, iogs, baseline / n.planner_estimate.time_weight]]) + "\n")


		exit(0)


		"""
		# code to find subgraphs with skip connections
		# something like that...
		# I don't remember why I wrote it, but it might be important at
		# some point.
		current_node = None
		current_graph = None
		graph_list = []
		outstanding_edges = []
		def process_node(n):
			nonlocal current_node
			nonlocal current_graph
			nonlocal graph_list
			nonlocal outstanding_edges
			if current_node is not None:
				for _input in current_node.inputs:
					del outstanding_edges[outstanding_edges.index(_input)]
				if len(outstanding_edges) > 0:
					if current_graph is None:
						current_graph = Graph()
						current_graph.heads.append(current_node)
						graph_list.append(current_graph)
					current_graph.nodes.append(current_node)
				else:
					current_graph = None
					graph_list.append(current_node)
			current_node = n

		def process_edge(e):
			nonlocal outstanding_edges
			outstanding_edges.append(e)

		graph.walk_graph(f_node=process_node, f_edge=process_edge)
		"""

#Multithreading to speed up initial estimate
#For some designs the initial estimate is really slow (taking up to 3 minutes) After multithreading the two most costly loops the performance improves (1m 3s using 4 cores)

#Get the CPU core count (CPU thread count/2). Any count greater than 4 will actually start degrading performance due to process creation overhead
cpuCount = int(os.cpu_count() / 2)
if cpuCount > CORE_LIMIT: cpuCount = CORE_LIMIT

#The main cycle estimate loops are moved to this function called by the new process. No locking is needed as all sums are local
def cycle_estimate_loop(threadID, n_channel_splits, kernel_shape, node, ret_value):
		#print("Starting", threadID)
		weights_per_oc_local = []

		for i in range(threadID, kernel_shape, cpuCount):
			num_weights = []
			for j in range(n_channel_splits):
				num_weights.append(np.nonzero(node.values[0][:,:,j::n_channel_splits,i])[0].size)
			weights_per_oc_local.append(max(1, *num_weights))
		

		weights_per_oc = np.sum(weights_per_oc_local)
		ret_value.value = int(weights_per_oc)
					
		#print("Exiting", threadID, "Ret value=", ret_value.value)
class FastOpt(tf.keras.Model):
	def __init__(self, graph):
		super(FastOpt, self).__init__()
		self.node_params = []
		self.buffer_loss = -mem_bound
		self.dsp_loss = -dsp_bound
		self.conv_nodes = [n for n in graph.nodes if n.type in ["DepthwiseConv2dNative", "Conv2D"]]
		parameter_count = self.optimization_parameter_count()
		self.input_tensor = np.zeros([len(self.conv_nodes), parameter_count])
		self.num_ram_types = len(list(arch.Architecture.current_arch.arch_spec["RAMs"].keys()))
		self.biases = np.zeros([len(self.conv_nodes), num_ram_types])
		self.tf_input = tf.Variable(initial_value=self.input_tensor, trainable=False)
		self.tf_biases = tf.Variable(initial_value=self.biases, trainable=False)
		self.update_constants()
		initial_parallelism = np.array([[n.n_channel_splits] for n in self.conv_nodes])
		self.parallelism = tf.Variable(initial_value=initial_parallelism, trainable=True)

	def call(self, _):
		x = self.tf_input * self.parallelism
		x[:,0] = 1/x[:,0]
		x[:,0] *= x[:,0]
		x[:,1:1+self.num_ram_types] += self.tf_biases
		


	def optimization_parameter_count(self):
		num_ram_types = len(list(arch.Architecture.current_arch.arch_spec["RAMs"].keys()))

		row_cycles_params = 1
		ram_params = num_ram_types * 2
		return row_cycles_params + ram_params

	def update_constants(self):
		num_ram_types = len(list(arch.Architecture.current_arch.arch_spec["RAMs"].keys()))
		for i,n in enumerate(self.conv_nodes):
			x = lambda: self.input_tensor[i]
			x[0] = 1 / node.planner_estimate.row_cycles
			start_index = 1
			for j,rt in enumerate(arch.Architecture.current_arch.arch_spec["RAMs"].keys()):
				self.biases[i][j] = node.planner_estimate.weight_ram_counts[j]
				x[start_index + j] = node.planner_estimate.weight_ram_grads[j]
				x[start_index + j + num_ram_types] = node.planner_estimate.act_ram_counts /  self.parallelism[i][0]
		self.tf_input.assign(self.input_tensor)
		self.tf_biases.assign(self.biases)

class FastOpt(tf.keras.Model):
	def __init__(self, graph):
		super(FastOpt, self).__init__()
		self.node_params = []
		self.buffer_loss = -mem_bound
		self.dsp_loss = -dsp_bound
		self.conv_nodes = [n for n in graph.nodes if n.type in ["DepthwiseConv2dNative", "Conv2D"]]
		parameter_count = self.optimization_parameter_count()
		self.input_tensor = np.zeros([len(self.conv_nodes), parameter_count])
		self.num_ram_types = len(list(arch.Architecture.current_arch.arch_spec["RAMs"].keys()))
		self.biases = np.zeros([len(self.conv_nodes), num_ram_types])
		self.tf_input = tf.Variable(initial_value=self.input_tensor, trainable=False)
		self.tf_biases = tf.Variable(initial_value=self.biases, trainable=False)
		self.update_constants()
		initial_parallelism = np.array([[n.n_channel_splits] for n in self.conv_nodes])
		self.parallelism = tf.Variable(initial_value=initial_parallelism, trainable=True)

	def call(self, _):
		x = self.tf_input * self.parallelism
		x[:,0] = 1/x[:,0]
		x[:,0] *= x[:,0]
		x[:,1:1+self.num_ram_types] += self.tf_biases
		


	def optimization_parameter_count(self):
		num_ram_types = len(list(arch.Architecture.current_arch.arch_spec["RAMs"].keys()))

		row_cycles_params = 1
		ram_params = num_ram_types * 2
		return row_cycles_params + ram_params

	def update_constants(self):
		num_ram_types = len(list(arch.Architecture.current_arch.arch_spec["RAMs"].keys()))
		for i,n in enumerate(self.conv_nodes):
			x = lambda: self.input_tensor[i]
			x[0] = 1 / node.planner_estimate.row_cycles
			start_index = 1
			for j,rt in enumerate(arch.Architecture.current_arch.arch_spec["RAMs"].keys()):
				self.biases[i][j] = node.planner_estimate.weight_ram_counts[j]
				x[start_index + j] = node.planner_estimate.weight_ram_grads[j]
				x[start_index + j + num_ram_types] = node.planner_estimate.act_ram_counts /  self.parallelism[i][0]
		self.tf_input.assign(self.input_tensor)
		self.tf_biases.assign(self.biases)

class PlannerEstimate():
	def __init__(self, node):
		self.time_weight = 0
		self.memory_weight = 0
		self.ia_m20k_count = 0
		self.requires_multiplier = False
		self.multiplier_count = 0
		self.cumulative_buffered_lines = 0
		self.buffer_for_input = []

		self.row_cycles = 0
		self.initialization_cycles = 0
		self.sustained_cycles_between_rows = 0
		self.change_in_channel_splits = None
		self.sparsity = 0
		self.average_rl_bits = 0
		self.compute_node_weights(node)
		self.muls_per_dsp = None
		if node.type in ["Conv2D", "DepthwiseConv2dNative"]:
			max_bit_width = max(node.get_bits(), node.get_bits(is_act=True))
			right_spec = [10000, 0]
			for spec in arch.Architecture.current_arch.arch_spec["DSPs"]["sizes"]:
				if max_bit_width < spec[0] and spec[0] < right_spec[0]:
					right_spec = spec
			self.muls_per_dsp = right_spec[1]
			assert(self.muls_per_dsp > 0)
		self.is_low_mem_node = False
		self.n_channel_splits = 2
		self.n_output_channel_groups = 1
		self.weight_ram_counts = None
		self.weight_ram_grads = None
		self.act_ram_counts = None
		self.weight_ram_width = 0
		self.weight_ram_depth = 0

	def compute_node_weights(self, node):
		if node.type == "swish_f32": node.type = "Swish"
		
		if node.type in ["BiasAdd", "Relu", "Relu6", "Swish", "Placeholder", "Pad", "Reshape", "Identity","Sigmoid","Tanh"]:
			return
		if node.type not in ["Add","AddV2", "MaxPool"]:
			self.requires_multiplier = True

		supported_nodes = ["Conv2D", "DepthwiseConv2dNative", "MaxPool", "Mean", "Add","AddV2"]
		if node.type not in supported_nodes:
			print("Node type " + str(node.type) + " not yet supported")
			assert(node.type in supported_nodes)

		supported_filter_types = ["Conv2D", "DepthwiseConv2dNative", "MaxPool"]
		if node.type == "Mean":
			kernel_shape = node.inputs[0]._from.example_outputs[0].shape
			kernel_shape = kernel_shape[1:] + (kernel_shape[-1],)
			strides = [1,1,1,1]
		elif node.type in supported_filter_types:
			kernel_shape = node.get_kernel_shape()
			strides = node.get_strides()
		else:
			kernel_shape = [1,1,node.inputs[0]._from.example_outputs[0].shape[-1],1]
			strides = [1,1,1,1]

		input_shape = node.inputs[0]._from.example_outputs[0].shape

		if kernel_shape[0] == 1 and kernel_shape[1] == 1:
			self.memory_weight = np.prod(kernel_shape[0:3]) * 2
		elif kernel_shape[0:2] == input_shape[1:3]:
			self.memory_weight = np.prod(input_shape)
		else:
			self.memory_weight = input_shape[2] * (kernel_shape[0] + 1) * input_shape[3]

		self.ia_m20k_count = math.ceil(self.memory_weight / 1024.)
		if node.type in ["Mean", "Add","AddV2", "MaxPool"]:
			# No need for multipliers
			return

		if self.row_cycles == 0:
			factor = 1
		elif self.row_cycles >= self.sustained_cycles_between_rows:
			factor = 1
		else:
			factor = float(self.row_cycles) / self.sustained_cycles_between_rows
		output_shape = node.example_outputs[0].shape
		multiplications = np.nonzero(node.values[0])[0].size
		self.time_weight = np.prod(output_shape[1:3]) * multiplications * factor
		self.sparsity = np.count_nonzero(node.values[0]) / np.prod(node.values[0].shape)
		self.average_rl_bits = clog2(1/self.sparsity)
		nonzero_ics = node.values[0].shape[2] * (1 - self.sparsity)
		self.change_in_channel_splits = np.ceil((np.arange(1, math.ceil(nonzero_ics)) ** 2) / nonzero_ics)
	# iogs: in order group size
	def compute_node_cycle_estimates(self, node, iogs=-1):

		self.row_cycles = 0
		self.initialization_cycles = 0
		self.sustained_cycles_between_rows = 0

		for i in node.inputs:
			p = i._from.planner_estimate
			self.initialization_cycles = max(p.initialization_cycles + p.row_cycles, self.initialization_cycles)
			self.sustained_cycles_between_rows = max(max(p.sustained_cycles_between_rows, p.row_cycles), self.sustained_cycles_between_rows)

		if not node._is_supported_filter():
			return

		if iogs == -1:
			iogs = self.n_channel_splits

		stride = node.get_strides()[0]
		output_shape = node.example_outputs[0].shape
		parallel_oc_count = self.multiplier_count // output_shape[2]
		kernel_shape = node.get_kernel_shape()
		if parallel_oc_count == 0:
			parallel_oc_count = 1
		if node.type in ["Conv2D", "DepthwiseConv2dNative"]:
			self.row_cycles = math.ceil((np.nonzero(node.values[0])[0].size / float(parallel_oc_count)) / 2.)
			self.multiplier_count = parallel_oc_count * output_shape[2]
			#self.time_weight = self.in_order_conv_time_estimate(kernel_shape, node, output_shape)
			max_nonzero_counts = self.max_nonzero_counts(iogs, kernel_shape, node)

			# Estimate total layer latency
			self.time_weight = self.out_of_order_conv_time_estimate(max_nonzero_counts, output_shape)
			(self.weight_ram_counts,self.weight_ram_width, self.weight_ram_depth) = self.weight_storage_ram_counts(node, max_nonzero_counts)

			if math.ceil(np.prod(kernel_shape[0:-1]) / 512.) > 1 and (self.multiplier_count * 2) < kernel_shape[-1]:
				self.ia_m20k_count = math.ceil((2**clog2(kernel_shape[0] + stride) * (output_shape[2]) * kernel_shape[-1]) / 1024.)
			else:
				self.ia_m20k_count = self.multiplier_count * math.ceil(np.prod(kernel_shape[0:-1]) / 512.)
		else:
			# max pool
			self.row_cycles = kernel_shape[1]
		rows_needed = kernel_shape[0]
		self.initialization_cycles += self.sustained_cycles_between_rows * (rows_needed - 1)
		overlap_rows = kernel_shape[0] - stride
		self.sustained_cycles_between_rows *= (kernel_shape[0] - overlap_rows)

	def in_order_conv_time_estimate(self, kernel_shape, node, output_shape):
		weights_per_oc = []
		for i in range(kernel_shape[-1]):
			num_weights = []
			for j in range(self.n_channel_splits):
				num_weights.append(np.nonzero(node.values[0][:,:,j::self.n_channel_splits,i])[0].size)
			weights_per_oc.append(max(1, *num_weights))
		return np.sum(weights_per_oc) * output_shape[1] #np.prod(output_shape[1:3])

	def max_nonzero_counts(self, in_order_group_size, kernel_shape, node):
		weights_per_oc = []

		# KEY:
		# CS: n_channel_splits
		# IOGS: In order group size
		# OC: Output Channels
		# IC: Input Channels
		# G: math.ceil(IC / CS)
		# IOW: In order width (width dimension if sorted_x_accumulation)

		# pad input channels to multiple of input channel groups, [H,W,IC,OC]
		input_channel_padding = (self.n_channel_splits - (kernel_shape[2] % self.n_channel_splits)) % self.n_channel_splits
		padded_filter = np.pad(node.values[0], [[0,0],[0,0],[0,input_channel_padding],[0,0]], mode="constant", constant_values=0)

		# Reshape into input channel grouped weights, [H,W,G,CS,OC], G: Group size, CS: n_channel_splits
		padded_input_channel_size = padded_filter.shape[2]
		group_size = padded_input_channel_size // self.n_channel_splits
		grouped_weights = np.reshape(padded_filter, [kernel_shape[0], kernel_shape[1], group_size, self.n_channel_splits, kernel_shape[-1]])

		# If this node will be implemented with sorted x accumulation the X dimensions of the kernel cannot
		# overlap their computation (like output channels)
		if self.sorted_x_accumulation(node):
			grouped_weights = np.transpose(grouped_weights, [0, 2, 3, 1, 4])
			grouped_weights = np.reshape(grouped_weights,
				[kernel_shape[0], 1, group_size, self.n_channel_splits, kernel_shape[-1]  * kernel_shape[1]])
			oc_iow = kernel_shape[-1] * kernel_shape[1]
		else:
			oc_iow = kernel_shape[-1]

		# get number of nonzero weights in each group in each output channel, [CS,OC*IOW] CS: n_channel_splits
		nonzero_count = np.count_nonzero(grouped_weights, (0,1,2))

		# pad input channel group dim to multiple of in order group size
		in_order_group_padding = (in_order_group_size - (self.n_channel_splits % in_order_group_size)) % in_order_group_size
		padded_nonzero_counts = np.pad(nonzero_count, [[0,in_order_group_padding],[0,0]], mode="constant", constant_values=0)
		# [CS/IOGS, IOGS, OC*IOW] IOGS: In order group size
		grouped_nonzero_counts = np.reshape(padded_nonzero_counts, [math.ceil(padded_nonzero_counts.shape[0]/in_order_group_size), in_order_group_size, oc_iow])
		# [CS/IOGS, OC*IOW]
		max_nonzero_counts = grouped_nonzero_counts.max(1)

		return max_nonzero_counts

	def next_parallelism_settings(self, oc_unroll_threshold, input_channels, output_channels, debug=False):
		ic_splits = self.n_channel_splits + 2
		oc_splits = self.n_output_channel_groups
		if (ic_splits > oc_unroll_threshold) or ic_splits > input_channels:
			ic_splits = math.ceil(self.n_channel_splits * oc_splits / (oc_splits + 1))
			oc_splits += 1
		ic_splits = min(ic_splits, input_channels)
		oc_splits = min(oc_splits, output_channels)
		
			
		return (ic_splits, oc_splits)


	def out_of_order_conv_time_estimate(self, max_nonzero_counts, output_shape):
		cycle_times = np.zeros(max_nonzero_counts.shape[0])

		for i in range(max_nonzero_counts.shape[-1]):
			min_cycle_times = cycle_times + max_nonzero_counts[:,i]
			cycle_times = np.maximum(cycle_times.max(), min_cycle_times)

		return cycle_times.max() * output_shape[1] / self.n_output_channel_groups #np.prod(output_shape[1:3])

	def weight_storage_ram_counts(self, node, max_nonzero_counts):
		"""stagger_info = BasicConv.get_staggered_weight_info(
			parallel_weights=self.n_channel_splits,
			stagger_every_n_weights=self.muls_per_dsp,
			break_chain_every_n_weights=27,
			delay_for_chain_break=9)
		(weights_per_oc,
			weights_per_oc_bits,
			serial_weights,
			serial_weights_bits) = BasicConv.get_serialized_weights_and_rls(
				weights=node.values[0],
				parallel_weights=self.n_channel_splits, stagger_every_n_weights=self.muls_per_dsp,
				is_depthwise=node.type == "DepthwiseConv2dNative", staggered_weight_info=stagger_info,
				group_x_dim=False, # should actually compute this
				delay_for_chain_break=9)"""
		mnzc_est = arch.Architecture.estimate_ram_counts((node.get_bits() + self.average_rl_bits) * self.n_channel_splits, np.sum(max_nonzero_counts, axis=1).max())
		#bc_est = arch.Architecture.estimate_ram_counts(np.sum(serial_weights_bits), len(serial_weights)) + mnzc_est
		return (mnzc_est, 0, 0)#np.sum(serial_weights_bits), len(serial_weights))

	def act_buffer_ram_counts(self, node):
		i_w = node.inputs[0]._from.example_outputs[0].shape[1]
		k_h = node.get_kernel_shape()[0]
		s_h = node.get_strides[0]
		input_channels = node.get_kernel_shape()[2]
		max_channels_per_group = math.ceil(input_channels / self.n_channel_splits)
		min_channels_per_group = input_channels // self.n_channel_splits
		num_groups_with_max = input_channels - math.floor(input_channels / min_channels_per_group)
		num_groups_with_min = self.n_channel_splits - num_groups_with_max
		lines_buffered = k_h + s_h
		ab_max = arch.Architecture.estimate_ram_counts(node.get_bits(is_act=True) * i_w, lines_buffered * max_channels_per_group)
		ab_min = arch.Architecture.estimate_ram_counts(node.get_bits(is_act=True) * i_w, lines_buffered * min_channels_per_group)
		return ab_max * num_groups_with_max + ab_min * num_groups_with_min

	def channel_splits_increment(self):
		if (self.n_channel_splits+1) >= len(self.change_in_channel_splits):
			return 0
		if (self.change_in_channel_splits[self.n_channel_splits+1] + self.n_channel_splits) > len(self.change_in_channel_splits):
			return len(self.change_in_channel_splits) - self.n_channel_splits
		return self.change_in_channel_splits[self.n_channel_splits+1]
		

	def sorted_x_accumulation(self, node):
		if node.type not in ["Conv2D", "DepthwiseConv2dNative"]:
			return False
		kernel_width = node.get_kernel_shape()[1]
		n_channel_splits = self.n_channel_splits
		stride = s_w = node.get_strides()[1]
		input_bits = node.input_nodes()[0].get_bits(is_act=True)
		return (kernel_width > 1) and ((n_channel_splits * input_bits) > 36) and (kernel_width > stride)
















