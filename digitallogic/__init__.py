"""
The Digital Logic Python Package (for short, RTL++) is a Register Transfer Level (RTL)
Hardware Description library.  Its overarching goal is to provide a user-friendly API
that allows for more agile hardware development without abstracting away hardware
contructs like High Level Synthesis (HLS) tools typically do.  Unlike HLS and even
Verilog and VHDL, RTL++ only allows for structural (as opposed to procedural) 
descriptions of circuits, but is otherwise very expressive and more flexible than
the other tools.  The benefits of RTL++ are essentially that:

  1.  The user has full control over the precise implementation of the hardware.
  This allows for constructs that simply can't be described when using HLS (e.g.
  a Bitonic Sorter or a Butterfly Network).  It also allows for finer control
  over pipelining for fanout trees and long distance data movement that HLS
  tools cannot predict.

  2.  Relative to Verilog and VHDL, dynamic generation of hardware is much
  more straightforward since you have the full flexible and expressive
  python toolkit at your disposal.  For example, if you want to build a
  series of system level registers, a register specification, and a network
  by which these are accessed, you can write a single python class with a
  method to add a register and a specification of how it's used.  That
  class can then build the network, the registers, and dump the register
  specification whenever the circuit gets built.

  3.  Modifications to existing code require far fewer changes than Verilog
  and VHDL.  In Verilog, if you want to export a new control signal from
  module A to drive a signal in module B when both are instantiated in
  module C, you have to modify all three modules (B and A need to have the control
  signal added to their port lists, and C needs to get a new wire and update the
  port lists of the module instantiations). In RTL++ this simplifies to a one line
  change.
"""
from digitallogic.digitallogic import *
import digitallogic.utils
