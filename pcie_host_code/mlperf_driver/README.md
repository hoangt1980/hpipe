This is a modified version of the HPIPE PCIe driver for use in the MLperf competition. This folder also contains to of the modified MLperf scripts that interface with our modified driver. All scripts mentioned are in the scripts folder in the MLperf repo

The MLperf API expects a folder with val_map.txt and a subfolder containing all the datset images. Our driver expects a custom binary where the images are in the same order as the original dataset. 

**Preparation**
========================================================================
Here are the steps for preparing a new Imagenet dataset:

**Step 1**
-----------------------------------------------------
Use the convert_png_to_bin.py script to convert a dataset to a binary file
`python convert_png_to_bin.py --input_png <input_image_folder> --output_bin <output_name> --output_image_size <desired_square_width> --enable_crop`

Enable crop is optional but may result in better accuracy if one image dimension is much bigger than the other. Otherwise the output images will be compressed.

**Step 2**
-----------------------------------------------------
Use the fix_hw_classes.py script to map the accelerator classes to the same classes as the dataset you are using.
`python fix_hw_classes.py --hw_label_file <hw_file_path> --actual_label_file <name_mapping_file_path> --actual_labels_mapping <software_mapping_file_path>`

Run the scripts help function for more info on what each input file should look like. A couple example files are included.

**Step 3**
-----------------------------------------------------
Create the val_map.txt file required by the MLperf example script using create_val_map.py
`python create_val_map.py --output_classes <validation_ground_thruth> --image_folder <relative_path_of_image_folder>`

The image folder path should be relative to the directory where the API script will be run from.

**Step 4**
-----------------------------------------------------
Make modifications to the PCIE_FUNDAMENTAL.cpp driver if needed (i.e. turn perf_mode on).

Re-compile the python API script and the C++ files using the boost library.
`bjam`

Make sure you have the jamfile in the same directory as the .cpp file and Python script. Jamroot should be one directory above. 

**Running the Accelerator**
========================================================================
Here are the steps for running the accelerator on the FPGA:

**Step 1**
-----------------------------------------------------
Copy over all compiled files and the main.py Python script to the machine you want to run the accelerator on. Also zip up and copy the dataset, as well as the binary file generated earlier.

In our case we are using the following machine (SAVI2) with a Terasic DE10-Pro Stratix 10 FPGA (1SG280LU2F50E1VG)
`142.1.145.194:2`

**Step 2**
-----------------------------------------------------
Program the accelerator with the compiled accelerator bitsream. On SAVI2 the steps are the following:

`sudo $QUARTUS_ROOTDIR/bin/quartus_pgm -m jtag -c 1 -o "p;/path/to/sof_file.sof"`
`sudo shutdown -r now`
`sudo sh -E load_driver`

**Step 3**
-----------------------------------------------------
Run the dataset using a modified version of the MLperf API script. Below is an example command.

`python3 main.py --model resnet50 --profile resnet50-hpipe --dataset-path /home/stanmari/MLperf_imagenet/ImageNet/ --mlperf_conf /home/stanmari/MLperf_test/mlperf_inference/mlperf.conf --user_conf /home/stanmari/MLperf_test/mlperf_inference/vision/classification_and_detection/user.conf`

Make sure all the file paths are correct. If you encounter errors it could be that the boost library is not installed for the current user. In that case use the following commands:

`wget https://dl.bintray.com/boostorg/release/1.71.0/source/boost_1_71_0.tar.gz`
`tar -xzvf boost_1_71_0.tar.gz`
`cd boost_1_71_0/`
`bootstrap.sh --with-libraries=python --with-python=python3.5`
`sudo ./b2 install`

You can replace the python version as needed. At the end, add this to your "bashrc" file: `export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH`