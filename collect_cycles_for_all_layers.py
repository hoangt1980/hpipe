import os
import subprocess
d = dict(os.environ)
add_layers = [
	"import/resnet_model/max_pooling2d/MaxPool",
	"import/resnet_model/Mean",
	"import/resnet_model/Relu",
	"import/resnet_model/add",
	"import/resnet_model/add_1",
	"import/resnet_model/add_2",
	"import/resnet_model/add_3",
	"import/resnet_model/add_4",
	"import/resnet_model/add_5",
	"import/resnet_model/add_6",
	"import/resnet_model/add_7",
	"import/resnet_model/add_8",
	"import/resnet_model/add_9",
	"import/resnet_model/add_10",
	"import/resnet_model/add_11",
	"import/resnet_model/add_12",
	"import/resnet_model/add_13",
	"import/resnet_model/add_14",
	"import/resnet_model/add_15"]

"""
	"import/resnet_model/conv2d/Conv2D",
	"import/resnet_model/conv2d_1/Conv2D",
	"import/resnet_model/conv2d_2/Conv2D",
	"import/resnet_model/conv2d_3/Conv2D",
	"import/resnet_model/conv2d_4/Conv2D",
	"import/resnet_model/conv2d_5/Conv2D",
	"import/resnet_model/conv2d_6/Conv2D",
	"import/resnet_model/conv2d_7/Conv2D",
	"import/resnet_model/conv2d_8/Conv2D",
	"import/resnet_model/conv2d_9/Conv2D",
	"import/resnet_model/conv2d_10/Conv2D",
	"import/resnet_model/conv2d_12/Conv2D",]
"""
conv_layers = [
	"import/resnet_model/conv2d_11/Conv2D",
	"import/resnet_model/conv2d_13/Conv2D",
	"import/resnet_model/conv2d_14/Conv2D",
	"import/resnet_model/conv2d_15/Conv2D",
	"import/resnet_model/conv2d_16/Conv2D",
	"import/resnet_model/conv2d_17/Conv2D",
	"import/resnet_model/conv2d_18/Conv2D",
	"import/resnet_model/conv2d_19/Conv2D",
	"import/resnet_model/conv2d_20/Conv2D",
	"import/resnet_model/conv2d_21/Conv2D",
	"import/resnet_model/conv2d_22/Conv2D",
	"import/resnet_model/conv2d_23/Conv2D",
	"import/resnet_model/conv2d_25/Conv2D",
	"import/resnet_model/conv2d_24/Conv2D",
	"import/resnet_model/conv2d_26/Conv2D",
	"import/resnet_model/conv2d_27/Conv2D",
	"import/resnet_model/conv2d_28/Conv2D",
	"import/resnet_model/conv2d_29/Conv2D",
	"import/resnet_model/conv2d_30/Conv2D",
	"import/resnet_model/conv2d_31/Conv2D",
	"import/resnet_model/conv2d_32/Conv2D",
	"import/resnet_model/conv2d_33/Conv2D",
	"import/resnet_model/conv2d_34/Conv2D",
	"import/resnet_model/conv2d_35/Conv2D",
	"import/resnet_model/conv2d_36/Conv2D",
	"import/resnet_model/conv2d_37/Conv2D",
	"import/resnet_model/conv2d_38/Conv2D",
	"import/resnet_model/conv2d_39/Conv2D",
	"import/resnet_model/conv2d_40/Conv2D",
	"import/resnet_model/conv2d_41/Conv2D",
	"import/resnet_model/conv2d_42/Conv2D",
	"import/resnet_model/conv2d_44/Conv2D",
	"import/resnet_model/conv2d_43/Conv2D",
	"import/resnet_model/conv2d_45/Conv2D",
	"import/resnet_model/conv2d_46/Conv2D",
	"import/resnet_model/conv2d_47/Conv2D",
	"import/resnet_model/conv2d_48/Conv2D",
	"import/resnet_model/conv2d_49/Conv2D",
	"import/resnet_model/conv2d_50/Conv2D",
	"import/resnet_model/conv2d_51/Conv2D",
	"import/resnet_model/conv2d_52/Conv2D",
	"import/resnet_model/dense/MatMul"]

param_template = """
{
    "build_from": null,
    "build_until": null,
    "debug_individual_stage": "%s",
    "dsp_target": 5000,
    "generated_circuit_dir": "generated_files/circuits/quartus_circuit",
    "mem_target": null,
    "oc_unroll_threshold": 10000,
    "op_precision_annotations_path": "default_precisions.annotations",
    "output_names": [
        "import/resnet_model/dense/BiasAdd"
    ],
    "pb_path": "/home/mat/drive1/different_sparsity_resnet_models/304811/frozen_graph.opt.swapped_max_pool_and_batch_norm.pb",
    "quartus": false,
    "target_act_bits": 8,
    "target_parameter_bits": 16,
    "sample_image": "individualImage.png",
    "top_level_module_type": "quartus"

}
"""

generate_circuit_command = """PYTHONPATH=`pwd` CUDA_VISIBLE_DEVICES= python3 hpipe/LayerComponents.py --param_file_path "automated_params.json" """

for k,v in d.items():
	os.putenv(k, v)

for layer_name in conv_layers:
	with open("automated_params.json", "w") as fh:
		fh.write(param_template % layer_name)
	os.system(generate_circuit_command)
	os.system("make VERILATOR_DEFINES= verilator")
	os.system("make verilator_run > verilator_layer_logs/" + layer_name.split("/")[-2] + ".txt")

	

"""
import/Placeholder Placeholder
import/resnet_model/conv2d/Conv2D Conv2D
import/resnet_model/max_pooling2d/MaxPool BiasAdd
import/resnet_model/batch_normalization/FusedBatchNorm MaxPool
import/resnet_model/Relu Relu
import/resnet_model/conv2d_1/Conv2D Conv2D
import/resnet_model/conv2d_2/Conv2D Conv2D
import/resnet_model/batch_normalization_1/FusedBatchNorm BiasAdd
import/resnet_model/Relu_1 Relu
import/resnet_model/conv2d_3/Conv2D Conv2D
import/resnet_model/batch_normalization_2/FusedBatchNorm BiasAdd
import/resnet_model/Relu_2 Relu
import/resnet_model/conv2d_4/Conv2D Conv2D
import/resnet_model/add Add
import/resnet_model/batch_normalization_3/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_3_split_by_bn Relu
import/resnet_model/conv2d_5/Conv2D Conv2D
import/resnet_model/batch_normalization_4/FusedBatchNorm BiasAdd
import/resnet_model/Relu_4 Relu
import/resnet_model/conv2d_6/Conv2D Conv2D
import/resnet_model/batch_normalization_5/FusedBatchNorm BiasAdd
import/resnet_model/Relu_5 Relu
import/resnet_model/conv2d_7/Conv2D Conv2D
import/resnet_model/add_1 Add
import/resnet_model/batch_normalization_6/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_6_split_by_bn Relu
import/resnet_model/conv2d_8/Conv2D Conv2D
import/resnet_model/batch_normalization_7/FusedBatchNorm BiasAdd
import/resnet_model/Relu_7 Relu
import/resnet_model/conv2d_9/Conv2D Conv2D
import/resnet_model/batch_normalization_8/FusedBatchNorm BiasAdd
import/resnet_model/Relu_8 Relu
import/resnet_model/conv2d_10/Conv2D Conv2D
import/resnet_model/add_2 Add
import/resnet_model/batch_normalization_9/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_9_split_by_bn Relu
import/resnet_model/conv2d_12/Conv2D Conv2D
import/resnet_model/conv2d_11/Conv2D Conv2D
import/resnet_model/batch_normalization_10/FusedBatchNorm BiasAdd
import/resnet_model/Relu_10 Relu
import/resnet_model/conv2d_13/Conv2D Conv2D
import/resnet_model/batch_normalization_11/FusedBatchNorm BiasAdd
import/resnet_model/Relu_11 Relu
import/resnet_model/conv2d_14/Conv2D Conv2D
import/resnet_model/add_3 Add
import/resnet_model/batch_normalization_12/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_12_split_by_bn Relu
import/resnet_model/conv2d_15/Conv2D Conv2D
import/resnet_model/batch_normalization_13/FusedBatchNorm BiasAdd
import/resnet_model/Relu_13 Relu
import/resnet_model/conv2d_16/Conv2D Conv2D
import/resnet_model/batch_normalization_14/FusedBatchNorm BiasAdd
import/resnet_model/Relu_14 Relu
import/resnet_model/conv2d_17/Conv2D Conv2D
import/resnet_model/add_4 Add
import/resnet_model/batch_normalization_15/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_15_split_by_bn Relu
import/resnet_model/conv2d_18/Conv2D Conv2D
import/resnet_model/batch_normalization_16/FusedBatchNorm BiasAdd
import/resnet_model/Relu_16 Relu
import/resnet_model/conv2d_19/Conv2D Conv2D
import/resnet_model/batch_normalization_17/FusedBatchNorm BiasAdd
import/resnet_model/Relu_17 Relu
import/resnet_model/conv2d_20/Conv2D Conv2D
import/resnet_model/add_5 Add
import/resnet_model/batch_normalization_18/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_18_split_by_bn Relu
import/resnet_model/conv2d_21/Conv2D Conv2D
import/resnet_model/batch_normalization_19/FusedBatchNorm BiasAdd
import/resnet_model/Relu_19 Relu
import/resnet_model/conv2d_22/Conv2D Conv2D
import/resnet_model/batch_normalization_20/FusedBatchNorm BiasAdd
import/resnet_model/Relu_20 Relu
import/resnet_model/conv2d_23/Conv2D Conv2D
import/resnet_model/add_6 Add
import/resnet_model/batch_normalization_21/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_21_split_by_bn Relu
import/resnet_model/conv2d_25/Conv2D Conv2D
import/resnet_model/conv2d_24/Conv2D Conv2D
import/resnet_model/batch_normalization_22/FusedBatchNorm BiasAdd
import/resnet_model/Relu_22 Relu
import/resnet_model/conv2d_26/Conv2D Conv2D
import/resnet_model/batch_normalization_23/FusedBatchNorm BiasAdd
import/resnet_model/Relu_23 Relu
import/resnet_model/conv2d_27/Conv2D Conv2D
import/resnet_model/add_7 Add
import/resnet_model/batch_normalization_24/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_24_split_by_bn Relu
import/resnet_model/conv2d_28/Conv2D Conv2D
import/resnet_model/batch_normalization_25/FusedBatchNorm BiasAdd
import/resnet_model/Relu_25 Relu
import/resnet_model/conv2d_29/Conv2D Conv2D
import/resnet_model/batch_normalization_26/FusedBatchNorm BiasAdd
import/resnet_model/Relu_26 Relu
import/resnet_model/conv2d_30/Conv2D Conv2D
import/resnet_model/add_8 Add
import/resnet_model/batch_normalization_27/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_27_split_by_bn Relu
import/resnet_model/conv2d_31/Conv2D Conv2D
import/resnet_model/batch_normalization_28/FusedBatchNorm BiasAdd
import/resnet_model/Relu_28 Relu
import/resnet_model/conv2d_32/Conv2D Conv2D
import/resnet_model/batch_normalization_29/FusedBatchNorm BiasAdd
import/resnet_model/Relu_29 Relu
import/resnet_model/conv2d_33/Conv2D Conv2D
import/resnet_model/add_9 Add
import/resnet_model/batch_normalization_30/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_30_split_by_bn Relu
import/resnet_model/conv2d_34/Conv2D Conv2D
import/resnet_model/batch_normalization_31/FusedBatchNorm BiasAdd
import/resnet_model/Relu_31 Relu
import/resnet_model/conv2d_35/Conv2D Conv2D
import/resnet_model/batch_normalization_32/FusedBatchNorm BiasAdd
import/resnet_model/Relu_32 Relu
import/resnet_model/conv2d_36/Conv2D Conv2D
import/resnet_model/add_10 Add
import/resnet_model/batch_normalization_33/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_33_split_by_bn Relu
import/resnet_model/conv2d_37/Conv2D Conv2D
import/resnet_model/batch_normalization_34/FusedBatchNorm BiasAdd
import/resnet_model/Relu_34 Relu
import/resnet_model/conv2d_38/Conv2D Conv2D
import/resnet_model/batch_normalization_35/FusedBatchNorm BiasAdd
import/resnet_model/Relu_35 Relu
import/resnet_model/conv2d_39/Conv2D Conv2D
import/resnet_model/add_11 Add
import/resnet_model/batch_normalization_36/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_36_split_by_bn Relu
import/resnet_model/conv2d_40/Conv2D Conv2D
import/resnet_model/batch_normalization_37/FusedBatchNorm BiasAdd
import/resnet_model/Relu_37 Relu
import/resnet_model/conv2d_41/Conv2D Conv2D
import/resnet_model/batch_normalization_38/FusedBatchNorm BiasAdd
import/resnet_model/Relu_38 Relu
import/resnet_model/conv2d_42/Conv2D Conv2D
import/resnet_model/add_12 Add
import/resnet_model/batch_normalization_39/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_39_split_by_bn Relu
import/resnet_model/conv2d_44/Conv2D Conv2D
import/resnet_model/conv2d_43/Conv2D Conv2D
import/resnet_model/batch_normalization_40/FusedBatchNorm BiasAdd
import/resnet_model/Relu_40 Relu
import/resnet_model/conv2d_45/Conv2D Conv2D
import/resnet_model/batch_normalization_41/FusedBatchNorm BiasAdd
import/resnet_model/Relu_41 Relu
import/resnet_model/conv2d_46/Conv2D Conv2D
import/resnet_model/add_13 Add
import/resnet_model/batch_normalization_42/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_42_split_by_bn Relu
import/resnet_model/conv2d_47/Conv2D Conv2D
import/resnet_model/batch_normalization_43/FusedBatchNorm BiasAdd
import/resnet_model/Relu_43 Relu
import/resnet_model/conv2d_48/Conv2D Conv2D
import/resnet_model/batch_normalization_44/FusedBatchNorm BiasAdd
import/resnet_model/Relu_44 Relu
import/resnet_model/conv2d_49/Conv2D Conv2D
import/resnet_model/add_14 Add
import/resnet_model/batch_normalization_45/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_45_split_by_bn Relu
import/resnet_model/conv2d_50/Conv2D Conv2D
import/resnet_model/batch_normalization_46/FusedBatchNorm BiasAdd
import/resnet_model/Relu_46 Relu
import/resnet_model/conv2d_51/Conv2D Conv2D
import/resnet_model/batch_normalization_47/FusedBatchNorm BiasAdd
import/resnet_model/Relu_47 Relu
import/resnet_model/conv2d_52/Conv2D Conv2D
import/resnet_model/add_15 Add
import/resnet_model/batch_normalization_48/FusedBatchNorm_bias BiasAdd
import/resnet_model/Relu_48_split_by_bn Relu
import/resnet_model/Mean Mean
import/resnet_model/Reshape Reshape
import/resnet_model/dense/MatMul Conv2D
import/resnet_model/dense/BiasAdd BiasAdd
import/resnet_model/final_dense Identity
"""


































































