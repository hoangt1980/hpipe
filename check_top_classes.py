import numpy as np

def print_classes(f):
	with open(f, "r") as fh:
		lines = [l for l in fh]
	del lines[0]
	lines = lines[1001*0:1*1001]
	lines = [int(l[:-1]) for l in lines]
	print(np.argsort(np.array(lines))[-10:])

print("Verilog")
print("  ", end="")
print_classes("verilog_images/bias_add_49.csv")
print("TensorFlow")
print("  ", end="")
print_classes("tensorflow_images/bias_add_49.csv")
