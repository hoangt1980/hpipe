import os
models = [130435,304811,70375,72760,75163,77566,80385,82173,85187,87601,90015,92187,94602]

param_template = """
{
    "build_from": null,
    "build_until": null,
    "debug_individual_stage": null,
    "dsp_target": 5000,
    "generated_circuit_dir": "generated_circuit",
    "mem_target": null,
    "output_names": [
        "import/resnet_model/final_dense"
    ],
    "pb_path": "/home/mat/drive1/different_sparsity_resnet_models/%d/frozen_graph.opt.swapped_max_pool_and_batch_norm.pb",
    "quartus": false,
    "sample_image": "individualImage.png"
}
"""

generate_circuit_command = """CUDA_VISIBLE_DEVICES= python3 gen_pure_verilog/LayerComponents.py --param_file_path "automated_params.json" """

for model in models:
	with open("automated_params.json", "w") as fh:
		fh.write(param_template % model)
	os.system(generate_circuit_command)
	os.system("make VERILATOR_DEFINES= verilator")
	os.system("make verilator_run > verilator_sparsity_logs/" + str(model) + ".txt")
