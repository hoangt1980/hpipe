from pprint import pprint
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import math

dla_scale_factor = (11520 / 3376.0) * 1.5
accelerators = {
	"HPIPE (Ours)" : {
		"latency" : [1.08],
		"throughput" : [4551],
		"batch" : ["HPIPE (Ours), B=1"],
		"offset" : [(1,-250)],
		"arrowprops" : {"width" : 0.1, "headwidth": 4, "headlength": 4},
		"va" : "center",
		"color" : "#FF1155"
	},

	"NVIDIA V100" : {
		"latency" : [0.87, 1.3, 2.4],#, 17],
		"throughput" : [1156, 1580, 3315],#, 7595],
		"batch" : ["V100, B=1","B=2","B=8"],#,"B=128"],
		"offset" : [(0.25,-75), (0.25,-75), (0.25,-100)],#, (-2,0)]
		"color" : "#00AA00"
		},

#	"Habana HL-100" : {
#		"latency" : [0.2, 0.5, 0.9, 1.0],
#		"throughput" : [7466, 13221, 14546, 15453],
#		"batch" : [1,4,8,10]
#	},

	"DLA-like A10" : {
		"latency" : [8.4],
		"throughput" : [119],
		"batch" : ["DLA-like A10, B=1"],
		"offset" : [(0.15, 100)],
		"ha" : "right",
		"color" : "#11AAFF"
	},

	"DLA-like S10 (Scaled)" : {
		"latency" : [8.4 / dla_scale_factor],
		"throughput" : [119 * dla_scale_factor],
		"batch" : ["DLA-like Optimistically\nScaled to S10, B=1"],
		"offset" : [(1.7, 400)],
		"arrowprops" : {"width" : 0.1, "headwidth": 4, "headlength": 4},
		"color" : "#11AAFF"
	},

	"Brainwave A10" : {
		"latency" : [1.8],
		"throughput" : [559],
		"batch" : ["Brainwave A10, B=1"],
		"offset" : [(0.2, -500)],
		"offset_arrow_head" : [0.05, -70],
		"arrowprops" : {"width" : 0.1, "headwidth": 4, "headlength": 4},
		"ha" : "center",
		"color" : "#332200"
	},

	"Brainwave S10 (Scaled)" : {
		"latency" : [1.8 / 5],
		"throughput" : [559 * 5],
		"batch" : ["Brainwave\nOptimistically Scaled to S10\nB=1"],
		"offset" : [(0.5, 500)],
		"arrowprops" : {"width" : 0.1, "headwidth": 4, "headlength": 4},
		"ha" : "left",
		"va" : "bottom",
		"color" : "#332200"
	}

}

pprint(accelerators)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
#plt.rcParams["figure.figsize"] = (4,3)
plt.rcParams["font.size"] = 15
plt.tight_layout()
annotate_shifts = [(-1.25, 100), (-0.75,300), (0.5,-75), 
	(0.5,-75), (0.25, -200), (-0.36, 100)]
for (k,v), m, s in zip(accelerators.items(), ["H", "s", "1", "1", "X", "X"], annotate_shifts):
	plt.plot(v["latency"], v["throughput"], marker=m, label=k, color=v["color"])
	if "batch" in v.keys():
		if "offset" in v.keys():
			offsets = v["offset"]
		else:
			offsets = [s] * len(v["latency"])
		for b,l,t,o in zip(*[v[t] for t in ["batch", "latency", "throughput"]], offsets):
			if b is not None:
				arrowprops=None
				if "arrowprops" in v:
					arrowprops=v["arrowprops"]
				ha="left"
				if "ha" in v:
					ha=v["ha"]
				va="bottom"
				if "va" in v:
					va=v["va"]
				if o[0] > 0 and o[1] > 0:
					x_shift = 30 * o[0] / math.sqrt(o[0] ** 2 + o[1] ** 2)
					y_shift = 30 * o[1] / math.sqrt(o[0] ** 2 + o[1] ** 2)
				else:
					x_shift = 0
					y_shift = 0
				if "offset_arrow_head" in v:
					x_shift = v["offset_arrow_head"][0]
					y_shift = v["offset_arrow_head"][1]
				plt.annotate(str(b), xy=(l+x_shift,t+y_shift), xytext=(l+o[0], t+o[1]), arrowprops=arrowprops, ha=ha)
#plt.legend()
plt.xlabel("Latency (ms)")
plt.ylabel("Throughput (images/s), ResNet-50")
#plt.plot([1], [2], marker="1")
#plt.plot([0.5,2], [1,1.5], marker="x")
#plt.show()
plt.savefig("throughput_vs_latency.pgf", dpi=300, bbox_inches="tight")
