#!/bin/bash
find verilator_layer_logs -type f |xargs -I@ bash -c "printf '@ '; head -n3 @ | tail -n1; echo" | sed -r 's/(.*conv2d_)([0-9]+)/\2 \1\2/' | sort -n | sed -r 's/^[0-9]+ //' | sed -r 's|verilator_layer_logs/(.*).txt.*time ([0-9]+)|\1 \2|' | grep " " > individual_layer_cycle_counts.txt && cat individual_layer_cycle_counts.txt
