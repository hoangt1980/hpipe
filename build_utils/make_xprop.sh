#!/bin/bash
idir=$1
xdir=$2
cat $idir/verilog_paths.txt | xargs vrq -passthru DISPLAY_FINISH_TIMES -passthru DONT_ALLOW_X_WRITE_TO_RAM -passthru DUMP -passthru TESTING -passthru WRITE_IMAGES -sv -tool xprop -dir $xdir
awk -F/ '{print "'"$xdir"'/"$NF}' $idir/verilog_paths.txt > $xdir/verilog_paths.txt