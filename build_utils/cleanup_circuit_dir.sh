#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <path_to_circuit_dir>"
		exit 1
fi

if [ ! -f $1/verilog_paths.txt ]; then
		echo "$1/verilog_paths.txt does not exist.  Exiting..."
		exit 1
fi
cwd=`pwd`

echo "Removing files not in the current circuit"
diff <(sed -r 's|.*/CIRCUIT/|CIRCUIT/|' $1/verilog_paths.txt | sort) <(find $1/CIRCUIT -type f | sed -r 's|.*/CIRCUIT/|CIRCUIT/|' | sort) | grep -P "^> " | awk '{print "'$1'/"$2}' | xargs -r rm

echo "Removing empty directories"
find $1 -type d -empty | xargs -r rmdir --ignore-fail-on-non-empty -p
#while [ ! "$(find "$1" -type d -empty)" = "" ]; do
		
#done
