#														Individual Unit Tests for Modules in HPIPE

#Paths to important folders
import os
import sys
sys.path.insert(0, '../')
DUMPED_FILES_PATH = './dumped_files/'
#Important Libraries and HPIPE Modules 
import warnings
import math
from hpipe.GraphBuilder import Node
from hpipe.LayerComponents import ActivationFunction
from digitallogic.digitallogic import Module
import json
import argparse
from digitallogic.utils import clog2, get_padding, print_stage_header, v_comment_string, get_quantized, BitArray, cast_fixed
from hpipe.GraphBuilder import Node, Edge
from hpipe.Plan import PlannerEstimate
import hpipe.LayerComponents as lc
import numpy as np
import pprint
import numpy as np
import digitallogic as dl
import tensorflow as tf
import tensorflow.keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import DepthwiseConv2D
from tensorflow.keras import backend as K
from collections import namedtuple
from functools import reduce
import digitallogic as dl
import csv
from pathlib import Path
from random import randint

tf_version = tf.__version__.split('.')
tf_version = float(".".join(tf_version[0:2]))

if tf_version < 2.0:
	tf.enable_eager_execution()
else:
	tf.compat.v1.enable_eager_execution()


#Read parameters from the system command
#Layer config
Activation_Modules = ["Sigmoid","ReLU","Swish","Tanh","ReLU6", "MaxPool"]

#Global delay declarations 
data_delay=2	#Previous module actual data delay 
write_delay=0	#Previous module ready signal delay (Control)
writes_delay=0	#Previous module ready signal delay (Data)

def swish(x):
	return (x/(1+np.exp(-x)))	

sigmoid_precision_parameters_input = {"a_sign_bits" : 1,
		"a_int" : 3,
		"a_f" : 6,
		"a_width" : 10,
		"a_exponent" : 0}
sigmoid_precision_parameters_output = {"a_sign_bits" : 1,
		"a_int" : 1,
		"a_f" : 14,
		"a_width" : 16,
		"a_exponent" : 0}
tanh_precision_parameters_input = {"a_sign_bits" : 1,
		"a_int" : 3,
		"a_f" : 6,
		"a_width" : 10,
		"a_exponent" : 0}
tanh_precision_parameters_output = {"a_sign_bits" : 1,
		"a_int" : 1,
		"a_f" : 14,
		"a_width" : 16,
		"a_exponent" : 0}
swish_precision_parameters_input = {"a_sign_bits" : 1,
		"a_int" : 4,
		"a_f" : 5,
		"a_width" : 10,
		"a_exponent" : 0}
swish_precision_parameters_output = {"a_sign_bits" : 1,
		"a_int" : 4,
		"a_f" : 3,
		"a_width" : 8,
		"a_exponent" : 0}

#Source: https://stackoverflow.com/questions/6800193/what-is-the-most-efficient-way-of-finding-all-the-factors-of-a-number-in-python
def find_factors(n):
	step = 2 if n % 2 else 1
	return set(reduce(list.__add__, ([i, n//i] for i in range(1, int(math.sqrt(n))+1, step) if n % i == 0)))

class DemoCircuit(dl.Module):

	# you can add whatever arguments you want here if you want to insure
	# you always get a reference to the current clock or the current reset,
	# declare this as def __init__(self, clock, reset, **kwargs):
	def __init__(self,reset, args, **kwargs):
		# when subclassing dl.Module you need to always call super __init__
		# and you must pass a module_name, a module kind, and the kwargs
		super(DemoCircuit, self).__init__("demo_circuit", kind="demo_circuit", **kwargs)
		# eventually I want to get rid of this, but for now we need to call self.inst()
		# to instantiate stuff, which happens so often that it's just a good idea to
		# get another reference to this which is the much shorter inst
		inst = self.inst

		self.clock = kwargs["clock"]

		Tested_Module = args.type
		input_w = args.width
		input_h = args.height
		input_c = args.ci
		kernel_w = args.kw
		kernel_h = args.kh
		stride_x = args.sx
		stride_y = args.sy
		padding_type = args.padding
		output_channel = args.co
		int_activation_input = args.ibi
		fraction_activation_input = args.ibf
		fraction_activation_output = args.obf
		int_activation_output = args.obi
		int_weight = args.wbi
		fraction_weight = args.wbf
		self.margin = args.margin

		original_module_type = Tested_Module
		if args.oc_parallelism > 1:
			Tested_Module = "ConvWrapper"
		
		# Now we need to explicitly enter this module's scope.  When we call inst()
		# from within this scope, signals and modules will be instatiated in this
		# component
		with dl.ModuleScope(self):
			

			#self.instantiate_quartus_ip = True
			#Read inputs, outputs and precision from kwargs 
			
			for key,value in kwargs.items():
				if key == "uut_outputs":
					self.uut_outputs = value
				if key == "uut_inputs":
					self.uut_inputs = value
				if key == "input_precision_parameters":
					self.input_precision_parameters = value
				if key == "output_precision_parameters":
					self.output_precision_parameters = value
				if key == "weights":
					self.weights = value					
			
			#Extract input and output  dimensions for general usage in the circuit	
			print("Inputs")
			print(self.uut_inputs.shape)
			print(self.uut_outputs.shape)
			input_b = np.array(self.uut_inputs).shape[1] #Batch size
			input_h = np.array(self.uut_inputs).shape[2] #Height
			input_w = np.array(self.uut_inputs).shape[3] #Width
			input_c = np.array(self.uut_inputs).shape[4] #Channels

			output_b = np.array(self.uut_outputs).shape[1]
			output_h = np.array(self.uut_outputs).shape[2]
			output_w = np.array(self.uut_outputs).shape[3]
			output_c = np.array(self.uut_outputs).shape[4]

			#clock2 = inst(dl.Clock, name="clock_0")
			#Create a fake node to pass in host the HPIPE module that you want to test
			fake_tf_graph_node = namedtuple("fake_tf_graph_node", "type name")
			input_customized_precision_parameters = self.input_precision_parameters
			output_customized_precision_parameters = self.output_precision_parameters
			# Note: The example_outpus or th precision specs that you pass in for a node are output properties not input 
			
			is_activation_function = False
			cycles_per_value = 1 
			
			# Select a random serializer value for the unit test. In the actual network this will be based on the throughput of the previous convolution
			factors = list(find_factors(input_w))
			random_factor = randint(0,len(factors)-1)
			#print("FACTORS", factors, factors[random_factor])
			cycles_per_value = factors[random_factor]

			#Instantiate the module you want to test
			########################################################################################################################################
			# 						Sigmoid
			"""
			fake_tf_sigmoid_node = fake_tf_graph_node("Sigmoid", "a_test_of_a_sigmoid_op")
			test_node = Node(fake_tf_sigmoid_node, self.uut_outputs, self.output_precision_parameters)
			input_node = Node(fake_tf_graph_node("Sigmoid", "fake_input_node"), self.uut_outputs, self.input_precision_parameters)
			input_edge = Edge(input_node, test_node)
			test_node.inputs = [input_edge]
			input_node.outputs = [input_edge]
			test_node.planner_estimate = PlannerEstimate(test_node)
			self.test_node = inst(lc.Sigmoid,test_node)
			"""
			########################################################################################################################################
			
			########################################################################################################################################
			# 						Tanh
			"""
			fake_tf_tanh_node = fake_tf_graph_node("Tanh", "a_test_of_a_tanh_op")
			test_node = Node(fake_tf_tanh_node, self.uut_outputs, self.output_precision_parameters)
			input_node = Node(fake_tf_graph_node("Tanh", "fake_input_node"), self.uut_outputs, self.input_precision_parameters)
			input_edge = Edge(input_node, test_node)
			test_node.inputs = [input_edge]
			input_node.outputs = [input_edge]
			test_node.planner_estimate = PlannerEstimate(test_node)
			self.test_node = inst(lc.Tanh,test_node)
			"""
			########################################################################################################################################

			########################################################################################################################################
			# 						ReLU
			"""
			fake_tf_relu_node = fake_tf_graph_node("Relu", "a_test_of_a_sigmoid_op")
			test_node = Node(fake_tf_relu_node, self.uut_outputs, self.output_precision_parameters)
			input_node = Node(fake_tf_graph_node("Relu", "fake_input_node"), self.uut_outputs, self.input_precision_parameters)
			input_edge = Edge(input_node, test_node)
			test_node.inputs = [input_edge]
			input_node.outputs = [input_edge]
			test_node.planner_estimate = PlannerEstimate(test_node)
			self.test_node = inst(lc.ReLU,test_node)"""
			########################################################################################################################################
			if(Tested_Module == "Conv2D" or Tested_Module == "DepthwiseConv2dNative"):
				########################################################################################################################################
				#						Conv2D/Fully-Connected 
				# let's also create a fake input node to connect our test_node to.  The implementations
				# of each node will look at the input node for the number of inputs and for their precisions
				# If we wanted to test multiple precisions in the same network, we would change precision_parameters
				# passed to this node.
				# Note: you will need multiple inputs for nodes like Add that take in multiple inputs
				# Note 
				fake_tf_conv_node = fake_tf_graph_node("Conv2D", "a_test_of_a_conv_op")
				test_node = Node(fake_tf_conv_node, self.uut_outputs, self.output_precision_parameters)
				input_node = Node(fake_tf_graph_node("Conv2D_input", "fake_input_node"), self.uut_inputs, self.input_precision_parameters)
				input_edge = Edge(input_node, test_node)
				test_node.inputs = [input_edge]
				input_node.outputs = [input_edge]
				# create a numpy array to use as the parameters for a layer (e.g. the weights for a Conv2D
				# or the biases for a BiasAdd).
				# In this case we are creating some random floating point weights with a kernel height of 1, width
				# of 3, 16 input channels, and 32 output channels
				test_node.values = [self.weights.astype(int)]
				"""# Now you can go through and add any other properties you need for your node, e.g.
				                        "padding",
				                        "strides",
				                        "dilation_rate",
				                        "data_format",
				                        "ksize",
				                        "axis"""
				test_node.properties["padding"] = padding_type # can be SAME or VALID
				# in BHWC order (Batch, Height, Width, Channels)
				test_node.properties["strides"] = [1,stride_x,stride_y,1]
				test_node.planner_estimate = PlannerEstimate(test_node)
				test_node.planner_estimate.n_channel_splits = args.ic_parallelism # use this to control input channel unrolling for Conv layers
				test_node.planner_estimate.n_output_channel_groups = 1
				test_node.type = Tested_Module


				# Now instantiate a node in your test circuit (assumes you have your own Test class that subclasses module)
				self.test_node = inst(lc.BasicConv, test_node, output_csv_path=args.output_csv_path, name = "design_under_test")
				########################################################################################################################################
				print("Instantisting regular conv")
			elif(Tested_Module == "ConvWrapper"):
				print("Instantisting conv wrapper")
				fake_tf_conv_node = fake_tf_graph_node("Conv2D", "a_test_of_a_ConvWrapper")
				test_node = Node(fake_tf_conv_node, self.uut_outputs, self.output_precision_parameters)
				input_node = Node(fake_tf_graph_node("Conv2D_input", "fake_input_node"), self.uut_inputs, self.input_precision_parameters)
				input_edge = Edge(input_node, test_node)
				test_node.inputs = [input_edge]
				input_node.outputs = [input_edge]
				test_node.values = [self.weights.astype(int)]
				test_node.properties["padding"] = padding_type # can be SAME or VALID
				test_node.properties["strides"] = [1,stride_x,stride_y,1]
				test_node.planner_estimate = PlannerEstimate(test_node)
				test_node.planner_estimate.n_channel_splits = args.ic_parallelism # use this to control input channel unrolling for Conv layers
				test_node.planner_estimate.n_output_channel_groups = args.oc_parallelism 
				test_node.type = original_module_type
				assert(args.oc_parallelism >= 2 and args.oc_parallelism % 2 == 0) #Should be an even number 2 or greater
				# Now instantiate a node in your test circuit (assumes you have your own Test class that subclasses module)
				self.test_node = inst(lc.ConvWrapper, test_node, output_csv_path=args.output_csv_path, name = "design_under_test")
			
			elif(Tested_Module == "Sigmoid"):
				input_customized_precision_parameters = sigmoid_precision_parameters_input
				output_customized_precision_parameters = sigmoid_precision_parameters_output
				fake_tf_conv_node = fake_tf_graph_node("Sigmoid", "a_test_of_a_sigmoid_op")
				test_node = Node(fake_tf_conv_node, self.uut_outputs, output_customized_precision_parameters)
				input_node = Node(fake_tf_graph_node("Sigmoid", "fake_input_node"), self.uut_inputs, input_customized_precision_parameters)
				input_edge = Edge(input_node, test_node)
				test_node.inputs = [input_edge]
				input_node.outputs = [input_edge]
				test_node.planner_estimate = PlannerEstimate(test_node)
				self.test_node = inst(lc.ActivationFunction, test_node,reduction_factor_override = cycles_per_value, name = "design_under_test")
				is_activation_function = True

			elif(Tested_Module == "Tanh"):
				input_customized_precision_parameters = tanh_precision_parameters_input
				output_customized_precision_parameters = tanh_precision_parameters_output
				fake_tf_conv_node = fake_tf_graph_node("Tanh", "a_test_of_a_tanh_op")
				test_node = Node(fake_tf_conv_node, self.uut_outputs, output_customized_precision_parameters)
				input_node = Node(fake_tf_graph_node("Tanh", "fake_input_node"), self.uut_inputs, input_customized_precision_parameters)
				input_edge = Edge(input_node, test_node)
				test_node.inputs = [input_edge]
				input_node.outputs = [input_edge]
				test_node.planner_estimate = PlannerEstimate(test_node)
				self.test_node = inst(lc.ActivationFunction, test_node, reduction_factor_override = cycles_per_value, name = "design_under_test")
				is_activation_function = True

			elif(Tested_Module == "ReLU"):
				fake_tf_relu_node = fake_tf_graph_node("Relu", "a_test_of_a_Relu_op")
				test_node = Node(fake_tf_relu_node, self.uut_outputs, self.output_precision_parameters)
				input_node = Node(fake_tf_graph_node("Relu", "fake_input_node"),  self.uut_inputs, self.input_precision_parameters)
				input_edge = Edge(input_node, test_node)
				test_node.inputs = [input_edge]
				input_node.outputs = [input_edge]
				test_node.planner_estimate = PlannerEstimate(test_node)
				self.test_node = inst(lc.ReLU,test_node,name = "design_under_test")
				is_activation_function = False # Note: ReLU does not currently make use of the same activation function module as Sigmoid, Tanh, and Swish


			elif(Tested_Module == "Swish"):
				input_customized_precision_parameters = swish_precision_parameters_input
				output_customized_precision_parameters = swish_precision_parameters_output
				fake_tf_relu_node = fake_tf_graph_node("Swish", "a_test_of_a_swish_op")
				test_node = Node(fake_tf_relu_node, self.uut_outputs, output_customized_precision_parameters)
				input_node = Node(fake_tf_graph_node("Swish", "fake_input_node"),  self.uut_inputs, input_customized_precision_parameters)
				input_edge = Edge(input_node, test_node)
				test_node.inputs = [input_edge]
				input_node.outputs = [input_edge]
				test_node.planner_estimate = PlannerEstimate(test_node)
				self.test_node = inst(lc.ActivationFunction,test_node, reduction_factor_override = cycles_per_value, name = "design_under_test")
				is_activation_function = True


			elif(Tested_Module == "MaxPool"):
				fake_tf_graph_node = namedtuple("fake_tf_graph_node", "type name get_attr")
				attrs = {
					"ksize" : [1, kernel_h, kernel_w, 1],
					"padding" : padding_type,
					"strides" : [1, stride_y, stride_x, 1]
				}
				def get_attr(attr):
					nonlocal attrs
					if attr in attrs:
						return attrs[attr]
					raise ValueError()
				fake_tf_maxpool_node = fake_tf_graph_node("MaxPool", "a_test_of_a_maxpool_op", get_attr)
				test_node = Node(fake_tf_maxpool_node, self.uut_outputs, output_customized_precision_parameters)
				print(test_node.get_kernel_shape())
				input_node = Node(fake_tf_graph_node("ReLU", "fake_input_node", get_attr),  self.uut_inputs, input_customized_precision_parameters)
				input_edge = Edge(input_node, test_node)
				test_node.inputs = [input_edge]
				input_node.outputs = [input_edge]
				test_node.planner_estimate = PlannerEstimate(test_node)
				self.test_node = inst(lc.MaxPool,test_node,name = "design_under_test")
			
			#Declare ROMs to carry golden input
			input_activation_precision = 1 + input_customized_precision_parameters["a_int"] + input_customized_precision_parameters["a_f"]
			input_rom_contents, input_quant_err, input_quant = self.pack_io_to_rom_content_list(self.uut_inputs, input_customized_precision_parameters["a_int"], input_customized_precision_parameters["a_f"], quantize=(Tested_Module!="Conv2D" and Tested_Module!="ConvWrapper" and Tested_Module != "DepthwiseConv2dNative"))
			input_rom = inst(dl.ROM, bits=input_activation_precision*input_w, content_list=input_rom_contents)
			self.input_rom = input_rom

			
			#Declare ROMs to carry golden output
			output_activation_precision = 1 + output_customized_precision_parameters["a_int"] + output_customized_precision_parameters["a_f"]
			output_rom_contents, output_quant_err, output_quant = self.pack_io_to_rom_content_list(self.uut_outputs, output_customized_precision_parameters["a_int"], output_customized_precision_parameters["a_f"], quantize=(Tested_Module!="Conv2D" and Tested_Module!="ConvWrapper" and Tested_Module != "DepthwiseConv2dNative"))
			output_rom = inst(dl.ROM, bits=output_activation_precision*output_w, content_list=output_rom_contents)
			self.output_rom = output_rom
			
			print("input quant error: ", input_quant_err, ", output quant error: ", output_quant_err)
			
			#Instantiate a counter to lower the throughput if needed
			throughput_counter = None
			if is_activation_function and cycles_per_value > 2:
				throughput_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(cycles_per_value-2), name="throughput_counter") #We subtract 2 cycles because the Idle state will take an extra cycle
					
			writing_input = inst(dl.Logic, bits=1, name="writing_input")
			# Instantiate a counter to feed in the input sequentially to the unit under test
			input_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=len(input_rom_contents)-1)
			self.input_counter = input_counter

			burst_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=input_c-1)
			self.burst_counter = burst_counter
			#Instantiate FSM to drive the input
			images_to_run_through = args.image_count
			inputs_to_run_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=images_to_run_through-1, name="inputs_to_run_counter")
			inputs_written_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=images_to_run_through, name="inputs_written_counter")


			
			start_writing = inst(dl.Logic, bits=1, name="start_writing")
			
			# Marius: need to add some extra states at the begining for depthwise convs as the oc_order FIFOs need some time to fill up
			# Otherwise the input values don't know where to go since each conv module only needs a subset of the channels
			if original_module_type == "DepthwiseConv2dNative":
				states = ["IDLE", "WAITING_1", "WAITING_2",  "WAITING_3",  "WRITING_INPUT"]
				edges = [
					["IDLE", "WAITING_1"],
					["WAITING_1", "WAITING_2"],
					["WAITING_2", "WAITING_3"],
					["WAITING_3", "WRITING_INPUT", start_writing],
					["WRITING_INPUT", "WAITING_3", self.burst_counter.is_done]]
			else:
				states = ["IDLE", "WRITING_INPUT"]
				edges = [
					["IDLE", "WRITING_INPUT", start_writing],
					["WRITING_INPUT", "IDLE", self.burst_counter.is_done]]			
			
			#Add extra waiting state if we want to lower throughput for activation functions
			if is_activation_function and cycles_per_value > 1:
				states.append("WAITING")
				edges = [
					["IDLE", "WRITING_INPUT", start_writing],
					["WRITING_INPUT", "WAITING", self.burst_counter.is_done],
					["WRITING_INPUT", "WAITING", inst(dl.NOT, self.burst_counter.is_done)]]
				
				#If waiting delay is small then we don't need to use the counter since FSM state change will cover it
				if cycles_per_value == 2:
					edges.append(["WAITING", "WRITING_INPUT", inst(dl.Constant, value=1, bits=1, name="one")])
				else:
					edges.append(["WAITING", "WRITING_INPUT", throughput_counter.is_done])
			
			state_machine = inst(dl.StateMachine, states, edges, None)
			writing_input.set_driver(state_machine.c["is_writing_input"])
			start_writing.set_driver(inst(dl.AND, self.test_node.space_to_write_line, inst(dl.NOT, inputs_written_counter.is_done)))
			# Connect the ROM address value with the counter value
			inputs_written_counter.set_increment_condition(self.input_counter.done_and_incrementing)
			self.input_rom.r_addr.set_driver(self.input_counter.current_value)
			self.input_rom.r_en.set_driver(state_machine.c["is_writing_input"]) #read the new line
			self.burst_counter.set_increment_condition(state_machine.c["is_writing_input"])
			self.input_counter.set_increment_condition(state_machine.c["is_writing_input"])
			if is_activation_function and cycles_per_value > 2: throughput_counter.set_increment_condition(state_machine.c["is_waiting"])
			# Calculate the delays the input has to wait before being fed to the unit (Right now it is not used )
			ROM_delay = 1 #delay due to physical delay of the ROM
			output_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=output_h*output_c-1, name="output_counter")
			control_to_input_delay = self.test_node.get_control_to_input_delay()
			if(control_to_input_delay <0):
				self.test_node._set_control_to_input_delay(0)
				control_to_input_delay = self.test_node.get_control_to_input_delay()
				uut_delay = ROM_delay
			else:
				uut_delay = control_to_input_delay - ROM_delay 
			 
			print("control_to_input_delay",control_to_input_delay)
			# Connect the ROM data to the input of the unit under test
			can_write_toggle = inst(dl.Counter, reset_value=0, increment_value=1, end_value=1, name="can_write_toggle")
			can_write_toggle_cycles = inst(dl.Counter, reset_value=0, increment_value=1, end_value=1000)
			can_write_toggle_cycles.set_increment_condition(inst(dl.Constant, value=1))
			can_write_toggle.set_increment_condition(can_write_toggle_cycles.is_done)
			condition = state_machine.c["is_writing_input"]#inst(dl.AND, self.test_node.space_to_write_line,inst(dl.NOT, reset),inst(dl.NOT, self.input_counter.is_done))
			self.condition = condition
			self.condition.copy()
			self.test_node.write.set_driver(self.condition)
			can_write_signal = can_write_toggle.current_value
			can_write_signal = inst(dl.NOT, reset)
			self.test_node.can_write_line.set_driver(can_write_signal) # This is the "valid" signal coming from the following module
			for i in range(input_w):
				self.test_node.inputs[i].set_driver(input_rom.r_data[(((input_w-1-i)+1)*input_activation_precision)-1:(input_w-1-i)*input_activation_precision].delay(uut_delay))
				self.test_node.writes[i].set_driver(self.condition.delay(uut_delay+ROM_delay))

			# compare outputs to golden outputs
			self.output_rom.r_en.set_driver(self.test_node.output_valid, delay=self.test_node.control_valid_to_output_valid_delay)
			output_rom_addr_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=len(output_rom_contents)-1, name="output_rom_addr_counter")
			self.output_rom.r_addr.set_driver(output_rom_addr_counter.current_value)
			output_rom_addr_counter.set_increment_condition(self.output_rom.r_en)
			output_rom_read_valid = self.output_rom.r_en.delay(1)
			self.output_number = output_rom_addr_counter.current_value.delay(1).copy()
			with dl.ParamScope(signed=True):
				self.output_rom_individual_outputs = [self.output_rom.r_data[(i+1)*output_activation_precision-1:i*output_activation_precision] for i in reversed(range(output_w))]
			self.delayed_outputs = [o.delay(1).copy() for o in self.test_node.outputs]
			for o in self.delayed_outputs:
				o._signed = True
			for o in self.output_rom_individual_outputs:
				o._signed = True
			self.compared_outputs = [inst(dl.MuxV2, output_rom_read_valid, 0, o - go, signed=True) for o,go in zip(self.delayed_outputs, self.output_rom_individual_outputs)]
			#self.should_end = self.input_counter.is_done.copy()
			self.output_counter = output_counter
			output_counter.set_increment_condition(self.output_rom.r_en.delay(1))
			inputs_to_run_counter.set_increment_condition(output_counter.done_and_incrementing)
			self.should_end = inst(dl.AND, output_counter.done_and_incrementing, inputs_to_run_counter.is_done)


	def pack_io_to_rom_content_list(self, t, int_p, frac_p, quantize=True):
		total_precision = 1 + int_p + frac_p # 1 for the sign
		height = t.shape[2]
		width = t.shape[3]
		channels = t.shape[4]
		if quantize:
			quant = get_quantized(t,1,int_p,frac_p)
			err = np.sum(np.abs(t - (quant.astype(np.float32) / (2**frac_p)))) / np.sum(np.abs(t)) * 100
		else:
			quant = t.astype(int)
			err = 0.
		print(quant.shape)
		print(height, channels, width)
		to_merge = np.swapaxes(quant,3,4).reshape((height*channels,width))
		input_columns = [BitArray(bits=total_precision, value_vector=to_merge[:,l]) for l in range(to_merge.shape[-1])]
		tmp = input_columns[-1]
		for _t in reversed(input_columns[:-1]):
			tmp += _t
		return (tmp,err,quant)


	def get_custom_verilog_str(self, t="  "):
		string = ""
		string += f"{t}always @(posedge {self.clock.name}) begin\n"
		string += f"{t*2}if ({self.should_end.name}) $finish;\n"
		if self.margin != -1:
			comparison_value = 2**self.margin#2 ** max(self.output_precision_parameters["a_f"] - 3, 0)
			for i,(co, go, do) in enumerate(zip(self.compared_outputs, self.output_rom_individual_outputs, self.delayed_outputs)):
				string += f"{t*2}if(({co.name} > {comparison_value}) || ({co.name} < -{comparison_value})) begin\n"
				string += f'{t*3}$error("%0t Output row %d column {i} expected to be %d.  Got %d.  Difference is %d.", $time, {self.output_number.name}, {go.name}, {do.name}, {co.name});\n'
				string += f'{t*3}$fatal;\n'
				string += f"{t*2}end\n"
		string += f"{t}end\n"
		return string



# We need a top level module that we won't actually generate for simulation
# with verilator.  This will allow us to ensure inputs and outputs are correctly
# set for the DemoCircuit module
class SimulatorTopLevelModule(dl.Module):
  def __init__(self, args, **kwargs):
    super(SimulatorTopLevelModule, self).__init__("demo_circuit", kind="demo_circuit", **kwargs)
    inst = self.inst

    Tested_Module = args.type
    input_w = args.width
    input_h = args.height
    input_c = args.ci
    kernel_w = args.kw
    kernel_h = args.kh
    stride_x = args.sx
    stride_y = args.sy
    padding_type = args.padding
    output_channel = args.co
    int_activation_input = args.ibi
    fraction_activation_input = args.ibf
    fraction_activation_output = args.obf
    int_activation_output = args.obi
    int_weight = args.wbi
    fraction_weight = args.wbf

    # we will set this property to prevent digitallogic from actually generating this module's verilog
    # this is what we do for builds targeted at use in quartus or verilator.
    self.should_create_verilog_definition = args.vcs
    # For builds targeting VCS we would set this to true, and design a module that has no inputs or outputs
    # and generates resets, clocks, and test vectors.
    for key,value in kwargs.items():
    	if key == "uut_outputs":
    		self.uut_outputs = value
    	if key == "uut_inputs":
    		self.uut_inputs = value
    	if key == "weights":
    		self.weights = value
    with dl.ModuleScope(self):
    	#np.random.rand(1,1,32,32,16)#
    	clock = inst(dl.Clock, name="clock_0")
    	reset = inst(dl.ForcedSignal, bits=1, name="reset",signed=False)
    	reset.add_value(1, 0)
    	reset.add_value(0, 100)
    	input_precision_parameters = {"a_sign_bits" : 1,"a_int" : int_activation_input,"a_f" : fraction_activation_input,"a_exponent" : 0,"sign_bits" : 1,"int" : int_weight,"f" : fraction_weight,"exponent" : 0}
    	output_precision_parameters = {"a_sign_bits" : 1,"a_int" : int_activation_output,"a_f" : fraction_activation_output,"a_exponent" : 0,"sign_bits" : 1,"int" : int_weight,"f" : fraction_weight,"exponent" : 0}
    	with dl.ParamScope(clock=clock, reset=reset,extend_to_match_bits=True):
    		demo_circuit = inst(DemoCircuit,uut_inputs=self.uut_inputs,uut_outputs = self.uut_outputs
    			,input_precision_parameters=input_precision_parameters,output_precision_parameters = output_precision_parameters
    			,weights = self.weights, name="my_demo_circuit",implement_with_megafunction=False, args=args)

    	


def create_golden_data(args):
	Path(DUMPED_FILES_PATH).mkdir(parents=True, exist_ok=True)
	Tested_Module = args.type
	input_w = args.width
	input_h = args.height
	input_c = args.ci
	kernel_w = args.kw
	kernel_h = args.kh
	stride_x = args.sx
	stride_y = args.sy
	padding_type = args.padding
	output_channel = args.co
	int_activation_input = args.ibi
	fraction_activation_input = args.ibf
	fraction_activation_output = args.obf
	int_activation_output = args.obi
	int_weight = args.wbi
	fraction_weight = args.wbf
	np.random.seed(5)
	tf.keras.backend.set_floatx("float64")
	if (Tested_Module == "Conv2D" or Tested_Module == "DepthwiseConv2dNative"):
		#Create input for tensorflow
		from tensorflow.keras import initializers
		K.clear_session()
		initializer = tf.keras.initializers.RandomUniform(minval=-0.1, maxval=0.1)#,seed = 5)
		model = Sequential()
		if Tested_Module == "DepthwiseConv2dNative":
			conv_layer = DepthwiseConv2D( kernel_size=(kernel_w, kernel_h), input_shape = (input_w,input_h,input_c),kernel_initializer=initializer,padding=padding_type,strides=(stride_x,stride_y), use_bias=False, activation=None)
		else:
			conv_layer = Conv2D(output_channel, kernel_size=(kernel_w, kernel_h), input_shape = (input_w,input_h,input_c),kernel_initializer=initializer,padding=padding_type,strides=(stride_x,stride_y), use_bias=False, activation=None)
		model.add(conv_layer)
		w = np.array(conv_layer.get_weights()[0])
		w_quantized = get_quantized(w,1,int_weight,fraction_weight).astype(np.float32)

		if isinstance(args.weights, np.ndarray):
			w = args.weights
			print(w)
			w_quantized = w.astype(np.float64)
			print(w_quantized)
			"""if not isinstance(w, np.floating):
				if args.iwbf == -1:
					print("You must pass in --iwbf (interpretation weight bits fractional) to initialize the weights with a non-floating-point numpy array.")
					sys.exit(-1)
				else:
					w = w.astype(np.float32) / 2. ** args.iwbf"""

		model.build(tf.TensorShape([None, input_h,input_w,input_c]))
		conv_layer.set_weights([w_quantized])
		if isinstance(args.input_act, np.ndarray):
			uut_inputs = args.input_act
			input_quantized = uut_inputs.astype(np.float64)
			"""if not isinstance(uut_inputs, np.floating):
				if args.iibf == -1:
					print("You must pass in --iibf (interpretation input bits fractional) to initialize the input activation with a non-floating-point numpy array.")
					sys.exit(-1)
				else:
					uut_inputs = uut_inputs.astype(np.float32) / (2. ** args.iibf)"""
		else:
			uut_inputs = np.random.rand(1,input_h,input_w,input_c).astype(np.float32)
			input_quantized = get_quantized(uut_inputs,1,int_activation_input,fraction_activation_input).astype(np.float64)
		uut_inputs = input_quantized
		#uut_inputs = np.zeros([input_w,input_h,input_c]).astype(np.float32)
		uut_outputs = np.array([model.predict(input_quantized)])
		exponent = (np.abs(uut_outputs).view(np.int64) >> 52) - 1023
		print("Max exponent: ", np.max(exponent))
		print("Min exponent: ", np.min(exponent))
		print(uut_outputs.dtype)
		uut_outputs = (uut_outputs // (2 ** ((fraction_activation_input+fraction_weight) - fraction_activation_output)))
		uut_outputs = np.maximum(uut_outputs, -2**(fraction_activation_output + int_activation_output))
		uut_outputs = np.minimum(uut_outputs, 2**(fraction_activation_output + int_activation_output)-1)
		uut_outputs = uut_outputs.astype(int)
		print(uut_outputs)
		print("Output max magnitude: ", np.max(np.abs(uut_outputs)))
		uut_inputs = np.array([uut_inputs])
		uut_inputs_dump = uut_inputs.reshape((input_w*input_h*input_c))
		uut_outputs_dump = (np.swapaxes(uut_outputs,3,4)).reshape(uut_outputs.shape[2]*uut_outputs.shape[3]*uut_outputs.shape[4])
		g = np.asarray(uut_outputs_dump)*2**fraction_activation_output
		g_in = np.asarray(uut_inputs_dump)*2**fraction_activation_input
		np.savetxt(DUMPED_FILES_PATH +  'golden_output.csv',g,delimiter=",",fmt='%d')
		np.savetxt(DUMPED_FILES_PATH +  'golden_input.csv',g_in,delimiter=",",fmt='%d')#fmt='%d')
		g_w = np.asarray(w).reshape(kernel_h*output_channel*input_c*kernel_w)#*2**fraction_weight #Store them as fixed point for easier debugging from a wave 
		np.savetxt(DUMPED_FILES_PATH + "weights.csv",g_w,delimiter=",",fmt='%0.20f')
		print(uut_outputs.shape)
		np.savetxt('output_shape.txt',uut_outputs.shape,fmt='%d')
		return([uut_inputs,w,uut_outputs])	

	elif (Tested_Module in Activation_Modules):
		#Create the golden data for the activation module under test
		if(Tested_Module == "Sigmoid"):
			uut_inputs = np.random.rand(input_h,input_w,input_c).astype(np.float32)#np.random.uniform(-8,8,(input_h,input_w,input_c))
			input_quantized = get_quantized(uut_inputs,1,sigmoid_precision_parameters_input['a_int'],sigmoid_precision_parameters_input['a_f']).astype(np.float32) / (2 ** sigmoid_precision_parameters_input['a_f'])
			uut_inputs = input_quantized
			uut_inputs = np.array([[uut_inputs]])
			uut_outputs = get_quantized(np.array(tf.keras.activations.sigmoid(uut_inputs).numpy()), 1, sigmoid_precision_parameters_output['a_int'], sigmoid_precision_parameters_output['a_f']) / (2 ** sigmoid_precision_parameters_output['a_f'])
			uut_inputs_dump = uut_inputs.reshape((input_w*input_h*input_c))
			uut_outputs_dump = (np.swapaxes(uut_outputs,3,4)).reshape(uut_outputs.shape[2]*uut_outputs.shape[3]*uut_outputs.shape[4])
			g = np.asarray(uut_outputs_dump)*2**sigmoid_precision_parameters_output['a_f']
			g_in = np.asarray(uut_inputs_dump)*2**sigmoid_precision_parameters_input['a_f']

		elif(Tested_Module == "ReLU"):
			uut_inputs = np.random.rand(input_h,input_w,input_c).astype(np.float32)#np.random.uniform(-8,8,(input_h,input_w,input_c))
			input_quantized = get_quantized(uut_inputs,1,int_activation_input,fraction_activation_input).astype(np.float32) / (2 ** fraction_activation_input)
			uut_inputs = input_quantized
			uut_inputs = np.array([[uut_inputs]])
			uut_outputs = get_quantized(np.array(tf.keras.activations.relu(uut_inputs).numpy()), 1, int_activation_output, fraction_activation_output) / (2 ** fraction_activation_output)
			uut_inputs_dump = uut_inputs.reshape((input_w*input_h*input_c))
			uut_outputs_dump = (np.swapaxes(uut_outputs,3,4)).reshape(uut_outputs.shape[2]*uut_outputs.shape[3]*uut_outputs.shape[4])
			g = np.asarray(uut_outputs_dump)*2**fraction_activation_output
			g_in = np.asarray(uut_inputs_dump)*2**fraction_activation_input

		elif(Tested_Module == "Swish"):
			uut_inputs = np.random.rand(input_h,input_w,input_c).astype(np.float32)#np.random.uniform(-8,8,(input_h,input_w,input_c))
			input_quantized = get_quantized(uut_inputs,1,swish_precision_parameters_input['a_int'],swish_precision_parameters_input['a_f']).astype(np.float32) / (2 ** swish_precision_parameters_input['a_f'])
			uut_inputs = input_quantized
			uut_inputs = np.array([[uut_inputs]])
			uut_outputs = get_quantized(np.array(swish(uut_inputs)), 1, swish_precision_parameters_output['a_int'], swish_precision_parameters_output['a_f']) / (2 ** swish_precision_parameters_output['a_f'])
			uut_inputs_dump = uut_inputs.reshape((input_w*input_h*input_c))
			uut_outputs_dump = (np.swapaxes(uut_outputs,3,4)).reshape(uut_outputs.shape[2]*uut_outputs.shape[3]*uut_outputs.shape[4])
			g = np.asarray(uut_outputs_dump)*2**swish_precision_parameters_output['a_f']
			g_in = np.asarray(uut_inputs_dump)*2**swish_precision_parameters_input['a_f']

		elif(Tested_Module == "Tanh"):
			uut_inputs = np.random.rand(input_h,input_w,input_c).astype(np.float32)#np.random.uniform(-8,8,(input_h,input_w,input_c))
			input_quantized = get_quantized(uut_inputs,1,tanh_precision_parameters_input['a_int'],tanh_precision_parameters_input['a_f']).astype(np.float32) / (2 ** tanh_precision_parameters_input['a_f'])
			uut_inputs = input_quantized
			uut_inputs = np.array([[uut_inputs]])
			uut_outputs = get_quantized(np.array(tf.keras.activations.tanh(uut_inputs).numpy()), 1, tanh_precision_parameters_output['a_int'], tanh_precision_parameters_output['a_f']) / (2 ** tanh_precision_parameters_output['a_f'])
			uut_inputs_dump = uut_inputs.reshape((input_w*input_h*input_c))
			uut_outputs_dump = (np.swapaxes(uut_outputs,3,4)).reshape(uut_outputs.shape[2]*uut_outputs.shape[3]*uut_outputs.shape[4])
			g = np.asarray(uut_outputs_dump)*2**tanh_precision_parameters_output['a_f']
			g_in = np.asarray(uut_inputs_dump)*2**tanh_precision_parameters_input['a_f']

		elif(Tested_Module == "MaxPool"):
			uut_inputs = np.random.rand(input_h,input_w,input_c).astype(np.float32)#np.random.uniform(-8,8,(input_h,input_w,input_c))
			input_quantized = get_quantized(uut_inputs,1,int_activation_input,fraction_activation_input).astype(np.float32) / (2 ** fraction_activation_input)
			uut_inputs = input_quantized
			uut_inputs = np.array([uut_inputs])
			uut_outputs = get_quantized(np.array([tf.keras.layers.MaxPool2D(pool_size=(kernel_h, kernel_w), strides=(stride_y, stride_x), padding=padding_type)(uut_inputs).numpy()]), 1, int_activation_output, fraction_activation_output) / (2 ** fraction_activation_output)
			uut_inputs = np.array([uut_inputs])
			uut_inputs_dump = uut_inputs.reshape((input_w*input_h*input_c))
			uut_outputs_dump = (np.swapaxes(uut_outputs,3,4)).reshape(uut_outputs.shape[2]*uut_outputs.shape[3]*uut_outputs.shape[4])
			g = np.asarray(uut_outputs_dump)*2**fraction_activation_output
			g_in = np.asarray(uut_inputs_dump)*2**fraction_activation_input

		
		np.savetxt(DUMPED_FILES_PATH +  'golden_output.csv',g,delimiter=",",fmt='%d')
		np.savetxt(DUMPED_FILES_PATH +  'golden_input.csv',g_in,delimiter=",",fmt='% 0.20f')#fmt='%d')
		np.savetxt('output_shape.txt',uut_outputs.shape,fmt='%d')
		return([uut_inputs,uut_outputs])

def parse_args(args):
	parser = argparse.ArgumentParser(description="Tests HPIPE Modules")
	parser.add_argument("--vcs", help="Add this flag to build a circuit intended for simulation in VCS.  If you don't pass this argument it will build a circuit for use in verilator.", const=True, default=False, action="store_const")
	parser.add_argument("--dir", help="Output circuit directory", type=str, default="demo_circuit_dir")
	parser.add_argument("--margin", help="Set the error margin.  The tests will fail if the absolute difference of a single value and the tensorflow computed value is greater than 2**<this value>.  Setting this to -1 (the default) disables the feature.", type=int, default=-1)

	parser.add_argument("--type", help="The type of operation to test.  One of ['Conv2D', 'Sigmoid', 'ReLU', 'Swish', 'Tanh'].", type=str, default="Conv2D")

	parser.add_argument("--input_act", "-i", help="Input a path to a 3D or 4D numpy file (BHWC) that will be used as the input activation.", type=str, default="")
	parser.add_argument("--height", help="The height of the input activation.", type=int, default=7)
	parser.add_argument("--width", "-w", help="The width of the input activation", type=int, default=-1)

	parser.add_argument("--ci", type=int, help="The number of input channels", default=10)

	parser.add_argument("--kh", type=int, default=-1)
	parser.add_argument("--kw", type=int, default=-1)
	parser.add_argument("--kernel", "-k", type=int, default=3)
	parser.add_argument("--co", type=int, default=10)
	parser.add_argument("--weights", type=str, default="")

	parser.add_argument("--bi", help="Integer bits to use for weights, inputs, and outputs", type=int, default=4)
	parser.add_argument("--bf", help="Fractional bits to use for weights, inputs, and outputs", type=int, default=12)
	parser.add_argument("--wbi", help="Integer bits to use for weights", type=int, default=-1)
	parser.add_argument("--wbf", help="Fractional bits to use for weights", type=int, default=-1)
	parser.add_argument("--ibi", help="Integer bits to use for inputs", type=int, default=-1)
	parser.add_argument("--ibf", help="Fractional bits to use for inputs", type=int, default=-1)
	parser.add_argument("--obi", help="Integer bits to use for outputs", type=int, default=-1)
	parser.add_argument("--obf", help="Fractional bits to use for outputs", type=int, default=-1)

	parser.add_argument("--iibi", help="Integer bits to interpret the input tensor if one was specified with --input_act.", type=int, default=-1)
	parser.add_argument("--iibf", help="Fractional bits to interpret the input tensor if one was specified with --input_act.", type=int, default=-1)
	parser.add_argument("--iwbi", help="Integer bits to interpret the weights if they were specified with --weights.", type=int, default=-1)
	parser.add_argument("--iwbf", help="Fractional bits to interpret the weights if they were specified with --weights.", type=int, default=-1)

	parser.add_argument("--stride", "-s", help="The stride of the filter", type=int, default=1)
	parser.add_argument("--sx", help="The x stride of the filter", type=int, default=-1)
	parser.add_argument("--sy", help="The y stride of the filter", type=int, default=-1)

	parser.add_argument("--padding", help="The padding type (either of ['SAME', 'VALID']).", type=str, default="SAME")

	parser.add_argument("--ic_parallelism", help="The input channel parallelism factor.", type=int, default=2)
	parser.add_argument("--oc_parallelism", help="The output channel parallelism factor. Should only be greater than 1 for ConvWrapper modules.", type=int, default=1)
	parser.add_argument("--output_csv_path", help="Specify a custom path for nodes to write their outputs to.", default=None)

	parser.add_argument("--image_count", help="Specify the number of images to run through the test module.  The default is 1.", default=1, type=int)
	parser.add_argument("--instantiate_quartus_ip", help="Causes instances to use Quartus IP WYSIWYGs wherever possible.", action="store_true", default=False)

	args = parser.parse_args(args[1:])

	# Infer width from height if it is not set
	if args.width == -1:
		args.width = args.height

	# Override height, width, and input channels if input_act is set
	if args.input_act != "":
		args.input_act = np.load(args.input_act)
		if len(args.input_act.shape) == 3:
			args.input_act = np.expand_dims(args.input_act, 0)
		args.height = args.input_act.shape[1]
		args.width = args.input_act.shape[2]
		args.ci = args.input_act.shape[3]

	# infer kh and kw if they are not set
	if args.kh == -1:
		args.kh = args.kernel
	if args.kw == -1:
		args.kw = args.kernel
	if args.weights != "":
		args.weights = np.load(args.weights)
		for i,p in enumerate("kh,kw,ci,co".split(",")):
			setattr(args, p, args.weights.shape[i])

	for p in "bi,bf".split(","):
		for t in "w,i,o".split(","):
			if getattr(args, t + p) == -1:
				setattr(args, t + p, getattr(args, p))

	if args.sx == -1:
		args.sx = args.stride
	if args.sy == -1:
		args.sy = args.stride

	return args



def main(args):
	args = parse_args(args)
	Tested_Module = args.type
	if(Tested_Module == "Conv2D" or Tested_Module == "DepthwiseConv2dNative"):
		[golden_input,w,golden_output] = create_golden_data(args)
	elif(Tested_Module in Activation_Modules):
		[golden_input,golden_output] = create_golden_data(args)
		w = []
	
	# we instantiate the top level module
	with dl.ParamScope(implement_with_megafunction=args.instantiate_quartus_ip):
		stlm = SimulatorTopLevelModule(uut_outputs = golden_output,uut_inputs=golden_input,weights=w, args=args)
	# and we will now generate the output circuit
	dl.Verilog.current_circuit.write_verilog_to_dir(args.dir)
  


if __name__ == "__main__":
  main(sys.argv)



