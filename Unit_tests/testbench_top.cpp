// Most of this comes from the Verilator documentation, so please reference that
// for explanations of what is going on here.
#include "Vmy_demo_circuit.h"
#include "verilated.h"
#ifdef DUMP
#include "verilated_fst_c.h"
#endif
#include <iostream>
#include <fstream>

double main_time = 0.0;       // Current simulation time
// This is a 64-bit integer to reduce wrap over issues and
// allow modulus.  You can also use a double, if you wish.
bool error = false;

#ifdef DUMP
        VerilatedFstC* tfp;
#endif

double sc_time_stamp () {       // Called by $time in Verilog
    return main_time;           // converts to double, to match
                                // what SystemC does
}

void eval_dump(Vmy_demo_circuit* top) {
#ifdef DUMP
        tfp->dump(main_time);
#endif
        top->eval();
#ifdef DUMP
        tfp->dump(main_time);
#endif
}


void tick(Vmy_demo_circuit* top) {
        top->clock_0 = !top->clock_0;
        eval_dump(top);
        main_time += 0.5;
        top->clock_0 = !top->clock_0;
        eval_dump(top);
        main_time += 0.5;
}

int main(int argc, char** argv, char** env) {


        Verilated::commandArgs(argc, argv);
        Verilated::traceEverOn(true);

#ifdef DUMP
        tfp = new VerilatedFstC;
#endif
        Vmy_demo_circuit* top = new Vmy_demo_circuit;
        top->clock_0 = 0;
#ifdef DUMP
        top->trace(tfp, 99);
        tfp->open("sim.fst");
#endif
        std::cout << "Beginning simulation " << std::endl;
        tick(top);
        top->demo_circuit_0_reset = 1;
        tick(top);
        top->demo_circuit_0_reset = 1;
        tick(top);
        top->demo_circuit_0_reset = 0;
        
        //top->sigmoid_0_can_write_line = 1;
        tick(top);
        //while(top->testbench_counter->testbench_counter >= 0)
        //for (int i = 0; i < 300000; ++i)
          //      tick(top);
        while(!Verilated::gotFinish()) 
        {
            tick(top);
        }

				bool had_error = error;//Verilated::errorCount() != 0;
#ifdef DUMP
        tfp->close();
#endif
        top->final();
        delete top;
        if (had_error) {
                std::cout << "ERROR: Simulation unsuccessful.\n";
                exit(1);
        }
        std::cout << "PASS: Simulation successful!\n";
        exit(0);
}
