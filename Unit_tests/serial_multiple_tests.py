#Script to run multiple tests on HPIPE (currently only Conv2D)
#Remeber the output precision for fraction is set manually to 14 (to be changed)
import os
import sys
import csv 
import numpy as np
from termcolor import colored

os.system('rm error_reports.csv')
out_f = open('error_reports.csv','a+')
out_f.write('Total L1-Norm (%), Largest Single-Pixel error \n')

quick_test = True
if(quick_test):
	num_tests= 4

#generated_circuit_name = "swish_0.csv"#'sigmoid_0.csv'#'relu_0.csv'
test_tolerance = 2 # Percentage (L1 Norm)
passed = True
failed_circuits = []

#Reed the different configs from a csv file
with open("serial_test_commands.txt",newline='') as fh:
	tests = [l for l in fh]

#Perform the tests
for i in range(num_tests):
	#Clean the folders
	os.system('make clean')
	#Run HPIPE to construct the circuit
	print(colored("Running " + tests[i], "yellow"))
	result = os.system(tests[i])
	if "run_dumped_unit_test_configs" in tests[i]:
		if result != 0:
			passed = False
			failed_circuits.append(i)
		continue
	#Run verilator to simulate the circuit and dump the outputs
	os.system('make verilator_no_wave >> logs.txt' )

	#Read the outputs and the golden values
	with open("output_shape.txt",newline='') as csvfile:
		values=csv.reader(csvfile,delimiter=',')
		shape_o = list(values)
	with open("dumped_files/golden_output.csv",newline='') as csvfile:
		values=csv.reader(csvfile,delimiter=',')
		golden = list(values)	
	with open("dumped_files/design_under_test.csv",newline='') as csvfile:
		values=csv.reader(csvfile,delimiter=',')
		verilog_values = list(values)


	verilog_array = np.array(np.hstack(verilog_values)).astype(np.float32)
	verilog_array = verilog_array.reshape(verilog_array.shape[0],1)
	golden = np.array(golden).astype(np.float32)
	np.savetxt('./dumped_files/golden1d.csv',verilog_array,delimiter=",",fmt='%d')
	#Calculate the L1-Norm and the largest single pixel error
	sub = np.abs(golden - verilog_array[1:golden.shape[0]+1])
	l1 = (np.sum(sub)/np.sum(np.abs(golden))) * 100
	largest = np.max(sub)
	if(l1 > test_tolerance):
		passed = False
		failed_circuits.append(i)

	#Write the Error measurements in a file
	out_f.write(str(l1) + ',' + str(largest)+'\n')



#Exit with a code 1 to force the pipeline to fail in case the tests failed
if(passed == False):
	print("Some Tests Failed :(")
	print(str(len(failed_circuits))+ ' tests failed  \n')
	for i in range(len(failed_circuits)):
		print('Failed at test #' , int(failed_circuits[i])+1)
	sys.exit(1)
else:
	print("All tests Passed :)")
