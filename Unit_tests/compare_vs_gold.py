import csv
import numpy as np
import sys
import os
"""output_fractional_bits = int(sys.argv[1])
output_w = int(sys.argv[2])
output_h = int(sys.argv[3])
output_c = int(sys.argv[4])"""

test_tolerance = 2
#Read the outputs and the golden values
with open("./output_shape.txt",newline='') as csvfile:
	values=csv.reader(csvfile,delimiter=',')
	shape_o = list(values)
with open("./dumped_files/golden_output.csv",newline='') as csvfile:
	values=csv.reader(csvfile,delimiter=',')
	golden = list(values)	
with open("./dumped_files/design_under_test.csv",newline='') as csvfile:
	values=csv.reader(csvfile,delimiter=',')
	verilog_values = list(values)


verilog_array = np.array(np.hstack(verilog_values)).astype(np.float32)
verilog_array = verilog_array.reshape(verilog_array.shape[0],1)
golden = np.array(golden).astype(np.float32)
np.savetxt('./dumped_files/golden1d.csv',verilog_array,delimiter=",",fmt='%d')
#Calculate the L1-Norm and the largest single pixel error
sub = np.abs(golden - verilog_array[1:golden.shape[0]+1])
l1 = (np.sum(sub)/np.sum(np.abs(golden))) * 100
largest = np.max(sub)
if(l1 > test_tolerance):
	passed = False
	#failed_circuits.append(i)





print('total L1 Norm is',l1,"%")
print('Largest single Pixel error is: ',largest)